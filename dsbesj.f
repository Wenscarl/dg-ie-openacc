        subroutine dsbesj(cx,n,csj,nd)
c>>>        implicit complex (c)
c>>>        implicit real (a-b,d-h,o-z)
        implicit real*4 (a-h,o-z)
        dimension csj(0:nd)
c        integer flag
        parameter (iacc=40,bigni=1.d-10,bigno=1.d+10)
c>>>>>        open(unit=12,file='junk.out',status='unknown')

c --- explicit calculation of low order bessel functions.
        cxinv = 1.0e0/cx
        if (n.eq.0) then
            csj(0)=sin(cx)*cxinv
            return
        end if
        if (n.eq.1) then
            csj(0)=sin(cx)*cxinv
            csj(1)=csj(0)*cxinv-cos(cx)*cxinv
            return
        end if
        if (n.eq.2) then
            csj(0)=sin(cx)*cxinv
            csj(1)=csj(0)*cxinv-cos(cx)*cxinv
            csj(2)=(3.0e0/cx/cx-1.0e0)*csj(0) - 3.0e0*cos(cx)/cx/cx
            return
        end if

c --- recurrence relations used to calculate higher orders.
c        flag=0
        csj(0)=sin(cx)*cxinv
        csj(1)=(csj(0)-cos(cx))*cxinv
        csjb=csj(0)
        csjm=csj(1)
        if (abs(cx).gt.real(n)) then
            do 11 j=2,n
                csjp=(2.0e0*real(j-1)+1.0e0)*cxinv*csjm-csjb
                csjb=csjm
                csjm=csjp
                csj(j)=csjm
  11        continue
        else
            csj1=csj(1)
            m=2*((n+int(sqrt(real(iacc*n))))/2)
            csjm=0.0e0
            csjb=1.0e0
c            i=0
            do 12 j=m,1,-1
                csjp=(2.0e0*real(j+1)+1.0e0)*cxinv*csjb-csjm
                csjm=csjb
                csjb=csjp
                if(j.le.n) csj(j)=csjp
c>>>>>                write(12,*) j, sjp
                if (abs(csjp).gt.bigno) then
                    csjm=csjm*1.d-10
                    csjb=csjb*1.d-10
                    if(j .le. n) then
                    do 20 lp = j,n
                      csj(lp)=csj(lp)*bigni
  20                continue
                    endif
                end if
  12        continue
            if (abs(csj(1)).eq.0.0e0) print *,'got 0 in sj'
            cdp=csj1/csj(1)
            do 30 jp = 1,n
              csj(jp) = csj(jp)*cdp
  30        continue
        end if
        return
        end
