#ifndef _GLOBAL_H
#define _GLOBAL_H

#include<fstream>
#include <iostream>
#include <complex>
#include <math.h>
// #include "mesh3d.h"
#include "float_complex.h"

#ifndef MAIN
extern Real memory_tot;


extern Integer * igrsp;
extern Integer * lxyzp;
extern Integer * modesp;
extern Real * sizelp;
extern Integer * lrsmupp;
extern Integer * lrsmdownp;
extern Integer * lrstlp;
extern Integer * kpstartp;


extern Integer mvmodelong;
extern Integer directRHS;
extern Real freq;
extern Real wl0; //wavelength
extern Integer maxpatch;
extern Integer maxnode;
extern Integer maxedge;
extern Real *xyznode;
extern Integer* iedgep;
extern Integer* ipatpntp;

extern Real *edgep;
extern Integer* familyp;
extern Real* xyzctrp;
extern Real* paerap;
extern Real* xyznormp;
extern Real* xyznodep;
extern Integer* ngridp;

extern Real* vt1p;
extern Real* vt2p;
extern Real* vt3p;
extern Real* wtp;

// extern Complex* currentp;
extern Complex* crhsp;
// extern Integer* igcsp;
#endif

#endif
