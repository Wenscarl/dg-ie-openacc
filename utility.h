#ifndef UTILITY_H
#define UTILITY_H

#include "float_vtr.h"

typedef long long int Int;

void showUsage(void);
void userInput(float &freq, float &thetaStart, float &thetaEnd, float &thetaStep, 
               float &phiStart, float &phiEnd, float &phiStep, float &Einc_mag, 
               float &Einc_phs, float &polAng, float &eps, bool &writeCurJ);

void writeInParam(const float freq, const float thetaStart, const float thetaEnd, 
                  const float thetaStep, const float phiStart, const float phiEnd, 
                  const float phiStep, const float Einc_mag, const float Einc_phs, 
                  const float polAng, const float eps, const bool writeCurJ, const char *prjName);

bool readInParam (float &freq, float &eps, const string fileName);

Integer findI(Integer i,Integer* ii);
float Len(float* a,float* b);
double Len(double* a,double* b);
void crossProd(Complex*, const vtr, Complex*);
void crossProd(const vtr,Complex*,  Complex*);
Integer postProcessing(char* prjName, Integer maxedge, Integer maxnode,Integer * iedgep,
		       Integer* ipatpntp,Real* xyznodep,Real* paerap);
		       
Int ImportTriangles(const char* prjName1, Int& maxnode1,Real** xyznodep1, Int& maxpatch1, Int** ipatpntp1, Int& maxedge1, Int** iedgep1,Integer**);
Int ImportTrianglesDIRECT(const char* prjName1, Int& maxnode1,Real** xyznodep1, Int& maxpatch1, Int** ipatpntp1, Int& maxedge1, Int** iedgep1,Integer**);
Int readInCurJ(const char *filename, Complex* buffer, Int Count);
Int readInCurJBin(const char *filename, Complex* buffer, Int Count);	
Real getArea(Real*,Integer*);
void runCommand(const char* buf);
bool testFileExisting(const char* buf, bool binary);	       
#endif
