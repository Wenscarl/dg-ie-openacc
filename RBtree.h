// DD_TREE.H: Definition of template class Tree
#ifndef DD_TREE_H
#define DD_TREE_H
#include <iostream>
#include <cassert>
//#include "dd_edge.h"
//#include "dd_face.h"
#include "RBtreenode.h"
#include <assert.h>
#include <iostream>
#include <stdlib.h>

typedef long long int Int;

template<class NODETYPE>
class Tree {
	private:
		Int id;
	public:
		Tree();
		Int insertNode(const NODETYPE &);
		void preOrderTraversal() const;
		TreeNode<NODETYPE> *findNode(const NODETYPE &);

  // set functions
		void setcounter(Int );
		void numberData(); // added to number the MortarInterfaces
		void number(); // added to number ints and doubles
		void numberData(Int );
		void ConvertToArray(NODETYPE **, Int);
		void ConvertToArray(NODETYPE **);
		void convert2Array(NODETYPE **ndArray, Int n); // added to number ints and doubles
		void freeMemory();
		void freeMemoryHelper(TreeNode<NODETYPE> *);
  // get counter
		Int getcounter();
		Int nMaxTreeLevel;
	private:
		TreeNode<NODETYPE> *rootPtr;
		TreeNode<NODETYPE> *tmpPtr;
		Int counter;

  // utility functions
// 		void insertNodeHelper(TreeNode<NODETYPE> **, const NODETYPE &,Int currentLevel);
		TreeNode<NODETYPE>* insertNodeHelper(TreeNode<NODETYPE> **, const NODETYPE &,Int currentLevel,TreeNode<NODETYPE>* parent);
		void findNodeHelper(TreeNode<NODETYPE> *, const NODETYPE &);
		void preOrderHelper(TreeNode<NODETYPE> *) const;
		void numberDataHelper(TreeNode<NODETYPE> *, Int );
		void numberDataHelper(TreeNode<NODETYPE> *); // added to number MortarInterfaces
		void numberHelper(TreeNode<NODETYPE> *); // added to number MortarInterfaces
		void convert2arrayHelper(TreeNode<NODETYPE> *ptr, NODETYPE **ndARRAY, Int &nn); // added to number MortarInterfaces
		void convertToarrayHelper(TreeNode<NODETYPE> *, NODETYPE **);
		void QuickconvertToarrayHelper(TreeNode<NODETYPE> *, NODETYPE **);
		
		void RB_case1(TreeNode<NODETYPE> *);
		void RB_case2(TreeNode<NODETYPE> *);
		void RB_case3(TreeNode<NODETYPE> *);
		void RB_case4(TreeNode<NODETYPE> *);
		void RB_case5(TreeNode<NODETYPE> *);
		void LeftRotate(TreeNode<NODETYPE> *);
		void RightRotate(TreeNode<NODETYPE> *);
};

template<class NODETYPE>
		Tree<NODETYPE>::Tree() { rootPtr = 0; counter = 0; nMaxTreeLevel=0;}

template<class NODETYPE>
void 		Tree<NODETYPE>::freeMemory() { 
		freeMemoryHelper(rootPtr);
		}
template<class NODETYPE>
void 		Tree<NODETYPE>::freeMemoryHelper(TreeNode<NODETYPE>* PTR) {
	if(PTR==NULL)
		return; 
	if(PTR->leftPtr!=NULL)
		freeMemoryHelper(PTR->leftPtr);
	if(PTR->rightPtr!=NULL)
		freeMemoryHelper(PTR->rightPtr);
	delete PTR;
	return;
}		
template<class NODETYPE>
		Int Tree<NODETYPE>::insertNode(const NODETYPE &value)
{ 
	Int oldCounter = counter;

	TreeNode<NODETYPE>* N=insertNodeHelper(&rootPtr, value,0,NULL); 
	if (counter == oldCounter) return 0;
	
	RB_case1(N);
	return 1;
}
template<class NODETYPE> void Tree<NODETYPE>::RB_case1(TreeNode<NODETYPE>* N)
{
	if(N->parent==NULL)
		N->color=RB_BLACK;
	else
		RB_case2(N);
	return;
}
template<class NODETYPE> void Tree<NODETYPE>::RB_case2(TreeNode<NODETYPE>* N){
	if(N->parent->color==RB_BLACK){
		return;
	}
	else
		RB_case3(N);		
}
template<class NODETYPE> void Tree<NODETYPE>::RB_case3(TreeNode<NODETYPE>* N){
	TreeNode<NODETYPE>* G=N->parent->parent;
	TreeNode<NODETYPE>* U=N->getUncle();
	if(U!=NULL && U->color==RB_RED){
		N->parent->color=RB_BLACK;
		U->color=RB_BLACK;
		G->color=RB_RED;
		RB_case1(G);
	}
	else
		RB_case4(N);
}
template<class NODETYPE> void Tree<NODETYPE>::RB_case4(TreeNode<NODETYPE>* N){
	TreeNode<NODETYPE>* G=N->parent->parent;
// 	TreeNode<NODETYPE>* U=N->getUncle();
	
	if(N==N->parent->rightPtr &&N->parent==G->leftPtr){
		LeftRotate(N->parent);
		N=N->leftPtr;
	}
	else if (N==N->parent->leftPtr &&N->parent==G->rightPtr){
		RightRotate(N->parent);
		N=N->rightPtr;
	}
	
	RB_case5(N);
}
template<class NODETYPE> void Tree<NODETYPE>::RB_case5(TreeNode<NODETYPE>* N){
	TreeNode<NODETYPE>* G=N->parent->parent;
// 	TreeNode<NODETYPE>* U=N->getUncle();
	N->parent->color=RB_BLACK;
	G->color=RB_RED;
	if(N==N->parent->leftPtr && N->parent==G->leftPtr)
		RightRotate(G);
	else {
#ifdef __DEBUG_ 
		assert(N==N->parent->rightPtr && N->parent==G->rightPtr);
#endif
		LeftRotate(G);
	}
}
template<class NODETYPE> void Tree<NODETYPE>::RightRotate(TreeNode<NODETYPE>* N){
	
	
	TreeNode<NODETYPE>* L=N->leftPtr;
	TreeNode<NODETYPE>* P=N->parent;
	assert(L!=NULL);
// 	TreeNode<NODETYPE>* Leaf1=L->leftPtr;
	TreeNode<NODETYPE>* Leaf2=L->rightPtr;
// 	TreeNode<NODETYPE>* Leaf3=N->rightPtr;
	
	//Dealing with descendent
	N->leftPtr=Leaf2;
	L->rightPtr=N;
	if(P==NULL){
		assert(rootPtr==N);
		rootPtr=L; //This is for root Ptr case;
	}
	else {
		if(P->leftPtr==N)
			P->leftPtr=L;
		else if(P->rightPtr==N)
			P->rightPtr=L;
		else {
			std::cerr<<"Error! find a pseudo parent!!\n";
			exit(-1);
		}
	}
	//Dealing with parents;
	if(Leaf2!=NULL)
		Leaf2->parent=N;
	N->parent=L;
	L->parent=P;
}
template<class NODETYPE> void Tree<NODETYPE>::LeftRotate(TreeNode<NODETYPE>* N){
	
	TreeNode<NODETYPE>* R=N->rightPtr;
	TreeNode<NODETYPE>* P=N->parent;
	assert(R!=NULL);
// 	TreeNode<NODETYPE>* Leaf1=N->leftPtr;
	TreeNode<NODETYPE>* Leaf2=R->leftPtr;
// 	TreeNode<NODETYPE>* Leaf3=R->rightPtr;
	
	//Dealing with descendent
	N->rightPtr=Leaf2;
	R->leftPtr=N;
	if(P==NULL){
		assert(rootPtr==N);
		rootPtr=R; //This is for root Ptr case;
	}
	else {
		if(P->leftPtr==N)
			P->leftPtr=R;
		else if(P->rightPtr==N)
			P->rightPtr=R;
		else {
			std::cerr<<"Error! find a pseudo parent!!\n";
			exit(-1);
		}
	}
	//Dealing with parents;
	if(Leaf2!=NULL)
		Leaf2->parent=N;
	N->parent=R;
	R->parent=P;
}
// This function receives a pointer to a pointer so the
// pointer can be modified
template<class NODETYPE>
		TreeNode<NODETYPE>* Tree<NODETYPE>::insertNodeHelper(TreeNode<NODETYPE> **ptr,
		const NODETYPE &value,Int currentLevel,TreeNode<NODETYPE>* parent)
{
	if (*ptr == 0) {
		*ptr = new TreeNode<NODETYPE>(value);
		(*ptr)->parent=parent;
		assert(*ptr != 0);
		nMaxTreeLevel=currentLevel>nMaxTreeLevel? currentLevel:nMaxTreeLevel;
		counter ++;
		return *ptr;
	} 
	else if (value < (*ptr)->data)
	{
		TreeNode<NODETYPE>* result=insertNodeHelper( &((*ptr)->leftPtr), value,currentLevel+1,*ptr);
		return result;
	}
	else if ( value > (*ptr)->data   )
	{
		TreeNode<NODETYPE>* result=	insertNodeHelper( &((*ptr)->rightPtr), value,currentLevel+1,*ptr);
		return result;
	}
	else
		return (*ptr);
}

template<class NODETYPE>
		TreeNode<NODETYPE> *Tree<NODETYPE>::findNode(const NODETYPE &value)
{
	tmpPtr = 0;
	findNodeHelper(rootPtr, value);

	return tmpPtr;
}

template<class NODETYPE>
		void Tree<NODETYPE>::findNodeHelper(TreeNode<NODETYPE> *ptr,
		const NODETYPE &value)
{
	if (ptr == 0) return;
  
	if (value == ptr->data) {
		tmpPtr = ptr;
    
		return;
	}
	else {
		if (value < ptr->data) findNodeHelper(ptr->leftPtr, value);
		else
			findNodeHelper(ptr->rightPtr, value);
	}
}

template<class NODETYPE>
		void Tree<NODETYPE>::preOrderTraversal() const
{ preOrderHelper(rootPtr); }

template<class NODETYPE>
		void Tree<NODETYPE>::preOrderHelper(TreeNode<NODETYPE> *ptr) const
{
	if (ptr != 0) {
		ptr->data.print();
		preOrderHelper(ptr->leftPtr);
		preOrderHelper(ptr->rightPtr);
	}
}

template<class NODETYPE>
		void Tree<NODETYPE>::setcounter(Int cnt) { counter = cnt; }

template<class NODETYPE>
		Int Tree<NODETYPE>::getcounter() { return counter; }

template<class NODETYPE>
		void Tree<NODETYPE>::numberData(Int bType)
{
	numberDataHelper(rootPtr, bType);
}

template<class NODETYPE>
		void Tree<NODETYPE>::numberDataHelper(TreeNode<NODETYPE> *ptr, Int bType)
{
	if (ptr == 0) return;

	if ((ptr->data).getbType() == bType) {
		if ((ptr->data).getcnt() == -1) {
			(ptr->data).setcnt(counter);
      
			counter ++;
		}
	}

	numberDataHelper(ptr->leftPtr, bType);
	numberDataHelper(ptr->rightPtr, bType);
}

//////////////////////////////////////////////////////////////////////////
// added by MV to number the MortarInterface for the DD
template<class NODETYPE>
		void Tree<NODETYPE>::numberData()
{
	numberDataHelper(rootPtr);
}

template<class NODETYPE>
		void Tree<NODETYPE>::numberDataHelper(TreeNode<NODETYPE> *ptr)
{
	if (ptr == 0) return;

	if ((ptr->data).getcnt() == -1) {
		(ptr->data).setcnt(counter);
		counter ++;
	}

	numberDataHelper(ptr->leftPtr);
	numberDataHelper(ptr->rightPtr);
}

// added by MV to number ints doubles etc.
template<class NODETYPE>
		void Tree<NODETYPE>::number()
{
	numberHelper(rootPtr);
}

template<class NODETYPE>
		void Tree<NODETYPE>::numberHelper(TreeNode<NODETYPE> *ptr)
{
	if (ptr == 0) return;
	counter ++;
	numberHelper(ptr->leftPtr);
	numberHelper(ptr->rightPtr);
}

template<class NODETYPE>
		void Tree<NODETYPE>::convert2Array(NODETYPE **ndArray, Int n)
{
	if (counter != n) return;

	Int cnt = 0;
	convert2arrayHelper(rootPtr, ndArray, cnt);
}

template<class NODETYPE>
		void Tree<NODETYPE>::convert2arrayHelper(TreeNode<NODETYPE> *ptr,
		NODETYPE **ndARRAY, Int &nn)
{
  //  Int nn;
	if (ptr == 0) return;
  //  ndARRAY[nn] = &(ptr->data);
	(*ndARRAY)[nn] = ptr->data;
	nn++;
	convert2arrayHelper(ptr->leftPtr, ndARRAY, nn);
	convert2arrayHelper(ptr->rightPtr, ndARRAY, nn);
}

//////////////////////////////////////////////////////////////////////////



// template<class NODETYPE>
// 		void Tree<NODETYPE>::ConvertToArray(NODETYPE **ndArray, Int n)
// {
// 	if (counter != n) return;
// 
// 	convertToarrayHelper(rootPtr, ndArray);
// }

template<class NODETYPE>
		void Tree<NODETYPE>::ConvertToArray(NODETYPE **ndArray)
{
	counter = 0;

	QuickconvertToarrayHelper(rootPtr, ndArray);
}

// template<class NODETYPE>
// void Tree<NODETYPE>::ConvertToArray(NODETYPE **ndArray, Int n)
// {
//   if (counter != n) return;
// 
//   id = 0;
//   convertToarrayHelper(rootPtr, ndArray);
// }

// template<class NODETYPE>
// void Tree<NODETYPE>::convertToarrayHelper(TreeNode<NODETYPE> *ptr,
// 					  NODETYPE **ndARRAY)
// {
//   if (ptr != 0) {
//     ndARRAY[id] = &(ptr->data);
//     id ++;
// 
//     convertToarrayHelper(ptr->leftPtr, ndARRAY);
//     convertToarrayHelper(ptr->rightPtr, ndARRAY);
//   }
// }
template<class NODETYPE>
		void Tree<NODETYPE>::ConvertToArray(NODETYPE **ndArray, Int n)
{
	if (counter != n) return;

	id = 0;
	convertToarrayHelper(rootPtr, ndArray);
}

template<class NODETYPE>
		void Tree<NODETYPE>::convertToarrayHelper(TreeNode<NODETYPE> *ptr,
		NODETYPE **ndARRAY)
{
	if (ptr != 0) {
		ndARRAY[id] = &(ptr->data);
		id ++;

		convertToarrayHelper(ptr->leftPtr, ndARRAY);
		convertToarrayHelper(ptr->rightPtr, ndARRAY);
	}
}
template<class NODETYPE>
		void Tree<NODETYPE>::QuickconvertToarrayHelper(TreeNode<NODETYPE> *ptr,
		NODETYPE **ndARRAY)
{
	if (ptr == 0) return;
  
	ndARRAY[counter] = &(ptr->data);
	counter ++;

	QuickconvertToarrayHelper(ptr->leftPtr, ndARRAY);
	QuickconvertToarrayHelper(ptr->rightPtr, ndARRAY);
}


#endif
