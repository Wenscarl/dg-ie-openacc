#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "array_pointer.h"
#include <stdio.h>


/***************************
 * dynamic memory allocation 
 ***************************/
void bld_ncmax_(Integer *ncmax) {

    if (*ncmax != 0) {
     printf("Sizeof Integer is %d\n",(int) sizeof(Integer));
     igcsp    = (Integer *) malloc (((*ncmax)    ) *sizeof(Integer));
     igallp   = (Integer *) malloc (((*ncmax) + 1) *sizeof(Integer));
     noselfp  = (Integer *) malloc((((*ncmax)+1)*2)*sizeof(Integer));
     igsndrsp = (Integer *) malloc  ((*ncmax)      *sizeof(Integer));

     if (igcsp  == NULL || igallp  == NULL || noselfp  == NULL
                        || igsndrsp == NULL) 
     { fprintf(stderr, "malloc fail for bld_ncmax \n");
         exit(1);
     }
    }
}


void bld_ncmax_maxedge_(Integer *ncmax, Integer *maxedge) {

    if (*maxedge != 0) {

     indexp   = (Integer *) malloc (((*ncmax) +(*maxedge)) *sizeof(Integer));

     if (indexp  == NULL ) { fprintf(stderr, 
         "malloc fail for bld_ncmax_maxedge \n");
         exit(1);
     }
    }
}


void bld_kpm_(Integer *kpm) {

    if (*kpm != 0) {

     cvlrp  = (Complex *) malloc (2*(*kpm) *sizeof(Complex));
     cgmp   = (Complex *) malloc (2*(*kpm) *sizeof(Complex));    


     if (cvlrp  == NULL || cgmp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_kpm \n");
         exit(1);
     }
    }
}

void bld_nkpm_(Integer *nkpm) {

    if (*nkpm != 0) {

     irsap   = (Integer *) malloc (   (*nkpm)  *sizeof(Integer)); 
     c_shiftp = (Complex *) malloc ((8*(*nkpm)) *sizeof(Complex));


     if (irsap  == NULL || c_shiftp == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_nkpm \n");
         exit(1);
     }
    }
}

void bld_ntlm_(Integer *ntlm) {

    if (*ntlm != 0) {

     ctlp     = (Complex *) malloc ((*ntlm) *sizeof(Complex));

     if (ctlp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_ntlm \n");
         exit(1);
     }
    }
}


void bld_niplm_(Integer *niplm) {

    if (*niplm != 0) {

     icsap   = (Integer *) malloc ((*niplm)   *sizeof(Integer));
     arrayp  = (Real *) malloc ((*niplm)   *sizeof(Real));

     if (icsap  == NULL || arrayp == NULL) 
     { fprintf(stderr, "malloc fail for bld_niplm \n");
         exit(1);
     }
    }
}


void bld_nsm_(Integer *nsm) {

    if (*nsm != 0) {

     csmp  = (Complex *) malloc (2 * (*nsm) *sizeof(Complex));

     if (csmp  == NULL )
         { fprintf(stderr, 
           "malloc fail for bld_nsm \n");
         exit(1);
     }
    }
}


void bld_ifar_lmax_(Integer *ifar, Integer *lmax) {

    if (*lmax != 0) {

     int q1, q2;
     q1 = 2*(2*(*ifar)+1) + 1;
     q2 = q1 * q1 * q1 * (*lmax);

     indextlp = (Integer *) malloc (q2 *sizeof(Integer));

     if (indextlp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_ifar_lmax \n");
         exit(1);
     }
    }
}

void bld_ngsndm_(Integer *ngsndm) {

    if (*ngsndm != 0) { 

     igsndcsp = (Integer *) malloc ((*ngsndm) *sizeof(Integer));

     if (igsndrsp  == NULL || igsndcsp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_ncmax_ngsndm \n");
         exit(1);
     }
    }
}


