      subroutine out_current( current, crhs,
     &   xyznode, ipatpnt, iedge,  xyzctr, edge, paera )

        use mlfma_input
        use mlfma_const
        use mlfma_param

      implicit none
! This code is modified by WXC to be compatible with .curJ file output in EMoffice
!      include 'mlfma_param.inc'
!      include 'mlfma_const.inc'
!      include 'mlfma_input.inc'

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode),  xyzctr(3, maxpatch), edge(maxedge),
     &        paera(maxpatch)
      complex  current(9, maxpatch), crhs(maxedge)

      integer*8 i, j, l, i3, n1, n2, n3, icoat, icomp, no_color
      real    signl, absdelta, absmin, absmax, abstemp,
     &        realdelta, realmin, realmax, realtemp

      parameter (no_color =28)

      do i = 1, maxpatch
         do j = 1, 9
            current(j,i) = 0.0
         enddo
      enddo
*
      do 100 i = 1, maxedge
         n1 = iedge(1,i)
         n2 = iedge(2,i)
         do l = 1, 2
            i3 = iedge(l+2,i)
            n3 = ipatpnt(1,i3)+ipatpnt(2,i3)+ipatpnt(3,i3)-n1-n2
            signl = float(3-2*l)
            do j = 1, 3
               current(j,i3) = (xyzctr(j,i3)-xyznode(j,n3))
     &                *signl*edge(i)*crhs(i) + current(j,i3)
            enddo
          enddo
100   continue
      do i = 1, maxpatch
         do j = 1, 3
            current(j,i) = 0.5/paera(i)*current(j,i)
            n1 = ipatpnt(j,i)
c            write(20,*) (xyznode(l,n1), l=1,3)
         enddo
      enddo
*
      call abs_real_normc2(current(1,1),3, abstemp, realtemp)
      absmin  = abstemp
      absmax  = abstemp
      realmin = realtemp
      realmax = realtemp
      do i = 1, maxpatch
         call abs_real_normc2(current(1,i),3, abstemp, realtemp)
         if (absmin  .gt.  abstemp)  absmin =  abstemp
         if (absmax  .lt.  abstemp)  absmax =  abstemp
         if (realmin .gt. realtemp) realmin = realtemp
         if (realmax .lt. realtemp) realmax = realtemp
      enddo
      absdelta  = (absmax  -  absmin)/float(no_color)
      realdelta = (realmax - realmin)/float(no_color)

!       write(11,*) maxpatch
!       do j = 1, maxpatch
!          call abs_real_normc2(current(1,j),3, abstemp, realtemp)
!          icoat = int((realtemp-realmin)/realdelta - 0.01) + 1
!          icomp = int((abstemp - absmin)/absdelta  - 0.01) + 1
! 	 write(11,*) j, icoat, icomp
!       enddo

      return
      end


      subroutine abs_real_normc2(carray, n, abstemp, realtemp)
      implicit none
      integer n
      real    abstemp, realtemp
      complex carray(n)
*
      integer i
      abstemp  = 0.0
      realtemp = 0.0
      do i = 1, n
         abstemp  = abstemp  + abs(carray(i))**2
         realtemp = realtemp + real(carray(i))**2
      enddo
      abstemp  = sqrt(abstemp)
      realtemp = sqrt(realtemp)
      return
      end
