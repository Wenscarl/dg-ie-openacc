#include "global.h"
#include "float_vtr.h"
#include "RBtree.h"
#include "utility.h"
#include "myedge.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <limits>

void showUsage(void)

{
	cout<<"Usage:~$SR-JCFIEDG-AS prjName ";
	cout<<"Polarization angle: 0deg=>theta only, 90deg=>phi only  \n"; 
	exit(-1);
}


void userInput(float &freq, float &thetaStart, float &thetaEnd, float &thetaStep, 
               float &phiStart, float &phiEnd, float &phiStep,
               float &Einc_mag, float &Einc_phs, float &polAng,
               float &eps, bool &writeCurJ) {

  float thetaRange;
  float phiRange; 
  char tChar;
  string input = "";

  // Get freq
  while (true) {
    cout << "Enter frequency (MHz): ";
    getline(cin, input);
    stringstream inStream(input);
    if (inStream >> freq)
      if (freq >= 1) break;     
    cout << "Invalid entry" << endl;
  }

  // Get Theta Range 
  while (true) {
    cout << "Enter range angles for THETA (-180< ? <180): Start End >> ";
    getline(cin, input);
    stringstream inStream(input);
    if ((inStream >> thetaStart) && (inStream >> thetaEnd)) {
      if (((thetaStart >= -180) && (thetaStart <= 180)) && ((thetaEnd >= -180) && (thetaEnd <= 180)))     
        break;    
    cout << "Invalid entry" << endl;
    }
  } 

  // Get Theta Step
  thetaStep = 0.0;
  thetaRange = thetaEnd - thetaStart;   
  if (fabs(thetaRange) > 0) {
    while (true) {
      cout << "Enter THETA step (0< ? <" << fabs(thetaRange) <<"): ";
      getline(cin, input);
      stringstream inStream(input);
      if (inStream >> thetaStep)
        if ( (thetaStep >= 0) && (thetaStep <= fabs(thetaRange)) ) break;  
      cout << "Invalid entry" << endl;
    }
  }
  if (thetaRange < 0) thetaStep = -1.0 * thetaStep;

  // Get Phi Range 
  while (true) {
    cout << "Enter range angles for PHI (-360< ? <360): Start End >> ";
    getline(cin, input);
    stringstream inStream(input);
    if ((inStream >> phiStart) && (inStream >> phiEnd)) {
      if (((phiStart >= -360) && (phiStart <= 360)) && ((phiEnd >= -360) && (phiEnd <= 360)))     
        break;    
    cout << "Invalid entry" << endl;
    }
  } 

  // Phi Step
  phiStep = 0.0;
  phiRange = phiEnd - phiStart;
  if (fabs(phiRange) > 0 ) {
    while (true) {
      cout << "Enter PHI step (0< ? <" << fabs(phiRange) <<"): ";
      getline(cin, input);
      stringstream inStream(input);
      if (inStream >> phiStep)
        if ( (phiStep >= 0) && (phiStep <= fabs(phiRange)) ) break; 
      cout << "Invalid entry" << endl; 
    }
  }
  if (phiRange < 0) phiStep = -1.0 * phiStep;   

  // Get Einc magnitude and phase
  while (true) {
    cout << "Enter incident E-field properties: Mag(V/m) Phase(deg) >> ";
    getline(cin, input);
    stringstream inStream(input);
    if ((inStream >> Einc_mag) && (inStream >> Einc_phs))
      if ((Einc_phs >= 0) && (Einc_phs <= 360)) break;  
    cout << "Invalid entry" << endl;
  } 

  // Get polarization angle
  while (true) {
    cout << "Enter polarization angle (0<?<360): ";
    getline(cin, input);
    stringstream inStream(input);
    if (inStream >> polAng)
      if ((polAng >= 0) && (polAng <= 360)) break;  
    cout << "Invalid entry" << endl;
  } 

  // Get tolerance
  while (true) {
    cout << "Enter MLFMA tolerance (default=0.001): ";
    getline(cin, input);
    stringstream inStream(input);
    if (inStream >> eps) {
      if ((eps > 0) && (eps <= 0.1)) break;   
    }
    else {
      eps = 0.001;
      break;
    }
  } 

  // Get
    writeCurJ = false; 
//  while (true) {
//    cout << "Write-out current distribution? (Y/N): ";
//    getline(cin, input);
//    if (input.length() == 1) {
//      tChar = input[0];
//      tChar = toupper(tChar);
//      if (tChar == 'Y') {
//        writeCurJ = true;  
//        break;
//      }
//      else if (tChar == 'N') {
//        writeCurJ = false;
//        break;
//      }    
//    }
//    cout << "Invalid entry" << endl;
//  }
}

void writeInParam(const float freq, const float thetaStart, const float thetaEnd, 
                  const float thetaStep, const float phiStart, const float phiEnd, 
                  const float phiStep, const float Einc_mag, const float Einc_phs, 
                  const float polAng, const float eps, 
                  const bool writeCurJ, const char *prjName) {

  //Write input parameter file
  string fileName;
  ofstream fout;
  fileName = string(prjName) + ".in";
  fout.open(fileName.c_str(),ios::out);  
  if (fout.fail()) {
    cerr << "Error opening " << fileName << " for writing!" <<endl;
    exit (1);
  }

  fout << "ID  Theta(deg)  Phi(deg)  Einc_mag|(V/m)  Einc_phase(deg) ";
  fout << "Pol_angle(deg)  outCurJ?(Y/N)" << endl;

  std::vector<float> thetaList, phiList;
  Int npts;
  float angle;
  float thetaRange, phiRange;  

  thetaRange = thetaEnd - thetaStart;
  phiRange = phiEnd - phiStart;

  angle = thetaStart;
  if (thetaRange > 0) {  // ascending theta points
    while (angle <= thetaEnd) {
      thetaList.push_back(angle);
      angle += thetaStep;  
    } 
  } 
  else if (thetaRange < 0) { // descending theta points
    while (angle >= thetaEnd) {
      thetaList.push_back(angle);
      angle += thetaStep;    
    }
  }
  else {// single pt theta
    thetaList.push_back(angle);
  }

  angle = phiStart;
  if (phiRange > 0) {  // ascending phi points
    while (angle <= phiEnd) {
      phiList.push_back(angle);
      angle += phiStep; 
    } 
  } 
  else if (phiRange < 0) { // descending phi points
    while (angle >= phiEnd) {
      phiList.push_back(angle);
      angle += phiStep;    
    }
  }
  else {  // single point phi
    phiList.push_back(angle);
  }
  
  const int nTheta = thetaList.size();
  const int nPhi = phiList.size();

  fout << nTheta * nPhi << " " << freq << " " << eps << endl ; 

  int ID = 1;
  for (int p = 0; p < nTheta; p++) {
    for (int q = 0; q < nPhi; q++) {
      fout << ID << " ";
      fout << thetaList[p] << " " << phiList[q]   << " "; 
      fout << Einc_mag   << " " << Einc_phs  << " ";
      fout << polAng     << " " << writeCurJ << endl;     
      ID++;
    } 
  }
 
  fout.close();
}


bool readInParam (float &freq, float &eps, const string fileName) {

  fstream file(fileName.c_str());
  Int npts;

  if (file.good()) {

    ifstream fin;
    fin.open(fileName.c_str(),ios::in);  
    if (fin.fail()) {
      cerr << "Error opening " << fileName << " for writing!" <<endl;
      exit (1);
    }
 
    string ignoreLine;
    getline(fin,ignoreLine);
    fin >> npts >> freq >> eps;  
    getline(fin,ignoreLine);
    fin.close();
    return true;
  }

  else return false;
}

Integer findI(Integer i,Integer* ii)
{
	for (Integer j=0;j<3;j++)
	{
		if(ii[j]==i)
			return j;
	}
	return -1;
}

float Len(float* a,float* b)
{
	float dx=a[0]-b[0];
	float dy=a[1]-b[1];
	float dz=a[2]-b[2];
	return sqrt(dx*dx+dy*dy+dz*dz);
}
double Len(double* a,double* b)
{
	double dx=a[0]-b[0];
	double dy=a[1]-b[1];
	double dz=a[2]-b[2];
	return sqrt(dx*dx+dy*dy+dz*dz);
}
	    
	    
Int ImportTriangles(const char* prjName1, Int& maxnode1,Real** xyznodep1, Int& maxpatch1, Int** ipatpntp1, Int& maxedge1, Int** iedgep1, Integer** triFlag)
{
	char buf[0x200];
	cout<<"Begin Import the first triangle set\n";
	sprintf(buf,"%s.tri",prjName1);
	fstream foo(buf,ios::in);
	if (foo.is_open()==false)
	{
		cout<<"tri file open failed!\n";
		exit(-1);
	}
	
	Real scale;
	foo>>scale;
	foo>>maxnode1;
	*xyznodep1=new Real [3*maxnode1];
	for(Int i=0;i<maxnode1*3;i++)
	{
		foo>>(*xyznodep1)[i];(*xyznodep1)[i]*=scale;
	}
	cout<<"node import finished\n";
	foo>>maxpatch1;
	(*ipatpntp1)=new Integer[3*maxpatch1];
	cout<<"triangle import \n";
	Tree<Edge> iedgeTree1;
	iedgeTree1.setcounter(0);
	for(Int i=0;i<maxpatch1;i++)
	{
		Int in[3];
		foo>>in[0]>>in[1]>>in[2];
		for(Int j=0;j<3;j++)
		{
			in[j]++; 
			(*ipatpntp1)[i*3+j]=in[j];
		}
		for(Int j=0;j<3;j++)
		{
			Edge ed(in[j],in[(j+1)%3]);
			if(iedgeTree1.insertNode(ed)==1) //insertion
			{
				assert(iedgeTree1.findNode(ed)->data.tri1<=0);
				iedgeTree1.findNode(ed)->data.tri1=i+1;
			}
			else
			{	//That iedge already exist
//                         assert(iedgeTree.findNode(ed)->data.tri2<=0);
				if(!(iedgeTree1.findNode(ed)->data.tri2<=0))
				{
					cout<<"Error!!!! One edge is found to belong to more than 2 triangles!!"<<endl;
					cout<<"Current Edge is :"<<ed.n1<< " "<<ed.n2<<endl;
					cout<<"Current triangle is: "<<i<<endl;
					cout<<"The 2 triangles already there are :"<<iedgeTree1.findNode(ed)->data.tri1 <<" "
							<<iedgeTree1.findNode(ed)->data.tri2<<endl;
					cout<<"Program terminate with error!\n Check the Geometry!\n"<<endl;
					exit(-1);
				}
				iedgeTree1.findNode(ed)->data.tri2=i+1;
			}
		}
	}
	foo.close();
	cout<<"triangle import finished\n"<<flush;

	///////////////////////////      
	Int nedge1=iedgeTree1.getcounter();
	cout<<"Number of edge in triangles set 1: "<<nedge1<<endl<<flush;
	Edge** tempEdge1=new Edge*[nedge1];
	cout<<"Convert iedgeTree to Array...."<<flush;
	iedgeTree1.ConvertToArray(tempEdge1,nedge1);
	cout<<"finished\n"<<flush;
      //Only the non-boundary edge is chosen.
	maxedge1=0;
	for(Int i=0;i<nedge1;i++)
	{
		if (tempEdge1[i]->tri1!=0 &&tempEdge1[i]->tri2!=0 )
			maxedge1++;
	}
	
	cout<<"Total iedge number in triangles 1:"<<maxedge1<<endl<<flush;
	(*iedgep1)=new Integer [maxedge1*4];
	Int count_edge=0;
	for(Int i=0;i<nedge1;i++)
	{
		if (tempEdge1[i]->tri1==0 ||tempEdge1[i]->tri2==0 )
			continue;
		(*iedgep1)[count_edge*4]	=tempEdge1[i]->n1;
		(*iedgep1)[count_edge*4+1]	=tempEdge1[i]->n2;
		(*iedgep1)[count_edge*4+2]	=tempEdge1[i]->tri1;
		(*iedgep1)[count_edge*4+3]	=tempEdge1[i]->tri2;
		count_edge++;
	}
	assert(count_edge==maxedge1);
	
	(*triFlag)=new Integer[maxpatch1];
	
	sprintf(buf,"%s.flg",prjName1);
	fstream fooFlag(buf,ios::in);
	if(fooFlag.is_open()==false){
		cout<<"Flag file: "<<buf<<" cannot be opened!\n";
		for(Int i=0;i<maxpatch1;i++){
			(*triFlag)[i]=0;
		}
	}
	else{
		cout<<"Flag file: "<<buf<<" is being loaded...";
		for(Int i=0;i<maxpatch1;i++){
			fooFlag>>(*triFlag)[i];
		}
		fooFlag.close();
		cout<<"Finished.\n";
	}
	
	
	
	
//       delete [] *tempEdge;
	delete[] tempEdge1;
	iedgeTree1.freeMemory();
	cout<<"Tree to array conversion finished for triangles 1\n"<<flush;
	return 0;
}
Int readInCurJ(const char *filename, Complex* buffer, Int Count)
{
	ifstream foo;
	foo.open(filename);
	if(foo.is_open()==false)
		return -1;
	for(Int i=0;i<Count;i++)
		foo>>reref(buffer[i])>>imref(buffer[i]);
	foo.close();
	return 0;
}
Int readInCurJBin(const char *filename, Complex* buffer, Int Count)
{
	ifstream foo;
	foo.open(filename,ios::binary);
	if(foo.is_open()==false)
	{
		for(Int i=0;i<Count;i++){
			buffer[i]=Complex(0.0);
		}
		return -1;
	}
	InputComplex* tmp=new InputComplex[Count];
	
	foo.read((char*)tmp,sizeof(InputComplex)*Count);
	foo.close();
	for(Int i=0;i<Count;i++){
		buffer[i]=(Complex)(tmp[i]);
	}
	delete[] tmp;
	return 0;
}

Real getArea(Real* xyznode,Integer* index)
{
	Integer n[3];
	for(Int i=0;i<3;i++)
		n[i]=index[i]-1;
	vtr r12,r01;
	r01.setvtr(xyznode[3*n[1]]-xyznode[3*n[0]],xyznode[3*n[1]+1]-xyznode[3*n[0]+1],
		   xyznode[3*n[1]+2]-xyznode[3*n[0]+2]);
	r12.setvtr(xyznode[3*n[2]]-xyznode[3*n[1]],xyznode[3*n[2]+1]-xyznode[3*n[1]+1],
		   xyznode[3*n[2]+2]-xyznode[3*n[1]+2]);
	vtr product=r01*r12;
	return product.magnitude()/2.0;
	
}
void runCommand(const char* buf)
{
	Int rc=0;
	rc=system(buf);
	if(rc!=0){
		cout<<"Attention! command : \n"<< buf<<" \n execution is failed!\n";
	}
}
bool testFileExisting(const char* buf, bool binary)
{
	ifstream foo;
	if(binary)
		foo.open(buf,ios::binary);
	else
		foo.open(buf);
	if(foo.is_open()){
		foo.close();
		return true;
	}
	else
		return false;
}
Int ImportTrianglesDIRECT(const char* prjName1, Int& maxnode1,Real** xyznodep1, Int& maxpatch1, Int** ipatpntp1, Int& maxedge1, Int** iedgep1,Integer** flagp)
{
	char buf[0x200];
	ifstream fin;
	sprintf(buf,"%s.node",prjName1);
	fin.open(buf,ios::binary);
	fin>> (maxnode1);
	(*xyznodep1)=new Real[maxnode1*3];
	for(Int i=0;i<3*maxnode1;i++)
		fin>> (*xyznodep1)[i];
	fin.close();
	
	sprintf(buf,"%s.edge",prjName1);
	fin.open(buf,ios::binary);
	fin>> (maxedge1);
	(*iedgep1)=new Integer[maxedge1*4];
	for(Int i=0;i<4*maxedge1;i++)
		fin>> (*iedgep1)[i];
	fin.close();
	
	sprintf(buf,"%s.patch",prjName1);
	fin.open(buf,ios::binary);
	fin>> (maxpatch1);
	(*ipatpntp1)=new Integer[maxpatch1*3];
	for(Int i=0;i<4*maxpatch1;i++)
		fin>> (*ipatpntp1)[i];
	fin.close();
	(*flagp)= new Integer[maxpatch1];
	for(Int i=0;i<maxpatch1;i++)
		(*flagp)[i]=0;
	return 0;	
}
