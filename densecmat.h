// DD_DENSECMAT.H
#ifndef DD_DENSECMAT_H
#define DD_DENSECMAT_H
#include "complex.h"
#include "myvector.h"
#include "comp.h"

typedef long long int Int;

class denseCMat {
 private:
  Int N, M;
  Complex *entry;

 public:
  denseCMat();
  denseCMat(const denseCMat &);
  denseCMat(Int, Int);// denseCMat(row, colum)
  ~denseCMat(); // added by MV
  void delMat();// temporary destructor gives problems

  // modifiers
  void setDim(Int, Int);
  void addEntry(Int r, Int c, Complex value );
  void addEntry( Int i, Complex value);
  void setEntry(Int, Int, Complex);
  void reset();
  void setCol(Int col, Vector &vect);
  void setRow(Int row, Vector &vect);

  // accessors
  Complex* getEntryPtr() { return entry; };
  Int getRowDim() const  { return N; };
  Int getColDim() const  { return M; };
  Complex getEntry(Int, Int) const;
  Complex* getColEntry(); // get the matrix in column format

  // operators
  denseCMat &operator =  (const denseCMat &);
  denseCMat  operator +  (const denseCMat &) const;
  denseCMat  operator -  (const denseCMat &) const;
  denseCMat  operator *  (const denseCMat &) const;
  Vector     operator *  (const Vector &x) const;
  denseCMat  operator *  (const Complex &) const;
  denseCMat  operator *  (const float &) const;
  denseCMat  operator /  (const Complex &) const;
  denseCMat  operator /  (const float &) const;
  denseCMat &operator += (const denseCMat &);
  denseCMat &operator -= (const denseCMat &);
  denseCMat &operator *= (const float &);
  denseCMat &operator *= (const Complex &);
  denseCMat &operator /= (const float &);
  denseCMat &operator /= (const Complex &);
  denseCMat transpose();
  denseCMat hermitian();
  denseCMat inverse();
  Complex operator() (Int i, Int j) const; // added my MV at 12/02/02


  // print functions
  void print();
  void printScreen(); // added by MV at 11/05/02
  void printScreen_1(); // added by MV at 11/05/02
//  void printMatlab(ostream &sout, char* name = "A", Int prec = 5);
  void printOnASCIIFile( char * ); // added by MV at 11/07/02
  void printOnBinaryFile( char * ); // added by MV at 11/18/02

  // append funcitons
  void appendOnASCIIFile( char * ); // added by MV at 11/07/02
  void appendOnBinaryFile( char * ); // added by MV at 11/18/02

  // read functions
  void readFromASCIIFile( char * );
  void readFromBinaryFile( char * );
  void readFromBinaryFile( FILE * );
  void readFromASCIIFileTrans( char * );
  void readFromBinaryFileTrans( char *filename );
};


////////////////////////////////////////////////////////////////////
//Free functions

// OVERLOAD FUNCTION VHXMXV for computing X^(H) x M x Y
Complex VHXMXV(Int, Complex *, denseCMat, Complex *);
Complex VHXMXV(Int, Complex *, Complex **, Complex *);
Complex VHXMXV(Int, Complex *, float  **, Complex *);

void Gaussj(denseCMat *);
void MXV(const denseCMat &, Complex *, Complex *);

#endif
