      function igl(lxyz,ig)

c.....find #igl from #ig(x,y,z).

      implicit none
      integer*8 igl,lxyz(3),ig(3)

      igl=ig(3)+lxyz(3)*(ig(2)+lxyz(2)*(ig(1)-1)-1)

      return
      end
