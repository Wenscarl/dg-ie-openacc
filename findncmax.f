      subroutine findncmax
     &( maxnode, maxpatch, maxedge,
     &  xyznode, ipatpnt, iedge,
     &  lmax, ncmax, lxyz, rmin, sizel, family)

cccccccccccccccccccc
c   find ncmax
cccccccccccccccccccc

      implicit none

c.....Input Data

      INTEGER*8 maxnode, maxpatch, maxedge
      INTEGER*8 iedge(4,maxedge),    ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode)
      INTEGER*8 lmax
      INTEGER*8 lxyz(3,0:lmax)
       REAL   rmin(3), sizel(3,0:lmax)

c.....Output Data
      
      INTEGER*8 ncmax
      INTEGER*8 family(maxedge)

c.....Working Variables

      INTEGER*8 l, n, i, ig(3), nozero, noempty, 
     &        igl, iglp, ip1, ip2, ncube

c.....For each basis, find which cube it belongs to

      l = lmax
      do n = 1, maxedge
         ip1 = iedge(1, n)
         ip2 = iedge(2, n)
         do i= 1, 3
            ig(i) = 1+ int((abs(0.5*(xyznode(i,ip1) + xyznode(i,ip2))
     &             - rmin(i)))/sizel(i,l)-1.0e-04)

            if ((ig(i).lt.1) .or. (ig(i).gt.lxyz(i,l))) then
c               write(*,*)' *** overflow in index for ig in maketree ***'
c               write(*,*)'It may caused by rounding off error'

c               print*,ig(i),lxyz(i,l)

               if (ig(i).lt.1) then 
                    ig(i) = 1
               end if

               if (ig(i).gt.lxyz(i,l))then
                    ig(i) = lxyz(i,l)
               end if

!                print*,ig(i),lxyz(i,l)

!               print*,ig(i),xyznode(i,ip1),xyznode(i,ip2)
!               print*,rmin(i),sizel(i,l),lxyz(i,l)
!					call exit(-1)
							
            endif
         enddo
            family(n) = igl(lxyz(1,l),ig)
      enddo

c....Sorting family, family is a vector for family and iwork in the note

      call sort1(maxedge,family)

c....Find nonempty groups in the finest level

      nozero = 1
      do n = 2, maxedge
         if(family(n).ne.family(nozero)) then
            nozero = nozero + 1
            family(nozero) = family(n)
         endif
      enddo

      ncube = nozero

c....Do all higher levels
      do l = lmax-1, 0, -1
         noempty = nozero
         do n = 1, noempty
            family(n) = iglp(family(n),lxyz(1,l+1),lxyz(1,l))
         enddo

c....Sorting family

         call sort1(noempty,family)

c....Find nonempty groups in this level

         nozero = 1
         do n=1,noempty
            if(family(n).ne.family(nozero)) then
               nozero = nozero + 1
               family(nozero) = family(n)
            endif
         enddo
         ncube = ncube + nozero
      enddo

      ncmax = ncube

      return
      end
