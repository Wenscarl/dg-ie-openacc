#ifndef GCR_HPP
#define GCR_HPP

#include <cstdio>
#include <vector>
#include <fstream>
#include "complex.h"
#include "densecmat.h"
#include "myvector.h"

typedef long long int Int;
typedef unsigned long long Unsigned;

template <class Matrix, class Preconditioner>
void gcr(Matrix &A, Preconditioner &M, Vector &b, Vector &x,
	 const double tol, const Unsigned maxit, const Unsigned restart) {

  const Int N = x.size();
  Unsigned file_counter = 0;

  Vector U[restart];
  Vector C[restart];
  ifstream fflag;

  for (Unsigned i = 0; i < restart; i++) {
    U[i] = x;
    U[i].reset();
    C[i] = x;
    C[i].reset();
  }

  Vector r;
  Vector tmp, z;
  //Vector dn, dnm1, dn_tilde;
  Complex conv_a = (0., 0.);
  tmp = M.solve(b);
  double norm_b = norm2(tmp);
  Unsigned k_max = 0;

  printf("%f\n", tol);

  Int index = 0, newIndex = 0;
  for (Unsigned outer_it = 0; outer_it < maxit; ++outer_it) {

    if ((outer_it % 25) == 0)
      r = M.solve(b - A*x);

    if (norm2(r) / norm_b < tol)
      break;

    index = newIndex;

    //z = M.solve(r);
    U[index] = r;
    C[index] = M.solve(A*U[index]);

    double alphaMin = 1.0e10;

    for (Unsigned j = 0; j < restart; ++j) {

      if (j == index) continue; 

      Complex alpha = dotHerm(C[j], C[index]);

      if (abs(alpha) < alphaMin) {
	alphaMin = abs(alpha);
	newIndex = j;
      }

      C[index] -= alpha*C[j];
      U[index] -= alpha*U[j];
    }

    Complex c_norm = norm2(C[index]);
    U[index] *= 1. / c_norm;
    C[index] *= 1. / c_norm;

    Complex val = dotHerm(C[index], r);

    x += val * U[index];
    r -= val * C[index];
    printf("%lld  %.6e\n", outer_it, norm2(r)/norm_b);
    fflush(stdout);


    if (norm2(r)/norm_b < tol)
      break;

    fflag.open("gcrstop.flag");

    if (fflag.is_open()) {
      fflag.close();
      cout<<"Forced terminate the iteration!"<<endl;
      int ret = system("rm gcrstop.flag");
      break;
    }
  }
}

#endif
