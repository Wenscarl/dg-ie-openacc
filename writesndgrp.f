      subroutine writesndgrp
     &( lmax, ncmax, lxyz, ifar, dfar,
     &  sizel, igcs, igrs, 
     &  ngsndm, igsndcs, igsndrs)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
c   find second neighbor groups for all nonempty groups.
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      implicit none

      INTEGER*8 lmax, ncmax
      INTEGER*8 lxyz(3,0:lmax), ifar
       REAL   dfar, sizel(3,0:lmax)
      INTEGER*8 igcs(ncmax), igrs(lmax+2)
      INTEGER*8 ngsndm, igsndcs(ngsndm), igsndrs(ncmax)

      INTEGER*8 ngsnd
      INTEGER*8 l, lrow, ipointer, noempty, ipgp, ig, ig3(3), igp3(3), 
     &        jlo, ix, iy, iz, i, ign, ign3(3), ignp3(3),
     &        ndp3(3), ndc3(3), igl

       write(*,*) 'Begin writesndgrp.f'

! 	open (93,file='Filesndgrp',status='unknown')
! 
!          write(93,*) ( igsndcs(i), i=1,ngsndm )
! 
!          write(93,*) ( igsndrs(i), i=1,ncmax )
! 
!         close(93)


        open (93,file='Filesndgrp',
     &  form='unformatted',access='direct',
     &       recl=8*(ngsndm+ncmax),
     &               status='unknown')

         write(93,rec=1)
     &  (igsndcs(i), i=1,ngsndm) ,
     &   (igsndrs(i), i=1,ncmax)
  
       close(93)

      return
      end
