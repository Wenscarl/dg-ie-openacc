      subroutine dlegendr(x,l,m,plgndr,ld1)
      implicit real*4 (a-h,o-z)
      dimension plgndr(0:ld1)
c      write(*,*) x,l,m
      if(m.lt.0.or.m.gt.l.or.abs(x).gt.1.) then
		write(*,*) 'bad arguments in dlegendr'
		call exit(-1)
		endif
      if(l.gt.ld1) then 
				write(*,*) 'l is large than ld1'
				call exit(-1)
		endif
      pmm=1.0e0
      if(m.gt.0) then
        somx2=sqrt((1.-x)*(1.+x))
        fact=1.
        do 11 i=1,m
          pmm=pmm*fact*somx2
          fact=fact+2.0e0
11      continue
      endif
      if(l.eq.m) then
        plgndr(l)=pmm
      else
        pmmp1=x*(2*m+1)*pmm
        if(l.eq.m+1) then
          plgndr(m)=pmm
          plgndr(m+1)=pmmp1
        else
          plgndr(m)=pmm
          plgndr(m+1)=pmmp1
          do 12 ll=m+2,l
            pll=(x*(2*ll-1)*pmmp1-(ll+m-1)*pmm)/(ll-m)
            plgndr(ll)=pll
            pmm=pmmp1
            pmmp1=pll
12        continue
        endif
      endif
      return
      end
