      subroutine fillvs(i, cvlr,
     &                  xyznode,ipatpnt,
     &                  iedge,xyzctr,xyznorm,edge,paera,
     &                  ngrid, vt1,vt2,vt3, wt, 
     &                  kp0, xyzc, dirk, kpt)


cccccccccccccccccccccccccc
c   fill vs in fmm
cccccccccccccccccccccccccc

        use mlfma_input
        use mlfma_const
        use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)

      INTEGER*8 kp0
      real xyzc(3), dirk(3,3,kp0)
      complex cvlr(2,kp0)
      INTEGER*8 i, kpt   

      INTEGER*8 l, k, j, i3, n1, n2, n3
      real rk(3), rc(3), pc(3), dotmul, signl
      INTEGER*8 kp,itp,ixyz
      complex cvthe, cvphi, ctmp
 
      do kp = 1, kpt
         cvthe = 0.0
         cvphi = 0.0

         do ixyz = 1, 3
            rk(ixyz) = rk0*dirk(ixyz, 1, kp)
         enddo

         n1 = iedge(1, i)
         n2 = iedge(2, i)
         do 10 l = 1, 2
            i3 = iedge(l+2,i)
            if( i3 .le. 0) then 
			goto 10 
		endif
            n3 = ipatpnt(1,i3)+ipatpnt(2,i3)+ipatpnt(3,i3)-n1-n2
            signl = float(3-2*l)
            do k=1,ngrid(irule_src)
               do j=1,3
                  rc(j) = vt1(k,irule_src)*xyznode(j,n1)+
     &                    vt2(k,irule_src)*xyznode(j,n2)+
     &                    vt3(k,irule_src)*xyznode(j,n3)
                  pc(j) = rc(j) - xyznode(j,n3) ! rho
                  rc(j) = rc(j) - xyzc(j)	! r_j-r_{m}
*                               ^ shift to cube center
               end do
               ctmp = signl*wt(k,irule_src)*exp((dotmul(rk,rc)*ci))

                  cvthe = cvthe + dotmul(dirk(1,2,kp),pc)*ctmp
                  cvphi = cvphi + dotmul(dirk(1,3,kp),pc)*ctmp

            enddo
10        enddo
         cvlr(1,kp) = 0.5*cvthe*edge(i)
         cvlr(2,kp) = 0.5*cvphi*edge(i)
         do itp=1,2
            cvlr(itp,kp) = conjg(cvlr(itp,kp))
         enddo

      enddo

      return
      end
