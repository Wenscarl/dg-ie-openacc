      function CFUNHX
     &( rhoxn, rr, rhosrc,
     &  ipfld,ipsrc,nearpat,
     &  fld2src,fld2src2,cagu,cagu2, cx, cg0, ck, ci )

      IMPLICIT NONE

c.....Input Data

       REAL rhoxn(3), rr(3), rhosrc(3)
       INTEGER*8 ipfld, ipsrc, nearpat
       REAL fld2src, fld2src2
       COMPLEX cagu, cagu2, cx, cg0, ck, ci

c.....Output Data

       COMPLEX cfunhx

c.....Working Variables

       REAL    tmp(3), fld2src3, dotmul
       COMPLEX cmp1, cmp2, cgrnp

c..................................................................
      
      cfunhx = (0.0,0.0)

      if( ipfld.eq.ipsrc ) RETURN

      call xmul(rr, rhosrc, tmp)
      fld2src3 = fld2src * fld2src2

      if(nearpat.eq.1) then         
         if(cabs(cagu).lt.0.01) then
            cmp1 = cagu*( 1.0-cagu2/18.0 )/8.0
            cmp2 =     -( 1.0-cagu2/10.0 )/3.0
            cgrnp=ck*ck*ck*(cmp1+ci*cmp2)
         else
            cgrnp=(cx*cg0+1.0+0.5*cagu2)/fld2src3
         end if
      else
         cgrnp= cx*cg0/fld2src3
      end if
      cfunhx = cgrnp*dotmul(rhoxn,tmp)

      RETURN
      END
