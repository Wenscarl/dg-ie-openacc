cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c....dfar:  distance between two groups which are considered using fmm.....c
c....diag_dis:  control parameter for dfar.................................c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE mlfma_const

      real pi, rad, deg
      real eps, small7, big7
      complex hj
      integer*8 ifar
      real    diag_dis, dfar

      parameter (pi       = 4.0*atan(1.0))
      parameter (rad      = pi/180.0)
      parameter (deg      = 1.0/rad)

      parameter (eps      = 1.00000000e-4)
      parameter (small7   = 1.00000000e-7)
      parameter (big7     = 1.00000000e7)

      parameter (hj       = (0.00000000, 1.00000000))
      parameter (ifar=1, diag_dis=1.732, dfar=2.1*diag_dis)

      END MODULE
