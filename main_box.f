      subroutine main_box
     &( MVmodelong,direct,frequency,nNode,nTri,nEdge,memory_tot, lmax, 
     &  iedge, edge, ipatpnt, xyzctr, 
     &  paera, xyznorm,xyznode,
     &  ngrid, vt1, vt2, vt3, wt, igrs, lxyz,
     &  modes, sizel,lrsmup, lrsmdown, lrstl, 
     &  family, kpstart,crhs,iepscg,contourMap)

        use mlfma_input
        use mlfma_const
        use mlfma_param
        use mlfma_progCtrl

        implicit none

	INTERFACE
	    SUBROUTINE bld_lmax(lmax)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_lmax':: bld_lmax
	        integer*8 lmax
!DEC$ ATTRIBUTES REFERENCE ::lmax 
          END SUBROUTINE

	    SUBROUTINE bld_ncmax(ncmax)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_ncmax':: bld_ncmax
	        integer*8 ncmax
!DEC$ ATTRIBUTES REFERENCE ::ncmax 
          END SUBROUTINE

	    SUBROUTINE bld_ncmax_maxedge(ncmax,maxedge)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_ncmax_maxedge':: bld_ncmax_maxedge
	        integer*8 ncmax,maxedge
!DEC$ ATTRIBUTES REFERENCE ::ncmax,maxedge 
          END SUBROUTINE

	    SUBROUTINE bld_kpm(kpm)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_kpm':: bld_kpm
	        integer*8 kpm
!DEC$ ATTRIBUTES REFERENCE ::kpm 
          END SUBROUTINE

	    SUBROUTINE bld_ifar_lmax(ifar,lmax)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_ifar_lmax':: bld_ifar_lmax
	        integer*8 ifar,lmax 
!DEC$ ATTRIBUTES REFERENCE ::ifar,lmax 
          END SUBROUTINE

	    SUBROUTINE c_main_tree( memory_tot,lmax,ncmax,
     $	  	    lsmax,lsmin,kp0,kpm,contourMap,maxedge)
!DEC$ ATTRIBUTES C,ALIAS:'_c_main_tree':: c_main_tree
		  real    memory_tot
	        integer*8 lmax,ncmax,lsmax,lsmin,kp0,kpm,maxedge
              integer*8 contourMap(maxedge) 
!             complex crhs(maxedge)
!DEC$ ATTRIBUTES REFERENCE ::memory_tot,lmax,ncmax,lsmax,lsmin,kp0,kpm 
          END SUBROUTINE
      END INTERFACE


c.....Input Data


!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      integer*8 MVmodelong,direct

      integer*8 lmax,nNode,nTri,nEdge
      REAL   memory_tot,frequency
       REAL   xyznode(3,maxnode)
      integer*8 ipatpnt(3,maxpatch), iedge(4,maxpatch)
      integer*8 contourMap(maxedge)
c.....Output Data

       REAL   xyzctr(3,maxpatch), xyznorm(3,maxpatch),
     &        edge(maxedge),      paera(maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      integer*8 ngrid(maxrule)
      integer*8 lsmax, lsmin, kpm, kp0
      integer*8 igrs(lmax+2)
      integer*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      integer*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      integer*8 kpstart(lmax)
      integer*8 family(maxedge)
      integer*8 ncmax
      COMPLEX crhs(maxedge)
      integer*8 kk
      REAL iepscg
      REAL edgelong, edgeavrg, rlengmax
      
!       COMPLEX  Einc(3,maxnode),Hinc(3,maxnode)
cccccccccccccccccccccccccccccccccccccccccccc
c Added by Xiaochuan
			  maxnode=nNode
			  maxedge=nEdge
			  maxpatch=nTri	
			  ipol=1
			  imono=1
			  thetai1=0.0
			  thetai2=0.0
			  nthetai=1
			  phii1=0.0
			  phii2=0.0
			  nphii=1
			  thetas1=0.0
			  thetas2=0.0
			  nthetas=1
			  phis1=0.0
			  phis2=0.0
			  nphis=1
                          alpha=0.5!0.5  1.0: EFIE 0.0:MFIE
                          icgbcg=1
                          itmax=500
                          epscg=iepscg
                          match=1
                          irule_src=1
                          irule_fld=1

                          mvmode=MVmodelong
                          directrhs=direct
                          freq=frequency
                          wl0=2.997956377e-1/freq
                          maxrule  = 11
			  maxgrid  = 64			    
cccccccccccccccccccccccccccccccccccccccccccc



      call initial_geom( maxnode,maxpatch,maxedge,
     &                   xyznode,ipatpnt, iedge,          
     &                   xyzctr, xyznorm, edge, paera, rnear2)

      call initial_cnst( wl0, 
     &                   rk0, rk2d4, eta0, cnste, cnsth, distmin, ci)

      call initial_inte( maxrule, maxgrid, match,
     &                   irule_fld, irule_src, irulef_near, irules_near,
     &                   vt1, vt2, vt3, wt,
     &                   ngrid)

      call setbox( maxnode, maxpatch, maxedge,
     &             xyznode, ipatpnt, iedge,
     &             lmax, lsmax, lsmin, kpm, kp0,
     &             lxyz, modes, rmin, sizel, wl0, pi, rk0)

      call findncmax(maxnode, maxpatch, maxedge,
     &               xyznode, ipatpnt,  iedge,
     &               lmax, ncmax, lxyz, rmin, sizel, family)

      call bld_ncmax(ncmax) 
      call bld_ncmax_maxedge(ncmax,maxedge) 
      call bld_kpm(kpm) 
      call bld_ifar_lmax(ifar,lmax)

      memory_tot= memory_tot
     &          + ncmax*(4+4) + (ncmax+1)*4*3
     &          +(ncmax+maxedge)*4
     &          + kpm*(2*8 + 2*8)
     &          +(2*ifar+1)*(2*ifar+1)*(2*ifar+1)*4

      call checksize(maxnode, maxpatch, maxedge,
     &     xyznode, ipatpnt, iedge, wl0,
     &     edgelong, edgeavrg, rlengmax)

      call c_main_tree(memory_tot,lmax,ncmax, lsmax, lsmin, kp0
     &,kpm,contourMap,maxedge)

      return
      end
