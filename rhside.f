      subroutine RHSIDE
     &( ipol,thetai,phii, alpha,
     &  maxnode, maxpatch, maxedge, maxrule, maxgrid,
     &  match, irule_fld, 
     &  xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &  ngrid, vt1, vt2, vt3, wt,
     &  rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &  epsr, epsi, amur, amui,
     &  crhs, EincPhs, polAng)

      use mlfma_progctrl
      use mlfma_const

      IMPLICIT none

c.....Input Data     
      INTEGER*8 ipol,irule_fld_local
      INTEGER*8 maxnode, maxpatch, maxedge, maxrule, maxgrid
      INTEGER*8 match, irule_fld
      INTEGER*8 ipatpnt(3,maxpatch), ! The tri array, where each tri is described by 3 global nodeID  
     &          iedge(4,maxedge)     ! The edge array, where each edge is described by 2 nodeID and 2 triID
       REAL   xyznode(3,maxnode),    ! array containing node coord 
     &        xyzctr(3,maxpatch),    ! array containing tri center coord
     &        xyznorm(3,maxpatch),   ! array containing tri normal vtr
     &        edge(maxedge),         ! array containing edge length
     &        paera(maxpatch)        ! array containing tri area
       REAL   thetai, phii, alpha
      INTEGER*8 ngrid(maxrule)
       REAL   vt1(maxgrid,maxrule), vt2(maxgrid,maxrule), ! Gaussian quadrature points for the 3 tri vertices, vt1
     &        vt3(maxgrid,maxrule), wt(maxgrid,maxrule)   ! vt2 and vt3. 'wt' is the weighting constant
       REAL   rk0, freq, wl0, rk2d4, eta0  ! rk0 is the freespace wavenumber (2pi/wl0), rk2d4 = (2pi/wl0)^2/4
      COMPLEX cnste, cnsth, ci !cnste = 1.0/( i * omega * mu * 4 * pi ), cnsth = -0.25 * eta0/(4*pi), ci   = (0.0, -1.0)
       REAL epsr(2), epsi(2), amur(2), amui(2)
c      COMPLEX Einc(9,maxpatch),Hinc(9,maxpatch)
       REAL EincPhs, polAng

c.....Output Data
      COMPLEX crhs(maxedge)
      complex HH(2)

c.....Working Variables

      INTEGER*8 i, j, l, ie, i3,  n1, n2, n3,II(3)
      REAL    thesin, thecos, phisin, phicos
      REAL    rfld(3),rhofld(3), pole(3), polh(3), rki(3) 
      REAL    rhoxn(3), anorm(3),  pctr(3), ectr(3)
      REAL    dotmul,  signl
      COMPLEX ct, crhse, crhsh, ceta, cx, ceps, cmu
      REAL    tau, zeta;
c      COMPLEX Elocal(3),Hlocal(3),INC
c      Real   alphai
c      INTEGER*8 iii,ll
c      INTEGER*8 Numtri


      thesin=sin(thetai)
      thecos=cos(thetai)
      phisin=sin(phii)
      phicos=cos(phii)
      rki(1)=thesin*phicos
      rki(2)=thesin*phisin
      rki(3)=thecos

c      if(ipol.eq.1) then
c         pole(1)= thecos*phicos
c         pole(2)= thecos*phisin
c         pole(3)=-thesin
c      end if
c      if(ipol.eq.2) then
c         pole(1)= -phisin
c         pole(2)=  phicos
c         pole(3)=  0.0
c      end if
        tau=rad*polAng 
        
        pole(1)=cos(tau)*thecos*phicos-sin(tau)*phisin
        pole(2)=cos(tau)*thecos*phisin+sin(tau)*phicos
        pole(3)=-cos(tau)*thesin
    
	irule_fld_local=4 ! local field integration or quadrature rule
         ceps = cmplx( epsr(1), epsi(1) )
         cmu  = cmplx( amur(1), amui(1) )
         cx   = ci * rk0
         ceta = 120.0 * pi
         call xmul(rki,pole,polh)

         write(*,80)'Theta, Phi = ', thetai/rad,', ',phii/rad
         write(*,81)'Kinc   = ', -rki(1), ' ', -rki(2), ' ', -rki(3)
         write(*,81)'Einc   = ', pole(1),' ', pole(2),' ', pole(3)
         write(*,82)'Ephase = ', EincPhs

80       format(A, F8.3, A F8.3)
81       format(A, F8.5, A, F8.5, A, F8.5)
82       format(A, F8.3)

         zeta = rad*EincPhs 

         do 100 ie=1,maxedge

            n1 = iedge(1,ie)
            n2 = iedge(2,ie)

            call getedgectr( xyznode(1,n1), xyznode(1,n2), ectr )
            crhse = (0.0,0.0) ! complex rhs for E-field
            crhsh = (0.0,0.0) ! complex rhs for H-field

            do 10 l=1,2
        
               i3 = iedge(2+l,ie) !get gobal triID associated with this edge
               if (i3 .le. 0) then 
			goto 10 
		endif

               n3 = ipatpnt(1,i3)+ipatpnt(2,i3)+ipatpnt(3,i3)-n1-n2 ! the nodeID opposite this edge  
               signl = float(3-2*l) !signl = +1 for l=1, and -1 for l=2
           
               call getpatctr( xyzctr(1,i3), pctr )  ! copy tri center coord to pctr
               call mv1to2(xyznorm(1,i3), anorm, 3)  ! copy xyznorm to anorm

               do i = 1,ngrid(irule_fld_local)

                  do j = 1,3
                     rfld(j) = vt1(i,irule_fld_local) * xyznode(j,n1) +
     &                         vt2(i,irule_fld_local) * xyznode(j,n2) +
     &                         vt3(i,irule_fld_local) * xyznode(j,n3)
				
                     if(match.eq.1) then
                        rhofld(j) = signl * ( rfld(j) - xyznode(j,n3) )
                     else
                        rhofld(j) = signl * ( ectr(j) - pctr(j) )
                     end if
                  enddo

                  ct = wt(i, irule_fld_local)*exp(-cx*dotmul(rki,rfld))
     &                *exp(-cx*zeta)

                  call mv1to2(rhofld, rhoxn, 3)
                  crhse=crhse-ct*dotmul(rhoxn,pole)

                  call xmul( rhofld, anorm, rhoxn)  ! WXCL
                  crhsh=crhsh+ct*dotmul(rhoxn,polh)
               end do

 10         continue

            crhs(ie) = (alpha * crhse + (1 - alpha) * crhsh)
     &              * 0.5 * float(match)

            if(match.eq.1) then
              crhs(ie) = crhs(ie)*edge(ie)
            else
            endif

 100     continue
      RETURN
      END
