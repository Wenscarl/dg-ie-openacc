#ifndef _COMP_H_
#define _COMP_H_

#ifdef _OPENMP
#include <omp.h>
#else
inline int omp_get_max_threads() {return 1;}
inline int omp_get_num_threads() {return 1;}
inline int omp_get_thread_num() {return 0;}
#endif

#endif // _COMP_H_
