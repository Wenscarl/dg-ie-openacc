#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
// #define _buildarrays_C_   1
#include "array_pointer.h"
#include <stdio.h>
/*
 * dynamic memory allocation 
 */
/*
void bld_maxedge_(Integer *maxedge) {

    if (*maxedge != 0) {

     iedgep = (Integer *) malloc (4*(*maxedge) *sizeof(Integer));
     edgep  = (Real *) malloc ((*maxedge) *sizeof(Real));
     familyp  = (Integer *) malloc ((*maxedge) *sizeof(Integer));

     if (iedgep == NULL || edgep == NULL || familyp == NULL ) 
        { fprintf(stderr, "malloc fail for bld_maxedge \n");
         exit(1);
     }
    }
}



void bld_maxpatch_(Integer *maxpatch) {

    if (*maxpatch != 0) {

     ipatpntp = (Integer *) malloc (3* (*maxpatch) *sizeof(Integer));
	xyzctrp  = (Real *) malloc (3* (*maxpatch) *sizeof(Real));
     paerap   = (Real *) malloc (   (*maxpatch) *sizeof(Real));
	xyznormp = (Real *) malloc (3* (*maxpatch) *sizeof(Real));

     if (ipatpntp == NULL || xyzctrp == NULL || paerap == NULL
                          || xyznormp == NULL) { fprintf(stderr, 
           "malloc fail for bld_maxpatch \n");
         exit(1);
     }
    }
}


void bld_maxnode_(Integer *maxnode) {

    if (*maxnode != 0) {

	xyznodep = (Real *) malloc (3* (*maxnode) *sizeof(Real));

     if (xyznodep == NULL) { fprintf(stderr, 
           "malloc fail for bld_maxnode \n");
         exit(1);
     }
    }
}

void bld_maxrule_(Integer *maxrule) {

    if (*maxrule != 0) {

     ngridp  = (Integer *) malloc ((*maxrule) *sizeof(Integer));

     if (ngridp  == NULL) { fprintf(stderr, 
           "malloc fail for bld_maxrule \n");
         exit(1);
     }
    }
}

void bld_maxgrid_maxrule_(Integer *maxgrid, Integer *maxrule) {

    if (*maxgrid != 0) {

     vt1p  = (Real *) malloc ((*maxgrid)* (*maxrule) *sizeof(Real));
     vt2p  = (Real *) malloc ((*maxgrid)* (*maxrule) *sizeof(Real));
     vt3p  = (Real *) malloc ((*maxgrid)* (*maxrule) *sizeof(Real));
      wtp  = (Real *) malloc ((*maxgrid)* (*maxrule) *sizeof(Real));

     if (vt1p  == NULL || vt2p  == NULL || vt3p  == NULL ||
         wtp   == NULL ) { fprintf(stderr, 
           "malloc fail for bld_maxgrid_maxrule \n");
         exit(1);
     }
    }
}

void bld_lmax_(Integer *lmax) {

    if (*lmax != 0) {

     igrsp    = (Integer *) malloc (((*lmax)  + 2) *sizeof(Integer));
     lxyzp   = (Integer *) malloc (3*(1+(*lmax)) *sizeof(Integer));
     modesp  = (Integer *) malloc ((*lmax) *sizeof(Integer)); 
     sizelp  = (Real *) malloc (3*(1+(*lmax)) *sizeof(Real));
     lrsmupp  = (Integer *) malloc ((1+(*lmax)) *sizeof(Integer));
     lrsmdownp= (Integer *) malloc ((1+(*lmax)) *sizeof(Integer));
     lrstlp   = (Integer *) malloc ((1+(*lmax)) *sizeof(Integer));
     kpstartp   = (Integer *) malloc ((*lmax) *sizeof(Integer));

     if (igrsp  == NULL || lxyzp == NULL   || modesp == NULL    || 
         sizelp == NULL || lrsmupp == NULL || lrsmdownp == NULL || 
         lrstlp == NULL || kpstartp == NULL) 
     { fprintf(stderr, "malloc fail for bld_lmax \n");
         exit(1);
     }
    }
}

*/
void _bld_ncmax(Integer *ncmax) {

    if (*ncmax != 0) {
     printf("Sizeof Integer is %d\n",(int) sizeof(Integer));
     igcsp    = (Integer *) malloc (((*ncmax)    ) *sizeof(Integer));
     igallp   = (Integer *) malloc (((*ncmax) + 1) *sizeof(Integer));
     noselfp  = (Integer *) malloc((((*ncmax)+1)*2)*sizeof(Integer));
     igsndrsp = (Integer *) malloc  ((*ncmax)      *sizeof(Integer));

     if (igcsp  == NULL || igallp  == NULL || noselfp  == NULL
                        || igsndrsp == NULL) 
     { fprintf(stderr, "malloc fail for bld_ncmax \n");
         exit(1);
     }
    }
}


void _bld_ncmax_maxedge(Integer *ncmax, Integer *maxedge) {

    if (*maxedge != 0) {

     indexp   = (Integer *) malloc (((*ncmax) +(*maxedge)) *sizeof(Integer));

     if (indexp  == NULL ) { fprintf(stderr, 
         "malloc fail for bld_ncmax_maxedge \n");
         exit(1);
     }
    }
}


void _bld_kpm(Integer *kpm) {

    if (*kpm != 0) {

     cvlrp  = (Complex *) malloc (2*(*kpm) *sizeof(Complex));
     cgmp   = (Complex *) malloc (2*(*kpm) *sizeof(Complex));    


     if (cvlrp  == NULL || cgmp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_kpm \n");
         exit(1);
     }
    }
}

void _bld_nkpm(Integer *nkpm) {

    if (*nkpm != 0) {

     irsap   = (Integer *) malloc (   (*nkpm)  *sizeof(Integer)); 
     c_shiftp = (Complex *) malloc ((8*(*nkpm)) *sizeof(Complex));


     if (irsap  == NULL || c_shiftp == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_nkpm \n");
         exit(1);
     }
    }
}

void _bld_ntlm(Integer *ntlm) {

    if (*ntlm != 0) {

     ctlp     = (Complex *) malloc ((*ntlm) *sizeof(Complex));

     if (ctlp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_ntlm \n");
         exit(1);
     }
    }
}


void _bld_niplm(Integer *niplm) {

    if (*niplm != 0) {

     icsap   = (Integer *) malloc ((*niplm)   *sizeof(Integer));
     arrayp  = (Real *) malloc ((*niplm)   *sizeof(Real));

     if (icsap  == NULL || arrayp == NULL) 
     { fprintf(stderr, "malloc fail for bld_niplm \n");
         exit(1);
     }
    }
}


void _bld_nsm(Integer *nsm) {

    if (*nsm != 0) {

     csmp  = (Complex *) malloc (2 * (*nsm) *sizeof(Complex));

     if (csmp  == NULL )
         { fprintf(stderr, 
           "malloc fail for bld_nsm \n");
         exit(1);
     }
    }
}


void _bld_ifar_lmax(Integer *ifar, Integer *lmax) {

    if (*lmax != 0) {

     int q1, q2;
     q1 = 2*(2*(*ifar)+1) + 1;
     q2 = q1 * q1 * q1 * (*lmax);

     indextlp = (Integer *) malloc (q2 *sizeof(Integer));

     if (indextlp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_ifar_lmax \n");
         exit(1);
     }
    }
}

void _bld_ngsndm(Integer *ngsndm) {

    if (*ngsndm != 0) { 

     igsndcsp = (Integer *) malloc ((*ngsndm) *sizeof(Integer));

     if (igsndrsp  == NULL || igsndcsp  == NULL)
         { fprintf(stderr, 
           "malloc fail for bld_ncmax_ngsndm \n");
         exit(1);
     }
    }
}


