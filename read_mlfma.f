      subroutine read_mlfma
     &(freq, ipol, imono,
     & thetai1, thetai2, nthetai, phii1, phii2, nphii,
     & thetas1, thetas2, nthetas, phis1, phis2, nphis,
     & alpha, match, irule_src, irule_fld,
     & icgbcg, itmax, epscg)

      implicit none

      INTERFACE
      SUBROUTINE fstrcat (s1, s2, scomb) 
!DEC$ ATTRIBUTES C,ALIAS:'_fstrcat':: fstrcat
      character*256 s1,s2,scomb
!DEC$ ATTRIBUTES REFERENCE :: s1,s2,scomb
          END SUBROUTINE
      END INTERFACE

      INTEGER*8 ipol, imono
       REAL   freq
       REAL   thetai1, thetai2, nthetai, phii1, phii2, nphii
       REAL   thetas1, thetas2, nthetas, phis1, phis2, nphis
       REAL   alpha
      INTEGER*8 icgbcg, itmax
       REAL   epscg
      INTEGER*8 match, irule_src, irule_fld

      character*256 runname, tailname, scomb
      INTEGER*8 nargs, iargc

c.....get commend line runname

      nargs = iargc ()    ! read the command line argument
      if(nargs.ne.1) then
        write(6,*)" USAGE: mlfma.x Runname"
        write(6,*)" Then mlfma will look for input file"
        write(6,*)"    Runname.mlfma_inp"
        write(6,*)" to start the computation"
        stop
      endif
      call getarg(1, runname)    ! retrieve command-line argument

      tailname = '.mlfma_inp'

      call fstrcat (runname, tailname, scomb)
      
ccccccccccccccccccccccccccccccccccccccccc
c......A --- CAD, FREQ and ANGLES.......c
ccccccccccccccccccccccccccccccccccccccccc

      open (1, file = scomb, status = 'old', err = 200)

      read(1,*)
      read(1,*) freq
      read(1,*)
      read(1,*) ipol
      read(1,*) imono

      read(1,*)
      read(1,*) thetai1
      read(1,*) thetai2
      read(1,*) nthetai
      read(1,*) phii1
      read(1,*) phii2
      read(1,*) nphii 

      read(1,*)
      read(1,*) thetas1
      read(1,*) thetas2
      read(1,*) nthetas 
      read(1,*) phis1
      read(1,*) phis2
      read(1,*) nphis

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c......for bistatic: do just 1st incident angle to avoid confusion......c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      if (imono .eq. 2) then
         nthetai=1 
         nphii=1      
      endif

ccccccccccccccccccccccccccc
c...... EM SETTING .......c
ccccccccccccccccccccccccccc

c      read(1,*) 
c      read(1,*) alpha


c      read(1,*) 
c      read(1,*) icgbcg
c      read(1,*) itmax
c      read(1,*) epscg

      alpha  = 1.0
      icgbcg = 1
      itmax  = 100
      epscg  = 0.001

      match=1
      irule_fld=1
      irule_src=1
	
      close(1)

      tailname='.mommax'
      call fstrcat(runname,tailname,scomb) 
      open(1,file=scomb,status='unknown')

      tailname='.xyz'
      call fstrcat(runname,tailname,scomb) 
      open(3,file=scomb,status='unknown')

      tailname='.ipat'
      call fstrcat(runname,tailname,scomb) 
      open(5,file=scomb,status='unknown')

      tailname='.edge'
      call fstrcat(runname,tailname,scomb) 
      open(4,file=scomb,status='unknown')

      tailname='.rcs'
      call fstrcat(runname,tailname,scomb) 
      open(10,file=scomb,status='unknown')

      tailname='.fld'
      call fstrcat(runname,tailname,scomb) 
      open(11,file=scomb,status='unknown')
			  
      
      return

c.....Input error termination

200   write (6, *) 'MLFMA input error'
      write (6, *) 'Missing input file ', scomb
      stop


      end
