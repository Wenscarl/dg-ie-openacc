      subroutine norm3d(r1,r2,r3,an)
      implicit none
      real r1(3), r2(3), r3(3), an(3), dotmul
      real r12(3), r13(3),tmp
      call vctadd( r2, r1, r12, -1 )
      call vctadd( r3, r1, r13, -1 )
      call xmul( r12, r13, an )
      tmp=sqrt( dotmul(an,an) )
      call scale( an, tmp )
      return
      end
