#ifndef DD_RBTREENODE_H
#define DD_RBTREENODE_H
#include <iostream>
#include <stdlib.h>
enum RB_TYPE{RB_BLACK,RB_RED} ;

typedef long long int Int;

template<class NODETYPE>
class TreeNode {
public: 
  NODETYPE data;
  RB_TYPE color;
  TreeNode(const NODETYPE &); // constructor
  NODETYPE getData() const; // return data
  NODETYPE *getDataPtr(); // return data Pointer

  TreeNode *leftPtr; // pointer to left subtree
  TreeNode *rightPtr; // pointer to right subtree  
  TreeNode * parent;
  TreeNode * getUncle()const ;
};


// constructor
template<class NODETYPE>
TreeNode<NODETYPE>::TreeNode(const NODETYPE &d)
{
  data = d;
  leftPtr = rightPtr = NULL;
  color=RB_RED;
  parent=NULL;
}

// Return a copy of the data value
template<class NODETYPE>
NODETYPE TreeNode<NODETYPE>::getData() const { return data; }

// Return a copy of the data value
template<class NODETYPE>
NODETYPE *TreeNode<NODETYPE>::getDataPtr() { return &data; }

template<class NODETYPE> TreeNode<NODETYPE>* TreeNode<NODETYPE>::getUncle()const {
	if (parent==NULL ||parent->parent==NULL)
		return NULL;
	if(parent->parent->leftPtr== parent)
		return parent->parent->rightPtr;
	else if(parent->parent->rightPtr== parent)
		return parent->parent->leftPtr;
	else {
		std::cerr<<"Error! the RB tree is not built correctly!\n";
		exit(-1);
	}
}
#endif
