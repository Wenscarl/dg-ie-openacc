      subroutine cacxnear(cx, cy, job,
     &                    maxedge, lmax, ncmax, nearm,
     &                    lxyz, ifar, dfar, 
     &                    igall, igcs, igrs, noself, canear,
     &                    ngnearm, igblkrs, igblkcs, igblk,
     &                    mfinestm, cfinest)

c   cy=cy+a(cx)
c   contributions from the near neighbor cubes

      implicit none

      INTEGER*8 job,mnt
      INTEGER*8 maxedge, lmax, ncmax, nearm, mfinestm
      INTEGER*8 lxyz(3,0:lmax), ifar
      real    dfar
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 noself(2,ncmax+1)  
      complex cy(maxedge), cx(maxedge)
      complex canear(nearm*maxedge), cfinest(mfinestm)
      INTEGER*8 ngnearm, igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)

      INTEGER*8 ipgp, ngnear, jlo, ipnear, mself, nself, indexm, indexn

	mnt=maxedge*nearm

      if(job.eq.0) then
c                                     cy=cy+a(cx)
c       write(*,*) 'start Near field coupling'
!$OMP PARALLEL DO PRIVATE(mself,indexm,ngnear,jlo,nself,indexn,ipnear)
         do ipgp = igrs(1), igrs(2)-1
            mself = noself(1,ipgp)
            indexm = igall(ipgp)
            do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
               jlo = igblkcs(ngnear)
               nself = noself(1,jlo)
               indexn = igall(jlo)
               ipnear = igblk(ngnear)


            call camcxrect_t(nself, nself, mself, canear(ipnear),
     &                       cx(indexn), cy(indexm))

 

            enddo
         enddo
!$OMP END PARALLEL DO

      else
c                                    cy=cy+transpose(a)(cx)
c       write(*,*) 'start Near field T coupling '
!$OMP PARALLEL DO PRIVATE(mself,indexm,ngnear,jlo,nself,indexn,
!$OMP& ipnear, cfinest)

         do ipgp = igrs(1), igrs(2)-1
            mself = noself(1,ipgp)
            indexm = igall(ipgp)
            do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
               jlo = igblkcs(ngnear)
               nself = noself(1,jlo)
               indexn = igall(jlo)
               ipnear = igblk(ngnear)

            call camcxrect(nself, nself, mself, canear(ipnear),
     &                     cx(indexm), cy(indexn), cfinest)
 
            enddo
         enddo
!$OMP END PARALLEL DO

      endif
      return
      end

      subroutine camcxrect(m, max, n, ca, cx, cy, ctemp)

c cy = cy + ca*cx

      implicit none
      INTEGER*8 m, max, n
      complex ca(max,n), cx(n), cy(m), ctemp(m)

      INTEGER*8 i, j
      do j = 1, m
         ctemp(j) = (0.0, 0.0)
      enddo
      do i = 1, n
         do j = 1, m
            ctemp(j) = ctemp(j) + ca(j,i)*cx(i)
         enddo
      enddo
c!$OMP CRITICAL
      do j = 1, m
         cy(j) = cy(j) + ctemp(j)
      enddo
c!$OMP END CRITICAL
      return
      end
         

      subroutine camcxrect_t(m, max, n, ca, cx, cy)

c      cy = cy + transpose(ca)*cx

      implicit none
      INTEGER*8 m, max, n
      complex ca(max,n), cx(m), cy(n)

      INTEGER*8 i, j
      complex ctemp
      do i = 1, n
         ctemp = (0.0, 0.0)
         do j = 1, m
            ctemp = ctemp + ca(j,i)*cx(j)
         enddo
         cy(i) = cy(i) + ctemp
      enddo
      return
      end


