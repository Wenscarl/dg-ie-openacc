cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c.....calculate the self and nearest terms, and second nearest terms,....c
c.....which are storaged in canear by bsr (block sparse row format)......c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine fillamn
     &(xyznode,ipatpnt,iedge,xyzctr, xyznorm, edge,paera,
     & ngrid, vt1, vt2, vt3, wt,
     & lmax, ncmax, nearm,
     & lxyz,
     & igall, igcs, igrs, index, family, noself,
     & canear, ipvt,
     & ngnearm, igblkrs, igblkcs, igblk,
     & epsr,epsi,amur,amui,contourMap)

      use mlfma_input
      use mlfma_const
      use mlfma_param
      use mlfma_progctrl
      implicit none


!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'
       INTEGER*8 contourMap(maxedge)
      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      INTEGER*8 lmax, ncmax, nearm, ngnearm
      INTEGER*8 lxyz(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 noself(2,ncmax+1)  
      COMPLEX canear(nearm*maxedge)
      INTEGER*8 ipvt(maxedge)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)
       REAL   epsr(2),epsi(2),amur(2),amui(2)

      INTEGER*8 ipointer
      INTEGER*8 l,ipself,ipnear,m,iglp,igp(3),ix,iy,iz,ig(3),ignear,
     &        noempty,jlo,n,igl,mself,i,j,ipgp,
     &        igt,igtp, ngnear, mn, nself, indexm, indexn

      INTEGER*8 ii,Numnear
 
      COMPLEX ck(2),ceps(2),cmu(2)
      COMPLEX ceta(2),ce(2),ch(2)

cccccccccccccccccccccccccccccccccccccccccccccccccc
c......Prepare for calculating interaction.......c
c............between near elements...............c
cccccccccccccccccccccccccccccccccccccccccccccccccc
       total=0

 	do ii=1,2
	   ceps(ii)=cmplx(epsr(ii),epsi(ii))
	   cmu(ii) =cmplx(amur(ii),amui(ii))
	   ck(ii)=rk0*sqrt(ceps(ii)*cmu(ii))!*(1.0,0.01)
			   !:WXC changed 
	   ceta(ii)=eta0*sqrt(cmu(ii)/ceps(ii))
	   ce(ii)=ci*ck(ii)*ceta(ii)
	   ch(ii)=(eta0*eta0)*ci*ck(ii)/ceta(ii)
	enddo

	ipself=1


ccccccccccccccccccccccccccccccccccccccccc
c.....Find near interaction element.....c
c.....not belong to precondition part...c
ccccccccccccccccccccccccccccccccccccccccc

      l=lmax
      ipointer=igrs(1)
      noempty=igrs(2)-igrs(1)
      ipnear = ipself
      ngnear = 0

	

      do 1000 ipgp=igrs(1),igrs(2)-1
         igblkrs(ipgp) = ngnear + 1
         igt=igcs(ipgp)
         call igxyz(igt,lxyz(1,l),igp)
         igtp=iglp(igt,lxyz(1,l),lxyz(1,l-1))
         mself=noself(1,ipgp)
         jlo=1
         do ix=max(1,igp(1)-ifar),min(lxyz(1,l),igp(1)+ifar)
            ig(1)=ix
         do iy=max(1,igp(2)-ifar),min(lxyz(2,l),igp(2)+ifar)
            ig(2)=iy
         do iz=max(1,igp(3)-ifar),min(lxyz(3,l),igp(3)+ifar)
            ig(3)=iz
            ignear=igl(lxyz(1,l),ig)

cccccccccccccccccccccccccccccccccccccccccc
c...is this belong to near neighbors?....c
cccccccccccccccccccccccccccccccccccccccccc

            if(sqrt(real((ix-igp(1))**2+(iy-igp(2))**2
     &                  +(iz-igp(3))**2)).lt.dfar) then

cccccccccccccccccccccccccccccccccccccccc
c.....is this box empty or not?........c
cccccccccccccccccccccccccccccccccccccccc

               call huntint(igcs(ipointer),noempty,ignear,jlo)
               if(jlo.gt.0) then
                  ngnear = ngnear + 1
                  igblk(ngnear) = ipnear
                  igblkcs(ngnear) = jlo
                  ipnear=ipnear+mself*noself(1,ipointer+jlo-1)
               endif
            endif
         enddo
         enddo
         enddo
1000  continue

ccccccccccccccccccccccccccccccccccccccccccccccccccc
c....Calculate the near interaction entries.......c
c....not belong to precondition part..............c
ccccccccccccccccccccccccccccccccccccccccccccccccccc

      igblkrs(igrs(2)) = ngnear + 1
!      igblk(ngnear+1)=ipnear-1
!       write (6,*) 'Begin fill'
!        call openfile();
! 
! !$OMP PARALLEL PRIVATE(mself, indexm, ngnear, jlo,
! !$OMP& nself, indexn, ipnear, i, m, j, n, mn)
! !$OMP DO SCHEDULE(GUIDED)
! 	do ipgp = igrs(1), igrs(2)-1
!          mself = noself(1,ipgp)
!          indexm = igall(ipgp) - 1
!          do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
!             jlo = igblkcs(ngnear)
!             nself = noself(1,jlo)
!             indexn = igall(jlo) - 1
!             ipnear = igblk(ngnear)
! 
!             do i = 1, mself
!                m = indexm + i
!                do j = 1, nself
!                   n = indexn + j
!                   mn = ipnear + (i-1)*nself + j-1
!                   call camnone(xyznode,ipatpnt,iedge,
!      &                         xyzctr, xyznorm, edge,paera,  
!      &                         ngrid, vt1, vt2, vt3, wt,
!      &                         ceps,cmu,ck,ceta,ce,ch,
!      &                         index(m), index(n), canear(mn),
!      &                         contourMap)
! 
! !	       write(7,*) index(m), index(n), canear(mn)
!                enddo !do j = 1, nself
!             enddo !do i = 1, mself
!          enddo !ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
!          call assm_progress(igrs)
!       enddo !do ipgp = igrs(1), igrs(2)-1
! !$OMP END DO
! !$OMP END PARALLEL	
! 
!       write (6,*) 'End fill'

        write (6,*) 'Begin fill sparser part of Amn'
!$OMP PARALLEL PRIVATE(mself, indexm, ngnear, jlo,
!$OMP& nself, indexn, ipnear, i, m, j, n, mn)
!$OMP DO SCHEDULE(GUIDED)

	 do ipgp = igrs(1), igrs(2)-1
         mself = noself(1,ipgp)
         indexm = igall(ipgp) - 1
         do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
            jlo = igblkcs(ngnear)
            nself = noself(1,jlo)
            indexn = igall(jlo) - 1
            ipnear = igblk(ngnear)

            Numnear = mself*nself
            if (Numnear.le.8600) then

            do i = 1, mself
               m = indexm + i
               do j = 1, nself
                  n = indexn + j
                  mn = ipnear + (i-1)*nself + j-1
                  call camnone(xyznode,ipatpnt,iedge,
     &                         xyzctr, xyznorm, edge,paera,  
     &                         ngrid, vt1, vt2, vt3, wt,
     &                         ceps,cmu,ck,ceta,ce,ch,
     &                         index(m), index(n), canear(mn),
     &                         contourMap)

               enddo
            enddo
          end if

         enddo
        call assm_progress(igrs)
      enddo
!$OMP END DO
!$OMP END PARALLEL


         write (6,*) 'Begin fill denser part of Amn'

	 do ipgp = igrs(1), igrs(2)-1
         mself = noself(1,ipgp)
         indexm = igall(ipgp) - 1
         do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
            jlo = igblkcs(ngnear)
            nself = noself(1,jlo)
            indexn = igall(jlo) - 1
            ipnear = igblk(ngnear)

            Numnear = mself*nself
            if (Numnear.gt.8600) then
!$OMP PARALLEL PRIVATE( i, m, j, n, mn)
!$OMP DO SCHEDULE(GUIDED)

            do i = 1, mself
               m = indexm + i
               do j = 1, nself
                  n = indexn + j
                  mn = ipnear + (i-1)*nself + j-1
                  call camnone(xyznode,ipatpnt,iedge,
     &                         xyzctr, xyznorm, edge,paera,  
     &                         ngrid, vt1, vt2, vt3, wt,
     &                         ceps,cmu,ck,ceta,ce,ch,
     &                         index(m), index(n), canear(mn),
     &                         contourMap)

               enddo
            enddo
!$OMP END DO
!$OMP END PARALLEL

          end if

         enddo
        ! call assm_progress(igrs)
      enddo


c.....write (6,*) 'End fill'

      return
      end



