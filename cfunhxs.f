      function CFUNHXS
     &( rhoxn, rho0, rhod, rhoxd, ds, ri1, ri1p, ri3, ri3p, ck2d2 )

      IMPLICIT NONE

c.....Input Data

       REAL rhoxn(3), rho0(3), rhod(3), rhoxd(3), ds, 
     &      ri1, ri1p(3), ri3, ri3p(3)
       COMPLEX ck2d2

c.....Output 

      COMPLEX cfunhxs

c.....Working Variables

      COMPLEX cmp1(3), cmp(3)
      INTEGER*8 i

c....................................................................


      cfunhxs=(0.0, 0.0)

      do i=1,3
         cmp1(i)=ck2d2*ri1p(i)+ri3p(i)
      enddo

      cmp(1)=rhod(2)*cmp1(3)-rhod(3)*cmp1(2)
      cmp(2)=rhod(3)*cmp1(1)-rhod(1)*cmp1(3)
      cmp(3)=rhod(1)*cmp1(2)-rhod(2)*cmp1(1)

      do i=1,3
         cmp(i)=cmp(i)-rhoxd(i)*(ck2d2*ri1*ds+ri3)
         cfunhxs=cfunhxs-rhoxn(i)*cmp(i)
      enddo

      RETURN
      END

