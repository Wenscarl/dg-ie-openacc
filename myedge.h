#ifndef MYEDGE_H
#define MYEDGE_H

typedef long long int Int;

class Edge
{
	public:
	Int id;
	Int n1,n2;
	Int tri1, tri2;
	Edge(){id=0;n1=0;n2=0;tri1=0;tri2=0;}
	Edge(Int a,Int b);
	const Edge& operator =(const Edge& right){id=right.id;n1=right.n1;n2=right.n2; return *this;}
	bool operator ==(const Edge& right)const ;
	bool operator <(const Edge& right)const ;
	bool operator >(const Edge& right)const ;
	Int getcnt()const{return id;}
};

#endif
