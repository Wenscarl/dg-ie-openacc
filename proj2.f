      subroutine PROJ2
     &( r, r1, r2, r3, r30, bn, aera, wl0,
     &  ds, r0,rho0,rhod,rhoxd,param_h,param_a,param_b)

      IMPLICIT NONE
      
c.....Input Data

       REAL r(3), r1(3), r2(3), r3(3), r30(3), bn(3), aera, wl0

c.....Output Data

       REAL ds, r0(3), rho0(3), rhod(3), rhoxd(3), 
     &      param_h(3),param_a,param_b(3)

c.....Working Variables
      Real s(3),d0(3),bn(3),zerod
	Real ds,tmp(3)
      INTEGER*8 iv, i
       Real tmpV1(3),tmpV2(3),tmpV3(3)
c.......................................................................

!       call norm3d( r1,r2,r3,bn ) !obtain the normal vector in the triangle

      zerod = wl0 * 1e-20
      call vctadd( r, r1, tmp, -1 )  ! tmp= r-r1
      ds = dotmul( tmp, bn )         ! ds= dot( (r-r1), normal)
      d0(1) =  bn(1)*ds              ! vec{d0}= vec{bn} * ds
      d0(2) =  bn(2)*ds
      d0(3) =  bn(3)*ds
      call vctadd( r,    d0,  r0,   -1 )
      call vctadd( r0,   r30, rho0, -1 )
      call vctadd( rho0, d0,  rhod,  1 )
	
	!calculate h
      !h1
	call vctadd( r2, r1,tmpV1,-1)
	call vctadd( r3, r1,tmpV2,-1)
	call xmul(tmpV1,tmpV2,tmpV3)
	tmp1= dotmul(tmpV3,bn)
	call vctadd( r3, r2,tmpV1,-1)
	s(1)=sqrt(dotmul(tmpV1,tmpV1))
	param_h(1)= tmp1/s(1)
	!h2
	call vctadd( r3, r0,tmpV1,-1)
	call vctadd( r1, r0,tmpV2,-1)
	call xmul(tmpV1,tmpV2,tmpV3)
	tmp1= dotmul(tmpV3,bn)
	call vctadd( r3, r1,tmpV1,-1)
	s(2)=sqrt(dotmul(tmpV1,tmpV1))
	param_h(2)= tmp1/s(2)
      !h3
	call vctadd( r1, r0,tmpV1,-1)
	call vctadd( r2, r0,tmpV2,-1)
	call xmul(tmpV1,tmpV2,tmpV3)
	tmp1= dotmul(tmpV3,bn)
	call vctadd( r1, r2,tmpV1,-1)
	s(3)=sqrt(dotmul(tmpV1,tmpV1))
	param_h(3)= tmp1/s(3)

	call vctadd(r0,r1,tmpV1,-1)
	call vctadd(r2,r1,tmpV2,-1)
	tmp1=dotmul(tmpV1,tmpV2)
	param_a(1)=tmp1/s(3)

	call vctadd(r0,r2,tmpV1,-1)
	call vctadd(r3,r2,tmpV2,-1)
	tmp1=dotmul(tmpV1,tmpV2)
	param_a(2)=tmp1/s(1)

	call vctadd(r0,r3,tmpV1,-1)
	call vctadd(r1,r3,tmpV2,-1)
	tmp1=dotmul(tmpV1,tmpV2)
	param_a(3)=tmp1/s(2)


	call vctadd(r0,r1,tmpV1,-1)
	call vctadd(r3,r1,tmpV2,-1)
	tmp1=dotmul(tmpV1,tmpV2)
	param_b(1)=tmp1/s(2)

	call vctadd(r0,r2,tmpV1,-1)
	call vctadd(r1,r2,tmpV2,-1)
	tmp1=dotmul(tmpV1,tmpV2)
	param_b(2)=tmp1/s(3)

	call vctadd(r0,r3,tmpV1,-1)
	call vctadd(r2,r3,tmpV2,-1)
	tmp1=dotmul(tmpV1,tmpV2)
	param_b(3)=tmp1/s(1)

      return
      end
