      subroutine main_tree( memory_tot, 
     &                      lmax, ncmax, lsmax, lsmin, kp0, kpm,
     &                      iedge, edge, ipatpnt, xyzctr, paera, 
     &                      xyznorm,xyznode,ngrid, vt1, vt2, vt3, 
     &                      wt, igrs, lxyz, modes, sizel,lrsmup, 
     &                      lrsmdown, lrstl, igall,igcs, family, 
     &                      index,cvlr, cgm, indextl, noself, 
     &                      kpstart,crhs1,contourMap)

      use mlfma_input
      use mlfma_const
      use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      INTERFACE
      SUBROUTINE bld_nkpm(nkpm)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_nkpm':: bld_nkpm
      integer*8 nkpm
!DEC$ ATTRIBUTES REFERENCE ::nkpm 
      END SUBROUTINE

      SUBROUTINE bld_niplm(niplm)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_niplm':: bld_niplm
      integer*8 niplm
!DEC$ ATTRIBUTES REFERENCE ::niplm 
      END SUBROUTINE

      SUBROUTINE bld_ntlm(ntlm)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_ntlm':: bld_ntlm
      integer*8 ntlm
!DEC$ ATTRIBUTES REFERENCE ::ntlm 
      END SUBROUTINE

      SUBROUTINE bld_ngsndm(ngsndm)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_ngsndm':: bld_ngsndm
      integer*8 ngsndm
!DEC$ ATTRIBUTES REFERENCE ::ngsndm 
      END SUBROUTINE

      SUBROUTINE bld_nsm(nsm)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_nsm':: bld_nsm
      integer*8 nsm 
!DEC$ ATTRIBUTES REFERENCE ::nsm 
      END SUBROUTINE

      SUBROUTINE c_main_cal(maxnode, maxpatch, maxedge, maxrule, 
     &                 maxgrid,memory_tot, 
     &                 lmax, ncmax, lsmax, lsmin, nearm, kp0, 
     &                 kpm, nkpm, ngsndm, niplm, nsm, ntlm, 
     &                 ngnearm, mfinestm, nanglem,crhs1,contourMap)
!DEC$ ATTRIBUTES C,ALIAS:'_c_main_cal':: c_main_cal
      integer*8 maxnode,maxpatch,maxedge,maxrule,maxgrid
      real    memory_tot
      integer*8 lmax, ncmax, lsmax, lsmin, nearm, kp0
      integer*8 kpm, nkpm, ngsndm, niplm, nsm, ntlm
      integer*8 ngnearm, mfinestm, nanglem,contourMap(maxedge)	
       complex crhs1(maxedge)
!DEC$ ATTRIBUTES REFERENCE ::maxnode,maxpatch,maxedge,maxrule,maxgrid
!DEC$ ATTRIBUTES REFERENCE ::memory_tot
!DEC$ ATTRIBUTES REFERENCE ::lmax, ncmax, lsmax, lsmin, nearm, kp0
!DEC$ ATTRIBUTES REFERENCE ::kpm, nkpm, ngsndm, niplm, nsm, ntlm
!DEC$ ATTRIBUTES REFERENCE ::ngnearm, mfinestm, nanglem 
      END SUBROUTINE
      END INTERFACE

c.....Input Data

       REAL   memory_tot
      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      INTEGER*8 lmax, lsmax, lsmin, ncmax, kp0, kpm
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 contourMap(maxedge)
c.....Output Data

      INTEGER*8 nsm, ntlm, nkpm, niplm, nanglem, nearm, ngnearm, 
     &        mfinestm, ngsndm  
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 noself(2,ncmax+1)
      COMPLEX Einc(9,maxpatch),Hinc(9,maxpatch)
      INTEGER*8 itest
      COMPLEX crhs1(maxedge)
      itest = 0

      call maketree(maxnode, maxpatch, maxedge,
     &              xyznode, ipatpnt, iedge,
     &              lmax, lsmax, lsmin, ncmax, kpm, nsm,
     &              lxyz, modes, rmin, sizel,
     &              igall, igcs, igrs, index, family, 
     &              lrsmup, lrsmdown)

       if (nsm .le. 0) nsm = 1

      call findnear(maxedge,
     &              lmax, ncmax, nearm, ngnearm, mfinestm,
     &              lxyz, ifar, dfar,
     &              igall, igcs, igrs, index, family, noself)

      if (ngnearm .le. 0) ngnearm = 1

      if(lmax.gt.1) then 

         call findntlm(lmax, lsmax, lsmin, ncmax, kpm, ntlm, 
     &                 nkpm,lxyz, modes, sizel, ifar,
     &                 dfar, kpstart)

         call findngsndm(lmax, ncmax, lxyz, ifar, dfar,
     &                   sizel, igcs, igrs, ngsndm)
      else

         ntlm = 1
         nkpm = 1
         ngsndm = 1

      endif

      niplm = nkpm * 16

      write(6,444) mvmode
      write(6,445)
      write(6,*)'   maxrule=',maxrule, '  maxgrid  =',maxgrid 
      write(6,*)'   lmax   =',lmax,    '  ncmax    =',ncmax
      write(6,*)'   lsmax  =',lsmax,   '  lsmin    =',lsmin
      write(6,*)'   kpm    =',kpm,     '  kp0      =',kp0
      write(6,*)'   ntlm   =',ntlm,    '  nsm      =',nsm
      write(6,*)'   nkpm   =',nkpm,    '  niplm    =',niplm
      write(6,*)'   ngnearm=',ngnearm, '  ngsndm   =',ngsndm
      write(6,*)'   nearm  =',nearm,   '  mfinestm =',mfinestm

444   format('MV mode = ',i1)
445   format('Array dimension parameters:')

      call bld_nkpm(nkpm)
      call bld_niplm(niplm)
      call bld_ntlm(ntlm)
      call bld_ngsndm(ngsndm)
      call bld_nsm(nsm)

      memory_tot = memory_tot 
     &           + nkpm*(4 + 8*8)
     &           + niplm*(4 + 4)
     &           + ntlm*8
     &           + ngsndm*4
     &           + nsm*(2*8)


!       call flush()
      if (ipol .eq. 3) then
         if (imono .eq. 1) then
            nanglem = nthetai*nphii
         elseif( imono .eq. 2) then
            nanglem = nthetas*nphis
         else
            nanglem = nthetas*nphis*nthetai*nphii
         endif
      else
         nanglem = 1
      endif

      memory_tot = memory_tot
     &           + float(maxedge)*(4 + 8 + 8 + 8*8 )
     &           + float(maxedge)*2*kp0*8*2 
     &           + float(maxedge)*( nearm*8) 
     &           + mfinestm*8
     &           + (ncmax+1)*4 + 2*ngnearm*4
     &           + 2*lsmax*lmax*4 + 9*nkpm*4
     &           + 2*nanglem*8 + 2*nanglem*4

      memory_tot = memory_tot*1e-6

      call  c_main_cal(maxnode, maxpatch, maxedge, maxrule,  
     &                 maxgrid,memory_tot, 
     &                 lmax, ncmax, lsmax, lsmin, nearm, kp0, 
     &                 kpm, nkpm, ngsndm, niplm, nsm, ntlm, 
     &                 ngnearm, mfinestm, nanglem,crhs1,
     &                 contourMap)


      return
      end
