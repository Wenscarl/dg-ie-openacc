CC= pgcc
CXX= pgc++
F77= pgf90
MKLPATH=/opt/intel/mkl/10.0.3.020/lib/em64t
TIMESTAMP=`date`
GDB=1
ifeq ($(GDB),0)
EXE=SR-JCFIEDG-AS
OMP=#-fopenmp
OPT=-O3 $(OMP)
else 
OPT=-g
EXE=SR-JCFIEDG-AS_GDB
OMP=
endif
LD=$(CXX)
CFLAGS=$(OPT) -I./include  
CXXFLAGS=$(OPT) -I./include -I./xmesh 
#-D_DEBUG

OpenBLASDIR = /home/cem/Shu_code/Softwares/Install/openblas/
#LDFLAG=-L. -lmlfma -L./library -lxmeshlz -lgfortran -L$(OpenBLASDIR)/lib -lopenblas -lgfortran  $(OMP)
#LDFLAG= $(OMP) -L./library -L./ -lxmeshlz -L/home/cem/Shu_code/Softwares/lapack-3.8.0 -llapack -lblas -lpgf90 -lpgf90_rpm1 -lpgf902 -lpgf90rtl -lpgftnrtl -lnspgc -lpgc -lrt -lm 
LDFLAG= $(OMP) -L./library -L./ -lxmeshlz -L/home/cem/Shu_code/Softwares/lapack-3.8.0 -llapack -lblas -pgf90libs -lnspgc -lpgc #-lrt -lm 

SRC= main.cc myedge.cc utility.cc geometry.cc



SRCF77 = mlfma_param.f mlfma_input.f mlfma_const.f \
mlfma_progctrl.f\
aera3.f cfunexs.f dsbesj.f fillvf.f getnorm.f igxyz.f \
main_tree.f proj.f setcoef.f anterpl.f cfunhx.f dsbesjh.f \
fillvfs.f getpatctr.f initial_cnst.f maketree.f read_geom.f \
setrule.f bicgm2.f cfunhxs.f dsbesy.f fillvs.f getr.f \
initial_geom.f sort1.f cacx.f cgm2.f farfld.f \
findncmax.f hunt.f initial_inte.f  rhside.f sort2.f cacxmp.f\
checksize.f fillamn.f findnear.f huntint.f interpl.f  rlength.f\
time2screen.f cacxnear.f cnst_func.f fillarray.f findngsndm.f \
igetn3.f main_box.f mtxele.f mtxele_e.f rnorm.f trans.f cacxpc.f dgauleg.f \
fillb16.f findntlm.f ighigh.f main_cal.f mv1to2.f rules2.f vctadd.f \
camnone.f dlegendr.f filldirk.f findsndgrp.f igl.f  norm3d.f \
scale.f xmul.f cfunex_new.f cfunex.f dotmul.f filltl.f getedgectr.f iglp.f \
 out_current.f setbox.f\
interface.f\
writetl.f readtl.f\
writesndgrp.f readsndgrp.f\
writevfs.f readvfs.f\
writeamn.f readamn.f 

SRCF77_2 =
# mlfma_param.f mlfma_input.f mlfma_const.f 

SRCCXX = assm_progress.cpp cfie_solve.cpp vector.cpp iml_vector.cpp  matrix_fmm.cpp densecmat.cpp quadrature.cpp \
mtxele_efie.cpp norm-cob.cpp

SRCC = array_build.c  fstrcat.c  main_call.c precond.c writevec.c


OBJ=$(SRC:.cc=.o) $(SRCF77:.f=.o) $(SRCC:.c=.o) $(SRCCXX:.cpp=.o) $(SRCF77_2:.f=.o)


# all: $(EXE)
# .cc.o:
# 	$(CXX) -c $(CFLAG) $<
# $(EXE): $(OBJ)
# 	@cd lib;make GDB=$(GDB)
# 	$(LD) $(OBJ) $(LDFLAG) -o $(EXE)
# 	mv $(EXE) $(EMoffice_HOME)/bin
# clean:
# 	make -C lib clean
# 	rm *~ *.o -rf

all: $(EXE)


$(EXE): $(OBJ)  
	$(LD) $(OBJ) $(LDFLAG) -o $(EXE)
	mv $(EXE) ./bin
$(EXE)_Ant: $(OBJ_ANT)
	$(LD) $(OBJ_ANT) $(LDFLAG) -o $(EXE)_Ant
	mv $(EXE)_Ant ./bin

libmlfma.a:
	make -j4 -C lib GDB=$(GDB) INTEL=$(INTEL)
libxmeshlz.a:
	make -j4 -C xmesh GDB=$(GDB) INTEL=$(INTEL)

clean:
#	make -C lib clean
#	make -C xmesh clean
	rm -f *~ *.o *.mod
