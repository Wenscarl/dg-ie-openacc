      function iglp(igt,lxyz,lxyzp)
*
*   find #igt child's parent #iglp
*-------------------------------------------------------------------
*
      implicit none
      integer*8 iglp,igt,lxyz(3),lxyzp(3),ig(3),igp(3),igl
*
      call igxyz(igt,lxyz,ig)
      call ighigh(ig,igp)
      iglp=igl(lxyzp,igp)
      return
      end
