      subroutine cacxpc
     &( cx0, cy, job, cwork1, cwork2, cvf, cvs, 
     &  maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, ntlm, niplm,
     &  lxyz, modes,
     &  igall, igcs, igrs, index, family, 
     &  lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, indextl,
     &  kpstart, nkpm, c_shift, icsa, irsa, array, 
     &  ngsndm, igsndcs, igsndrs,
     &  nearm, canear, noself, ipvt, ngnearm, igblkrs, igblkcs, igblk,
     &  mfinestm, cfinest )
	
	use mlfma_const
      implicit none
!      nclude 'mlfma_const.inc'

      INTEGER*8 maxedge, lmax, lsmax, ncmax, kpm, kp0, nkpm,
     &        nsm, ntlm, niplm, ngsndm, mfinestm,
     &        nearm, ngnearm, job  

      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
      INTEGER*8 irsa(nkpm+1), icsa(niplm)
       REAL   array(niplm)
      COMPLEX ctl(ntlm), csm(2,nsm)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 noself(2,ncmax+1), ipvt(maxedge)    
      COMPLEX cvf(2,kp0,maxedge), cvs(2,kp0,maxedge)
      COMPLEX canear(nearm*maxedge), cfinest(mfinestm)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)
      COMPLEX c_shift(nkpm,2,2,2)
      INTEGER*8 igsndcs(ngsndm), igsndrs(ncmax)      

      COMPLEX cx0(maxedge), cy(maxedge)
      COMPLEX cwork1(maxedge), cwork2(maxedge)
        
      INTEGER*8 i

ccccccccccccccccccccccccccccccccccccccccccccccccccc
c.....MLFMA.......................................c
ccccccccccccccccccccccccccccccccccccccccccccccccccc 

      do i=1, maxedge
         cy(i)=0.0
      enddo

      call cacx(cx0, cy, job, cwork1, cvf, cvs,
     &   maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, ntlm, niplm,
     &   lxyz, modes,
     &   igall, igcs, igrs, index, family, 
     &   lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, indextl,
     &   kpstart, nkpm, c_shift, icsa, irsa, array, 
     &   ngsndm, igsndcs, igsndrs,
     &   nearm, noself, canear, ngnearm, igblkrs, igblkcs, igblk,
     &   mfinestm, cfinest)

  
      return
      end

