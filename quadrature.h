// QUADRATURE.H
// Gaussian quadrature rules for numerically long long integrate 'smooth' functions over
// simplexes, triangle for 2d and tetrahedra for 3d.

#ifndef QUADRATURE_H
#define QUADRATURE_H
typedef float Real;
#define one_third 0.333333333333333
#define two_third 0.666666666666667
#define one_sixth 0.166666666666667

// added by Marinos N. Vouvakis & Seung-Cheol Lee 08/12/02
// Order = 4
#define g6_a 0.816847572980459
#define g6_b ( 1.0 - g6_a ) / 2.0
#define g6_c 0.108103018168070
#define g6_d ( 1.0 - g6_c ) / 2.0
#define g6_W1 0.109951743655322
#define g6_W2 0.223381589678011
// added by  Seung-Cheol Lee & Marinos N. Vouvakis 08/12/02
// Order = 5
#define g9_a 0.437525248383384
#define g9_b 1.0 - 2.0 * g9_a
#define g9_c 0.797112651860071
#define g9_d 0.165409927389841
#define g9_e 1.0 - g9_c - g9_d
#define g9_W1 0.205950504760887
#define g9_W2 0.063691414286223
// added by Marinos N. Vouvakis  & Seung-Cheol Lee 08/12/02
// Order = 6
#define g12_a 0.063089014491502
#define g12_b 1.0 - 2.0 * g12_a
#define g12_c 0.249286745170910
#define g12_d 1.0 - 2.0 * g12_c
#define g12_e 0.636502499121399
#define g12_f 0.310352451033785
#define g12_g 1.0 - g12_e - g12_f
#define g12_W1 0.050844906370207
#define g12_W2 0.116786275726379
#define g12_W3 0.082851075618374
// Order = 7
#define g13_a 0.638444188569809
#define g13_b 0.312865496004875
#define g13_c 0.048690315425316
// added by MV 03/04/03
// Order = 8
#define g16_z0_2 0.04666912123569507  
#define g16_z1_2 0.4766654393821525
#define g16_z2_2 0.4766654393821525

#define g16_z0_3 0.9324563118910393
#define g16_z1_3 0.03377184405448033
#define g16_z2_3 0.03377184405448033

#define g16_z0_4 0.4593042216691921
#define g16_z1_4 0.2703478891654040
#define g16_z2_4 0.2703478891654040

#define g16_z0_5 0.05146433548666149
#define g16_z1_5 0.7458294907672514
#define g16_z2_5 1. - g16_z0_5 - g16_z1_5
 
#define g16_W1 -0.2834183851113958
#define g16_W2 0.2097208857979572/3.
#define g16_W3 0.05127273801480265/3.
#define g16_W4 0.6564896469913506/3.
#define g16_W5 0.3659351143072855/6.
// added by MV 03/04/03
// Order = 9
#define g19_z0_2 0.02063496160252593
#define g19_z1_2 0.4896825191987370
#define g19_z2_2 0.4896825191987370

#define g19_z0_3 0.1258208170141290
#define g19_z1_3 0.4370895914929355
#define g19_z2_3 0.4370895914929355

#define g19_z0_4 0.6235929287619356
#define g19_z1_4 0.1882035356190322 
#define g19_z2_4 0.1882035356190322 

#define g19_z0_5 0.9105409732110941
#define g19_z1_5 0.04472951339445297
#define g19_z2_5 0.04472951339445297

#define g19_z0_6 0.03683841205473626
#define g19_z1_6 0.7411985987844980
#define g19_z2_6 1. - g19_z0_6 - g19_z1_6
 
#define g19_W1 0.09713579628279610
#define g19_W2 0.09400410068141950/3.
#define g19_W3 0.2334826230143263/3.
#define g19_W4 0.2389432167815273/3.
#define g19_W5 0.07673302697609430/3.
#define g19_W6 0.2597012362637364/6.
// added by MV 03/04/03
// Order = 11
#define g27_z0_1 0.9352701037774565
#define g27_z1_1 0.03236494811127173
#define g27_z2_1 0.03236494811127173

#define g27_z0_2 0.7612981754348137
#define g27_z1_2 0.1193509122825931
#define g27_z2_2 0.1193509122825931 
		 
#define g27_z0_3 -0.06922209654151433
#define g27_z1_3 0.5346110482707572
#define g27_z2_3 0.5346110482707572
		 
#define g27_z0_4 0.5933801991374367
#define g27_z1_4 0.2033099004312816
#define g27_z2_4 0.2033099004312816
		 
#define g27_z0_5 0.2020613940682885
#define g27_z1_5 0.3989693029658558
#define g27_z2_5 0.3989693029658558
		 
#define g27_z0_6 0.05017813831049474
#define g27_z1_6 0.5932012134282132
#define g27_z2_6 1. - g27_z0_6 - g27_z1_6

#define g27_z0_7 0.02102201653616613
#define g27_z1_7 0.8074890031597923
#define g27_z2_7 1. - g27_z0_7 - g27_z1_7
 
#define g27_W1 0.04097919300803106/3.
#define g27_W2 0.1085536215102866/3.
#define g27_W3 0.002781018986881812/3.
#define g27_W4 0.1779689321422668/3.
#define g27_W5 0.2314486047444677/3.
#define g27_W6 0.3140226717732234/6.
#define g27_W7 0.1242459578348437/6.
// added by MV 03/07/03
// Order = 12
#define g33_z0_1 0.023565220452390
#define g33_z1_1 0.488217389773805
#define g33_z2_1 0.488217389773805

#define g33_z0_2 0.120551215411079
#define g33_z1_2 0.439724392294460
#define g33_z2_2 0.439724392294460

#define g33_z0_3 0.457579229975768
#define g33_z1_3 0.271210385012116
#define g33_z2_3 0.271210385012116

#define g33_z0_4 0.744847708916828
#define g33_z1_4 0.127576145541586
#define g33_z2_4 0.127576145541586

#define g33_z0_5 0.957365299093579
#define g33_z1_5 0.021317350453210
#define g33_z2_5 0.021317350453210

#define g33_z0_6 0.115343494534698
#define g33_z1_6 0.275713269685514
#define g33_z2_6 0.608943235779788

#define g33_z0_7 0.022838332222257
#define g33_z1_7 0.281325580989940
#define g33_z2_7 0.695836086787803

#define g33_z0_8 0.025734050548330
#define g33_z1_8 0.116251915907597
#define g33_z2_8 0.858014033544073

#define g33_W1 0.025731066440455
#define g33_W2 0.043692544538038
#define g33_W3 0.062858224217885
#define g33_W4 0.034796112930709
#define g33_W5 0.006166261051559
#define g33_W6 0.040371557766381
#define g33_W7 0.022356773202303
#define g33_W8 0.017316231108659

extern  Real g2d_1[1][4];
extern  Real g2d_3[3][4];
extern  Real g2d_4[4][4];
extern  Real g2d_6[6][4];
extern  Real g2d_7[7][4];
extern  Real g2d_9[9][4];
extern  Real g2d_12[12][4];
extern  Real g2d_13[13][4];
extern  Real g2d_16[16][4];
extern  Real g2d_19[19][4];
extern  Real g2d_27[27][4];
extern  Real g2d_33[33][4];

#ifdef __cplusplus
extern "C" {
#endif
void getformula_(long long int*, long long int*,           Real *, Real *, Real *, Real *);
void getgauss_  (long long int*, long long int*, long long int*,      Real *, Real *, Real *, Real *);
void getduffy_  (long long int*, long long int*, long long int*, long long int*, Real *, Real *, Real *, Real *);
void GetFormula(long long int, long long int,           Real *, Real *, Real *, Real *);
void getGauss  (long long int, long long int, long long int,      Real *, Real *, Real *, Real *);
void getDuffy  (long long int, long long int, long long int, long long int, Real *, Real *, Real *, Real *);
void GetFormula1D(long long int np, long long int id,Real *zeta0, Real *zeta1,Real *weight);
void getDuffyEdge(long long int outLoop, long long int singularLoop,long long int outId, long long int id,Real *zeta0, Real *zeta1, Real *zeta2,Real *wgt);
void getCornerDuffy(long long int innerLoop, long long int i,Real *zeta0, Real *zeta1, Real *zeta2,Real *wgt);
#ifdef __cplusplus
}
#endif

 
#endif
