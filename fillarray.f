      subroutine fillarray(lsmax, kpm, niplm, lsl, lsh, xl, xh, 
     &           array, icsa, irsa, indexa)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c  fill the interpolation matrix, array, which is storaged by 
c  crs (compressed row storage)
c  call:      fillb16
c  call from: filltl
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	use mlfma_const
      implicit none

!      nclude 'mlfma_const.inc'

      INTEGER*8 lsmax, kpm, niplm
      INTEGER*8 lsl, lsh, icsa(niplm), irsa(kpm+1), indexa
       REAL   xl(lsmax), xh(lsmax),  array(niplm)

      INTEGER*8   modemax
      PARAMETER(modemax=2000)
       REAL     thetal(0:modemax+1), dt(0:modemax)

      INTEGER*8   mode, i, ls, kptl, itl, kp, ith, iph, ipl,
     &          ixy, ix, it, iy, ip, itt
       REAL     b16(16), dphil, dphih, xth, 
     &          th, dt0, dtm, dtp, u, ph, v, temp1, temp2, sign
             
       mode(i,ls) = i + ls - (i+ls-1)/ls*ls

c.....check if lsmax overflows
      if(lsmax.gt.modemax) then
         write(*,*) 'lsmax is gteater than modemax'
         write(*,*) 'please increase modemax in fillarray'
      endif

      kptl = 4 + 2*lsl*lsl
      thetal(0) = pi
      do i = 1, lsl
         thetal(i) = acos(xl(i))
         dt(i-1) = thetal(i) - thetal(i-1)
      enddo
      thetal(lsl+1)=0.
      dt(lsl)=-thetal(lsl)
      dphil=pi/real(lsl)
      dphih=pi/real(lsh)
      itl=0
      kp=0
      do ith=1,lsh
         xth=xh(ith)
         call  hunt(xl,lsl,xth,itl)
         th=acos(xth)
         dt0=dt(itl)
         if(itl.eq.0) then
           dtm=dt0
         else
           dtm=dt(itl-1)
         endif
         if(itl.eq.lsl) then
           dtp=dt0
         else
           dtp=dt(itl+1)
         endif
         dtm=dt0/(dt0+dtm)
         dtp=dt0/(dt0+dtp)
         u=(th-thetal(itl))/dt0
         do iph=1,2*lsh
            kp=kp+1
            ph=(iph-1)*dphih
            ipl=1+int(ph/dphil+1.e-4)
            v=(ph-(ipl-1)*dphil)/dphil
            call fillb16(u,v,dtm,dtp,b16)
            ixy=0
            do ix=1,4
               if((indexa+1).gt.niplm) then 
						   write(*,*) 'overflow in interpolation'
						call exit(-1)
				endif
               it=ix+itl-2
               temp1=0.
               temp2=0.
               do iy=1,4  
                  ixy=ixy+1         
                  ip=mode(iy+ipl-2,2*lsl)
                  if((it.eq.0).or.(it.eq.(lsl+1))) then
                     temp1=temp1+b16(ixy)*cos((ip-1.)*dphil)
                     temp2=temp2+b16(ixy)*sin((ip-1.)*dphil)
                  else
                     if(it.eq.(-1)) then
                        itt=1
                        ip=mode(ip+lsl,2*lsl)
                        sign=-1.
                     elseif(it.eq.(lsl+2)) then
                        itt=lsl
                        ip=mode(ip+lsl,2*lsl)
                        sign=-1.
                     else
                        itt=it
                        sign=1.
                     endif
                     if(abs(b16(ixy)).gt.1e-6) then
                        indexa=indexa+1
                        icsa(indexa)=(itt-1)*2*lsl+ip
                        array(indexa)=b16(ixy)*sign
                     endif
                  endif
               enddo
               if(it.eq.0) then
                  icsa( indexa+1)=kptl-1
                  array(indexa+1)=temp1
                  icsa( indexa+2)=kptl
                  array(indexa+2)=temp2
                  indexa=indexa+2
               elseif(it.eq.(lsl+1)) then
                  icsa( indexa+1)=kptl-3
                  array(indexa+1)=temp1
                  icsa( indexa+2)=kptl-2
                  array(indexa+2)=temp2
                  indexa=indexa+2
               endif
            enddo
            irsa(kp+1)=indexa+1
         enddo
      enddo
      do i=1,4
         if((indexa+1).gt.niplm) then 
					 write(*,*) 'overflow in interpolation'
					 call exit(-1)
			endif
         indexa=indexa+1
         kp=kp+1
         icsa( indexa)=kptl-4+i
         array(indexa)=1.
         irsa(kp+1)=indexa+1
      enddo
      return
      end
