#include <numeric>
#include "iml_vector.hpp"
#include "comp.h"

iml_vector::scaler_type dot(const iml_vector &x, const iml_vector &y)
{
  const long long int max_th = omp_get_max_threads();
  std::vector<iml_vector::scaler_type> result_array(max_th);

#pragma omp parallel
  {
    iml_vector::scaler_type result = 0;
#pragma omp for schedule(static)
    for (unsigned long long int i = 0; i < x.dim; ++i) {
      result += conj(x(i))*(y(i));
    }

    result_array.at(omp_get_thread_num()) = result;
  }

  return std::accumulate(result_array.begin(), result_array.end(), iml_vector::scaler_type(0));
}

float norm(const iml_vector &x)
{
  return sqrt(real(dot(x,x)));
}

float norm_inf(const iml_vector &x)
{
  if (x.dim == 0)
    return 0.0;

  float result = abs(x.data[0]);
  for (long long int i = 1; i < x.dim; ++i)
    if (abs(x.data[i]) > result) {result = abs(x.data[i]);}

  return result;
}

iml_vector operator*(const iml_vector::scaler_type &C, const iml_vector &v)
{
  iml_vector result(v.dim);

#pragma omp parallel for schedule(static)
  for (unsigned long long int i = 0; i < v.dim; ++i) {
    result.data[i] = C*v.data[i];
  }

  return result;
}
