#include <set>
#include <ctime>
#include <cstdio>
#include <complex>
#include <math.h>
#include "quadrature.h"
#include <xmesh_vector.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits>
#include <iostream>
#include "comp.h"

typedef long long int Integer;
// #define WRITEOUT
typedef float Real;
typedef std::complex<float> Complex;
const Real PI=acos(-1.0);
FILE* fp_global=NULL;
FILE* fp_global2=NULL;
static Integer total=0;
const Real BIG=std::numeric_limits<Real>::max();
extern "C" {
  void mtxele_efie_( 
             Integer* ii, 
		 Integer* jj, 
		 Complex* ck,
		 Integer* maxnode, 
		 Integer* maxpatch, 
		 Integer* maxedge,
		 Real* xyznode, 
		 Integer* ipatpnt, 
		 Integer* iedge, 
		 Real* xyzctr, 
		 Real* xyznorm, 
		 Real* edge, 
		 Real* paera,
		 Real* freq, 
		 Real* wl0,
		 Real* eta0, 
		 Complex* ci,
		 Complex* cnxet,
		 Complex* ,
		 Integer* contourMap
 			);
  void openfile_();
  void closefile_();
void rules2(Integer i1,
		Integer i2,
		Real* 	r1,
		Real* 	r2,
		Real		distmin,
		Integer	irulef_near,
		Integer	irules_near,
		bool&	nearpat,
		Integer&	irulef,
		Integer&	irules);
}
Complex getIntegral(const xMesh::vector r0,const xMesh::vector r1,const xMesh::vector r2,Integer InteriorIntegralRule,
		    Complex ck,Complex ci,bool debug=false);
Real calcWeight(const xMesh::vector rfld,const xMesh::vector r1_src,const xMesh::vector r2_src,const xMesh::vector r3_src);

void openfile_()
{
#ifdef WRITEOUT
	fp_global=fopen("checkNF_new","w");
	if(fp_global==NULL)
		exit(-1);
	fp_global2=fopen("checkNF2_new","w");
	if(fp_global2==NULL)
		exit(-1);
#endif
}
void closefile_()
{
#ifdef WRITEOUT
	fclose(fp_global);fclose(fp_global2);
#endif
}
void getr(Real* r1_fld,Real* r2_fld,Real* r3_fld,
	    Real vt1,Real vt2,Real vt3,Real* rfld,Real* rhofld);
template<class T> T mysqr(T dd){
	return dd*dd;
}
void diff(Real* a,Real* b,Real* c){
	for (int i=0;i<3;i++){
		c[i]=a[i]-b[i];
	}
}
void xmul(Real* a,Real* b,Real* c){
	for (int i=0;i<3;i++){
		c[i]=a[(i+1)%3]*b[(i+2)%3]-a[(i+2)%3]*b[(i+1)%3];
	}
}
Real dotmul(Real* a,Real* b)
{
	return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}
Real edgeDist(const xMesh::vector input,const xMesh::vector a,const xMesh::vector b)
{
	xMesh::vector l1=input-a;
	xMesh::vector l2=input-b;
	Real h=  (l1*l2).norm() / (a-b).norm();
	Real mini=l1.norm();
	Real cos2= dotP((a-b),input-b);
	Real cos1= dotP((b-a),input-a);
	
	if(cos1>=0.0 &&cos2>=0.0)
		return h;
	if(cos1<=0)
		return l1.norm();
	if(cos2<=0)
		return l2.norm();
	
// 	if(l2.norm()<mini)
// 		mini=l2.norm();
// 	if(h<mini)
// 		mini=h;
// 	return mini;
}

xMesh::vector triProj(const xMesh::vector input,const xMesh::vector r0,const xMesh::vector r1,const xMesh::vector r2)
{
	xMesh::vector normal= (r1-r0)*(r2-r1);
	if(normal.norm()>0)
		normal.normalize();
	xMesh::vector rp= input-r0;
	xMesh::vector result= input- normal * dotP(rp,normal);
	return result;
}

void mtxele_efie_( 
		Integer* ii_, 
		 Integer* jj_, 
		 Complex* ck_,
		 Integer* maxnode, 
		 Integer* maxpatch, 
		 Integer* maxedge,
		 Real* xyznode, 
		 Integer* ipatpnt, 
		 Integer* iedge, 
		 Real* xyzctr, 
		 Real* xyznorm, 
		 Real* edge, 
		 Real* paera,
		 Real* freq_, 
		 Real* wl0_,
		 Real* eta0_, 
		 Complex* ci_,
		 Complex* cnxetv,
		 Complex* cnxets,
		 Integer* contourMap
     		)
{
	Integer ii=*ii_ -1;
	Integer jj=*jj_ -1;
	Complex& ck=*ck_;
	Real& freq=*freq_;
	Real& wl0=*wl0_;
	Real& eta0=*eta0_;
	Complex& ci=*ci_;
	bool contourSrc=false;
	bool contourFld=false;
	bool writeout=false; //For debug use
	bool SurfSign = false;
	Complex   ck2d2=ck*ck/ Complex(2.0);
	Integer n1fld = iedge[  ii*4 +0]  -1;
	Integer n2fld = iedge[  ii*4 +1]  -1;
	Integer n1src = iedge[  jj*4 +0]  -1;
	Integer n2src = iedge[  jj*4 +1]  -1;
	
// 	if(ii==1074&&jj==11)
// 		writeout=true;

	
	if(ii==1074&&jj==1074)
		writeout=false;	
	if( iedge[  ii*4 +2] <1 ||iedge[  ii*4 +3] <1)
		contourFld=false;
	if( iedge[  jj*4 +2] <1 ||iedge[  jj*4 +3] <1)
		contourSrc=true;
	
	xMesh::vector r1_src(xyznode[n1src*3  ],
			     xyznode[n1src*3+1],
			     xyznode[n1src*3+2]);
	xMesh::vector r2_src(xyznode[n2src*3  ],
			     xyznode[n2src*3+1],
			     xyznode[n2src*3+2]);
	xMesh::vector r1_fld(xyznode[n1fld*3  ],
			     xyznode[n1fld*3+1],
			     xyznode[n1fld*3+2]);
	xMesh::vector r2_fld(xyznode[n2fld*3  ],
			     xyznode[n2fld*3+1],
			     xyznode[n2fld*3+2]);
	
	xMesh::vector ectr;
	ectr=(r1_fld+r2_fld)/2.0;
	xMesh::vector ectr2;
	ectr2=(r1_src+r2_src)/2.0;
	*cnxetv= Complex(0.0);
	*cnxets= Complex(0.0);

	Complex result_efies[2][2];
	Complex result_contour_fld[2];
	Complex result_contour_src[2];
	Integer outerGauss=6;
	Integer innerGauss=6;
	Integer outerDuffy=4;
	Integer innerDuffy=27;
	Real EdgeFld=edge[ii];
	Real EdgeSrc=edge[jj];

	if (SurfSign)	
	for(Integer idFld=0;idFld<2;idFld++){
		
		Real signfld = float(1.0f-2.0f*idFld);
		Integer ipfld=iedge[2+idFld+ (ii)*4] -1;
		Real AreaFld=paera[ipfld];
		if(ipfld<0)
			continue;
		Integer n3fld= ipatpnt[ipfld*3]+ ipatpnt[ipfld*3+1]+ipatpnt[ipfld*3+2] - n1fld -n2fld -3;
		xMesh::vector r3_fld(xyznode[n3fld*3  ],
				     xyznode[n3fld*3+1],
				     xyznode[n3fld*3+2]);
		xMesh::vector midFld= (r3_fld+r2_fld+r1_fld) * 0.3333f;
		xMesh::vector anorm(xyznorm[ipfld*3  ],
				    xyznorm[ipfld*3+1],
				    xyznorm[ipfld*3+2]);
		xMesh::vector pctr(xyzctr[ipfld*3  ],
				   xyzctr[ipfld*3+1],
				   xyzctr[ipfld*3+2]);
		xMesh::vector rhot;
		rhot=ectr-pctr;
		Complex cefiev=Complex(0.0f);
		Complex cefies=Complex(0.0f);
		for(Integer idSrc=0;idSrc<2;idSrc++){
		     Real signsrc = float( 1.0 - 2 * idSrc );
		     Integer ipsrc = iedge[2+idSrc+ jj*4 ]-1;
		     Real AreaSrc=paera[ipsrc];
		     if(ipsrc<0)
				continue;
		     Integer n3src= ipatpnt[ipsrc*3]+ ipatpnt[ipsrc*3+1]+ipatpnt[ipsrc*3+2] - n1src -n2src -3;
		     xMesh::vector r3_src(xyznode[n3src*3  ],
					  xyznode[n3src*3+1],
					  xyznode[n3src*3+2]);
		     xMesh::vector bnorm(xyznorm[ipsrc*3  ],
					 xyznorm[ipsrc*3+1],
					 xyznorm[ipsrc*3+2]);
			xMesh::vector midSrc= (r3_src+r2_src+r1_src) * 0.3333f;
			
			bool isNear=(ipsrc==ipfld);
			if(!isNear){
				xMesh::vector tmp=midFld-midSrc;
				Real rk0= abs(ck)*tmp.norm();
				if(rk0< 0.1||ii==jj){
					outerGauss=27;
					innerGauss=27;
				}else if(rk0<0.5){
					outerGauss=4;
					innerGauss=4;
				}else if(rk0<0.7){
					outerGauss=4;
					innerGauss=4;
				}else{
					outerGauss=1;
					innerGauss=1;
				}
			}
			Complex csumev=Complex(0.0);
			Complex csumes=Complex(0.0);
			
			Integer outerLoop= isNear?outerDuffy:outerGauss;
			Integer innerLoop= isNear?innerDuffy:innerGauss;
			for(Integer ifgrid=0;ifgrid<outerLoop;ifgrid++){
				Real vt[3],wto;
				GetFormula(outerLoop,ifgrid,&(vt[0]),&(vt[1]),&(vt[2]),&wto);
				xMesh::vector rfld= (r1_fld*vt[0]+ r2_fld*vt[1]+r3_fld*vt[2]);
				xMesh::vector rhofld=rfld-r3_fld;				
				xMesh::vector rhoxnt=rhofld;
				xMesh::vector rhoxnx= rhofld*anorm;

				Complex czev=Complex(0.0); //Vector part
				Complex czes=Complex(0.0); //Scalar part
				for(long long int isgrid=0;isgrid<innerLoop;isgrid++){ //Go through every points in the source grid
					Real vti[3],wti;	
					if(!isNear)
						GetFormula(innerLoop,isgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
					else
						getDuffy(outerLoop,innerLoop,ifgrid,isgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
					xMesh::vector rsrc;
					if(isNear) 
						rsrc= (r1_fld*vti[0] + r2_fld*vti[1]+r3_fld*vti[2]);	
					else
						rsrc= (r1_src*vti[0] + r2_src*vti[1]+r3_src*vti[2]);
					xMesh::vector rhosrc = rsrc-r3_src;
					xMesh::vector rr = rsrc-rfld;
					Real distance= rr.norm();
					if(distance==0.0f){
						printf("Suck!\n");
					}
					Complex cg0= exp(ci*ck*distance);
					Complex Green= cg0 / distance;
					Real zr=dotP(rhosrc,rhofld);
					Complex comp1= wti* Green * zr;
					Complex comp2= - Green*Complex(wti*4.0,0.0)/(ck*ck);
					czev += wti*  zr * Green;
					czes += Green*Complex( -4.0*wti,0.0) /(ck*ck);
				}
				
				csumev += wto * czev;
				csumes += wto * czes;	
				
			} //for(Integer ifgrid=0;ifgrid<outerLoop;ifgrid++){
			cefiev += signsrc * csumev;
			cefies += signsrc * csumes;
			result_efies[idFld][idSrc] = signfld* signsrc * csumes;
		}//for(Integer idSrc=0;idSrc<2;idSrc++){
//		*cnxetv += signfld* cefiev;
//		*cnxets += signfld* cefies;
		*cnxetv += signfld* cefiev + signfld*cefies;
	}//for(int idFld=0;idFld<2;idFld++)  
	Real rnst= edge[ii]*edge[jj]/(16.0f*PI);
//	*cnxetv*=rnst;
//	*cnxets*=rnst;	
	*cnxetv*=rnst;
	

	
	const Integer LineIntegralRule=5;
	const Integer InteriorIntegralRule=5;
	Complex LineIntegralPart[2]={Complex(0.0,0.0),Complex(0.0,0.0)};	

	if(contourFld) {  
		//contour as the receiver
		bool useDuffy=false;
		long long int duffyUsed=0;
		Real signfld=-1.0;
		Integer idfld=iedge[2+ ii*4 ]-1;
		Integer idfld_mirror=iedge[2+ (contourMap[ii]-1)*4 ]-1;
		Complex cnxetv_c=Complex(0.0);
		xMesh::vector rfld_mid= (r1_fld+r2_fld)*0.5;
		for(Integer idSrc=0;idSrc<2;idSrc++){
			
			Real signsrc = float( 1.0 - 2 * idSrc ); 
			Integer ipsrc = iedge[2+idSrc+ jj*4 ]-1;  //id of source triangle
			if(ipsrc<0) continue;
			Complex cnxetv_v_src=Complex(0.0);
			Integer n3src= ipatpnt[ipsrc*3]+ ipatpnt[ipsrc*3+1]+ipatpnt[ipsrc*3+2] - n1src -n2src -3;
			xMesh::vector r3_src(xyznode[n3src*3  ],
					     xyznode[n3src*3+1],
					     xyznode[n3src*3+2]);		
			xMesh::vector rsrc_mid= (r1_src+r2_src+r3_src) /3.0;
			for(long long int ifgrid=0;ifgrid<LineIntegralRule;ifgrid++){
				Real v1d[2],wt1d;
				Complex cnxetv_v_src_grid=Complex(0.0);
				GetFormula1D(LineIntegralRule,ifgrid,&(v1d[0]),&(v1d[1]),&wt1d);
				xMesh::vector rfld= r1_fld * v1d[0] + r2_fld * v1d[1];
				
				Integer useDuffy=0;
				Integer innerLoop=4;
				Real dist=(rsrc_mid-rfld_mid).norm();
				Real distEdge[3]={0,0,0};
				Real distEdgeMin=BIG;
				xMesh::vector rfld_proj= triProj(rfld,r1_src,r2_src,r3_src);
				if( idfld == ipsrc){
					useDuffy=1; //Conformal case	
					duffyUsed=1;
				}
				else
				{
					//Judging from the distance from the current node to the triangle (based on the edge distNCE)					
					distEdge[0]= edgeDist(rfld_proj,r1_src,r2_src)/ (r1_src-r2_src).norm();
					distEdge[1]= edgeDist(rfld_proj,r2_src,r3_src)/ (r2_src-r3_src).norm();
					distEdge[2]= edgeDist(rfld_proj,r3_src,r1_src)/ (r3_src-r1_src).norm();
					for(int jjj=0;jjj<3;jjj++){
						if(distEdgeMin>distEdge[jjj])
							distEdgeMin=distEdge[jjj];
					}
					
					if(distEdgeMin<0.02) {
						useDuffy=1;
						duffyUsed=1;

					}
					else{
						useDuffy=0;
						if(dist*abs(ck)  <0.1 )
							innerLoop=13;
						else if(dist*abs(ck)  <0.5 )
							innerLoop=6;
						else 
							innerLoop=4;
					}
					
				}
				if(useDuffy==0){
					for(long long int isgrid=0;isgrid<innerLoop;isgrid++){ 
						Real vti[3],wti;
						GetFormula(innerLoop,isgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
						xMesh::vector rsrc = r1_src * vti[0] + r2_src * vti[1] + r3_src * vti[2] ;
						xMesh::vector rr   = rsrc - rfld;
						Real distance= rr.norm();
						if(distance==0.0f){
							printf("Suck!2\n");
						}
						Complex cg0   = exp(ci*ck*distance);
						Complex Green = cg0/distance;
						Complex comp2 = Green/(ck*ck);
						cnxetv_v_src_grid+= comp2* wti;						
					}
				}
				else if(useDuffy==1 ){					
					Real weight;					
					weight=calcWeight(rfld,r1_src,r2_src,r3_src);
					if((weight)>0.01|| weight<-0.01){
#ifdef WRITEOUT
						if(writeout)
							fprintf(fp_global,"fld1: %d %d idFld:%d  %d v:%f %f\n",ii,jj,idSrc,ifgrid,v1d[0],v1d[1]);
#endif
						cnxetv_v_src_grid+=weight*getIntegral(rfld,r1_src,r2_src,InteriorIntegralRule,ck,ci);
					}
					weight=calcWeight(rfld,r2_src,r3_src,r1_src);
					if((weight)>0.01|| weight<-0.01){
#ifdef WRITEOUT
						if(writeout)
							fprintf(fp_global,"fld2: %d %d idFld:%d  %d v:%f %f\n",ii,jj,idSrc,ifgrid,v1d[0],v1d[1]);
#endif
						cnxetv_v_src_grid+=weight*getIntegral(rfld,r2_src,r3_src,InteriorIntegralRule,ck,ci);
					}
					weight=calcWeight(rfld,r3_src,r1_src,r2_src);
					if((weight)>0.01|| weight<-0.01){
#ifdef WRITEOUT
						if(writeout)
							fprintf(fp_global,"fld3: %d %d idFld:%d  %d v:%f %f\n",ii,jj,idSrc,ifgrid,v1d[0],v1d[1]);
#endif
						cnxetv_v_src_grid+=weight*getIntegral(rfld,r3_src,r1_src,InteriorIntegralRule,ck,ci);
					}
				}
				cnxetv_v_src+= cnxetv_v_src_grid*wt1d;
			}//for(int ifgrid=0;ifgrid<LineIntegralRule;ifgrid++){
			cnxetv_c += signsrc * cnxetv_v_src ;
			result_contour_fld[idSrc]=signfld*signsrc * cnxetv_v_src ;
		}
#ifdef WRITEOUT
		if(true||writeout){
			if(duffyUsed==0)
				fprintf(fp_global2,"fld %d %d %f %f\n",ii,jj,cnxetv_c.real(),cnxetv_c.imag());
			else
				fprintf(fp_global2,"Fld %d %d %f %f\n",ii,jj,cnxetv_c.real(),cnxetv_c.imag());
		}
#endif
		LineIntegralPart[0]=signfld*cnxetv_c;
	}	
	if(contourSrc){
		//contour as the source 
		bool useDuffy=false;
		Real signsrc=-1.0;
		long long int duffyUsed=0;
		Integer idsrc=iedge[2+ jj*4 ]-1;
		Integer idsrc_mirror=iedge[2+ (contourMap[jj]-1)*4 ]-1;
		Complex cnxetv_c=Complex(0.0);
		xMesh::vector rsrc_mid= (r1_src+r2_src)*0.5;
		for(Integer idFld=0;idFld<2;idFld++){
			Real signfld = float( 1.0 - 2 * idFld );
			Integer ipfld = iedge[2+idFld+ ii*4 ]-1;
			if(ipfld<0) continue;
			Complex cnxetv_v_fld=Complex(0.0);
			Integer n3fld= ipatpnt[ipfld*3]+ ipatpnt[ipfld*3+1]+ipatpnt[ipfld*3+2] - n1fld -n2fld -3;
			xMesh::vector r3_fld(xyznode[n3fld*3  ],
					     xyznode[n3fld*3+1],
					     xyznode[n3fld*3+2]);		
			xMesh::vector rfld_mid= (r1_fld+r2_fld+r3_fld) /3.0;
			for(long long int isgrid=0;isgrid<LineIntegralRule;isgrid++){
				Real v1d[2],wt1d;
				Complex cnxetv_v_fld_grid=Complex(0.0);
				GetFormula1D(LineIntegralRule,isgrid,&(v1d[0]),&(v1d[1]),&wt1d);
				xMesh::vector rsrc= r1_src * v1d[0] + r2_src * v1d[1];
				//determine of we need to use duffy
				Integer useDuffy=0;
				Integer innerLoop=4;
				Real dist=(rsrc_mid-rfld_mid).norm();
				Real distEdge[3]={0,0,0};
				Real distEdgeMin=BIG;
				xMesh::vector rsrc_proj= triProj(rsrc,r1_fld,r2_fld,r3_fld); 
				//project the soruce point to the receiving triangle
				if( idsrc == ipfld){
					useDuffy=1;
					duffyUsed=1;
				}
				else
				{
					//Judging from the distance from the current node to the triangle (based on the edge distNCE)					
					distEdge[0]= edgeDist(rsrc_proj,r1_fld,r2_fld)/ (r1_fld-r2_fld).norm();
					distEdge[1]= edgeDist(rsrc_proj,r2_fld,r3_fld)/ (r2_fld-r3_fld).norm();
					distEdge[2]= edgeDist(rsrc_proj,r3_fld,r1_fld)/ (r3_fld-r1_fld).norm();
					for(int jjj=0;jjj<3;jjj++){
						if(distEdgeMin>distEdge[jjj])
							distEdgeMin=distEdge[jjj];
					}
					
					if(distEdgeMin<0.02) {
						useDuffy=1;
						duffyUsed=1;
// 						printf("edgeDist: %f, %f, %f \n", distEdge[0], distEdge[1], distEdge[2]);
					}
					else{
						useDuffy=0;
						if(dist*abs(ck)  <0.1 )
							innerLoop=13;
						else if(dist*abs(ck)  <0.5 )
							innerLoop=6;
						else 
							innerLoop=4;
					}
				}
				if(useDuffy==0){
					//Use Gauss integration.
					for(long long int ifgrid=0;ifgrid<innerLoop;ifgrid++){ 
						Real vti[3],wti;
						GetFormula(innerLoop,ifgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
						xMesh::vector rfld = r1_fld * vti[0] + r2_fld * vti[1] + r3_fld * vti[2] ;
						xMesh::vector rr   = rsrc - rfld;
						Real distance= rr.norm();
						if(distance==0.0f){
							printf("Suck!2\n");
						}
						Complex cg0   = exp(ci*ck*distance);
						Complex Green = cg0/distance;
						Complex comp2 = Green/(ck*ck);
						cnxetv_v_fld_grid+= comp2* wti;
					}
				}
				else if(useDuffy==1 ){
					Real weight;					
					weight=calcWeight(rsrc,r1_fld,r2_fld,r3_fld);
					if((weight)>0.01|| weight<-0.01){
#ifdef WRITEOUT
						if(writeout)
							fprintf(fp_global,"src1: %d %d idFld:%d  %d v:%f %f\n",
								ii,jj,idFld,isgrid,v1d[0],v1d[1]);
#endif
							Complex tmpp=getIntegral(rsrc,r1_fld,r2_fld,InteriorIntegralRule,ck,ci);
						cnxetv_v_fld_grid+=weight*tmpp;

					}
					
					weight=calcWeight(rsrc,r2_fld,r3_fld,r1_fld);
					if((weight)>0.01|| weight<-0.01)
					{
#ifdef WRITEOUT
						if(writeout)
							fprintf(fp_global,"src2: %d %d idFld:%d  %d v:%f %f\n",ii,jj,idFld,isgrid,v1d[0],v1d[1]);
#endif
						Complex tmpp=getIntegral(rsrc,r2_fld,r3_fld,InteriorIntegralRule,ck,ci);
						cnxetv_v_fld_grid+=weight*tmpp;
					}
					
					weight=calcWeight(rsrc,r3_fld,r1_fld,r2_fld);
					if((weight)>0.01|| weight<-0.01){
#ifdef WRITEOUT
						if(writeout)
							fprintf(fp_global,"src3: %d %d idFld:%d  %d v:%f %f\n",ii,jj,idFld,isgrid,v1d[0],v1d[1]);
#endif
						Complex tmpp=getIntegral(rsrc,r3_fld,r1_fld,InteriorIntegralRule,ck,ci);
						cnxetv_v_fld_grid+=weight*tmpp;
					}
					
				}
				cnxetv_v_fld+= cnxetv_v_fld_grid*wt1d;
			}
			cnxetv_c += signfld * cnxetv_v_fld ;			
			result_contour_src[idFld]=signsrc*signfld * cnxetv_v_fld ;			
		}
		LineIntegralPart[1]=signsrc*cnxetv_c;
#ifdef WRITEOUT
		if(true||writeout){
			if(duffyUsed==0)
				fprintf(fp_global2,"src %d %d %f %f\n",ii,jj,cnxetv_c.real(),cnxetv_c.imag());
			else
				fprintf(fp_global2,"Src %d %d %f %f\n",ii,jj,cnxetv_c.real(),cnxetv_c.imag());
		}
#endif
	}
	*cnxetv -= LineIntegralPart[0]* edge[ii]*edge[jj]/(4.0f*PI);
	*cnxets -= LineIntegralPart[1]* edge[ii]*edge[jj]/(4.0f*PI);
}
Real calcWeight(const xMesh::vector rfld,const xMesh::vector r1_src,const xMesh::vector r2_src,const xMesh::vector r3_src)
{
	xMesh::vector normal= (r1_src-r2_src)* (r2_src-r3_src);
	xMesh::vector normal2= (r1_src-rfld)* (r2_src-rfld);
	Real ratio=  normal2.norm()/normal.norm();
	if(dotP(normal,normal2)<0)
		ratio=-ratio;
	return ratio;
}
Complex getIntegral(const xMesh::vector r0,const xMesh::vector r1,const xMesh::vector r2,Integer InteriorIntegralRule,
		    Complex ck,Complex ci,bool DEBUG)
{
	Complex result=0.0; //return  int_{\Delta}{}
	if(DEBUG)
	{
		fprintf(fp_global,"getIntegral:........ rsrc: %f %f %f\n",r0.getx(),r0.gety(),r0.getz());
		
	}
	for(long long int isgrid=0;isgrid<InteriorIntegralRule*InteriorIntegralRule;isgrid++)
	{
		Real vti[3],wti;
		getCornerDuffy(InteriorIntegralRule,isgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
		xMesh::vector rsrc = r0 * vti[0] + r1 * vti[1] + r2 * vti[2] ;
		xMesh::vector rr   = rsrc - r0;
		Real distance= rr.norm();
		if(distance==0.0f){
			distance = 1e-7;
		//	printf("Suck!33\n");
		}
		Complex cg0   = exp(ci*ck*distance);
		Complex Green = cg0/distance;
		Complex comp2 = Green/(ck*ck);
		result+=comp2*wti;
		if(DEBUG){
// 			fprintf(fp_global,"%d rfld(%f,%f,%f)rr(%f,%f,%f)Green:(%f,%f)\n",
// 				isgrid,rsrc.getx(),rsrc.gety(),rsrc.getz(), 
// 				rr.getx(),rr.gety(),rr.getz(),
// 				Green.real(),Green.imag()
// 			);
			fprintf(fp_global,"%lld %f %f %f %f %f %f %f %f\n",
				isgrid,rsrc.getx(),rsrc.gety(),rsrc.getz(), 
				rr.getx(),rr.gety(),rr.getz(),
				Green.real(),Green.imag()
			);
		}
	}
	if(DEBUG)
		fprintf(fp_global,"End........\n");

	return result;
}
void getr(Real* r1_fld,Real* r2_fld,Real* r3_fld,
	    Real vt1,Real vt2,Real vt3,Real* rfld,Real* rhofld)
{
	for (int i=0;i<3;i++){
		rfld[i]= vt1*r1_fld[i]+vt2*r2_fld[i]+vt2*r2_fld[i];
		rhofld[i]=rfld[i]-r3_fld[i];
	}
}
void rules2(Integer i1,
		Integer i2,
		Real* 	r1,
		Real* 	r2,
		Real		distmin,
		Integer	irulef_near,
		Integer	irules_near,
		bool&		nearpat,
		Integer&	irulef,
		Integer&	irules)
{
	Real d=0.0f;
	nearpat=false;
	if(i1==i2)
		d=0.0f;
	else{
		for(int i=0;i<3;i++)
			d+= mysqr<Real>(r1[i]-r2[i]);
	}
	if(sqrt(d)<distmin){ 
		nearpat=true;
		irulef=irulef_near;
		irules=irules_near;
	}
}
