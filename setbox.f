      subroutine setbox
     &( maxnode, maxpatch, maxedge,
     &  xyznode, ipatpnt, iedge,
     &  lmax, lsmax, lsmin, kpm, kp0,
     &  lxyz, modes, rmin, sizel, wl0, pi, rk0)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c   find a box to hold all bases, and divide the box to lmax levels,
c   find  no. of boxes and sizes for each lavel.
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      implicit none

c.....Input Data

      INTEGER*8 maxnode, maxpatch, maxedge
      INTEGER*8 iedge(4,maxedge),    ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode)
      INTEGER*8 lmax

c.....Output Data

      INTEGER*8 lsmax, lsmin, kpm, kp0
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   rmin(3), sizel(3,0:lmax), wl0, pi, rk0

c.....Working Variables

      INTEGER*8 ls
      INTEGER*8 i, n, l, ip1, ip2
       REAL   posmin(3), posmax(3), rlmax, rsize, temp

c.....find minimum and maximum coordinates of conductors

      ip1 = iedge(1, 1)
      ip2 = iedge(2, 1)
      do i = 1, 3
         temp = 0.5*(xyznode(i, ip1) + xyznode(i, ip2))
         posmin(i) = temp
         posmax(i) = temp
      enddo
      do n = 2, maxedge
         ip1 = iedge(1, n)
         ip2 = iedge(2, n)
         do i = 1, 3
            temp = 0.5*(xyznode(i, ip1) + xyznode(i, ip2))
            if (temp .lt. posmin(i)) then
               posmin(i) = temp
            elseif (temp .gt. posmax(i)) then
               posmax(i) = temp
            endif
         enddo
      enddo

c.....find the real size of cubics in each level

      rsize=0
      do i = 1, 3
         lxyz(i, 0) = 1
         sizel(i, 0) = posmax(i) - posmin(i)
         if (sizel(i, 0) .gt. rsize) rsize = sizel(i,0)
      enddo
      do l = 1, lmax
         rsize = rsize/2.
         do i = 1, 3
            if (sizel(i, l-1) .gt. rsize) then
               sizel(i, l)=rsize
               lxyz(i, l) = lxyz(i, l-1)*2
            else
               sizel(i, l) = sizel(i, l-1)
               lxyz(i, l)  = lxyz(i, l-1)
            endif
         enddo
      enddo

c.....calculate or input the no. of modes needed in each level

      do l=1,lmax
         rlmax = sqrt(sizel(1, l)**2 + sizel(2 ,l)**2 + sizel(3, l)**2)
            ls = int(1. + rk0*rlmax + 1.0*log(rk0*rlmax + pi))
c	if(mod(ls,2) .eq. 0) ls =ls +1
         modes(l)=ls
      enddo

c.....determine lsmax,lsmin

      if(lmax .gt. 1) then
         lsmax = modes(2)
         lsmin = modes(2)
         do l = 3, lmax
            if(modes(l).gt.lsmax) lsmax=modes(l)
            if(modes(l).lt.lsmin) lsmin=modes(l)
         enddo
         kp0   = 2*lsmin**2+4
         kpm   = 2*lsmax**2+4
      else
         lsmax = 1
         lsmin = 1
         kp0 = 1
         kpm = 1
      endif

      do l = 2, lmax
        if(modes(l).gt.lsmax) then 
		
			write(*,*) '**** overflow in no. of modes ****'
			call exit(-1)
		endif
      enddo

      if ( (lmax.gt.1) .and. (modes(lmax).gt.lsmin) ) 
     &     then
				
					write(*,*) '** overflow in finest level ***'
					call exit(-1)
				endif
c.....set the size to box size in each level

      do i = 1, 3
         sizel(i, lmax) = rsize
      enddo
      do l = lmax-1, 0, -1
         do i = 1, 3 
            if (sizel(i, l) .le. rsize) then
                sizel(i, l) = sizel(i, l+1)
            else
                sizel(i, l) = rsize*2.
            endif
         enddo
         rsize = rsize*2.
      enddo

c....calculate the coordinates of the back-left-bottom corner for
c....the largest box

      do i = 1, 3
         rmin(i)  =0.5*(posmin(i) + posmax(i) - sizel(i,0))
      enddo


      return
      end
