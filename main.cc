#define MAIN
#include "global.h"
#include "f77binding.h"
#include "myedge.h"
#include "RBtree.h"
#include <math.h>
#include <stdlib.h>
//#include "vtr.h"
#include "utility.h"
#include "globalVar.h"
#include "string.h"	
#include "geometry.h"
using namespace std;
#include <omp.h>

const Integer DUMMY_MODE 	=  0;
const Integer WORK_MODE		=  1;
const Integer POST_PROCESS_MODE	= -1;

// #define externalEdge
const Integer MAXRULE = 11;
const Integer MAXGRID = 64;

bool directGEOMETRY   = false;

typedef long long int Int;

int main(int argc, char* argv[])
{
  cout << "CFIE-DG for Multiple Backscattering of Non-Penetrable Target" << endl;
  cout << "Author: Z.Peng, K-H.LIM and J-F.Lee" << endl;  
  cout << "Version R1 built in " << __DATE__ << " " << __TIME__ << endl << flush;
  cout << endl;

  Int rc;
  fstream foo;
  Integer* count = NULL;
  float eps = 0.01;
  char* prjName = argv[1];
//	freq = atof(argv[2]) / 1000;
  directRHS = 0;
  Int firstTime = 0; //0: first time; 1: not first time

  if (argc < 2) {
    showUsage();
    return 0;
  }

  string inParamName = string(prjName) + ".in";
  rc = readInParam (freq, eps, inParamName);
  string input;
  bool inFileExist = false;
  bool useInParam = false;
  char tChar;

  if (rc == true) {
    while (true) {
      cout << inParamName << " exist. Use it? (Y/N): ";
//       getline(cin, input);
      input = 'Y';
      if (input.length() == 1) {
        tChar = input[0];
        tChar = toupper(tChar);
        if (tChar == 'Y') {
          useInParam= true;  
          break;
        }
        else if (tChar == 'N') {
          useInParam = false;
          break;
        }    
      }
      cout << "Invalid entry" << endl;
    }
  }
 
  if ((rc == false) || ((rc == true) && (useInParam == false)))
  {
    float thetaStart, thetaEnd, thetaStep;
    float phiStart, phiEnd, phiStep;
    float Einc_mag, Einc_phs, polAng;
    bool outCurJ; 
 //   float t1, t2, dt, p1, p2, dp, et1, et2, ep1, ep2;
 //   bool ww;

    userInput(freq, thetaStart, thetaEnd, thetaStep, phiStart, phiEnd, phiStep, 
              Einc_mag, Einc_phs, polAng, eps, outCurJ);    
    
    writeInParam(freq, thetaStart, thetaEnd, thetaStep, phiStart, phiEnd, phiStep, 
                 Einc_mag, Einc_phs, polAng, eps, outCurJ, prjName);    
  }
  

        
        freq = freq / 1000;

//	if (argc >= 4) {
//	  eps = atof(argv[3]);
//	}

	mvmodelong = 1;
	
//	if (argc >= 5) {
//	  mvmodelong = atoi(argv[4]);
//	}

//	if (argc >= 6) {
//	  firstTime = atoi(argv[5]);
 //	  cout << "*********\n firstTime= " << firstTime << "!\n";
//	}
	
	char buf[0x200];
	Int i,j,k;
	Integer mode   = WORK_MODE;
	bool dummyMode = false;
	
	if (directGEOMETRY) {
  	  cout << "DIRECT GEOMETRY MODE IS ENABLE!\n";
	}
	
	if (freq == 0.0) {
	  mode = DUMMY_MODE;
	  wl0 = 1.0;
	  cout << "Dummy Mode enabled, zero result will be outputed.!!\n" << flush;
	}

	else if (freq < 0) {
	  mode = POST_PROCESS_MODE;
	  cout << "POST_PROCESS_MODE Mode enabled\n" << endl << flush;
	}

	else {
	  mode = WORK_MODE;
	  wl0  = 0.3 / freq; //wavelength
  	  cout << "Wavelength in freespace: " << wl0 << endl;
	}
	
	cout << "MV mode" << mvmodelong << endl;
	if (directRHS == 1)
	  cout << "Direct RHS = 1!\n";
	else if (directRHS == 2)
	  cout << "Direct RHS = 2!\n";
	else
	  cout << "Direct RHS = ?!\n";

	Integer maxpatch = 0;
	Integer maxnode  = 0;
	Integer maxedge  = 0;

	xyznodep = NULL;
	iedgep   = NULL;
	ipatpntp = NULL;	
	edgep    = NULL;
	familyp  = NULL;
	xyzctrp  = NULL;
	paerap   = NULL;
	xyznormp = NULL;
	ngridp   = NULL;	
	vt1p     = NULL;
	vt2p     = NULL;
	vt3p     = NULL;
	wtp      = NULL;

	Integer* triFlag         = NULL;
	Integer* boundaryTriFlag = NULL;
	Integer* contourMap      = NULL;
	triSurf mesh;

	if (!directGEOMETRY) {
	  mesh.setName(prjName);
	  mesh.importFromTriFileById(prjName);
  	  mesh.setFem(false);
	  mesh.setSO(false);

#ifndef COMPLETE
 	  maxnode  = mesh.getnNodeFO();
	  maxedge  = mesh.getnEdgeFO();
	  maxpatch = mesh.getnTriFO();
 	  xyznodep = new Real[3* maxnode];
	  iedgep   = new Integer[4*maxedge]; // Edge array containing the 2 nodeID and 2 triID associated with the each edge
	  ipatpntp = new Integer[3*maxpatch];// Tri array containing the 3 nodeID associated with each tri
	  triFlag  = new Integer[maxpatch];
	  mesh.exportXYZNODE(xyznodep);
	  mesh.exportEDGE(iedgep,1,1);
	  mesh.exportTRI(ipatpntp,1);
	  mesh.exportFlag(triFlag);
#endif
	  Int nContourEdge = 0;
	  std::set<Int> contourEdgeIndex;
	  contourEdgeIndex.clear();

#ifdef COMPLETE
	  maxnode  = mesh.getnNodeFO();
	  maxedge  = 3 * mesh.getnTriFO();
	  maxpatch = mesh.getnTriFO();
	  xyznodep = new Real[3* maxnode];
	  iedgep   = new Integer[4*maxedge];
	  Integer nEdge    = mesh.getnEdgeFO();
	  Integer* tmpEdge = new Integer[nEdge*4];
	  ipatpntp = new Integer[3*maxpatch];
	  triFlag  = new Integer[maxpatch];
	  mesh.exportXYZNODE(xyznodep);
	  mesh.exportTRI(ipatpntp,1);
	  mesh.exportFlag(triFlag);
	  mesh.exportEDGE(tmpEdge,1,1);
	  for (Int i = 0; i < maxpatch; i++) {
	    for ( Int k = 0; k < 3; k++) {
	      Int offset = (i * 3 + k) * 4;
	      iedgep[offset + 0] = ipatpntp [i * 3 + k];
	      iedgep[offset + 1] = ipatpntp [i * 3 + (k + 1) % 3];
	      iedgep[offset + 2] = i + 1;
	      iedgep[offset + 3] = -1;
	    }
	  }
#endif

#ifdef TWO_HALVES
	  printf("***************\nContour!\n****************\n");
	  for (Int i = 0; i < maxedge; i++) {
	    Int nd1 = iedgep[i * 4] - 1;
	    Int nd2 = iedgep[i*4+1]-1;
	    Real y1 = xyznodep[nd1 * 3 + 1];
	    Real y2 = xyznodep[nd2 * 3 + 1];
	    Real z1 = xyznodep[nd1 * 3 + 2];
	    Real z2 = xyznodep[nd2 * 3 + 2];
	    Real x1 = xyznodep[nd1 * 3];
	    Real x2 = xyznodep[nd2 * 3];
	    if (fabs(y1) < 0.001 && fabs(y2) < 0.001) {
	      contourEdgeIndex.insert(i);
	    }
	  }
		
	  nContourEdge = contourEdgeIndex.size();
	  contourMap   = new Integer [maxedge+nContourEdge];
	  for (Int i = 0; i < maxedge + nContourEdge; i++)
	    contourMap[i]=-1;
			
 	  if (nContourEdge > 0) {
	    printf("Contour Edges: %d\n", nContourEdge);
	    Integer* newedgep = new Integer[4 * maxedge + 4 * nContourEdge];
	    printf("Move from old iedgep to newedgep %d\n", maxedge);
	    for (Int i = 0; i < maxedge; i++) {
	      std::set<Int>::iterator it= contourEdgeIndex.find(i);
	      if (it == contourEdgeIndex.end()) {
	        for (Int k = 0; k < 4; k++)
		  newedgep[i * 4 + k] = iedgep[i * 4 + k];
	      }
	      else {
	        newedgep[i * 4]     = iedgep[i * 4];
		newedgep[i * 4 + 1] = iedgep[i * 4 + 1];
		newedgep[i * 4 + 2] = iedgep[i * 4 + 2];
		newedgep[i * 4 + 3] = -1;
	      }
  	    }
	    printf("Generate new edge\n");
	    Integer cntContour = 0;
	    for (std::set<Int>::iterator it = contourEdgeIndex.begin(); it!=contourEdgeIndex.end(); it++) {
	      newedgep[(cntContour + maxedge) * 4    ] = iedgep[(*it) * 4 + 1];
	      newedgep[(cntContour + maxedge) * 4 + 1] = iedgep[(*it) * 4];
	      newedgep[(cntContour + maxedge) * 4 + 2] = iedgep[(*it) * 4  + 3];
	      newedgep[(cntContour + maxedge) * 4 + 3] = -1;
	      contourMap[cntContour + maxedge] = *it + 1; //Comply Fortran tradition
	      contourMap[*it] = cntContour + maxedge + 1; //Comply Fortran tradition
	      cntContour++;
	    }
	    delete[] iedgep;
	    iedgep = newedgep;
	    maxedge += nContourEdge;
	    printf("Finished\n");
			
	  } // end if (nContourEdge > 0)
	  else {
	  }
// 		ofstream ctrMap("contourMap");
// 		for(int i=0;i<maxedge;i++){
// 			ctrMap<<i<<"\t"<<contourMap[i]<<"\n";
// 		}
// 		ctrMap.close();

#endif
			

#ifdef WRITEOUT
		FILE* fpp=fopen("debug_mesh_node","w");
		for(Int i=0;i<maxnode;i++){
			fprintf(fpp,"Node %d\t%f\t%f\t%f\n",i+1,xyznodep[3*i],xyznodep[3*i+1],xyznodep[3*i+2]);
		}
		fclose(fpp);
		fpp=fopen("debug_mesh_edge","w");
		for(Int i=0;i<maxedge;i++){
			fprintf(fpp,"edge %d\t%d\t%d\t%d\t%d\n",i+1,iedgep[i*4],iedgep[i*4+1],iedgep[i*4+2],iedgep[i*4+3]);
		}
		fclose(fpp);
		fpp=fopen("debug_mesh_tri","w");
		for(Int i=0;i<maxedge;i++){
			fprintf(fpp,"triangle %d\t%d\t%d\t%d\n",i+1,ipatpntp[i*3],ipatpntp[i*3+1],ipatpntp[i*3+2]);
		}
		fclose(fpp);		
#endif
	}
	else{
		rc= ImportTrianglesDIRECT(prjName, maxnode,&xyznodep, maxpatch, &ipatpntp, maxedge, &iedgep,&triFlag);
	}
	
/*
	Int rc1=0,rc2=0,rc1_sc=0,rc2_sc=0;
	double sumE=0.0,sumH=0.0;
	cout<<"DirectGEOMERY is not enabled!\n";
	Einc=new Complex[9*maxpatch];
	Hinc=new Complex[9*maxpatch];
	Complex* Einc_SC=new Complex[9*maxpatch];
	Complex* Hinc_SC=new Complex[9*maxpatch];
	sprintf(buf,"%s.curE",prjName);
	rc1= readInCurJBin(buf,Einc,maxpatch*9);
	sprintf(buf,"%s.SC.curE",prjName);
	rc1_sc= readInCurJBin(buf,Einc_SC,maxpatch*9);
	
	sprintf(buf,"%s.curH",prjName);
	rc2= readInCurJBin(buf,Hinc,maxpatch*9);
	sprintf(buf,"%s.SC.curH",prjName);
	rc2_sc= readInCurJBin(buf,Hinc_SC,maxpatch*9);
	
//		if(rc1_sc==0){ //if SC field is available
	if(firstTime==0) {//First time
	    for(Int i=0;i<maxpatch;i++){
		if(triFlag[i]!=0){ //For touching surface, add it with the self coupling field.
		    for(Int j=0;j<9;j++){		
			Einc[i*9+j]=0.0; 
			Hinc[i*9+j]=0.0;
		    }
		}
	    }
	}
	else
	{
	     for(Int i=0;i<maxpatch;i++){

		if(triFlag[i]!=0){ //For touching surface, add it with the self coupling field.  
		    for(Int j=0;j<9;j++){		
// 			Einc[i*9+j]= (Einc[i*9+j])*((Complex)(1.0)) - (Einc_SC[i*9+j] ) *((Complex)(1));
			
// 			Einc[i*9+j]=  (Einc_SC[i*9+j] )*Complex(1.0) ;
// 			Hinc[i*9+j]=  (Hinc_SC[i*9+j] )*Complex(1.0);
			Einc[i*9+j]=  (Einc_SC[i*9+j] )*Complex(-1.0) ;
			Hinc[i*9+j]=  Hinc[i*9+j]*Complex(0.5) +(Hinc_SC[i*9+j] )*Complex(-1.0);
		    }
		}else {
			for(Int j=0;j<9;j++){
			Einc[i*9+j]=  (Einc[i*9+j] )*Complex(1.0) ;
			Hinc[i*9+j]=  (Hinc[i*9+j] )*Complex(1.0);
			}
                }

	    }
	}
	delete[] Einc_SC;
	delete[] Hinc_SC;
	    ////////////////////////////////////
	    //The incident field is calculated in each triangles.

	sumE=0.0;sumH=0.0;
	for(Int i=0;i<9*maxpatch;i++){
		sumE+= abs(Einc[i])*abs(Einc[i]);
		sumH+= abs(Hinc[i])*abs(Hinc[i]);
	}

	  
	if(directRHS!=1&&mode!=POST_PROCESS_MODE&& (sumE<1.0e-20 && sumH<1.0e-20)  ) 
	{
	cout<<"Incident Field energy is less than 1.e-10, dummy Mode enabled\n";
	mode=DUMMY_MODE;
	}


          ////////////////////////////////////
	if(mode==DUMMY_MODE)
	{
		fstream foo1;
		cout<<"Dummy Mode, generate dummy solution\n";
		sprintf(buf,"%s.xvtr",prjName);
		foo1.open(buf,ios::out|ios::binary);
		foo1.write((char*)(&maxedge),sizeof(Integer));
		InputComplex a=InputComplex(0.0);
		for(i=0;i<maxedge;i++)
			foo1.write((char*)(&a),sizeof(InputComplex));
		foo1.close(); 
			
		sprintf(buf,"%s.curJ",prjName);
		foo1.open(buf,ios::out);
		for(i=0;i<maxpatch;i++)
		{
			for(j=0;j<9;j++)
				foo1<<"0.0 0.0\n";
			foo1<<endl;
		}
		foo1.close();
			
		cout<<"dummy solution outputed\n";
		delete[]xyznodep;
		delete[]ipatpntp;
	
	
		exit(0);
	} //if(mode==DUMMY_MODE)
	else if(mode==POST_PROCESS_MODE)
	{
		//
			fstream foo1;
			sprintf(buf,"%s.xvtr",prjName);
			foo1.open(buf,ios::in|ios::binary);
			Integer dimXvtr;
			foo1.read((char*)(&dimXvtr),sizeof(Integer));
				
			assert(dimXvtr==maxedge);
			InputComplex* tempXvtr=new InputComplex[dimXvtr];
			foo1.read((char*)(tempXvtr),sizeof(InputComplex)*dimXvtr);
			foo1.close();
	
			Complex* Jsca=new Complex[maxpatch*9];
			count=new Integer[maxpatch*3];
			Real sign=-1.0;
			cout<<"Attention: the current is NOT averaged in the node, which will perserve the accuracy\n";
	
			for(i=0;i<maxpatch*9;i++)
				Jsca[i]=0.0;
			for(i=0;i<maxpatch*3;i++)
				count[i]=0;
			for(i=0;i<maxedge;i++){
				Integer e1=iedgep[4*i+2]-1;
// 				Real AREA=paerap[e1]*2.0;
				Real AREA=getArea(xyznodep,&(ipatpntp[e1*3]))*2.0;
				Integer n3=ipatpntp[e1*3]+ipatpntp[e1*3+1]+ipatpntp[e1*3+2]-iedgep[4*i]-iedgep[4*i+1]-1;
				Integer n1=iedgep[4*i]-1;
				Integer n2=iedgep[4*i+1]-1;
				Integer in1=findI(n1+1,&(ipatpntp[e1*3]));assert (in1!=-1);
				Integer in2=findI(n2+1,&(ipatpntp[e1*3]));assert (in2!=-1);
				Integer in3=findI(n3+1,&(ipatpntp[e1*3]));assert (in3!=-1);
				Jsca[9*e1+in1*3]+=crhsp[i]*(xyznodep[n3*3]-xyznodep[n1*3])/AREA*sign*edgep[i];
				Jsca[9*e1+in1*3+1]+=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n1*3+1])/AREA*sign*edgep[i];
				Jsca[9*e1+in1*3+2]+=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n1*3+2])/AREA*sign*edgep[i];
				Jsca[9*e1+in2*3]+=crhsp[i]*(xyznodep[n3*3]-xyznodep[n2*3])/AREA*sign*edgep[i];
				Jsca[9*e1+in2*3+1]+=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n2*3+1])/AREA*sign*edgep[i];
				Jsca[9*e1+in2*3+2]+=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n2*3+2])/AREA*sign*edgep[i];  
				count[3*e1+in1]++;
				count[3*e1+in2]++;
				Integer e2=iedgep[4*i+3]-1;
// 				AREA=paerap[e2]*2.0;
				AREA=getArea(xyznodep,&(ipatpntp[e2*3]))*2.0;
				n3=ipatpntp[e2*3]+ipatpntp[e2*3+1]+ipatpntp[e2*3+2]-iedgep[4*i]-iedgep[4*i+1]-1;
				in3=findI(n3+1,&(ipatpntp[e2*3]));assert (in3!=-1);
				in1=findI(n1+1,&(ipatpntp[e2*3]));assert (in1!=-1);
				in2=findI(n2+1,&(ipatpntp[e2*3]));assert (in2!=-1);
		
				Jsca[9*e2+in1*3]-=crhsp[i]*(xyznodep[n3*3]-xyznodep[n1*3])/AREA*sign*edgep[i];
				Jsca[9*e2+in1*3+1]-=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n1*3+1])/AREA*sign*edgep[i];
				Jsca[9*e2+in1*3+2]-=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n1*3+2])/AREA*sign*edgep[i];
				Jsca[9*e2+in2*3]-=crhsp[i]*(xyznodep[n3*3]-xyznodep[n2*3])/AREA*sign*edgep[i];
				Jsca[9*e2+in2*3+1]-=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n2*3+1])/AREA*sign*edgep[i];
				Jsca[9*e2+in2*3+2]-=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n2*3+2])/AREA*sign*edgep[i];          
				count[3*e2+in1]++;
				count[3*e2+in2]++;    
			}
			sprintf(buf,"%s.curJ",prjName);
			foo.open(buf,ios::out);
			if(!foo.good())
				cout<<"output curJ file error!\n";
			for(i=0;i<maxpatch;i++)	{
				for(j=0;j<9;j++){
					foo<<Jsca[i*9+j].real()<<" "<<Jsca[i*9+j].imag()<<endl;
				}
				foo<<endl;
			}
		
		foo.close();
		
		delete[] Jsca;
		delete[]count;
		delete[] tempXvtr;

		delete[] iedgep;
		delete[]xyznodep;
		delete[]ipatpntp;
		delete[]edgep;
//		delete[] Einc;
//		delete[] Hinc;
		
		return 0;
	} //end else if(mode==POST_PROCESS_MODE)
*/	
	

	
       //So far the node/iedge/patch are generated.
	//Generate the memory space,
	edgep	=new Real[maxedge];
	familyp	=new Integer[maxedge];
	xyzctrp	=new Real[maxpatch*3];
	paerap	=new Real[maxpatch];
	xyznormp=new Real[maxpatch*3];
	
	
	crhsp	=new Complex[maxedge];
	ngridp	=new Integer[MAXRULE];
	
	vt1p	=new Real[MAXRULE*MAXGRID];
	vt2p	=new Real[MAXRULE*MAXGRID];
	vt3p	=new Real[MAXRULE*MAXGRID];
	wtp	=new Real[MAXRULE*MAXGRID];
	
	//checksize:
	Real wl0m1=1.0/wl0;
	Real wl0m2=wl0m1*wl0m1;
	Real rmax=0.0;
	Real dmin=1.e9;
	Real ravg=0.0;
	for(i=0;i<maxedge;i++){
		Integer ii=iedgep[i*4];
		Integer jj=iedgep[i*4+1];
		Real X[3];
		X[0]=xyznodep[3*(ii-1)]-xyznodep[3*(jj-1)];
		X[1]=xyznodep[1+3*(ii-1)]-xyznodep[1+3*(jj-1)];
		X[2]=xyznodep[2+3*(ii-1)]-xyznodep[2+3*(jj-1)];
		Real rl=sqrt(X[0]*X[0]+X[1]*X[1]+X[2]*X[2]);
		if(rl>rmax) rmax=rl;
		if(rl<dmin) dmin=rl;
		ravg+=rl;
	}
	Real edgeavrg=ravg*wl0m1/((Real)(maxedge));
	Real edgelong=rmax*wl0m1;
	
	Real rlengmax=0.0;
	Real posMin,posMax;
	for(i=0;i<3;i++)
	{
		posMax=xyznodep[i+3*(ipatpntp[0,0]-1)];
		posMin=posMax;
		for(j=0;j<maxpatch;j++)
		{
			for(k=0;k<3;k++)
			{
				Integer ip=ipatpntp[k+j*3]-1;
				posMax=posMax>xyznodep[i+ip*3]?posMax:xyznodep[i+ip*3];
				posMin=posMin<xyznodep[i+ip*3]?posMin:xyznodep[i+ip*3];
			}
		}
		rlengmax=rlengmax>(posMax-posMin)?rlengmax:(posMax-posMin);
	}
	rlengmax*=wl0m1;

	Integer lmax=0;
	while(rlengmax>0.5)
	{
		lmax++;
		rlengmax*=0.5;
	}
	if(lmax==0) 
		lmax=1;
	if(lmax==1)
		cout<<"1 level, Full matrix is used\n";
	else
		cout<<lmax<<" level,\n";
	
	//bld_lmax
	igrsp=new Integer[lmax+2];
	lxyzp=new Integer[3*(1+lmax)];
	modesp=new Integer[lmax];
	sizelp=new Real[3*(1+lmax)];
	lrsmupp=new Integer [1+lmax];
	lrsmdownp=new Integer[1+lmax];
	lrstlp=new Integer[1+lmax];
	kpstartp=new Integer[lmax];
	
	memory_tot=0.0;
	memory_tot+=((Real)maxedge)*(4*4+4+4);
	memory_tot+=((Real)maxpatch)*(3*4+3*4+4+3*4);
	memory_tot+=((Real)maxnode)*(3*4);
	memory_tot+=((Real)MAXRULE)*(4);
	memory_tot+=((Real)(MAXGRID*MAXRULE))*(4*4);
	
	memory_tot+=((Real)(lmax))*(2*4);
	memory_tot+=((Real)(lmax+2))*4;
	memory_tot+=((Real)(lmax+1))*(3*4+3*4+3*4);
/*	
	if(false&& directRHS!=0){
		cout<<"Load External RHS!\n";
		sprintf(buf,"%s.RHS",prjName);
		ifstream  ftmp(buf,ios::binary);
		ftmp.read((char*)crhsp,sizeof(Complex)*maxedge);
		ftmp.close();
#ifdef OLDIMPLEMENTATION		
		Int rr=0;
		Complex* crhs1=new Complex[maxedge];
		Complex* crhs2=new Complex[maxedge];
		sprintf(buf,"%s_sc.rhs",prjName);
		rr=readInCurJBin(buf,crhs1,maxedge);
		if(rr!=0){
			cout<<"Error! external rhs: "<<buf<<" is not available!\n";
			exit(0);
		}
		sprintf(buf,"%s_SC.rhs",prjName);
		rr=readInCurJBin(buf,crhs2,maxedge);
		for(Int i=0;i<maxedge;i++){
			Int flag0=triFlag[iedgep[i*4+2]-1];
			Int flag1=triFlag[iedgep[i*4+3]-1];

			crhsp[i]=Complex(0.0);

			if(flag0==0 && flag1==0){
				crhsp[i]=-crhs1[i]; //_sc,rhs
			}

			if(flag0!=0 && flag1!=0){
				crhsp[i]=crhs2[i]; //_SC.rhs
			}

//				crhsp[i]=-crhs1[i];

		}
		delete[] crhs2;
		delete[] crhs1;
#endif
          }
*/ 
	
  string fileName;
  ofstream fout;
  fileName = "info";
  fout.open(fileName.c_str(),ios::out);  
  if (fout.fail()) {
    cerr << "Error opening " << fileName << " for writing!" <<endl;
    exit (1);
  }

  fout << prjName << endl;
  fout.close();

        //Call the wrapper
	cout<<"Call MLFMA\n";

	//if(mvmodelong==1) //if reading mode
	//{
	//	char buf[0x200];
	//	bool rc=true;
	//	sprintf(buf,"%s.FileamnData",prjName);
	//	rc &= testFileExisting(buf,true);
	//	sprintf(buf,"%s.FileamnIndex",prjName);
	//	rc &= testFileExisting(buf,false);
	//	sprintf(buf,"%s.Filesndgrp",prjName);
	//	rc &= testFileExisting(buf,false);
		//sprintf(buf,"%s.Filetl",prjName);
		//rc &= testFileExisting(buf,false);
	//	sprintf(buf,"%s.Filevfs",prjName);
	//	rc &= testFileExisting(buf,false);
	//	if(!rc){
	//		cout<<"MLFMA data file is missing, check the files. Writing mode enforced!\n";
	//		mvmodelong=0;
	//	}
	//}
	//if(mvmodelong==1) //reading
	//{
	//	char buf[0x200];
	//	cout<<"Moving the temporary file...";
	//	sprintf(buf,"mv %s.FileamnData FileamnData ",prjName);
	//	runCommand(buf);
	//	sprintf(buf,"mv %s.FileamnIndex FileamnIndex ",prjName);
	//	runCommand(buf);
	//	sprintf(buf,"mv %s.Filesndgrp  Filesndgrp ",prjName);
	//	runCommand(buf);
		//sprintf(buf,"mv %s.Filetl Filetl ",prjName);
		//runCommand(buf);
	//	sprintf(buf,"mv %s.Filevfs Filevfs ",prjName);
	//	runCommand(buf);
	//	cout<<"Finished.";
	//}

	mvmodelong = 0; //always generate the MLFMA data, 1 = read existing MLFMA data

	main_box_(&mvmodelong, &firstTime,&freq,&maxnode,&maxpatch,&maxedge,&memory_tot, &lmax,
		 iedgep, edgep, ipatpntp, xyzctrp,
		 paerap, xyznormp,xyznodep,
		 ngridp, vt1p, vt2p, vt3p, wtp, igrsp, lxyzp,
		 modesp, sizelp,
		 lrsmupp, lrsmdownp, lrstlp, familyp, kpstartp,crhsp,&eps,contourMap);

		//cout<<"Saving the temporary file...";
		//sprintf(buf,"mv FileamnData %s.FileamnData",prjName);
		//runCommand(buf);
		//sprintf(buf,"mv FileamnIndex %s.FileamnIndex",prjName);
		//runCommand(buf);
		//sprintf(buf,"mv Filesndgrp %s.Filesndgrp",prjName);
		//runCommand(buf);
		//sprintf(buf,"mv Filetl %s.Filetl",prjName);
		//runCommand(buf);
		//sprintf(buf,"mv Filevfs %s.Filevfs",prjName);
		//runCommand(buf);
		//cout<<"Finished.\n";

      //InputComplex* tempXvtr=new InputComplex[maxedge];
      //for(i=0;i<maxedge;i++)
      //      tempXvtr[i]=((InputComplex)(crhsp[i]));
      //cout<<"Exporting xvtr file\n";
      //sprintf(buf,"%s.xvtr",prjName);
      //foo.open(buf,ios::out|ios::binary);
      //foo.write((char*)(&maxedge),sizeof(Integer));
      //foo.write((char*)tempXvtr,sizeof(InputComplex)*maxedge);
      //foo.close();	
      
      //  delete[] tempXvtr;
	  
      Complex* Jsca=new Complex[maxpatch*9];
      count=new Integer[maxpatch*3];
      Real sign=-1.0;
      cout<<"Attention: the current is NOT averaged in the node, which will perserve the accuracy\n";
      		//Calculate the current on the n1 node
		//Here, in1 is the mapping from n1 (the first node in the edge) to the local id in its patch
		//AREA=1.0;
      for(i=0;i<maxpatch*9;i++)
	      Jsca[i]=0.0;
      for(i=0;i<maxpatch*3;i++)
	      count[i]=0;
      for(i=0;i<maxedge;i++)
      {
		Integer in1,in2,in3,n1,n2,n3;
	      Integer e1=iedgep[4*i+2]-1;
		Integer e2=iedgep[4*i+3]-1;
	      Real AREA=2.0*paerap[e1];
	      if(e1>=0){
		n3=ipatpntp[e1*3]+ipatpntp[e1*3+1]+ipatpntp[e1*3+2]-iedgep[4*i]-iedgep[4*i+1]-1;
	      n1=iedgep[4*i]-1;
	      n2=iedgep[4*i+1]-1;
		////map the three node to the local nodes
	      in1=findI(n1+1,&(ipatpntp[e1*3]));assert (in1!=-1);
	      in2=findI(n2+1,&(ipatpntp[e1*3]));assert (in2!=-1);
	      in3=findI(n3+1,&(ipatpntp[e1*3]));assert (in3!=-1);
                
            Jsca[9*e1+in1*3]+=crhsp[i]*(xyznodep[n3*3]-xyznodep[n1*3])/AREA*sign*edgep[i];
	      Jsca[9*e1+in1*3+1]+=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n1*3+1])/AREA*sign*edgep[i];
	      Jsca[9*e1+in1*3+2]+=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n1*3+2])/AREA*sign*edgep[i];
	      Jsca[9*e1+in2*3]+=crhsp[i]*(xyznodep[n3*3]-xyznodep[n2*3])/AREA*sign*edgep[i];
	      Jsca[9*e1+in2*3+1]+=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n2*3+1])/AREA*sign*edgep[i];
	      Jsca[9*e1+in2*3+2]+=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n2*3+2])/AREA*sign*edgep[i];	
	      count[3*e1+in1]++;
	      count[3*e1+in2]++;
		}

	      if(e2>=0){
	      AREA=2.0*paerap[e2];
                //AREA=1.0;
	      n3=ipatpntp[e2*3]+ipatpntp[e2*3+1]+ipatpntp[e2*3+2]-iedgep[4*i]-iedgep[4*i+1]-1;
		in3=findI(n3+1,&(ipatpntp[e2*3]));assert (in3!=-1);
	      in1=findI(n1+1,&(ipatpntp[e2*3]));assert (in1!=-1);
	      in2=findI(n2+1,&(ipatpntp[e2*3]));assert (in2!=-1);

	      Jsca[9*e2+in1*3]-=crhsp[i]*(xyznodep[n3*3]-xyznodep[n1*3])/AREA*sign*edgep[i];
	      Jsca[9*e2+in1*3+1]-=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n1*3+1])/AREA*sign*edgep[i];
	      Jsca[9*e2+in1*3+2]-=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n1*3+2])/AREA*sign*edgep[i];
	      Jsca[9*e2+in2*3]-=crhsp[i]*(xyznodep[n3*3]-xyznodep[n2*3])/AREA*sign*edgep[i];
	      Jsca[9*e2+in2*3+1]-=crhsp[i]*(xyznodep[n3*3+1]-xyznodep[n2*3+1])/AREA*sign*edgep[i];
	      Jsca[9*e2+in2*3+2]-=crhsp[i]*(xyznodep[n3*3+2]-xyznodep[n2*3+2])/AREA*sign*edgep[i];		
	      count[3*e2+in1]++;
	      count[3*e2+in2]++;    
		}
      }


      sprintf(buf,"%s.curJ",prjName);
      foo.open(buf,ios::out);
      if(!foo.good())
	      cout<<"output curJ file error!\n";
      for(i=0;i<maxpatch;i++)
      {
              
	      for(j=0;j<9;j++)
	      {
		      foo<<Jsca[i*9+j].real()<<" "<<Jsca[i*9+j].imag()<<endl;
	      }
	      foo<<endl;
      }
      foo.close();

	if(directGEOMETRY){
		sprintf(buf,"%s.TRI",prjName);
		ofstream ftriangle(buf);
		ftriangle.precision(10);
		ftriangle<<1.0<<endl;
		ftriangle<<maxnode<<endl;
		for(Int i=0;i<maxnode;i++){
			ftriangle<<scientific<<xyznodep[i*3]<<"\t"<<xyznodep[i*3+1]<<"\t"<<xyznodep[i*3+2]<<"\n";
		}
		ftriangle<<maxpatch<<endl;
		for(Int i=0;i<maxpatch;i++){
			ftriangle<<ipatpntp[i*3]-1<<"\t"<<ipatpntp[i*3+1]-1<<"\t"<<ipatpntp[i*3+2]-1<<"\n";
		}
		ftriangle.close();
	}


      delete[] contourMap;
//       iedgeTree.freeMemory();
	delete[] triFlag;
// 	delete[] boundaryTriFlag;
	delete[]Jsca;
	delete[]count;

	delete[] iedgep;
	delete[]xyznodep;
	delete[]ipatpntp;
	
	delete[] edgep;
	delete[] familyp;
	delete[] xyzctrp;
	delete[] paerap;
	delete[] xyznormp;
	delete[] ngridp;
	delete[] vt1p;
	delete[] vt2p;
	delete[] vt3p;	
	delete[] wtp;
	
	delete[] igrsp;
	delete[] lxyzp;
	delete[] modesp;
	delete[] sizelp;
	delete[] lrsmupp;
	delete[] lrsmdownp;
	delete[] lrstlp;
	
	delete[] kpstartp;
//	if(directRHS!=1){
//	delete[] Einc;
//	delete[] Hinc;
//	}
	delete[] crhsp;
}


void crossProd(Complex* input, const vtr norm, Complex* output)
{
        //input x norm
	Complex temp[3];
	double tmp[3];
	tmp[0]=norm.getx();
	tmp[1]=norm.gety();
	tmp[2]=norm.getz();
	for(Int i=0;i<3;i++)
		temp[i]=(input[(i+1)%3]) * (Complex)(tmp[(i+2)%3]) - (input[(i+2)%3]) * (Complex)(tmp[(i+1)%3]);
	for(Int i=0;i<3;i++)
		output[i]=temp[i];
}
void crossProd(const vtr norm, Complex* input, Complex* output)
{
        //input x norm
	Complex temp[3];
	double tmp[3];
	tmp[0]=norm.getx();
	tmp[1]=norm.gety();
	tmp[2]=norm.getz();
	for(Int i=0;i<3;i++)
		temp[i]=(input[(i+2)%3]) * (Complex)(tmp[(i+1)%3]) - (input[(i+1)%3]) * (Complex)(tmp[(i+2)%3]) ;
	for(Int i=0;i<3;i++)
		output[i]=temp[i];
}
