//
//          Basic vector class (Complex precision)
//
// modified from: Sparselib_1_5d/mv/src/mvvt.cc
//                Sparselib_1_5d/mv/src/mvblast.cc
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include "myvector.h"
#include <vector>
#include <string.h>

#include <functional>
#include <numeric>

using namespace std;

Vector::Vector()  : p_(0), dim_(0) {};

Vector::Vector(Unsigned n)
{
  dim_ = n;

  if (n == 0) p_ = 0;
  else p_ = new Complex[n];

  Complex tmp( 0.0 , 0.0 );
#pragma omp parallel for schedule(static)
  for ( Unsigned i = 0; i < n; i++ )
    p_[i] = tmp;
}

Vector::Vector(Int n, float val )
{
  dim_ = n;
  if (n == 0) p_ = 0;
  else p_ = new Complex[n];

  Complex tmp( val, 0.0 );
#pragma omp parallel for schedule(static)
  for ( Int i = 0; i < n; i ++ )
    p_[i] = tmp;
}

Vector::Vector(Unsigned n, const Complex& v)
{
  dim_ = n;
  if (n == 0) p_ = 0;
  else p_ = new Complex[n];

#pragma omp parallel for schedule(static)
  for (Unsigned i = 0; i < n; i ++)
    p_[i] = v;
}

Vector::Vector(const Vector & m)
{
  dim_ = m.dim_;
  if (m.dim_ == 0) p_ = 0;
  else p_ = new Complex[m.dim_];

  Int N = m.dim_;

#pragma omp parallel for schedule(static)
  for (Int i = 0; i < N; i ++)
    p_[i] = m.p_[i];
}

Vector::Vector(Unsigned n, const Complex* data)
{
  dim_ = n;
  if (n == 0) p_ = 0;
  else p_ = new Complex[n];

  memcpy(p_, data, n*sizeof(Complex));
}


void Vector::setSize( Int dim )
{
  if (p_) delete [] p_;
  if (dim == 0) p_ = 0;
  else p_ = new Complex [dim];

  if (p_ == 0) {
    cerr << "Error: 0 pointer in Vector.setSize(Int)" << endl;
    cerr << "       Most likely out of memory... " << endl;
    exit(1);
  }

  dim_ = (Unsigned)dim;
}

// operators and member functions
Vector& Vector::operator=(const Vector & m)
{
  Int N = m.dim_;
  Int i;

  newsize(N);

#pragma omp parallel for schedule(static) private(i)
  // no need to test for overlap, since this region is new
  for (i = 0; i< N; i++)       // careful not to use bcopy()
    p_[i] = m.p_[i];        // here, but Complex::operator= Complex.

  return *this;
}

Vector& Vector::operator=(const Complex & m)
{
#ifdef TRACE_VEC
    cout << "> Vector::operator=(const Complex & m)  " << endl;
#endif

    // unroll loops to depth of length 4
    Int N = size();
    Int Nminus4 = N-4;
    Int i;

    for (i = 0; i < Nminus4; ) {
      p_[i++] = m;
      p_[i++] = m;
      p_[i++] = m;
      p_[i++] = m;
    }

    for (; i<N; p_[i++] = m);   // finish off last piece...

#ifdef TRACE_VEC
    cout << "< Vector::operator=(const Complex & m)  " << endl;
#endif
    return *this;
}

Vector& Vector::operator=(const float & m)
{
#ifdef TRACE_VEC
  cout << "> Vector::operator=(const float & m)  " << endl;
#endif

  Complex tmp( m, 0.0); // added

  Int N = size();
  Int i;
#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < N; i ++ ) p_[i] = tmp;

#ifdef TRACE_VEC
  cout << "< Vector::operator=(const float & m)  " << endl;
#endif
  return *this;
}

Vector& Vector::newsize(Unsigned n)
{
#ifdef TRACE_VEC
  cout << "> Vector::newsize(Unsigned n) " << endl;
#endif

  if (dim_ != n) {                   // only delete and new if
    // the size of memory is really
    if (p_) delete [] p_;           // changing, otherwise just
    if (n == 0) p_ = 0;
    else p_ = new Complex[n];              // copy in place.

    dim_ = n;
  }

#ifdef TRACE_VEC
  cout << "< Vector::newsize(Unsigned n) " << endl;
#endif

  return *this;
}

Vector::~Vector()
{
  if (p_) delete [] p_;
}

ostream&   operator<<(ostream& s, const Vector& V)
{
  Int N = V.size();

  for (Int i = 0; i < N; i++)
    s << i << "  (" << V(i).real() << ", " << V(i).imag() << ")" << endl;

  return s;
}

void Vector::conjugate()
{
#pragma omp parallel for schedule(static)
  for ( Unsigned i = 0; i < this->size(); i++ )
    p_[i] = conj(p_[i]);
}

//===================================
// DOT PRODUCT AND 2-NORM OPERATIONS
//===================================

Complex dot(const Vector &x, const Vector &y)
{
  //  Check for compatible dimensions:
  if (x.size() != y.size()) {
//       cout << "Incompatible dimensions in dot(). " << endl;
//       exit(1);
    }

const Int max_th = omp_get_max_threads();
vector<Complex> result_array(max_th);

#pragma omp parallel
  {
  Complex temp(0.0,0.0);
#pragma omp for schedule(static)
  for (Unsigned i=0; i<x.size();i++) 
  {
    temp += x(i)*y(i);
  }
  result_array.at(omp_get_thread_num()) = temp;
  }

  return accumulate(result_array.begin(), result_array.end(), Complex(0., 0.)); 
}

Complex dotHerm(const Vector &x, const Vector &y)
{
  //  Check for compatible dimensions:
  if (x.size() != y.size()) {
    cout << "Incompatible dimensions in dot(). " << endl;
    exit(1);
  }

const Int max_th = omp_get_max_threads();
vector<Complex> result_array(max_th);

#pragma omp parallel
  {
  Complex temp(0.0,0.0);
#pragma omp for schedule(static)
  for (Unsigned i = 0; i < x.size(); i++) 
  {
    temp += conj(x(i))*y(i);
  }
  result_array.at(omp_get_thread_num()) = temp;
  }

  return accumulate(result_array.begin(), result_array.end(), Complex(0., 0.));
}

// L2 norm
float norm2(const Vector &x)
{
  Complex cval = 0.0;
  cval = dotHerm(x, x);

//  for (Unsigned i = 0; i < x.size(); i++) {
//   cval += x(i)*conj(x(i));
//  }
  cval = sqrt(cval.real());

  return cval.real();
}

// Infinite norm
float normInf(const Vector &x)
{
  float d, dmax = 0.0;
  for (Unsigned i = 0; i < x.size(); i++) {
    d = abs(x(i));
    if ( d > dmax )
      dmax = abs(x(i));
  }
  return dmax;
}

// L1 norm
float norm1(const Vector &x)
{
  float sum = 0.0;
  for (Unsigned i = 0; i < x.size(); i++)  sum += abs(x(i));
  return sum;
}

//=================================
// BLAS1 OPERATIONS
//=================================

Vector& operator*=(Vector &x, const Complex &a)
{
  Int N = x.size();
  for (Int i = 0; i < N; i ++)
    x(i) *= a;
  return x;
}

Vector operator*(const Complex &a, const Vector &x)
{
  Int N = x.size();
  Vector result(N);
#pragma omp parallel for schedule(static)
  for (Int i = 0; i < N; i ++)
    result(i) = x(i) * a;
  return result;
}

Vector operator*(const Vector &x, const Complex &a)
{
    // This is the other commutative case of vector*scalar.
    // It should be just defined to be
    // "return operator*(a,x);"
    // but some compilers (e.g. Turbo C++ v.3.0) have trouble
    // determining the proper template match.  For the moment,
    // we'll just duplicate the code in the scalar * vector
    // case above.

  Int N = x.size();
  Vector result(N);

#pragma omp parallel for schedule(static)
  for (Int i = 0; i < N; i ++)
    result(i) = x(i) * a;
  return result;

}

Vector operator*(const Vector &x, const float &a)
{
  Int N = x.size();
  Vector result(N);

#pragma omp parallel for schedule(static)
  for (Int i = 0;i<N;i++)
    result(i) = x(i)*a;
  return result;

}

Vector operator+(const Vector &x, const Vector &y)
{
  Unsigned N = x.size();
  if (N != y.size()) {
	cout << "Incompatible vector lengths in +." << endl;
	cout << "N=" << N << " y.size=" << y.size() << endl;
	exit(1);
  }

  Vector result(N);

#pragma omp parallel for schedule(static)
  for (Unsigned i = 0; i < N; i ++)
    result(i) = x(i) + y(i);
  return result;
}

Vector operator-(const Vector &x, const Vector &y)
{
  Unsigned N = x.size();
  if (N != y.size()) {
	cout << "Incompatible vector lengths in -." << endl;
	cout << "N=" << N << " y.size=" << y.size() << endl;
	exit(1);
  }

  Vector result(N);

#pragma omp parallel for schedule(static)
  for (Unsigned i = 0; i < N; i ++) result(i) = x(i) - y(i);

  return result;
}

Vector& operator+=(Vector &x, const Vector &y)
{
  Unsigned N = x.size();
  if (N != y.size()) {
	cout << "Incompatible vector lengths in +=." << endl;
	cout << "N=" << N << " y.size=" << y.size() << endl;
	exit(1);
  }

#pragma omp parallel for schedule(static)
  for (Unsigned i = 0; i < N; i ++)
    x(i) += y(i);
  return x;
}

Vector& operator-=(Vector &x, const Vector &y)
{
  Unsigned N = x.size();
  if (N != y.size()) {
	cout << "Incompatible vector lengths in -=." << endl;
	cout << "N=" << N << " y.size=" << y.size() << endl;
	exit(1);
  }

#pragma omp parallel for schedule(static)
  for (Unsigned i = 0; i < N; i ++)
    x(i) -= y(i);
  return x;
}


//======================================================================
void Vector::readFromBinaryFile( char *filename, Int dim)
{
  FILE *fd;

  fd = fopen( filename, "rb");
  if (fd == 0) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  this->dim_ = (Unsigned)dim;
  if (p_) delete [] p_;
  if (dim_ == 0) p_ = 0;
  else p_ = new Complex[dim_];
  size_t ret = fread( this->p_, sizeof(Complex), dim, fd );
  fclose(fd);
}

void Vector::readFromBinaryFile( char *filename)
{
  FILE *fd;
  size_t ret;
  fd = fopen( filename, "rb");
  if (fd == 0) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  ret = fread( &(this->dim_), sizeof(Unsigned), 1, fd );
  if (p_) delete [] p_;
  if (dim_ == 0) p_ = 0;
  else p_ = new Complex[dim_];
  ret = fread( this->p_, sizeof(Complex), this->dim_, fd );
  fclose(fd);
}

void Vector::printOnBinaryFile( char *filename)
{
  FILE *foo;

  foo = fopen(filename, "wb");
  if ( foo == 0) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  //  fwrite( &(this->dim_), sizeof(Unsigned), 1, foo );
  fwrite( this->p_, sizeof(Complex), this->dim_, foo );
  fclose(foo);
}

void Vector::appendOnBinaryFile( char *filename)
{
  FILE *foo;

  foo = fopen(filename, "ab");
  if ( foo == 0) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  fwrite( &(this->dim_), sizeof(Unsigned), 1, foo );
  fwrite( this->p_, sizeof(Complex), this->dim_, foo );
  fclose(foo);
}

void Vector::readFromASCIIFile( char *filename)
{
  FILE *fd;
  int ret;
  fd = fopen( filename, "rt");
  if (fd == 0) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  ret = fscanf( fd, "%lld\n", &this->dim_);
  if (dim_ == 0) p_ = 0;
  else p_ = new Complex[dim_];
  Unsigned i;
  float real, imag;
  for ( i = 0; i < dim_; i++ ) {
    ret = fscanf( fd, "%e %e\n", &real, &imag );
    this->p_[i] = Complex(real, imag );
  }
  fclose(fd);
}

void Vector::printOnASCIIFile( char *filename)
{
  FILE *foo;

  foo = fopen(filename, "wt");
  if ( foo == 0) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }

  fprintf(foo, "%lld\n", this->dim_);
  Unsigned i;
  for ( i = 0; i < dim_; i++ ) {
    fprintf(foo, "%e  %e\n", p_[i].real(), p_[i].imag());
  }

  fclose(foo);
}


void Vector::appendOnASCIIFile( char *filename)
{
  FILE *foo;

  foo = fopen(filename, "at");
  if ( foo == 0) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }

  fprintf(foo, "%lld\n", this->dim_);
  Unsigned i;
  for ( i = 0; i < dim_; i++ ) {
    fprintf(foo, "%e  %e\n", p_[i].real(), p_[i].imag());
  }

  fclose(foo);
}
