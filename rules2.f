      subroutine rules2
     &( i1, i2, r1, r2, distmin,
     &  irulef_near, irules_near, 
     &  nearpat, irulef, irules)

      IMPLICIT NONE

c.....Input Data 

      INTEGER*8 i1, i2, irulef_near, irules_near
       REAL   r1(3), r2(3), distmin

c.....Output Data

      INTEGER*8 nearpat, irulef, irules
      
c.....Working Variables

       REAL d

c....................................................................

      nearpat = 0
      if(i1.eq.i2) then
         d=0.0
      else
         d=sqrt( (r1(1)-r2(1))**2+
     &           (r1(2)-r2(2))**2+
     &           (r1(3)-r2(3))**2  )
      end if
       
      if(d.le.distmin) then
         nearpat = 1
         irulef = irulef_near
         irules = irules_near
      end if
      return
      end
