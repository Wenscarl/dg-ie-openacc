      subroutine MTXELE_E
     &( ii, jj, ck, sing, match,
     &  irule_fld, irule_src, irulef_near, irules_near, 
     &  maxnode, maxpatch, maxedge, maxrule, maxgrid,
     &  xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &  ngrid, vt1, vt2, vt3, wt, rnear2, distmin,
     &  rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &  cnxet, cnxhx)
      use mlfma_progctrl
      IMPLICIT NONE

c.....Input Data

      INTEGER*8 ii, jj, match, irule_fld, irule_src,
     &        irulef_near, irules_near,
     &        maxnode, maxpatch, maxedge, maxrule, maxgrid
      INTEGER*8 ipatpnt(3, maxpatch), iedge(4, maxedge),
     &        ngrid(maxrule)
      COMPLEX ck, ci
       REAL   sing, rnear2, distmin, rk0, freq, wl0,
     &        rk2d4,eta0,   cnste,   cnsth
       REAL   xyznode(3, maxnode), xyzctr(3, maxpatch), 
     &        xyznorm(3, maxpatch),
     &        edge(maxedge), paera(maxpatch),
     &        vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
       REAL   pi, eps
       PARAMETER( pi=3.141592653, eps=1e-4 )

c.....Output Data

       COMPLEX cnxet, cnxhx,cnxet2,tmpvar

c.....Working Variables

      INTEGER*8 i, j, igetn3, irulef0, irules0,
     &        lfld, n1fld,  n2fld,   n3fld,   irulef,
     &        lsrc, n1src,  n2src,   n3src,   irules

       REAL signfld,  signsrc,  aera
       REAL rfld(3),  rsrc(3),  rhofld(3), rhosrc(3),
     &      rr(3),    r1(3),    r2(3),     r3(3),
     &      rhoxnt(3),rhoxnx(3), rxns(3),  anorm(3), bnorm(3),  
     &      fld2src2, dotmul,
     &      ectr(3),   pctr(3),   rhot(3), 
     &      rf,       rs,        d,         rnst

      COMPLEX cefie,cefie2,csume,csume2,cze,cze2,cfunex_new
     &        ,cfet,cfet2,cfet3,cfunex,
     &        cmfie,  csumh,   czh,   cfunhx,  cfhx,
     &        cagu,   cagu2,   cg0,   cx,      cfunexs,
     &        ck2d2,   cfunhxs,tmpp
      complex rtemp
       REAL r0(3),  rhod(3), rhoxd(3), rho0(3), ri1p(3), ri3p(3),
     &      fld2src, ri1, ri3, ds
      INTEGER*8 ipfld, ipsrc, nearpat,far,nearr


      ! for duffy integral 
      INTEGER*8 oD,iD,oG,iG ! outer and innter loop for duffy
      INTEGER*8 iout,iin,idc1,idc2,flag
      Real vtd1,vtd2,vtd3,wti,wto
      Real vto1,vto2,vto3,tmppp(3),tmppn(3)
c.......................................................................
      oD=1
	iD=27
	oG=4
	iG=7
      ck2d2=ck*ck/2.0
      flag=0
      n1fld = iedge( 1, ii )
      n2fld = iedge( 2, ii )
      n1src = iedge( 1, jj )
      n2src = iedge( 2, jj )

      call mv1to2( xyznode(1,n1src), r1, 3 )
      call mv1to2( xyznode(1,n2src), r2, 3 )

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
c.... .set integration rules: depending on the distance of the............c 
c..... two basises, different rules are used. when the ...................c
c.....(squared) distance is larger than rnear2, irule_fld and ............c
c......irule_src are used; otherwise, the near-field rules................c
c......(irulef_near, irules_near) are used................................c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      far=1
      irulef0 = irule_fld
      irules0 = irule_src
      d = 0.0

      do i=1,3
        rf = xyznode(i,n1fld)+xyznode(i,n2fld)
        rs = xyznode(i,n1src)+xyznode(i,n2src)
        d = d + abs((rf-rs)*(rf-rs))
      end do

      if( 0.25*d .le. rnear2 ) then
         irulef0 = irulef_near
         irules0 = irules_near
      end if

c.....................................................................

      call getedgectr(xyznode(1,n1fld),xyznode(1,n2fld),ectr)
	cnxet2 = (0.0, 0.0)
      cnxet = (0.0, 0.0)
      do 400 lfld = 1, 2
         signfld = float(3-2*lfld)
         ipfld = iedge(2+lfld,ii)     
         if( ipfld .le.0) then 
		goto 400
	   endif
         n3fld = igetn3( n1fld, n2fld, ipatpnt(1,ipfld) )
         call mv1to2( xyznorm(1,ipfld), anorm, 3 )
         call mv1to2( xyzctr(1,ipfld),  pctr,  3 )
         call vctadd( ectr, pctr, rhot, -1 ) 
         ! pointing from the mid point of triangle to the mid point of edge.
        ! only used when match==2
         cefie = (0.0,0.0)
	   cefie2 = (0.0,0.0)
         do 200 lsrc = 1, 2

            signsrc = float( 3 - 2 * lsrc )
            ipsrc = iedge( 2+lsrc, jj )
		if (ipsrc .le.0) then
			goto 200
		endif
            n3src = igetn3( n1src, n2src,ipatpnt(1,ipsrc) )
		
            call mv1to2(xyznode(1,n3src), r3, 3)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c..... check the distance of the two patches. if this.................c
c..... distance is small, special treatment is .......................c
c..... equired to calculate the matrix elements.......................c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
            irulef = irulef0
            irules = irules0
		! when the src and receiving triangle are close enough, change the irules_near
            call rules2(ipfld, ipsrc, xyzctr(1,ipfld), xyzctr(1,ipsrc), 
     &                  distmin, irulef_near,  irules_near, 
     &                  nearr, irulef, irules )
            !for the near coupling, move the normal vector of the source triangle to bnorm.
		
		!Based on Jiangong's suggestion
		nearpat=0
		if(ipsrc.eq.ipfld) then 
			nearpat=1
                  flag=1
		endif

		if(nearr.eq.1) then
                far=0
                flag=1
            endif

		csume2= (0.0,0.0)
            csume = (0.0,0.0)
            if(nearpat.eq.0) then
               if(nearr.eq.1) then 
				oG=4
			else
				oG=1
			endif
		! Go through the every points in the source triangle
            do 100 i = 1, oG! go throught the integral rule of the receiving triangle.
               idc1=i-1
		   call getformula(oG,idc1,vto1,vto2,vto3,wto)
               call getr( ! get the r and rho of sampling points
     &           xyznode(1,n1fld),xyznode(1,n2fld),xyznode(1,n3fld),
     &           vto1,vto2,vto3,rfld,rhofld)
               call mv1to2( rhofld, rhoxnt, 3 )  !rhoxnt = rho
               call xmul( rhofld, anorm, rhoxnx ) ! rhoxnx= rho  x Normal

		   cze = (0.0,0.0)
		   cze2 = (0.0,0.0)			
               do j = 1, iG ! for source
                  idc2=j-1
! 			call getgauss(oG,idc1,idc2,vtd1,vtd2,vtd3,wti)
			call getformula(iG,idc2,vtd1,vtd2,vtd3,wti)
                  call getr
     &             ( xyznode(1,n1src),xyznode(1,n2src),xyznode(1,n3src),
     &               vtd1,vtd2,vtd3,rsrc,rhosrc )

                  call vctadd( rfld,rsrc,rr, -1 )
                  call cnst_func( rr, ck, ci,
     &                            fld2src, fld2src2, cagu, cagu2, 
     &                            cx, cg0 )
                  cfet=cfunex_new(rhoxnt,rhosrc,paera(ipfld),
     &                         paera(ipsrc),edge(ii),edge(jj),nearpat,
     &                     fld2src, cagu, cagu2, cx, cg0, ck, ci,cfet2)     

                  cze  =cze  + wti * cfet
	      	cze2 =cze2 + wti * cfet2
               end do !do j = 1, ngrid(irules)
    		   csume2 = csume2 + wto * cze2
		   csume = csume + wto * cze
100         continue
             else ! if(nearpat.eq.0) then  For near

		! Go through the every points in the source triangle
            do 150 i = 1, oD! go throught the integral rule of the receiving triangle.
                  idc1=i-1
			call getformula(oD,idc1,vto1,vto2,vto3,wto)
               call getr( ! get the r and rho of sampling points
     &           xyznode(1,n1fld),xyznode(1,n2fld),xyznode(1,n3fld),
     &           vto1,vto2,vto3,rfld,rhofld)
               call mv1to2( rhofld, rhoxnt, 3 )  !rhoxnt = rho
               call xmul( rhofld, anorm, rhoxnx ) ! rhoxnx= rho  x Normal

		   cze = (0.0,0.0)
		   cze2 = (0.0,0.0)			
               do j = 1, iD ! for source
                  idc2=j-1
			call getduffy(oD,iD,idc1,idc2,vtd1,vtd2,vtd3,wti)
                  call getr
     &             ( xyznode(1,n1src),xyznode(1,n2src),xyznode(1,n3src),
     &               vtd1,vtd2,vtd3,rsrc,rhosrc )

                  call vctadd( rfld,rsrc,rr, -1 )
                  call cnst_func( rr, ck, ci,
     &                            fld2src, fld2src2, cagu, cagu2, 
     &                            cx, cg0 )
                  cfet=cfunex_new(rhoxnt,rhosrc,paera(ipfld),
     &                         paera(ipsrc),edge(ii),edge(jj),nearpat,
     &                     fld2src, cagu, cagu2, cx, cg0, ck, ci,cfet2)     
                  wti=wti
                  cze  =cze  + wti * cfet 
	      	cze2 =cze2 + wti * cfet2 
               end do !do j = 1, ngrid(irules)
			tmpvar=cze2*ck*ck
    		   csume2 = csume2 + wto * cze2
		   csume = csume + wto * cze
150         continue
             endif !if(nearpat.eq.0) then
 		if(total.le.10000) then
		write(2000,*)'--',ii,jj,
     &      signfld*signsrc *csume,
     &      signfld*signsrc*csume2
		total=total+1
		endif
            cefie = cefie + signsrc * csume
		cefie2= cefie2 + signsrc * csume2

200      continue !do 200 lsrc = 1, 2
 
         cnxet = cnxet + signfld*cefie
	   cnxet2 = cnxet2 + signfld *cefie2
400   continue !do 400 lfld = 1, 2
      

      call getedgectr(xyznode(1,n1fld),xyznode(1,n2fld),ectr)
      call getedgectr(xyznode(1,n1src),xyznode(1,n2src),tmppp)
      call vctadd(ectr,tmppp,tmppn,-1)
      tmpp=sqrt(dotmul(tmppn,tmppn))
	rnst = edge(jj)*float(match)/(16.0*pi)
      rnst = rnst*edge(ii)

      if(total.le.10000) then
	if(ii.eq.jj) then
	write(2000,*)flag,ii,jj,cnxet,cnxet2*2.0,cnxet+cnxet2*2.0,rnst
	else 
	write(2000,*)flag,ii,jj,cnxet,cnxet2*4.0,cnxet+cnxet2*4.0,rnst
	endif            
	total=total+1
	endif

      if (ii.eq.jj) then
            cnxet = cnxet*rnst+cnxet2*rnst*2.0
      else
            cnxet = cnxet*rnst+cnxet2*rnst*4.0
      endif
!       if(ii.eq.jj) cnxet=cnxet/2.0


      
      RETURN
      END
      
