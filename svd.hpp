#ifndef SVD_HPP
#define SVD_HPP

#include <complex>
#include <vector>
#include <algorithm>
#include <cstdlib>

using std::complex;

typedef long long int Int;

// lapack prototypes
extern "C" {
  void zgesvd_(char *JOBU, char *JOBVT, Int *M, Int *N, complex<double> *A,
	       Int *LDA, double *S, complex<double> *U, Int *LDU,
	       complex<double> *VT, Int *LDVT, complex<double> *WORK,
	       Int *LWORK, double *RWORK, Int *INFO, Int JOBU_len, Int JOBVT_len);
  void cgesvd_(char *JOBU, char *JOBVT, Int *M, Int *N, complex<float> *A,
	       Int *LDA, float *S, complex<float> *U, Int *LDU,
	       complex<float> *VT, Int *LDVT, complex<float> *WORK,
	       Int *LWORK, float *RWORK, Int *INFO, Int JOBU_len, Int JOBVT_len);
}


// prototype for the function that will actually
// call the proper lapack function
template<typename T> void
the_svd(char JOBU, char JOBVT, Int M, Int N, complex<T> *A,
	Int LDA, T *S, complex<T> *U, Int LDU,
	complex<T> *VT, Int LDVT, complex<T> *WORK,
	Int LWORK, T *RWORK, Int &INFO);


// now the functions that the user calls, which
// takes care of workspace allocation automatically
template <typename T>
void gesvd(char JOBU, char JOBVT, Int M, Int N, complex<T> *A,
	   Int LDA, T *S, complex<T> *U, Int LDU,
	   complex<T> *VT, Int LDVT, Int &INFO)
{
  std::vector<T> RWORK(5*std::max(M,N));

  // first, get optimum workspace size
  complex<T> tmp;
  the_svd(JOBU, JOBVT, M, N, A, LDA, S, U, LDU,
	  VT, LDVT, &tmp, -1, &RWORK[0], INFO);
  Int LWORK = Int(tmp.real());

  std::vector<complex<T> > WORK(LWORK);

  the_svd(JOBU, JOBVT, M, N, A, LDA, S, U, LDU,
	  VT, LDVT, &WORK[0], LWORK, &RWORK[0], INFO);
}


// specialization for single and double precision

template<> inline void
the_svd<float>(char JOBU, char JOBVT, Int M, Int N, complex<float> *A,
	       Int LDA, float *S, complex<float> *U, Int LDU,
	       complex<float> *VT, Int LDVT, complex<float> *WORK,
	       Int LWORK, float *RWORK, Int &INFO)
{
  cgesvd_(&JOBU, &JOBVT, &M, &N, A,
	  &LDA, S, U, &LDU,
	  VT, &LDVT, WORK,
	  &LWORK, RWORK, &INFO, 1, 1);
}

template<> inline void
the_svd<double>(char JOBU, char JOBVT, Int M, Int N, complex<double> *A,
		Int LDA, double *S, complex<double> *U, Int LDU,
		complex<double> *VT, Int LDVT, complex<double> *WORK,
		Int LWORK, double *RWORK, Int &INFO)
{
  zgesvd_(&JOBU, &JOBVT, &M, &N, A,
	  &LDA, S, U, &LDU,
	  VT, &LDVT, WORK,
	  &LWORK, RWORK, &INFO, 1, 1);
}

#endif
