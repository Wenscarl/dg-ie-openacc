#ifndef COMPLEX_H
#define COMPLEX_H

#include <complex>

using namespace std;
typedef float Real;
typedef long long int Integer;
typedef complex<Real> Complex;
typedef complex<double> InputComplex;

template<typename T>
inline T& reref(complex<T>& z)
{
    return reinterpret_cast<T(&)[2]>(z)[0];
}

template<typename T>
inline T& imref(complex<T>& z)
{
    return reinterpret_cast<T(&)[2]>(z)[1];
}

#endif

