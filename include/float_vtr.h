// VTR.H
#ifndef VTR_H
#define VTR_H

class vtr {
  //friend float dotP(vtr, vtr);
  //friend float Length(vtr);
  //friend float Determinant(vtr, vtr, vtr);
  
 private:
  float x, y, z;

 public:
  // constructor
  vtr(float xx=0.0, float yy=0.0, float zz=0.0)
  	{setvtr(xx,yy,zz);}
  
  // operators
  vtr operator+(const vtr &o) const
  	{return vtr(x+o.x,y+o.y,z+o.z);}
  vtr operator-(const vtr &o) const
  	{return vtr(x+o.x,y+o.y,z+o.z);}
  vtr operator*(const vtr &o) const
  	{return vtr(y*o.z-z*o.y,z*o.x-x*o.z,x*o.y-y*o.x);}
  vtr operator*(const float f) const
  	{return vtr(x*f,y*f,z*f);}
  vtr operator/(const float f) const
  	{return vtr(x/f,y/f,z/f);}
  vtr& operator=(const vtr &o)
  	{setvtr(o.x,o.y,o.z); return *this;}

  // set functions
  void setvtr(float xx, float yy, float zz)
  	{x=xx;y=yy;z=zz;}
  void reset()
  	{x=0.0;y=0.0;z=0.0;}

  void unitvtr() {Scale(1.0/magnitude());}
  void addvtr(float xx = 0.0, float yy = 0.0, float zz = 0.0)
  	{setvtr(x+xx,y+yy,z+zz);}
  void subvtr(float xx = 0.0, float yy = 0.0, float zz = 0.0)
  	{setvtr(x-xx,y-yy,z-zz);}

  void Scale(float f = 1.0)
  	{setvtr(x*f,y*f,z*f);}
  void negate() {Scale(-1.0);}

  // get functions
  float getx() const {return x;}
  float gety() const {return y;}
  float getz() const {return z;}
  float magSquare() const {return x*x+y*y+z*z;}
  float magnitude() const {return sqrt(magSquare());}

  // print
  //void print();
};

#endif
