      subroutine SCALE
     &( x, d)

      IMPLICIT NONE

      REAL x(3), d
      INTEGER*8 i

      do i=1,3
         x(i)=x(i)/d
      enddo

      RETURN
      END
