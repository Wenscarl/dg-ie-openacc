      subroutine fillb16(u,v,dm,dp,b16)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c   calculate the 16 coefficients for interpolation
c   using central difference with non-equal space.
c   call:        hunt
c   called from: fillarray
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      real b16(16)
      integer*8 ncs(48),nrs(17)
      real wt(16,16),x(16),y(16),nmatrix(48)
      save wt,nmatrix,ncs,nrs
      data wt/ 1,   0,  -3,   2, 4*0,  -3,   0,   9,  -6,   2,   0,
     &   -6,   4, 8*0,   3,   0,  -9,   6,  -2,   0,   6,  -4,10*0,
     &    9,  -6, 2*0,  -6,   4, 2*0,   3,  -2, 6*0,  -9,   6, 2*0, 
     &    6,  -4, 4*0,   1,   0,  -3,   2,  -2,   0,   6,  -4,   1,
     &    0,  -3,   2, 8*0,  -1,   0,   3,  -2,   1,   0,  -3,   2,
     & 10*0,  -3,   2, 2*0,   3,  -2, 6*0,   3,  -2, 2*0,  -6,   4, 
     &  2*0,   3,  -2,   0,   1,  -2,   1, 5*0,  -3,   6,  -3,   0,
     &    2,  -4,   2, 9*0,   3,  -6,   3,   0,  -2,   4,  -2,10*0,
     &   -3,   3, 2*0,   2,  -2, 2*0,  -1,   1, 6*0,   3,  -3, 2*0,  
     &   -2,   2, 5*0,   1,  -2,   1,   0,  -2,   4,  -2,   0,   1,
     &   -2,   1, 9*0,  -1,   2,  -1,   0,   1,  -2,   1,10*0,   1,
     &   -1, 2*0,  -1,   1, 6*0,  -1,   1, 2*0,   2,  -2, 2*0,  -1,
     &    1/
      data nmatrix/ 1, 1, 1, 1,-1, 1,0,-1, 1, 0,-1, 1, 0,-1, 1,0,
     &  -1, 1,-1, 1,-1, 1,-1, 1, 1,-1,-1, 1,0,0, 1,-1,-1, 1,0, 0,
     &   1,-1,-1, 1, 0, 0, 1,-1,-1, 1,0,0/
      data ncs    / 6,10,11, 7, 2,10,6, 6,14,10, 7,15,11, 3,11,7, 
     &   5, 7, 9,11,10,12, 6, 8, 1, 3, 9,11,5,7, 5, 7,13,15,9,11,
     &   6, 8,14,16,10,12, 2, 4,10,12,6,8/
      data nrs    /1,2,3,4,5,8,11,14,17,19,21,23,25,31,37,43,49/

      do i=1,16
c         write(11,10) (int(wt(i,j)),j=1,16)
      enddo
10    format(16i4)

c   form t
      x(1)=1.
      x(2)=v
      x(3)=v*v
      x(4)=v*v*v
      do i=2,4
         do j=1,4
            x(4*(i-1)+j)=u*x(4*(i-2)+j)
         enddo
      enddo

c    w*t
      do i=1,16
         y(i)=0.
         do j=1,16
            y(i)=y(i)+wt(j,i)*x(j)
         enddo
      enddo

c   d*w*t
      do i=4,12,8
         y(i+1)=dm*y(i+1)
         y(i+2)=dp*y(i+2)
         y(i+3)=dp*y(i+3)
         y(i+4)=dm*y(i+4)
      enddo
      do i=9,16
         y(i)=0.5*y(i)
      enddo   

c  update n
      rm=1./dm-1.
      rp=1./dp-1.
      do i=4,13,9
         nmatrix(i+1)=-1./rm
         nmatrix(i+2)=rm
         nmatrix(i+3)=1./rm-rm
      enddo
      do i=7,10,3
         nmatrix(i+1)=-rp
         nmatrix(i+2)=1./rp
         nmatrix(i+3)=rp-1./rp
      enddo
      do i=24,42,18
         nmatrix(i+1)= 1./rm
         nmatrix(i+2)=-1./rm
         nmatrix(i+3)=-rm
         nmatrix(i+4)= rm
         nmatrix(i+5)=-(1./rm-rm)
         nmatrix(i+6)=  1./rm-rm
      enddo
      do i=30,36,6
         nmatrix(i+1)= rp
         nmatrix(i+2)=-rp
         nmatrix(i+3)=-1./rp
         nmatrix(i+4)= 1./rp
         nmatrix(i+5)=-(rp-1./rp)
         nmatrix(i+6)=  rp-1./rp
      enddo 

c  find n*d*w*t
      do i=1,16
         b16(i)=0.
      enddo
      do j=1,16
         do i=nrs(j),nrs(j+1)-1
            b16(ncs(i))=b16(ncs(i))+nmatrix(i)*y(j)
         enddo
      enddo
      return
      end
