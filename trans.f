            subroutine trans(bn,r,r0,r2,r3, u1,p10,r10,r11,al1)
      implicit none
        ! bn is the normal of the triangle
      real bn(3),r(3),r0(3),r2(3),r3(3)
      !output
      real u1(3), p10, r10, r11, al1(2)
      ! working vector
      real pm(3),rm(3),rmid(3), pmid(3), s1(3), tmp(3), dotmul
      real e1
      call vctadd( r3,r2, s1, -1 )  ! s1=r3-r2
      e1 = sqrt( dotmul(s1,s1) )   ! e1=|s1|
      call scale( s1, e1 ) !s1=\hat{r3-r2}   
      call xmul(  s1, bn, u1 )!unit vector prep to edge(2,3)
      call vctadd( r2, r0, pm, -1 ) ! r2- r0
      call vctadd( r2, r,  rm, -1 ) ! r2- r
      al1(1) = dotmul(pm,s1)        ! dot(r2-r0, \hat{r3-r2}  ) 
      al1(2) = e1 + al1(1)          ! |s1|+dot(r2-r0, \hat{r3-r2}  ) 
      r11 = sqrt(abs(dotmul(rm,rm))) !r11=|r2- r|
       ! orthogonalize r2 with 
      tmp(1) = r2(1) - al1(1)*s1(1)
      tmp(2) = r2(2) - al1(1)*s1(2)
      tmp(3) = r2(3) - al1(1)*s1(3)
      call vctadd( tmp, r, rmid, -1 )
      r10 = sqrt(abs(dotmul(rmid,rmid)))

      call vctadd( tmp, r0, pmid, -1 )
      p10 = dotmul(pmid,u1)
      
      return
      end
