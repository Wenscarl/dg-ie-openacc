      subroutine cacx
     &( cx0, cy, job, cx, cvf, cvs,
     &  maxedge, lmax, lsmax, ncmax, kpm, kp0, 
     &  nsm, ntlm, niplm, lxyz, modes,
     &  igall, igcs, igrs, index, family, 
     &  lrsmup, lrsmdown, csm, cvlr, cgm, ctl, 
     &  lrstl, indextl,
     &  kpstart, nkpm, c_shift, icsa, irsa, array, 
     &  ngsndm, igsndcs, igsndrs, 
     &  nearm, noself, canear, ngnearm, igblkrs, 
     &  igblkcs, igblk, mfinestm, cfinest)

      use mlfma_const
      implicit none

!      nclude 'mlfma_const.inc'

      INTEGER*8 maxedge, lmax, lsmax, ncmax, kpm, kp0, nkpm,
     &        nsm, ntlm, niplm, ngsndm, mfinestm,
     &        nearm, ngnearm, job

      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
      INTEGER*8 irsa(nkpm+1), icsa(niplm)
      REAL   array(niplm)
      COMPLEX ctl(ntlm), csm(2,nsm)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 noself(2,ncmax+1)
      COMPLEX cvf(2,kp0,maxedge), cvs(2,kp0,maxedge)
      COMPLEX canear(nearm*maxedge), cfinest(mfinestm)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)
      COMPLEX c_shift(nkpm,2,2,2)
      INTEGER*8 igsndcs(ngsndm), igsndrs(ncmax)      

      COMPLEX cx0(maxedge),cy(maxedge), cx(maxedge)

      INTEGER*8 i

      if(job.eq.0) then

           do i = 1,maxedge
              cy(i) = (0.0,0.0)
              cx(i) = cx0(index(i))
           enddo

           if(lmax.gt.1) then

         call cacxmp(cx, cy, job, cvf,
     &		     cvs(1,1,1),
     &               maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &               ntlm, niplm, lxyz, modes,
     &               igall, igcs, igrs, index, family, 
     &               lrsmup, lrsmdown, csm, cvlr, cgm, ctl(1), lrstl, 
     &               indextl,kpstart, nkpm,
     &               c_shift(1,1,1,1), icsa, irsa, array, 
     &               ngsndm, igsndcs, igsndrs)

       else
       endif

           call cacxnear(cx, cy, job,
     &                   maxedge, lmax, ncmax, nearm,
     &                   lxyz, ifar, dfar, 
     &                   igall, igcs, igrs, noself, canear,
     &                   ngnearm, igblkrs, igblkcs, igblk,
     &                   mfinestm, cfinest)

      else

           do i = 1, maxedge
            cy(i) = 0e0
            cx(i) = conjg(cx0(index(i)))
           enddo

           if(lmax.gt.1) then
c         call cacxmp(cx, cy, 0, cvf,
c     &		     cvs(1,1,1),
c     &               maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
c     &               ntlm, niplm, lxyz, modes,
c     &               igall, igcs, igrs, index, family, 
c     &               lrsmup, lrsmdown, csm, cvlr, cgm, ctl(1), lrstl, 
c     &               indextl,kpstart, nkpm,
c     &               c_shift(1,1,1,1), icsa, irsa, array, 
c     &               ngsndm, igsndcs, igsndrs)
        call cacxmp(cx, cy, job, cvs(1,1,1), 
     &               cvf(1,1,1),
     &               maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &               ntlm, niplm, lxyz, modes,
     &               igall, igcs, igrs, index, family, 
     &               lrsmup, lrsmdown, csm, cvlr, cgm, ctl(1), lrstl, 
     &               indextl,kpstart, nkpm,
     &               c_shift(1,1,1,1), icsa, irsa, array, 
     &               ngsndm, igsndcs, igsndrs)

      else
      endif

           call cacxnear(cx, cy, job,
     &                   maxedge, lmax, ncmax, nearm,
     &                   lxyz, ifar, dfar, 
     &                   igall, igcs, igrs, noself, canear,
     &                   ngnearm, igblkrs, igblkcs, igblk,
     &                   mfinestm, cfinest)


           do i = 1, maxedge
              cy(i) = conjg(cy(i))
           enddo

      endif

           do i = 1,maxedge
             cx(index(i)) = cy(i)
           enddo

      do i = 1, maxedge
         cy(i) = cx(i)
      enddo

      return
      end
