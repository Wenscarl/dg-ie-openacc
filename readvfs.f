      subroutine readvfs
     &( cvf, cvs,
     &  xyznode,ipatpnt, iedge,  xyzctr, xyznorm, edge,paera, 
     &  NGRID,  vt1, vt2, vt3, wt,
     &  lmax, lsmax, lsmin, ncmax, kp0,
     &  lxyz, modes, sizel,
     &  igall, igcs, igrs, index, family,
     &  xgl, wgl, dirk)

ccccccccccccccccccccccccccccccccc
c   fill cvf, cvs in fmm
ccccccccccccccccccccccccccccccccc

        use mlfma_input
        use mlfma_const
        use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      INTEGER*8 lmax, lsmax, lsmin, ncmax, kp0
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
       REAL   xgl(lsmax), wgl(lsmax), dirk(3,3,kp0)
      COMPLEX cvf(2,kp0, maxedge),cvs(2,kp0,maxedge)

      INTEGER*8 ipointer, ls, kpt
      INTEGER*8 l, igt, ig(3), ixyz, ipindex, ib
       REAL   xyzc(3)

      INTEGER*8 rrec
c.....the finest level
       INTEGER*8 i,j,k
c.....the finest level

      write(*,*) 'Begin readvfs.f'

! 	open (94,file='Filevfs',status='unknown')
! 
! 
!         do j=1,kp0
!           do i=1,2
!                read(94,*) ( cvf(i,j,k), k=1,maxedge )
!           end do
!          end do
! 
!         do j=1,kp0
!           do i=1,2
!                read(94,*) ( cvs(i,j,k), k=1,maxedge )
!           end do
!          end do
! 
!       close(94)


         open (94,file='Filevfs',
     &  form='unformatted',access='direct',
     &  recl=16*maxedge,
     &               status='unknown')
 
        rrec=1
         do i=1,2
           do j=1,kp0
            read(94,rec=rrec) ( cvf(i,j,k), k=1,maxedge )
            rrec=rrec+1
           end do
         end do
 
         do i=1,2
           do j=1,kp0
            read(94,rec=rrec) ( cvs(i,j,k), k=1,maxedge )
            rrec=rrec+1
           end do
         end do

         close(94)

      return
      end
