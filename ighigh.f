      subroutine ighigh(ig,igp)
*
*  find #igp(x,y,z) for parent from #ig(x,y,z) for child
*---------------------------------------------------------------------
*
      implicit none
      integer*8 ig(3),igp(3),i
*
      do i=1,3
        igp(i)=(1+ig(i))/2
      enddo
      return
      end
