      subroutine time2screen(time_tot, memory_tot, string_rept)

      REAL          time_tot, memory_tot
      CHARACTER*256 string_rept
      REAL          dtime, tarray(2), delt

      delt=dtime(tarray)
      time_tot=time_tot+delt
      write(6,100) string_rept, delt, memory_tot
      call flush()
100   format(a27, '  Delta_CPU=',f9.2, '  Memory(MB)=', f13.2)

      return
      end
