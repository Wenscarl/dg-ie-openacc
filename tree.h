// TREE.H: Definition of template class Tree
#ifndef TREE_H
#define TREE_H
#include <iostream>
#include <assert.h>

typedef long long int Int;
template<class NODETYPE>
class TreeNode {
public: 
  NODETYPE data;

  TreeNode(const NODETYPE &); // constructor
  NODETYPE getData() const; // return data
  NODETYPE *getDataPtr(); // return the pointer to data

  TreeNode *leftPtr; // pointer to left subtree
  TreeNode *rightPtr; // pointer to right subtree  
};


// constructor
template<class NODETYPE>
TreeNode<NODETYPE>::TreeNode(const NODETYPE &d)
{
  data = d;
  leftPtr = rightPtr = 0;
}

// Return a copy of the data value
template<class NODETYPE>
NODETYPE TreeNode<NODETYPE>::getData() const { return data; }

// Return a copy of the data value
template<class NODETYPE>
NODETYPE *TreeNode<NODETYPE>::getDataPtr() { return &data; }

template<class NODETYPE>
class Tree {
public:
  Tree();
  ~Tree();
  Int insertNode(const NODETYPE &);
  void preOrderTraversal() const;
  TreeNode<NODETYPE> *findNode(const NODETYPE &);

  // set functions
  void setcounter(Int );
  void ConvertToArray(NODETYPE **, Int);

  // get counter
  Int getcounter();
  void freeMemory(void);
private:
void FREE(TreeNode<NODETYPE> *);
  TreeNode<NODETYPE> *rootPtr;
  TreeNode<NODETYPE> *tmpPtr;
  Int counter, id;

  // utility functions
  void insertNodeHelper(TreeNode<NODETYPE> **, const NODETYPE &);
  void findNodeHelper(TreeNode<NODETYPE> *, const NODETYPE &);
  void preOrderHelper(TreeNode<NODETYPE> *) const;
  void convertToarrayHelper(TreeNode<NODETYPE> *, NODETYPE **);
};

template<class NODETYPE>
Tree<NODETYPE>::Tree() { rootPtr = 0; counter = 0; }
template<class NODETYPE>
		Tree<NODETYPE>::~Tree() 
{ 
// 	if(counter==0||rootPtr==0)
// 		return;
// 	else
// 	{
// 		FREE(rootPtr);
// 		delete rootPtr;
// 		return;
// 	} 
}
template<class NODETYPE> void 
		Tree<NODETYPE>::freeMemory(void)
{
	if(counter==0||rootPtr==0)
		return;
	else
	{
		FREE(rootPtr);
		delete rootPtr;
		return;
	} 
} 
template<class NODETYPE>
		void Tree<NODETYPE>::FREE(TreeNode<NODETYPE> *P) 
{ 
	if(P->leftPtr!=NULL)
	{
		FREE(P->leftPtr);
		delete P->leftPtr;
	}
	if(P->rightPtr!=NULL)
	{
		FREE(P->rightPtr);
		delete P->rightPtr;
	}
	
	return;
}
template<class NODETYPE>
Int Tree<NODETYPE>::insertNode(const NODETYPE &value)
{ 
  Int oldCounter = counter;

  insertNodeHelper(&rootPtr, value); 
  
  if (counter == oldCounter) return 0;
  return 1;
}

// This function receives a pointer to a pointer so the
// pointer can be modified
template<class NODETYPE>
void Tree<NODETYPE>::insertNodeHelper(TreeNode<NODETYPE> **ptr,
				      const NODETYPE &value)
{
  if (*ptr == 0) {
    *ptr = new TreeNode<NODETYPE>(value);
    assert(*ptr != 0);

    counter ++;
  } 
  else 
    if (value < (*ptr)->data)
      insertNodeHelper( &((*ptr)->leftPtr), value);
    else
      if (value > (*ptr)->data)
	insertNodeHelper( &((*ptr)->rightPtr), value);
      else
	return;
}

template<class NODETYPE>
TreeNode<NODETYPE> *Tree<NODETYPE>::findNode(const NODETYPE &value)
{
  tmpPtr = 0;
  findNodeHelper(rootPtr, value);

  return tmpPtr;
}

template<class NODETYPE>
void Tree<NODETYPE>::findNodeHelper(TreeNode<NODETYPE> *ptr,
				    const NODETYPE &value)
{
  if (ptr == 0) return;
  
  if (value == ptr->data) {
    tmpPtr = ptr;
    
    return;
  }
  else {
    if (value < ptr->data) findNodeHelper(ptr->leftPtr, value);
    else
      findNodeHelper(ptr->rightPtr, value);
  }
}

template<class NODETYPE>
void Tree<NODETYPE>::preOrderTraversal() const
{ preOrderHelper(rootPtr); }

template<class NODETYPE>
void Tree<NODETYPE>::preOrderHelper(TreeNode<NODETYPE> *ptr) const
{
  if (ptr != 0) {
    ptr->data.print();
    preOrderHelper(ptr->leftPtr);
    preOrderHelper(ptr->rightPtr);
  }
}

template<class NODETYPE>
void Tree<NODETYPE>::setcounter(Int cnt) { counter = cnt; }

template<class NODETYPE>
Int Tree<NODETYPE>::getcounter() { return counter; }

template<class NODETYPE>
void Tree<NODETYPE>::ConvertToArray(NODETYPE **ndArray, Int n)
{
  if (counter != n) return;

  id = 0;
  convertToarrayHelper(rootPtr, ndArray);
}

template<class NODETYPE>
void Tree<NODETYPE>::convertToarrayHelper(TreeNode<NODETYPE> *ptr,
					  NODETYPE **ndARRAY)
{
  if (ptr != 0) {
    ndARRAY[id] = &(ptr->data);
    id ++;

    convertToarrayHelper(ptr->leftPtr, ndARRAY);
    convertToarrayHelper(ptr->rightPtr, ndARRAY);
  }
}

#endif

