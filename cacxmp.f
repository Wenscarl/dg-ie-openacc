      subroutine cacxmp
     &( cx, cy, job, cvl, cvr,
     &  maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, ntlm, niplm,
     &  lxyz, modes,
     &  igall, igcs, igrs, index, family, 
     &  lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, indextl,
     &  kpstart, nkpm, c_shift, icsa, irsa, array, 
     &  ngsndm, igsndcs, igsndrs)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
c   matrix-vector multiplication: fmm parts.
c   going down: based on child level 2 --> levels
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      use mlfma_const
      implicit none

!      include 'mlfma_const.inc'

      INTEGER*8 maxedge, lmax, lsmax, ncmax, kpm, kp0, nkpm,
     &        nsm, ntlm, niplm, ngsndm,job

      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
      INTEGER*8 irsa(nkpm+1), icsa(niplm)
       REAL   array(niplm)
      COMPLEX ctl(ntlm), csm(2,nsm)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      COMPLEX c_shift(nkpm,2,2,2)
      INTEGER*8 igsndcs(ngsndm), igsndrs(ncmax)      

      COMPLEX cvl(2,kp0,maxedge), cvr(2,kp0,maxedge)
      COMPLEX cx(maxedge), cy(maxedge)

      INTEGER*8 ipointer, ls, kpt
      INTEGER*8 ns, l, lrow, igt, ig(3), ipindex, 
     &        kp, inear, ix, iy, iz,
     &        lsp, lsc, kptp, kptc, nsdownpl, nsdowncl,
     &        nsupcl, ipp, ipc, noemptyp, noemptyc, ntlc,
     &        ilo, iglp, igp(3), jplo, nsdownp, 
     &        nsdownc, indexcsf, igfl, igf(3), 
     &        indexcss, igsl,igs(3), nsup, jx, jy, jz, ntl,
     &        indexc, nkp, ns_level
      COMPLEX ctemp
      complex,pointer::cgmTmp(:,:), cvlrTmp(:,:)

      do ns = 1, nsm
         csm(1,ns) = (0.0,0.0)
         csm(2,ns) = (0.0,0.0)
      enddo

c....calculate the multipole expansion coefficients
c....start the finest level

      l = lmax
      lrow = 1
      ns = 0
      ls = modes(l)
      kpt = 4 + 2*ls*ls

c      delt=dtime(tarray)
c$doacross local(ipointer, ns, ipindex, ctemp, kp)

c      write(*,*) 'Fist time openmp in CACXMP'
!$OMP PARALLEL DO PRIVATE(ns,ipindex,ctemp,kp)
      do ipointer = igrs(lrow), igrs(lrow+1)-1
         ns = (ipointer-1)*kpt
         do ipindex = igall(ipointer), igall(ipointer+1)-1
            ctemp = cx(ipindex)
            do kp = 1,kpt
               csm(1,ns+kp) = csm(1,ns+kp) + cvr(1,kp,ipindex)*ctemp
               csm(2,ns+kp) = csm(2,ns+kp) + cvr(2,kp,ipindex)*ctemp
            enddo
         enddo
      enddo
!$OMP END PARALLEL DO

c      delt=dtime(tarray)
c      write(*,*) 'after finnest level: ',delt,tarray
      ns_level = (igrs(lrow+1)-1)*kpt

c  go up for higher levels
c      delt=dtime(tarray)
      do l = lmax-1, 2, -1
         nkp    = kpstart(l)
         lrow   = lmax - l + 1
         lsp    = modes(l)
         lsc    = modes(l+1)
         kptp   = 4 + 2*lsp*lsp
         kptc   = 4 + 2*lsc*lsc
         ipc    = igrs(lrow-1)
         nsupcl = lrsmup(l+1) - 1

c$doacross local(ipointer, ns, iglp, igp, ipindex, indexc, nsup,
c$&   igt, cgm, ig, ix, iy, iz, kp, ctemp)

cc!$OMP PARALLEL DO PRIVATE(ns,iglp,igp,ipindex,indexc,nsup,cgm,
cc!$OMP& igt,ig,ix,iy,iz,kp,ctemp)

!$OMP PARALLEL DO PRIVATE(ns,iglp,igp,ipindex,indexc,nsup,
!$OMP& igt,ig,ix,iy,iz,kp,ctemp,cgmTmp)

         do ipointer = igrs(lrow), igrs(lrow+1)-1

	allocate(cgmTmp (2,kptp)) 

            ns = ns_level + kptp*(ipointer-igrs(lrow))
            iglp = igcs(ipointer)
            call igxyz(iglp,lxyz(1,l),igp)
            do ipindex=igall(ipointer),igall(ipointer+1)-1
               indexc=index(ipindex)
               nsup=nsupcl+kptc*(indexc-ipc)
               call interpl(kpm, niplm, csm(1,nsup+1), 
     &    kptp, array, icsa, irsa(nkp+1), cgmTmp)
               igt=igcs(indexc)
               call igxyz(igt,lxyz(1,l+1),ig)
               ix = ig(1) - 2*igp(1) + 1
               iy = ig(2) - 2*igp(2) + 1
               iz = ig(3) - 2*igp(3) + 1

c           for up                     down
c                  job                      job  
c           ix    0    1            ix     0    1
c
c            0    2    1             0     1    2
c            1    1    2             1     2    1
c
c           2 - abs(ix-job)         1 + abs(ix-job)

               ix = 2 - abs(ix-job)
               iy = 2 - abs(iy-job)
               iz = 2 - abs(iz-job)
               do kp = 1, kptp
                  ctemp = c_shift(kp+nkp,ix,iy,iz)
                  csm(1,ns+kp) = csm(1,ns+kp) + cgmTmp(1,kp)*ctemp
                  csm(2,ns+kp) = csm(2,ns+kp) + cgmTmp(2,kp)*ctemp
               enddo
            enddo
c               ^ children of ipointer's group
c            ns=ns+kptp

	 deallocate (cgmTmp)

         enddo
!$OMP END PARALLEL DO

c            ^ ipointer'th noempty group
         ns_level = ns_level + kptp*(igrs(lrow+1)-igrs(lrow))
      enddo
c         ^ l'th level
c      delt=dtime(tarray)
c      write(*,*) 'after going up: ',delt,tarray

c  go down
      do l = 2, lmax
         lrow     = lmax - l + 1
         lsp      = modes(l-1)
         lsc      = modes(l)
         kptp     = 4 + 2*lsp*lsp
         kptc     = 4 + 2*lsc*lsc
         nsdownpl = lrsmdown(l-1) - 1
         nsdowncl = lrsmdown(l) - 1
         nsupcl   = lrsmup(l)   - 1
         ipp      = igrs(lrow+1)
         ipc      = igrs(lrow)
         noemptyp = igrs(lrow+2) - ipp
         noemptyc = ipp - ipc
         nkp = kpstart(l-1)
c
         ntlc=lrstl(l)-1
         if(l.lt.lmax) then
            do ns = lrsmdown(l), lrsmdown(l-1)-1
               csm(1,ns) = (0.0,0.0)
               csm(2,ns) = (0.0,0.0)
            enddo
         endif

c$doacross local(ilo, indexcsf, nsdownc, igfl, igf, nsdownp,
c$&  iglp, igp, jplo, ix, iy, iz, kp, ctemp, cvlr, cgm, 
c$&  inear, indexcss, igsl, igs, nsup, jx, jy, jz, ntl,
c$&  ipindex)

!      write(*,*) 'Third time openmp in CACXMP'

cc!$OMP PARALLEL DO PRIVATE(indexcsf,nsdownc,igfl,igf,nsdownp,
cc!$OMP& iglp,igp,jplo,ix,iy,iz,kp,ctemp,cvlr,cgm,inear,indexcss,
cc!$OMP& igsl,igs,nsup,jx,jy,jz,ntl,ipindex)

!$OMP PARALLEL DO PRIVATE(indexcsf, nsdownc,igfl,igf,igp,nsdownp,
!$OMP& iglp,jplo,ix,iy,iz,kp,ctemp,inear,indexcss,
!$OMP& igsl,igs,nsup,jx,jy,jz,ntl,ipindex,cgmTmp,cvlrTmp)

         do ilo = 1, noemptyc

	 allocate(cgmTmp (2,kptc))
	 allocate(cvlrTmp (2,kptp))

            indexcsf = ilo+ipc-1
            nsdownc  = nsdowncl + kptc*(indexcsf-ipc)
            igfl     = igcs(indexcsf)
            call igxyz(igfl,lxyz(1,l),igf)

c.....contributions from parents
            if(l.gt.2) then
               igp(1) = (1+igf(1))/2
               igp(2) = (1+igf(2))/2
               igp(3) = (1+igf(3))/2
               iglp   = igp(3)+lxyz(3,l-1)*(igp(2)+lxyz(2,l-1)*
     &                     (igp(1)-1)-1)
               jplo = 1
               call huntint(igcs(ipp),noemptyp,iglp,jplo)
               nsdownp = nsdownpl + kptp*(jplo-1)

               ix = igf(1) - 2*igp(1) + 1
               iy = igf(2) - 2*igp(2) + 1
               iz = igf(3) - 2*igp(3) + 1
               ix = 1 + abs(ix-job)
               iy = 1 + abs(iy-job)
               iz = 1 + abs(iz-job)
               do kp = 1, kptp
                  ctemp = c_shift(kp+nkp,ix,iy,iz)
                  cvlrTmp(1,kp) = csm(1,nsdownp+kp)*ctemp
                  cvlrTmp(2,kp) = csm(2,nsdownp+kp)*ctemp
               enddo
               call anterpl(kpm, niplm, kptc, kptp, 
     &              array, icsa, irsa(nkp+1), cvlrTmp, cgmTmp)
               if(l.lt.(lmax)) then
                  do kp = 1, kptc
                     csm(1,nsdownc+kp) = cgmTmp(1,kp)
                     csm(2,nsdownc+kp) = cgmTmp(2,kp)
                  enddo
               endif
            endif
            if(lmax.eq.2) then
               do kp = 1, kptc
                  cgmTmp(1,kp) = (0.0,0.0)
                  cgmTmp(2,kp) = (0.0,0.0)
               enddo
            endif

c.......contributions from second-near neighbors

            do inear = igsndrs(indexcsf), igsndrs(indexcsf+1)-1
               indexcss = igsndcs(inear)
               igsl     = igcs(indexcss)
               call igxyz(igsl,lxyz(1,l),igs)
               nsup = nsupcl + kptc*(indexcss-ipc)
               jx   = (1-2*job)*(igf(1)-igs(1))
               jy   = (1-2*job)*(igf(2)-igs(2))
               jz   = (1-2*job)*(igf(3)-igs(3))
               ntl  = ntlc + kptc*(indextl(jx,jy,jz,l)-1)
               if(l.lt.lmax) then
                  do kp = 1, kptc
                     csm(1,nsdownc+kp) = csm(1,nsdownc+kp)
     &                     + ctl(ntl+kp)*csm(1,nsup+kp)
                     csm(2,nsdownc+kp) = csm(2,nsdownc+kp)
     &                     + ctl(ntl+kp)*csm(2,nsup+kp)
                  enddo
               else
                  do kp = 1, kptc
             cgmTmp(1,kp) = cgmTmp(1,kp) + csm(1,kp+nsup)*ctl(ntl+kp)
             cgmTmp(2,kp) = cgmTmp(2,kp) + csm(2,kp+nsup)*ctl(ntl+kp)
                  enddo
               endif
            enddo
            if(l.eq.lmax) then
c                                     finest level
               do ipindex = igall(indexcsf), igall(indexcsf+1)-1
                  ctemp = 0.
                  do kp = 1, kptc
               ctemp = ctemp + cvl(1,kp,ipindex)*cgmTmp(1,kp)
     &                       + cvl(2,kp,ipindex)*cgmTmp(2,kp)
                  enddo
                  cy(ipindex) = cy(ipindex)+ctemp
               enddo
            endif

	deallocate (cgmTmp)
	deallocate (cvlrTmp)

         enddo
c            ^ ilo'th noempty group
!$OMP END PARALLEL DO
      enddo
c         ^ l'th level

      return
      end
