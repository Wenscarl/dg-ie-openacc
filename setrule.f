      subroutine setrule( irule1,irule2 )

      IMPLICIT NONE

c.....Input Data

      INTEGER*8 irule1

c.....Output Data

      INTEGER*8 irule2

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c.. rules 1 through 8  are for galerkin method.........c
c.. rules 9 through 11 are for line match..............c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      if( irule1 .eq. 1 ) irule2 = 2
      if( irule1 .eq. 2 ) irule2 = 5
      if( irule1 .eq. 5 ) irule2 = 6
      if( irule1 .eq. 6 ) irule2 = 7
      if( irule1 .eq. 7 ) irule2 = 7
      if( irule1 .eq. 8 ) irule2 = 8

      if( irule1 .eq. 9 ) irule2 = 10
      if( irule1 .eq.10 ) irule2 = 11
      if( irule1 .eq.11 ) irule2 = 11

      RETURN
      END
