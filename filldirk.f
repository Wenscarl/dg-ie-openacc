      subroutine filldirk(ls,lsmax,kpm,xgl,wgl,dirk)

cccccccccccccccccccccccccccccccccccccccccccccc
c  fill the matrix dirk for level l
cccccccccccccccccccccccccccccccccccccccccccccc
	use mlfma_const
!	nclude 'mlfma_const.inc'

      integer*8 ls, lsmax, kpm
      real    xgl(lsmax), wgl(lsmax), dirk(3,3,kpm)

      integer*8 it, iph, kp
      real    cst, snt, phi, csp, snp

c      kpt=4+2*ls*ls
      call dgauleg(-1e0,1e0,xgl,wgl,ls,lsmax) 
!xgl is the x-sampling points for Gauss Legendre integral
!wgl is the corresponding weight
      dphi=2e0*pi/real(2*ls)
      do it=1,ls
        cst=xgl(it)
        snt=sqrt((1e0-cst)*(1e0+cst))
        do iph=1,2*ls
          kp=(it-1)*2*ls+iph
          phi=real(iph-1)*dphi
          csp=cos(phi)
          snp=sin(phi)
          dirk(1,1,kp)=snt*csp
          dirk(2,1,kp)=snt*snp
          dirk(3,1,kp)=cst
          dirk(1,2,kp)=cst*csp
          dirk(2,2,kp)=cst*snp
          dirk(3,2,kp)=-snt
          dirk(1,3,kp)=-snp
          dirk(2,3,kp)=csp
          dirk(3,3,kp)=0e0
        enddo
      enddo

      do it=1,-1,-2
        cst=real(it)
        snt=sqrt((1e0-cst)*(1e0+cst))
        do iph=0,1
          kp=kp+1
          phi=real(iph)*pi/2.
          csp=cos(phi)
          snp=sin(phi)
          dirk(1,1,kp)=snt*csp
          dirk(2,1,kp)=snt*snp
          dirk(3,1,kp)=cst
          dirk(1,2,kp)=cst*csp
          dirk(2,2,kp)=cst*snp
          dirk(3,2,kp)=-snt
          dirk(1,3,kp)=-snp
          dirk(2,3,kp)=csp
          dirk(3,3,kp)=0e0
        enddo
      enddo
      return
      end
