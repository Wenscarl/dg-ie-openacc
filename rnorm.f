      real function rnorm(cx,maxedge)
*
*     ||cx(n)||^2
*----------------------------------------------------------------
      integer*8 maxedge, i
      complex cx(maxedge)
      rnorm = 0.
      do i = 1, maxedge
         rnorm = rnorm + abs(cx(i)*cx(i))
      enddo
      return
      end
