      function rlength(xyzip,xyzjp)
*
*   calculate the length from ip'th node to jp'th node.
* -----------------------------------------------------------------
*
      implicit none

      real rlength, xyzip(3), xyzjp(3)
*
      rlength=sqrt((xyzip(1)-xyzjp(1))**2
     &            +(xyzip(2)-xyzjp(2))**2
     &            +(xyzip(3)-xyzjp(3))**2)
      return
      end
