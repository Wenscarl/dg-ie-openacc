      subroutine findntlm(
     &   lmax, lsmax, lsmin, ncmax, kpm, ntlm, nkpm,
     &   lxyz, modes, sizel, ifar, dfar, kpstart)

ccccccccccccccccccccc
c  find ntlm
ccccccccccccccccccccc

      implicit none

      INTEGER*8 lmax, lsmax, lsmin, ncmax, kpm, ntlm, nkpm
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL    sizel(3,0:lmax)
      INTEGER*8 ifar
       REAL    dfar
      INTEGER*8 kpstart(lmax)

      INTEGER*8 ls, kpt, nkp
      INTEGER*8 ig(3), igp(3), ntl, l, nltl, ix, iy, iz

c.....estimate size for ntlm

      ntl = 0
      nkp = 0
      do l = 2, lmax
         ls = modes(l)
         kpt = 4 + 2*ls*ls
         nltl=0
         do ix=max(-(2*ifar+1),1-lxyz(1,l)),min((2*ifar+1),lxyz(1,l)-1)
            ig(1)=1+abs(ix)
         do iy=max(-(2*ifar+1),1-lxyz(2,l)),min((2*ifar+1),lxyz(2,l)-1)
            ig(2)=1+abs(iy)
         do iz=max(-(2*ifar+1),1-lxyz(3,l)),min((2*ifar+1),lxyz(3,l)-1)
            ig(3)=1+abs(iz)
            call ighigh(ig,igp)
            if((sqrt(real((igp(1)-1)**2+(igp(2)-1)**2+(igp(3)-1)**2))
     &         .lt.dfar).and.((abs(ix).gt.ifar).or.(abs(iy).gt.ifar).or.
     &         (abs(iz).gt.ifar).or.
     &         (sqrt(real(ix*ix+iy*iy+iz*iz)).ge.dfar))) then
                nltl=nltl+1
            endif
         enddo
         enddo
         enddo
         ntl = ntl + nltl*kpt
         kpstart(l) = nkp
         nkp = nkp + kpt
      enddo

      ntlm = ntl 
      nkpm = nkp

      return
      end
