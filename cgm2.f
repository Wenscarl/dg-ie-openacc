      subroutine cgm2(ce, cx, icgbcg, itmax, epscg, ier, nunss, 
     &                  cr, cp, ct, cwork1, cwork2, cvf, cvs,
     &                  maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &                  ntlm, niplm, lxyz, modes,
     &                  igall, igcs, igrs, index, family, 
     &                  lrsmup, lrsmdown, csm, cvlr, cgm, ctl,lrstl,
     &                  indextl,
     &                  kpstart, nkpm, c_shift, icsa, irsa, array, 
     &                  ngsndm, igsndcs, igsndrs,
     &                  nearm, canear, noself, ipvt,
     &                  ngnearm, igblkrs, igblkcs, igblk,
     &                  mfinestm, cfinest)

      use mlfma_const
      implicit none

!	nclude 'mlfma_const.inc'

      INTEGER*8 maxedge, lmax, lsmax, ncmax, kpm, kp0, nkpm,
     &        nsm, ntlm, niplm, ngsndm, mfinestm,
     &        nearm, ngnearm
      INTEGER*8 ier, nunss, icgbcg, itmax
       REAL   epscg

      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
      INTEGER*8 irsa(nkpm+1), icsa(niplm)
       REAL   array(niplm)
      COMPLEX ctl(ntlm), csm(2,nsm)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 noself(2,ncmax+1), ipvt(maxedge)    
      COMPLEX cvf(2,kp0,maxedge), cvs(2,kp0,maxedge)
      COMPLEX canear(nearm*maxedge), cfinest(mfinestm)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)
      COMPLEX c_shift(nkpm,2,2,2)
      INTEGER*8 igsndcs(ngsndm), igsndrs(ncmax)      

      COMPLEX ce(maxedge),cx(maxedge)
      COMPLEX cr(maxedge),cp(maxedge),
     &        ct(maxedge)
      COMPLEX cwork1(maxedge), cwork2(maxedge)

      INTEGER*8 neqs, i, k
       REAL   ar, ap, fp0, fp2, rnorm
      neqs = maxedge
      write(6,21) neqs
21    format('                     total unknowns   =',I10)

      write(*,*) 'Begin CG'
      write(*,*) 'Preconditioner initializing'
      write(*,*)  'Preconditioner Finished'
c===========for test
c	ce=b;
      do 18 i=1,neqs
        cx(i) = ce(i)
18      continue

c	ct=Ab;	      
       call  cacxpc(cx, ct, 0, cwork1, cwork2, cvf, cvs, 
     &               maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &               ntlm, niplm, lxyz, modes,
     &               igall, igcs, igrs, index, family, 
     &               lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, 
     &               indextl,
     &               kpstart, nkpm, c_shift, icsa, irsa, array, 
     &               ngsndm, igsndcs, igsndrs,
     &               nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &               igblkcs, igblk, mfinestm, cfinest)
c===========test over
       call  cacxpc(cx, ct, 0, cwork1, cwork2, cvf, cvs, 
     &               maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &               ntlm, niplm, lxyz, modes,
     &               igall, igcs, igrs, index, family, 
     &               lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, 
     &               indextl,
     &               kpstart, nkpm, c_shift, icsa, irsa, array, 
     &               ngsndm, igsndcs, igsndrs,
     &               nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &               igblkcs, igblk, mfinestm, cfinest)
c	now ct= Ab; r=b-Ab ( r= b- ct)
      do 8 i=1,neqs
        cr(i) = ce(i)-ct(i)
8      continue
c	M^{-1} (b-Ab) 
        fp0=rnorm(cr,neqs)
        fp2=fp0
        write( *,*)  'Iterations, Residual Norm, Current |x(1)|', fp2
c	calculate AH r
        call  cacxpc(cr, ct, 1, cwork1, cwork2, cvf, cvs, 
     &               maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &               ntlm, niplm, lxyz, modes,
     &               igall, igcs, igrs, index, family, 
     &               lrsmup, lrsmdown, csm, cvlr, cgm,  ctl, lrstl, 
     &               indextl,
     &               kpstart, nkpm, c_shift, icsa, irsa, array, 
     &               ngsndm, igsndcs, igsndrs,
     &               nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &               igblkcs, igblk, mfinestm, cfinest)
c	calculate p1        
        ar=rnorm(ct,neqs)
      do 30 i=1,neqs
	   cp(i)=ct(i)/ar
30	continue
      do 1000 k = 1, itmax
           ier=k
c	calculate Apk	
        call  cacxpc(cp, ct, 0, cwork1, cwork2, cvf, cvs, 
     &                  maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &                  ntlm, niplm, lxyz, modes,
     &                  igall, igcs, igrs, index, family, 
     &                  lrsmup, lrsmdown, csm, cvlr, cgm,  ctl, lrstl, 
     &                  indextl,
     &                  kpstart, nkpm, c_shift, icsa, irsa, array, 
     &                  ngsndm, igsndcs, igsndrs,
     &                  nearm, canear, noself, ipvt, ngnearm, igblkrs,
     &                  igblkcs, igblk,
     &                  mfinestm, cfinest)
           ap=rnorm(ct,neqs)
       do 40 i=1,neqs
        cx(i)=cx(i)+cp(i)/ap
40         continue
           do 45 i=1,neqs
           cr(i)=cr(i)-ct(i)/ap
45        continue
           fp2=rnorm(cr,neqs)
           write(*, 5) k,sqrt(fp2/fp0),abs(cx(nunss+1))
 5         format(2x,i6,3x,2(2x,e13.6))
           if(sqrt(fp2).gt.1e20) then
              write(*,5) k,sqrt(fp2),abs(cx(nunss+1))
              write(*,*) '**** nan in cg  ****'
              stop
           endif
          if( fp2.lt.epscg*epscg*fp0 ) return
c	calculate the Ar(k+1) 
          call  cacxpc(cr, ct, 1, cwork1, cwork2, cvf, cvs, 
     &                  maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &                  ntlm, niplm, lxyz, modes,
     &                  igall, igcs, igrs, index, family, 
     &                  lrsmup, lrsmdown, csm, cvlr, cgm,  ctl, lrstl,
     &                  indextl,
     &                  kpstart, nkpm, c_shift, icsa, irsa, array, 
     &                  ngsndm, igsndcs, igsndrs,
     &                  nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &                  igblkcs, igblk, mfinestm, cfinest)
           ar=rnorm(ct,neqs)
         do 50 i=1,neqs
            cp(i)=cp(i)+ct(i)/ar
50         continue
1000      continue 
      write(*,*)' too many iterations !'
      return
      end
