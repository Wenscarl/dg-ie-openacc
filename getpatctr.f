      subroutine getpatctr( rc, r ) 
c.....copy coord point 'rc' to 'r' 

      implicit none

c.....Input Data
      real rc(3)

C.....Output Data
      real r(3)

c.....Working Variables
      integer j

      do j = 1,3
         r(j) = rc(j)
      end do

      return
      end
