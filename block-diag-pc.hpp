#ifndef BLOCK_DIAG_PC_HPP
#define BLOCK_DIAG_PC_HPP

#include <complex>
#include <vector>

#include "iml_vector.hpp"
typedef float Real;
typedef long long int Int;
typedef unsigned long long int Unsigned;

class BlockDiagPc {
private:
	typedef std::complex<Real> Complex;

public:
  BlockDiagPc(const Int *igrs, const Int noself[][2], const Int *igall,
	      const Int *igblkrs, const Int *igblkcs, const Int *igblk,
	      const Complex *canear, const Int *index);

  void solve(const iml_vector &x, iml_vector &result);

private:
  // data from mlfmm program
  const Int *igrs;
  const Int (*noself)[2];
  const Int *igall;
  const Int *igblkrs;
  const Int *igblkcs;
  const Int *igblk;
  const Int *index;

  Unsigned maxedge;

  // data for the block diagonal preconditioner
  // array to hold all of the coefficients
  std::vector<Complex> cbdp;
  
  // array indicating where each block begins in cbdp
  std::vector<int> bdpidx;

  // pivot information from lapack
  std::vector<Int> bdpipiv;

  // scratchpad
  std::vector<Complex> scratch;


  Int ipgp;
  Int mself;
  Int indexm;
  Int ipbdp;
  Int ngnear;
  Int jlo;
  Int indexn;
  Int ipnear;
  Int i, j;
  Int mn, mn2;
};

#endif
