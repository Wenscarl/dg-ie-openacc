      function RdotC(x,y)
      implicit COMPLEX
      COMPLEX dotmul, y(3)
      REAL x(3)
      dotmul=x(1)*y(1)+x(2)*y(2)+x(3)*y(3)
      return
      end