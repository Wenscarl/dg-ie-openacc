      subroutine interpl
     &(kpm, niplm, csm, kptp, array, icsa, irsa, cgm)

ccccccccccccccccccccccccccccccccccccccccccc
c  interpolation
c  less points: csm; more points: cgm
ccccccccccccccccccccccccccccccccccccccccccc

      implicit none

      integer*8 kpm, niplm, kptp, icsa(niplm), irsa(kpm+1)
      real array(niplm)
      complex csm(2,*), cgm(2,kpm)

      integer*8 kp, k
      complex ctemp1, ctemp2

      do kp = 1, kptp
         ctemp1 = (0.0,0.0)
         ctemp2 = (0.0,0.0)
         do k = irsa(kp), irsa(kp+1)-1
            ctemp1 = ctemp1 + array(k)*csm(1,icsa(k))
            ctemp2 = ctemp2 + array(k)*csm(2,icsa(k))
         enddo
        cgm(1,kp) = ctemp1
        cgm(2,kp) = ctemp2
      enddo

      return
      end
