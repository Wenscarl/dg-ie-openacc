      subroutine FINDNEAR
     &( maxedge,
     &  lmax, ncmax, nearm, ngnearm, mfinestm,
     &  lxyz, ifar, dfar,
     &  igall, igcs, igrs, index, family, noself)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c   find the self and nearest terms (self, caself),
c   and second nearest terms (near, canear) for fillamn.
c   canear and caself are combined to one, canear, which is storaged
c   using bsr (block sparse row format)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      implicit none

C......Input Data

      INTEGER*8 maxedge
      INTEGER*8 lmax, ncmax
      INTEGER*8 lxyz(3,0:lmax), ifar
       REAL   dfar
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)

c......Output Data

      INTEGER*8 nearm, ngnearm, mfinestm      
      INTEGER*8 noself(2,ncmax+1)     

c......Working Variables
      INTEGER*8 ipnear
      INTEGER*8 ipointer
      INTEGER*8 l,ipself,iglp,igp(3),ix,iy,iz,ig(3),ignear,
     &        noempty,jlo,igl,mself,ipgp,
     &        igt,igtp, ngnear,mselft

c                                 ipgp
c          1=igrs(1)             igrs(2)                         igrs(3)
c                   finest level          next to finest
c noself(1,*)      no. of groups   the firts pointer of sum of unknowns
c noself(2,*)        (unused)      the first pointer of sum of matrices

      mfinestm = 0
      do ipgp=igrs(1),igrs(2)-1
         mself = igall(ipgp+1)-igall(ipgp)
         noself(1,ipgp) = mself
         if (mself .gt. mfinestm) mfinestm = mself
      enddo
      ipself=1
      mselft=1
 
      l=lmax
      noempty=igrs(2)-igrs(1)

      ipointer = igrs(1)
      ipnear = ipself
      ngnear = 0
      do 1000 ipgp=igrs(1),igrs(2)-1
         igt=igcs(ipgp)
         call igxyz(igt,lxyz(1,l),igp)
         igtp=iglp(igt,lxyz(1,l),lxyz(1,l-1))
         mself=noself(1,ipgp)
         jlo=1
         do ix=max(1,igp(1)-ifar),min(lxyz(1,l),igp(1)+ifar)
            ig(1)=ix
         do iy=max(1,igp(2)-ifar),min(lxyz(2,l),igp(2)+ifar)
            ig(2)=iy
         do iz=max(1,igp(3)-ifar),min(lxyz(3,l),igp(3)+ifar)
            ig(3)=iz
            ignear=igl(lxyz(1,l),ig)

c....Does this belong to near neighbors?
            if(sqrt(real((ix-igp(1))**2+(iy-igp(2))**2
     &                  +(iz-igp(3))**2)).lt.dfar) then

c....Is this box empty or not?
               call huntint(igcs(ipointer),noempty,ignear,jlo)
               if( jlo.gt.0) then
                  ngnear = ngnear + 1
                  ipnear=ipnear+mself*noself(1,ipointer+jlo-1)
               endif
            endif
         enddo
         enddo
         enddo
1000  continue

      nearm  = 1 + (ipnear-1)/maxedge
      ngnearm = ngnear

      return
      end
