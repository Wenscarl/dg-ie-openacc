cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c.....calculate the self and nearest terms, and second nearest terms,....c
c.....which are storaged in canear by bsr (block sparse row format)......c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine writeamn
     &(xyznode,ipatpnt,iedge,xyzctr, xyznorm, edge,paera,
     & ngrid, vt1, vt2, vt3, wt,
     & lmax, ncmax, nearm,
     & lxyz,
     & igall, igcs, igrs, index, family, noself,
     & canear, ipvt,
     & ngnearm, igblkrs, igblkcs, igblk,
     & epsr,epsi,amur,amui)

      use mlfma_input
      use mlfma_const
      use mlfma_param

      implicit none

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      INTEGER*8 lmax, ncmax, nearm, ngnearm
      INTEGER*8 lxyz(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 noself(2,ncmax+1)  
      COMPLEX canear(nearm*maxedge)
      INTEGER*8 ipvt(maxedge)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)
       REAL   epsr(2),epsi(2),amur(2),amui(2)

      INTEGER*8 ipointer
      INTEGER*8 l,ipself,ipnear,m,iglp,igp(3),ix,iy,iz,ig(3),ignear,
     &        noempty,jlo,n,igl,mself,i,j,ipgp,
     &        igt,igtp, ngnear, mn, nself, indexm, indexn

      INTEGER*8 ii
 
      COMPLEX ck(2),ceps(2),cmu(2)
      COMPLEX ceta(2),ce(2),ch(2)

      INTEGER*8 k

      INTEGER*8 rrec

      write(*,*) 'Begin writeamn.f'

        open (95,file='FileamnData',
     &  form='unformatted',access='direct',recl=8*maxedge,
     &               status='unknown')

!        do j=1,maxedge
!          do i=1,nearm
!             k=(i-1)*maxedge+j
!              write(95,rec=k) canear(k)
!           end do
!        end do

        rrec=1

        do k=1,nearm

          write(95,rec=rrec)
     &   (canear((k-1)*maxedge+j),j=1,maxedge)
          rrec = rrec+1

        end do

       close (95)


        open (195,file='FileamnIndex',
     &  form='unformatted',access='direct',
     &  recl=8*(maxedge+ncmax+1+ngnearm+ngnearm+2),

     &               status='unknown')

         write(195,rec=1)
     &  (ipvt(k),k=1,maxedge),
     &   (igblkrs(k), k=1,ncmax+1),
     &   (igblkcs(k), k=1,ngnearm+1),
     &    (igblk(k), k=1,ngnearm+1)
  

        close(195)

      return
      end
