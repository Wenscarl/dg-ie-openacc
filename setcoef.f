      subroutine SETCOEF
     &( maxrule, maxgrid, vt1,vt2,vt3,wt,ngrid )

      IMPLICIT NONE
      
c.....Input Data

      INTEGER*8 maxrule, maxgrid

c.....Output Data

       REAL vt1(maxgrid,maxrule), vt2(maxgrid,maxrule)
       REAL vt3(maxgrid,maxrule),  wt(maxgrid,maxrule)
      INTEGER*8 ngrid(maxrule)

C.....Working Variables

      INTEGER*8 irule, n, i
      real u(64), v(64), ww(64), sqrt3, sqrt15


      sqrt3 = 1.73205080756888
      sqrt15 = 3.87298334620742
    
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c...........initial values for the grids are:........................c
c...........the first point is the weight center of the triangle.....c
c...........the 2nd,3rd and 4th point are the 1st, 2nd and 3rd.......c 
c............vertices of the triangle.................................c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      do 10 irule = 1, maxrule

         u(1) = 0.0
         v(1) = 0.0
         u(2) = 1.0
         v(2) = 0.0
         u(3) = -0.5
         v(3) =  0.5*sqrt3
         u(4) = -0.5
         v(4) = -0.5*sqrt3
   
         if( irule.eq.1 ) then
            n = 1
            ww(1) = 1.0
         else if( irule.eq.2 ) then
            n = 4
            u(2) = 0.5
            v(2) = 0.0
            u(3) = -0.25
            u(4) = -0.25
            v(3) =  0.25*sqrt3
            v(4) = -0.25*sqrt3
            ww(1) = 1.0/4.0
            ww(2) = 1.0/4.0
            ww(3) = 1.0/4.0
            ww(4) = 1.0/4.0
         else if( irule.eq.3 ) then
            n = 4
            ww(1) = 3.0/4.0
            ww(2) = 1.0/12.0
            ww(3) = 1.0/12.0
            ww(4) = 1.0/12.0
         else if( irule.eq.4 ) then
            n = 7
            ww(1) = 27.0 / 60.0
            ww(2) =  3.0 / 60.0
            ww(3) =  3.0 / 60.0
            ww(4) =  3.0 / 60.0
            ww(5) =  8.0 / 60.0
            ww(6) =  8.0 / 60.0
            ww(7) =  8.0 / 60.0
            u(5) = -0.5
            u(6) =  0.25
            u(7) =  0.25
            v(5) =  0.0
            v(6) =  0.25 * sqrt3
            v(7) = -0.25 * sqrt3
         else if( irule.eq.5 ) then
            n = 7
            ww(1) = 27.0/120.0
            ww(2) = ( 155.0 - sqrt15 ) / 1200.0
            ww(3) = ww(2)
            ww(4) = ww(2)
            ww(5) = ( 155.0 + sqrt15 ) / 1200.0
            ww(6) = ww(5)
            ww(7) = ww(5)
            u(2) = ( 1.0 + sqrt15 ) / 7.0
            u(3) = (-1.0 - sqrt15 ) / 14.0
            u(4) = u(3)
            u(5) = ( 1.0 - sqrt15 ) / 7.0
            u(6) = ( sqrt15 - 1.0 ) / 14.0
            u(7) = u(6)
            v(2) = 0.0
            v(3) = ( sqrt15 + 1.0 ) * sqrt3 / 14.0
            v(4) =-v(3)
            v(5) = 0.0
            v(6) = ( sqrt15 - 1.0 ) * sqrt3 / 14.0
            v(7) =-v(6)
         else if((irule.ge.6).and.(irule.le.7) ) then
c            call getuvw(irule,n,u,v,ww)
         else if(irule.eq.8) then
            n=3
            ww(1) = 1.0/3.0
            ww(2) = 1.0/3.0
            ww(3) = 1.0/3.0
            u(1) =-3.0/16.0
            u(2) =-3.0/16.0
            u(3) = 6.0/16.0
            v(1) = 3.0*sqrt3/16.0
            v(2) =-3.0*sqrt3/16.0
            v(3) = 0.0
         else if(irule.eq.9) then
            n=1
            u(1) = 0.0
            ww(1) = 1.0
         else if(irule.eq.10) then
            n=2
            u(1) = -0.5773502692
            u(2) =  0.5773502692
            ww(1) = 0.5
            ww(2) = 0.5
         else if(irule.eq.11) then
            n=3
            u(1) = -0.7745966692
            u(2) =  0.0
            u(3) =  0.7745966692
            ww(1) = 5.0/18.0
            ww(2) = 8.0/18.0
            ww(3) = 5.0/18.0
         else
            write(*,*) 'rule not implemented.'
			call exit(-1)
         end if

         ngrid(irule) = n
         if( irule.le.8 ) then
            do i=1,n
               vt1(i,irule) = ( 1.0 - u(i) + sqrt3 * v(i) ) /3.0
               vt2(i,irule) = ( 1.0 - u(i) - sqrt3 * v(i) ) /3.0
               vt3(i,irule) = ( 1.0 + u(i) * 2.0 ) / 3.0
               wt(i,irule) = ww(i)
            end do
         else
            do i=1,n
               vt1(i,irule) = ( 5.0 + u(i) ) / 12.0
               vt2(i,irule) = ( 5.0 + u(i) ) / 12.0
               vt3(i,irule) = ( 1.0 - u(i) ) /  6.0
               wt(i,irule) = ww(i)
            end do
         end if

10    continue

      RETURN
      END
      
