      subroutine filltl
     &( lmax, lsmax, lsmin, ncmax, kpm, ntlm, niplm, nkpm,
     &  lxyz, modes, sizel,
     &  ctl, lrstl, indextl,
     &  xgl, wgl, dirk, kpstart, rk0, ci, cnste, c_shift, 
     &  icsa, irsa, array)

ccccccccccccccccccccccccccccccccccccccccccccccc
c  fill the transformation matrix ctl
c  fill shifting matrix c_shift
c  fill interpolation matrix array
ccccccccccccccccccccccccccccccccccccccccccccccc

	use mlfma_const
      implicit none

!      nclude 'mlfma_const.inc'
      
      INTEGER*8 lmax, lsmax, lsmin, ncmax, kpm, ntlm, niplm, nkpm
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 lrstl(lmax+1),
     &        indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &        -(2*ifar+1):(2*ifar+1),1:lmax)
      COMPLEX ctl(ntlm)
      INTEGER*8 kpstart(lmax)
       REAL   xgl(lsmax, lmax), wgl(lsmax, lmax), dirk(3,3,nkpm)
       REAL   rk0
      COMPLEX ci, cnste, c_shift(nkpm,2,2,2)
      INTEGER*8 icsa(niplm), irsa(nkpm)
       REAL   array(niplm)

      INTEGER*8   modemax
      PARAMETER(modemax=2000)
       REAL     trash(0:modemax), plgndr(0:modemax), window(0:modemax)
      COMPLEX   cspbh(0:modemax), phitmp(2*modemax)

      INTEGER*8   ls, kpt, nkp, lsp, lsc, indexa
       REAL     dphi    
      INTEGER*8   ig(3), igp(3), ntl, l, nltl ,kp, ix, iy, iz, ll, 
     &          nflat, ntail, it,iph,lsum
       REAL     xmn(3), xk0, xk
      COMPLEX   c0, cweight, cil, ctemp
       REAL     dxyz(3), shift(3), temp
      write(*,*) 'Begin filltl.f'
      call flush()	
c  check if lsmax overflows

       if(lsmax.gt.modemax) then
          write(*,*) 'lsmax is greater than modemax'
          write(*,*) 'please increase modemax in filltl'
          write(*,*) 'lsmax = ', lsmax
          write(*,*) 'modemax = ', modemax
       endif

c   fill translation matrix ctl
	write(*,*) 'fill translation matrix'
	call flush()
      c0=rk0/4e0/pi*ci*(-cnste)*rk0*rk0 
c                     ! last 3 terms comes from lu's efie
      do ix= -(2*ifar+1),(2*ifar+1)
        do iy=-(2*ifar+1),(2*ifar+1)
         do iz=-(2*ifar+1),(2*ifar+1)
           do ll=1,lmax
                indextl(ix,iy,iz,ll)=0.0
           enddo
         enddo
        enddo
      enddo
      ntl = 0
      nkp = 0
      lrstl(2) = ntl + 1
      do l = 2, lmax
	 write(*,*) 'Level: ',l
	 call flush()
         nltl = 0
         ls = modes(l)
         lsum=ls-1
         kpt = 4 + 2*ls*ls
         dphi = 2e0*pi/real(2*ls)
         nflat = (ls+1)/2
         ntail = ls - nflat
         do ll = 0, nflat
            window(ll)=1.
         enddo
         do ll = 1, ntail
            window(ll+nflat)=1.
         enddo
         call filldirk(ls, lsmax, kpt, xgl(1, l), wgl(1,l),
     &                dirk(1,1,nkp+1) )
         do ix=max(-(2*ifar+1),1-lxyz(1,l)),min((2*ifar+1),lxyz(1,l)-1)
            ig(1) = 1 + abs(ix)
            xmn(1) = real(ix)*sizel(1,l)
         do iy=max(-(2*ifar+1),1-lxyz(2,l)),min((2*ifar+1),lxyz(2,l)-1)
            ig(2) = 1 + abs(iy)
            xmn(2)=real(iy)*sizel(2,l)
         do iz=max(-(2*ifar+1),1-lxyz(3,l)),min((2*ifar+1),lxyz(3,l)-1)
            ig(3) = 1 + abs(iz)
            xmn(3) = real(iz)*sizel(3,l)
            call ighigh(ig,igp)
            if((sqrt(real((igp(1)-1)**2+(igp(2)-1)**2+(igp(3)-1)**2))
     &      .lt.dfar).and.((abs(ix).gt.ifar).or.(abs(iy).gt.ifar).or.
     &       (abs(iz).gt.ifar).or.
     &      (sqrt(real(ix*ix+iy*iy+iz*iz)).ge.dfar))) then
               nltl = nltl + 1
               indextl(ix,iy,iz,l) = nltl
               if((ntl+kpt).gt.ntlm) then 
					write(*,*) 'overflow in no. of ctl'
					call exit(-1)
				endif
               xk0 = rk0*sqrt(xmn(1)**2+xmn(2)**2+xmn(3)**2)
               call dsbesjh(xk0,ls,trash,cspbh,lsmax)
               do it = 1, ls
                  cweight = c0*dphi*wgl(it,l)
!$OMP PARALLEL DO PRIVATE(kp,xk,plgndr,ctemp,cil,ll)
!$OMP& SCHEDULE(STATIC,10)
                  do iph = 1, 2*ls
                     kp = iph + 2*ls*(it-1)
                     xk = ( dirk(1,1,kp+nkp)*xmn(1)
     &                    + dirk(2,1,kp+nkp)*xmn(2)
     &                    + dirk(3,1,kp+nkp)*xmn(3))*rk0/xk0
                     if((abs(xk).gt.1.).and.(abs(xk).lt.(1.+1.e-6))) 
     &                    xk = xk/abs(xk)
                     call dlegendr(xk,ls,0,plgndr,lsmax)
                     ctemp = cspbh(0)*plgndr(0)
                     cil = ci
                     do ll = 1, ls !lsum
                        ctemp = ctemp + float(2*ll+1)*plgndr(ll)
     &                       *cil*cspbh(ll)*window(ll)
                        cil = cil*ci
                     enddo
                     phitmp(iph) = cweight*ctemp
c$$$                     ntl = ntl + 1
c$$$                     ctl(ntl) = cweight*ctemp
                  enddo
!$OMP END PARALLEL DO
                  do iph = 1, 2*ls
                     ntl = ntl + 1
                     ctl(ntl) = phitmp(iph)
                  enddo
               enddo
               do kp = kpt-3, kpt
                  ntl =ntl + 1
                  ctl(ntl) = 0.
               enddo
            else
               indextl(ix,iy,iz,l)=0
            endif
         enddo
         enddo
         enddo

c.....fill shifting matrix
	write(*,*) 'fill shifting matrix'
	  call flush()
         if(l.lt.lmax) then
            do ix = 1, 3
               if(lxyz(ix,l).eq.lxyz(ix,l+1)) then
                  dxyz(ix) = 0.0
               else
                  dxyz(ix) = 0.25*sizel(ix,l)
               endif
            enddo
            do ix = 1, 2
               shift(1) = dxyz(1)*(2*ix-3)
            do iy = 1, 2
               shift(2) = dxyz(2)*(2*iy-3)
            do iz = 1, 2
               shift(3) = dxyz(3)*(2*iz-3)
               do kp = nkp+1, nkp+kpt
                  temp = 0.0
                  do it = 1, 3
                     temp = temp + shift(it)*dirk(it,1,kp)
                  enddo
                  c_shift(kp,ix,iy,iz) = exp((rk0*temp*ci))
               enddo
            enddo
            enddo
            enddo
         endif
*
         lrstl(l+1) = ntl + 1
         nkp = nkp + kpt
      enddo

c....fill interpolation matrix
      write(*,*) 'fill interpolation matrix'
	call flush()
      nkp = 0
      indexa = 0
      irsa(1) = indexa + 1
      do l = 2, lmax-1
         lsp = modes(l)
         lsc = modes(l+1)
         kpt = 4 + 2*lsp*lsp
         call fillarray(lsmax, kpt, niplm, lsc, lsp, xgl(1,l+1), 
     &        xgl(1,l), array, icsa, irsa(nkp+1), indexa)

         nkp = nkp + kpt
      enddo

      return
      end
