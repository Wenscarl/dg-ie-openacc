//
// C++ Implementation: densecmat
//
// Description: 
//
//
// Author: Feiran Lei,,,, <flei@esl0245.osuesl.net>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "densecmat.h"
//#include "Constants.h"


denseCMat::denseCMat() : N(0), M(0), entry(0)
{
}

denseCMat::denseCMat(const denseCMat &right)
{
  Int i, NZ;

  N = right.N;
  M = right.M;
  NZ = N*M;

  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i++) entry[i] = right.entry[i];
}


denseCMat::denseCMat(Int rDim, Int cDim)
{
  Int NZ;

  N = rDim;
  M = cDim;
  NZ = N * M;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
}

denseCMat::~denseCMat()
{
  if (entry != 0) delete [] entry;
}

void denseCMat::delMat()
{
  N = 0;
  M = 0;
  if (entry != 0) {
    delete [] entry;
    entry = 0;
  }
}

void denseCMat::setDim(Int rowDim, Int colDim)
{
  Int NZ;

  if (N == rowDim && M == colDim) {
    NZ = N*M;

#pragma omp parallel for schedule(static)
    for (Int i = 0; i < NZ; i++) entry[i] = 0.0;
    return;
  }

  if (entry != 0) delete [] entry;

  N = rowDim;
  M = colDim;
  NZ = N * M;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
}

void denseCMat::addEntry(Int rr, Int cc, Complex value)
{
  Int nn;

  nn = rr * M + cc;
  entry[nn] = entry[nn] + value;
}

void denseCMat::addEntry(Int nn, Complex value) // added by MV 01/09/03
{
  entry[nn] = entry[nn] + value;
}

void denseCMat::reset()
{
  Int i, NZ;

  NZ = N * M;
  if (NZ == 0) return;
  else 
  
#pragma omp parallel for schedule(static)
  for (i = 0; i < NZ; i ++) entry[i] = 0.0;
}

void denseCMat::setCol( Int col, Vector &vect )
{
  if ( vect.size() != static_cast<unsigned Int>(getRowDim()) ) {
    cout <<"incompatible vector and matrix Row dimensions in denseCMat::setCol"
	 << endl;
    cout << "vect.size()=" << vect.size() << endl;
    cout << "getRowDim()=" << getRowDim() << endl;
    exit(0);
  }
  if ( col > getColDim() ) {
    cout << "col is grater than matrix column dimension in denseCMat::setCol"
	 << endl;
    cout << "col=" << col << endl;
    cout << "getColDim()=" << getColDim() << endl;
    exit(0);
  }

  Int rr, nn;

#pragma omp parallel for schedule(static) private(rr, nn)
  for ( rr = 0; rr < N; rr++ ) {
    nn = rr * M + col;
    entry[nn] = entry[nn] + vect(rr);
  }

}

void denseCMat::setRow( Int row, Vector &vect )
{
  if ( vect.size() != static_cast<unsigned Int>(getColDim()) ) {
    cout <<"incompatible vector and matrix Row dimensions in denseCMat::setCol"
	 << endl;
    exit(0);
  }
  if ( row > getRowDim() ) {
    cout << "row is grater than matrix row dimension in denseCMat::setCol"
	 << endl;
    exit(0);
  }

  Int cc, nn;

#pragma omp parallel for schedule(static) private(cc, nn)
  for ( cc = 0; cc < M; cc++ ) {
    nn = row * M + cc;
    entry[nn] = entry[nn] + vect(cc);
  }

}

Complex denseCMat::getEntry(Int rr, Int cc) const
{
  Int nn;

  nn = rr * M + cc;
  return entry[nn];
}

Complex* denseCMat::getColEntry()
{
  Complex* colEntry = new Complex[N*M];
  if (!colEntry) {
    cerr << "denseCMat::getColEntry: couldn't allocate memory for matrix" << endl;
    return 0;
  }

  Int rr, cc;

#pragma omp parallel for schedule(static) private(rr, cc)
  for (rr = 0; rr < N; rr++) {
    for (cc = 0; cc < M; cc++) {
      colEntry[cc*N + rr] = entry[rr*M + cc];
    }
  }

  return colEntry;
}

denseCMat& denseCMat::operator = (const denseCMat &right)
{
  Int i, NZ;

  if (N == right.N && M == right.M) {
    NZ = N*M;

#pragma omp parallel for schedule(static) private(i)
    for (i = 0; i < NZ; i++) entry[i] = right.entry[i];
    return *this;
  }

  N = right.N;
  M = right.M;
  NZ = N*M;

  if (entry != 0) delete [] entry;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i++) entry[i] = right.entry[i];

  return *this;
}

denseCMat denseCMat::operator + (const denseCMat &operand2) const
{
  denseCMat sum;
  Int i, j, nn;
  Complex cval;

  if (N != operand2.N || M != operand2.M) {
    cout << "Error in Performing Matrix Sum " << endl;
    exit(1);
  }

  sum.setDim(N, M);
  nn = 0;

#pragma omp parallel for schedule(static) private(i, j, cval) firstprivate(nn)
  for (i = 0; i < N; i ++) {
    for (j = 0; j < M; j ++) {
      cval = entry[nn];
      cval = cval + operand2.getEntry(i, j);
      nn ++;

      sum.addEntry(i, j, cval);
    }
  }

  return sum;
}

denseCMat denseCMat::operator - (const denseCMat &operand2) const
{
  denseCMat sub;
  Int i, j, nn;
  Complex cval;

  if (N != operand2.N || M != operand2.M) {
    cout << "Error in Performing Matrix Subtraction " << endl;
    exit(1);
  }

  sub.setDim(N, M);
  nn = 0;
 
//#pragma omp parallel for schedule(static) private(i, j, cval) firstprivate(nn)
  for (i = 0; i < N; i ++) {
    for (j = 0; j < M; j ++) {
      cval = entry[nn];
      cval = cval - operand2.getEntry(i, j);
      nn ++;

      sub.addEntry(i, j, cval);
    }
  }

  return sub;
}

denseCMat denseCMat::operator * (const denseCMat &operand2) const
{
  denseCMat mult;
  Int i, j, k, row, col;
  Complex cval, aval, bval;

  if (M != operand2.N) {
    cout << "Error in Matrix Multiplication " << endl;
    exit(1);
  }

  mult.setDim(N, operand2.M);

#pragma omp parallel for schedule(static) private(i, j, cval, aval, bval, row, col)
  for (i = 0; i < N; i ++) {
    row = i;
    for (j = 0; j < operand2.M; j ++) {
      col = j;

      cval= Complex (0.0, 0.0);
      for (k = 0; k < M; k ++) {
	aval = getEntry(i, k);
	bval = operand2.getEntry(k, j);
	cval = cval + aval * bval;
      }
      mult.addEntry(row, col, cval);
    }
  }

  return mult;
}

denseCMat denseCMat::transpose()
{
  denseCMat transP;
  Int i, j, row, col;
  Complex cval;

  transP.setDim(M, N);

#pragma omp parallel for schedule(static) private(i, j, col, row, cval)
  for (i = 0; i < M; i ++) {
    row = i;
    for (j = 0; j < N; j ++) {
      col = j;
      cval = getEntry(col, row);
      transP.addEntry(row, col, cval);
    }
  }

  return transP;
}

denseCMat denseCMat::hermitian()
{
        denseCMat transP;
        Int i, j, row, col;
        Complex cval;

        transP.setDim(M, N);

#pragma omp parallel for schedule(static) private(i, j, cval, row, col)
        for (i = 0; i < M; i ++) {
                row = i;
                for (j = 0; j < N; j ++) {
                        col = j;
                        cval = getEntry(col, row);
                        transP.addEntry(row, col, conj(cval));
                }
        }

        return transP;
}

void denseCMat::setEntry(Int rr, Int cc, Complex value)
{
  Int nn;
  if (N*M == 0) 
  {
    cout << "Error in denseCMat::setEntry" << endl;
    exit(0);
  }
  nn = rr * M + cc;
  entry[nn] = value;
}

void denseCMat::print()
{
  Int i, j, nn;
  FILE *foo;

  foo = fopen("elem.mat", "a");

  nn = 0;

  for (i = 0; i < N; i ++) {
    for (j = 0; j < M; j ++) {
      fprintf(foo, "( %e, %e ) ", entry[nn].real(),
	      entry[nn].imag());
      if ((j + 1) % 3 == 0) fprintf(foo, "\n");

      nn ++;
    }
    fprintf(foo, "\n");
  }
  fprintf(foo, "\n\n\n");
  fclose(foo);
}

// added by MV 06/02/03
void denseCMat::appendOnASCIIFile( char *filename )
{
  FILE *foo;
  Int  i;

  foo = fopen(filename, "at");
  if (foo == NULL) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  fprintf(foo, "%lld  %lld\n", this->N, this->M);
  for (i =0; i < this->N * this->M; i++) {
    fprintf(foo, "%e  %e\n",
	    getEntry( i / M, i % M ).real(), getEntry( i / M, i % M ).imag());
    //    if ((i % M) == (M-1)) fprintf(foo, "\n");
  }
  fclose(foo);
}

// added by MV 06/02/03
void denseCMat::appendOnBinaryFile( char *filename )
{
  FILE *foo;

  foo = fopen(filename, "ab");
  if ( foo == NULL) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  fwrite( &N, sizeof(Int), 1, foo );
  fwrite( &M, sizeof(Int), 1, foo );
  fwrite( this->entry, sizeof(Complex), (this->N) * (this->M), foo );
  fclose(foo);
}


// added by MV 11/07/02
void denseCMat::printOnASCIIFile( char *filename )
{
  FILE *foo;
  Int  i;

  foo = fopen(filename, "wt");
  if (foo == NULL) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  fprintf(foo, "%lld  %lld\n", this->N, this->M);
  for (i =0; i < this->N * this->M; i++) {
    fprintf(foo, "%e  %e\n",
	    getEntry( i / M, i % M ).real(), getEntry( i / M, i % M ).imag());
    //    if ((i % M) == (M-1)) fprintf(foo, "\n");
  }
  fclose(foo);

}

// added by MV 11/18/02
void denseCMat::printOnBinaryFile( char *filename )
{
  FILE *foo;

  foo = fopen(filename, "wb");
  if ( foo == NULL) {
    fprintf(stderr, "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  fwrite( &N, sizeof(Int), 1, foo );
  fwrite( &M, sizeof(Int), 1, foo );
  fwrite( this->entry, sizeof(Complex), (this->N) * (this->M), foo );
  fclose(foo);
}

// added by MV 11/05/02
void denseCMat::printScreen()
{
  Int i, j;
  cout << "RowDim=" << this->getRowDim()
       << " ColDim=" << this->getColDim() << endl;
  for (i = 0; i < this->getRowDim(); i ++) {
    for (j = 0; j < this->getColDim(); j ++) {
      if (abs(getEntry(i,j)) < 1.e-10) continue;
      cout<< i << " " << j  << " ("
          << this->getEntry(i, j).real() << ", "
          << this->getEntry(i, j).imag() << ")" << endl;
    }
    cout << endl;
  }
}

// added by MV 11/05/02
void denseCMat::printScreen_1()
{
  Int i, NZ;
  NZ = (this->N) * (this->M);

  printf("%lld  %lld\n", N, M);
  for (i =0; i < NZ; i++) {
    printf( "%e  %e\n", entry[i].real(), entry[i].imag());
    //    if ((i % Nsrc) == (Nsrc-1)) fprintf(foo, "\n");
  }
}
/*
void denseCMat::printMatlab(ostream &sout, char* name, Int prec)
{
  Int i, j;

  sout.setf(ios::scientific);
  sout.precision(prec);
  sout << name << " = [" << endl;
  for (i = 0; i < N; i++) {
    for (j = 0; j < M; j++) {
      sout << " complex("
           << setw(prec+7) << entry[i*M + j].real() << ","
           << setw(prec+8) << entry[i*M + j].imag() << ")";
    }
    sout << ";" << endl;
  }
  sout << "];" << endl;
}
*/
void denseCMat::readFromASCIIFile( char *filename )
{
  Int i, NZ;
  int ret;
  float real, imag;
  FILE *fd;

  fd = fopen( filename, "rt");
  if (fd == NULL) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  ret = fscanf( fd, "%lld %lld\n", &this->N, &this->M );
  NZ = N * M;
  if (entry) delete [] entry;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
    for ( i = 0; i < NZ; i++ ) {
      ret = fscanf( fd, "%e %e\n", &real, &imag );
      this->entry[i] = Complex(real, imag);
    }
  fclose(fd);
}


void denseCMat::readFromBinaryFile( char *filename )
{
  Int NZ;
  FILE *fd;

  fd = fopen( filename, "rb");
  if (fd == NULL) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  size_t reterror;
  reterror = fread( &(N), sizeof(Int), 1, fd );
  reterror = fread( &(M), sizeof(Int), 1, fd );
  NZ = N * M;
  if (entry != 0) delete [] entry;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
  reterror = fread( entry, sizeof(Complex), NZ, fd );
  fclose(fd);
}

void denseCMat::readFromBinaryFile( FILE *fd )
{
  Int NZ;
  size_t reterror;
  reterror = fread( &(this->N), sizeof(Int), 1, fd );
  reterror = fread( &(this->M), sizeof(Int), 1, fd );
  NZ = (this->N) * (this->M);
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
  reterror = fread( this->entry, sizeof(Complex), NZ, fd );
}

void denseCMat::readFromASCIIFileTrans( char *filename )
{
  Int r,c, NZ;
  int reterror;
  float real, imag;
  FILE *fd;

  fd = fopen( filename, "rt");
  if (fd == NULL) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  reterror = fscanf( fd, "%lld %lld\n", &this->N, &this->M );
  NZ = N * M;
  if (entry) delete [] entry;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
  for ( c = 0; c < M; c++ ) {
    for ( r = 0; r < N; r++ ) {
      reterror = fscanf( fd, "%e %e\n", &real, &imag );
      //      this->entry[i] = Complex(real, imag);
      this->entry[r * M + c] = Complex(real, imag);
    }
  }
  fclose(fd);
}


void denseCMat::readFromBinaryFileTrans( char *filename )
{
  Int r,c, NZ;
  size_t ret;
  //  float real, imag;
  FILE *fd;

  fd = fopen( filename, "rb");
  if (fd == NULL) {
    printf( "Couldn't open file %s ! \n", filename);
    exit(1);
  }
  ret = fread( &(this->N), sizeof(Int), 1, fd );
  ret = fread( &(this->M), sizeof(Int), 1, fd );
  NZ = N * M;
  if (entry != 0) delete [] entry;
  if (NZ == 0) entry = 0;
  else entry = new Complex[NZ];
  for ( c = 0; c < M; c++ ) {
    for ( r = 0; r < N; r++ ) {
      //      fscanf( fd, "%le %le\n", &real, &imag );
      //      this->entry[r * M + c] = Complex(real, imag);
      ret = fread( &(this->entry[r * M + c]), sizeof(Complex), 1, fd );
    }
  }
  fclose(fd);
}

denseCMat &denseCMat::operator*=(const float &val)
{
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) {
    entry[i] = entry[i] * val;
  }

  return *this;
}

denseCMat &denseCMat::operator/=(const float &val)
{
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) {
    entry[i] = entry[i] / val;
  }

  return *this;
}

Vector denseCMat::operator * (const Vector &x) const
{

  if( x.size() != static_cast<unsigned Int>(getColDim()) ) {
    cout << "incopatible matrix and vector dimensions in denseCMat::operator*"
	 << endl;
    cout << "x.size()=" << x.size()
	 << " A.getColDim=" << this->getColDim() <<endl;
    exit(0);
  }


  Int R = getRowDim();
  Int C = getColDim();
  Complex cval;
  Vector y((unsigned Int)R);


  // perform matrix vector multiplication
  Int r, c;

#pragma omp parallel for schedule(static) private(r, c, cval)
  for (r = 0; r < R; r ++) {
    for (c = 0; c < C; c ++) {
      cval = getEntry(r, c) * (x[c]);
      y[r] = y[r] + cval;
    }
  }
  return y;
}

denseCMat denseCMat::operator * (const Complex &val) const
{
  denseCMat mult(N, M);
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i++) mult.entry[i] = entry[i] * val;

  return mult;
}

denseCMat denseCMat::operator * (const float &val) const
{
  denseCMat mult(N, M);
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i++) mult.entry[i] = entry[i] * val;

  return mult;
}

denseCMat denseCMat::operator / (const Complex &val) const
{
  denseCMat result(N, M);
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i++) result.entry[i] = entry[i] / val;

  return result;
}

denseCMat denseCMat::operator / (const float &val) const
{
  denseCMat result(N, M);
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i++) result.entry[i] = entry[i] / val;

  return result;
}

denseCMat &denseCMat::operator*=(const Complex &val)
{
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) {
    entry[i] *= val;
  }

  return *this;
}

denseCMat &denseCMat::operator/=(const Complex &val)
{
  Int i, NZ;

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) {
    entry[i] /= val;
  }

  return *this;
}

denseCMat &denseCMat::operator+=(const denseCMat &operand2)
{
  Int i, NZ;

  if (N != operand2.N ||
      M != operand2.M) {
    cout << "Adding two different sized dense Matrix " << endl;
    exit(1);
  }

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) {
    entry[i] += operand2.entry[i];
  }

  return *this;
}

denseCMat &denseCMat::operator-=(const denseCMat &operand2)
{
  Int i, NZ;

  if (N != operand2.N ||
      M != operand2.M) {
    cout << "Adding two different sized dense Matrix " << endl;
    exit(1);
  }

  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) {
    entry[i] -= operand2.entry[i];
  }

  return *this;
}

void Gaussj(denseCMat *a)
{
  Int    n, *indxc, *indxr, *ipiv;
  Int    i, icol = 0, irow = 0, j, k, l, ll;
  float big;
  Complex  dum, pivinv, tval, unitVAL(1.0, 0.0), zeroVAL(0.0, 0.0);

  if (a->getRowDim() != a->getColDim()) {
    printf("Trying to Invert a non-Square matrix \n");
    exit(1);
  }
  n = a->getRowDim();
  indxc = new Int[n];
  indxr = new Int[n];
  ipiv = new Int[n];

#pragma omp parallel for schedule(static) private(j)
  for (j = 0; j < n; j ++) ipiv[j] = 0;

//#pragma omp parallel for schedule(static) private(i, j, k, big, irow, icol, tval, pivinv, dum)
  for (i = 0; i < n; i ++) {
    big = 0.0;
    for (j = 0; j < n; j ++)
      if (ipiv[j] != 1)
	for (k = 0; k < n; k ++) {
	  if (ipiv[k] == 0) {
	    if (abs(a->getEntry(j, k)) >= big) {
	      big = abs(a->getEntry(j, k));
	      irow = j; icol = k;
	    }
	  } else if (ipiv[k] > 1) {
	    printf("Gaussj: Singular Matrix -- 1\n");
	    exit(1);
	  }
	}
    ++(ipiv[icol]);
    if (irow != icol) {
      for (l = 0; l < n; l ++) {
	tval = a->getEntry(irow, l);
	a->setEntry(irow, l, a->getEntry(icol, l));
	a->setEntry(icol, l, tval);
      }
    }
    indxr[i] = irow;
    indxc[i] = icol;
    if (abs(a->getEntry(icol, icol)) < 1.e-12) {
      printf("Gaussj: Singular Matrix -- 2\n");
      exit(1);
    }
    pivinv = unitVAL / (a->getEntry(icol, icol));
    a->setEntry(icol, icol, unitVAL);

    for (l = 0; l < n; l ++) {
      a->setEntry(icol, l, (a->getEntry(icol, l)) * pivinv);
    }
    for (ll = 0; ll < n; ll ++)
      if (ll != icol) {
	dum = a->getEntry(ll, icol);
	a->setEntry(ll, icol, zeroVAL);

	for (l = 0; l < n; l ++) {
	  tval = a->getEntry(icol, l) * dum;
	  a->setEntry(ll, l, (a->getEntry(ll, l)) - tval);
	}
      }
  }

#pragma omp parallel for schedule(static) private(l, k, tval)
  for (l = (n-1); l >= 0; l --) {
    if (indxr[l] != indxc[l])
      for (k = 0; k < n; k ++) {
	tval = a->getEntry(k, indxr[l]);
	a->setEntry(k, indxr[l], a->getEntry(k, indxc[l]));
	a->setEntry(k, indxc[l], tval);
      }
  }

  delete[] indxc;
  delete[] indxr;
  delete[] ipiv;
}

denseCMat denseCMat::inverse()
{
  denseCMat invert;
  Int i, NZ;

  invert.setDim(N, M);
  NZ = N * M;

#pragma omp parallel for schedule(static) private(i)
  for (i = 0; i < NZ; i ++) invert.entry[i] = entry[i];
  Gaussj(&invert);

  return invert;
}

Complex denseCMat::operator()(Int i, Int j)  const // added my MV at 12/02/02
{
  if (( i > N ) || ( j > M ) || ( i < 0 ) || ( j < 0 )) {
    cout << "Invalid bounds on denseCMat access operator ()!! ("
	 << i << ", " << j << ")" << endl;
    exit(0);
  } else
    return this->getEntry(i, j);
}







// OVERLOAD function
//   VHXMXV(Int, Complex *, denseCMat, Complex *);
//   VHXMXV(Int, Complex *, Complex **, Complex *);
//   VHXMXV(Int, Complex *, float **, Complex *);
// VHXMXV is used to compute X^(H) x M x Y. dim is the dimension of the matrix
// xv is the X vector on the left and yv is the vector on right and m is the square matrix.
Complex VHXMXV(Int dim, Complex *xv, denseCMat m, Complex *yv)
{
  Complex xval, mval, rval(0.0, 0.0);
  Int     i, j;

  for (i = 0; i < dim; i ++) {
    xval = Complex (xv[i].real(), -xv[i].imag());

    for (j = 0; j < dim; j ++) {
      mval = m.getEntry(i, j);
      rval = rval + (xval * mval) * yv[j];
    }
  }

  return rval;
}
Complex VHXMXV(Int dim, Complex *xv, Complex *m[], Complex *yv)
{
  Complex xval, rval(0.0, 0.0);
  Int     i, j;

  for (i = 0; i < dim; i ++) {
    xval = Complex (xv[i].real(), -xv[i].imag());

    for (j = 0; j < dim; j ++)
      rval = rval + (xval * m[i][j]) * yv[j];
  }

  return rval;
}

Complex VHXMXV(Int dim, Complex *xv, float *m[], Complex *yv)
{
  Complex xval, rval(0.0, 0.0);
  Int     i, j;

  for (i = 0; i < dim; i ++) {
    xval = Complex (xv[i].real(), -xv[i].imag());

    for (j = 0; j < dim; j ++)
      rval = rval + (xval * m[i][j]) * yv[j];
  }

  return rval;
}

void MXV(const denseCMat &a, Complex *x, Complex *y)
{
  Int i, j;
  Complex cval;

  // first reset y
  for (i = 0; i < a.getRowDim(); i ++) y[i] = Complex (0.0, 0.0);

  // perform matrix vector multiplication
  for (i = 0; i < a.getRowDim(); i ++) {
    for (j = 0; j < a.getColDim(); j ++) {
      cval = (a.getEntry(i, j)) * (x[j]);

      y[i] = y[i] + cval;
    }
  }
}
