	subroutine dsbesy(cx,n,csy,nd)
c>>>        implicit complex (c)
c>>>        implicit real (a-b,d-h,o-z)
        implicit real*4 (a-h,o-z)
        dimension csy(0:nd)
        parameter (iacc=40,bigni=1.d-10,bigno=1.d+10)
c>>>>>        open(unit=12,file='junk.out',status='unknown')
c
        cxinv = 1.0e0/cx
c        
c --- explicit calculation of low order bessel functions.
        if (n.eq.0) then
            csy(0)=-cos(cx)*cxinv
            return
        end if
        if (n.eq.1) then
            csy(0)=-cos(cx)*cxinv
            csy(1)=csy(0)*cxinv-sin(cx)*cxinv
            return
        end if

c --- recurrence relations used to calculate higher orders.
        csy(0)=-cos(cx)*cxinv
        csy(1)=csy(0)*cxinv-sin(cx)*cxinv
        csyb=csy(0)
        csym=csy(1)
        do 11 j=2,n
          csyp=(2.0e0*real(j-1)+1.)*cxinv*csym-csyb
          csyb=csym
          csym=csyp
          csy(j)=csym
  11    continue
        return
        end
