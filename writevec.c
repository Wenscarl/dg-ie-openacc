#include <stdio.h>
#include <stdlib.h>

typedef long long int Integer;
typedef float Real;

void writevec_(Integer* dim, Real* data) {
  FILE* fp;
  fp = fopen("rhs","wb");
  if (fp == NULL) {
    printf("Error in output rhs!\n");
    exit(-1);
  }
  fwrite(data, 2*(*dim), sizeof(Real), fp);
  fclose(fp);
}
