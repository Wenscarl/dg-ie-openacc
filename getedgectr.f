      subroutine getedgectr( r1,r2,ectr )
c.....Get center coord of an edge - between node 'r1' and 'r2'

      implicit none

c.....Input Data
      real r1(3), r2(3)

c.....Output Data
      real ectr(3)

c.....Working Variables
      integer j

      do j=1,3
         ectr(j) = 0.5*( r1(j) + r2(j) )
      end do

      return
      end
