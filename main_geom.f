       subroutine main_geom
     &( memory_tot,
     &  xyznode, ipatpnt, iedge,
     &  xyzctr, xyznorm, edge, paera,
     &  ngrid, vt1, vt2, vt3, wt,
     &  family)

        use mlfma_input
	  use mlfma_const
        use mlfma_param

        implicit none

!        nclude 'mlfma_input.inc'
!        nclude 'mlfma_const.inc'
!        nclude 'mlfma_param.inc'

	INTERFACE
	    SUBROUTINE bld_lmax(lmax)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_lmax':: bld_lmax
	        integer*8 lmax
!DEC$ ATTRIBUTES REFERENCE ::lmax 
          END SUBROUTINE

	    SUBROUTINE c_main_box(memory_tot,lmax)
!DEC$ ATTRIBUTES C,ALIAS:'_c_main_box':: c_main_box
		  real    memory_tot
	        integer*8 lmax 
!DEC$ ATTRIBUTES REFERENCE ::memory_tot,lmax 
          END SUBROUTINE
      END INTERFACE

       REAL   memory_tot
      integer*8 lmax
      integer*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode)
       REAL   xyzctr(3,maxpatch), xyznorm(3,maxpatch),
     &        edge(maxedge),      paera(maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      integer*8 ngrid(maxrule)
      integer*8 family(maxedge)

       REAL   edgelong, edgeavrg, rlengmax

	call read_geom(maxnode, maxpatch, maxedge,
     &                 xyznode, ipatpnt, iedge)

	call checksize(maxnode, maxpatch, maxedge,
     &                 xyznode, ipatpnt, iedge, wl0,
     &                 edgelong, edgeavrg, rlengmax)

 
c.......decide number of levels

	lmax=0
	
10	if (rlengmax.gt.0.3) then
	   lmax=lmax+1
	   rlengmax = 0.5*rlengmax
	   goto 10
	endif

	if (lmax.eq.0) lmax=1


	if (lmax.eq.1) then
	   print*,'             Full Matrix Is Used'
	else
           write(*,12) LMAX
 12   format('                              LMAX    =',i4)

	endif

        call bld_lmax(lmax)

        memory_tot = memory_tot + 
     &              lmax*2*4 + (lmax+2)*4 + (1+lmax)*(3*4+3*4+3*4)

        call c_main_box(memory_tot, lmax)

	return
	end 
