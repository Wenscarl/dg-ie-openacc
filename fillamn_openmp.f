cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c.....calculate the self and nearest terms, and second nearest terms,....c
c.....which are storaged in canear by bsr (block sparse row format)......c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine fillamn
     &(xyznode,ipatpnt,iedge,xyzctr, xyznorm, edge,paera,
     & ngrid, vt1, vt2, vt3, wt,
     & lmax, ncmax, nearm,
     & lxyz,
     & igall, igcs, igrs, index, family, noself,
     & canear_e, canear_m, bc_canear, ipvt,
     & ngnearm, igblkrs, igblkcs, igblk,
     & epsr,epsi,amur,amui)

      use mlfma_input
      use mlfma_const
      use mlfma_param

      implicit none


!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL(real_kind)   xyznode(3,maxnode), edge(maxedge),
     &     xyzctr(3, maxpatch), paera(maxpatch), xyznorm(3, maxpatch)
       REAL(real_kind)   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      INTEGER*8 lmax, ncmax, nearm, ngnearm
      INTEGER*8 lxyz(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 noself(2,ncmax+1)  
      COMPLEX(cplx_kind) canear_e(nearm*maxedge),
     &     canear_m(nearm*maxedge), bc_canear(nearm*maxedge)
      INTEGER*8 ipvt(maxedge)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm), 
     &        igblk(ngnearm)
       REAL(real_kind)   epsr(2),epsi(2),amur(2),amui(2)

      INTEGER*8 ipointer
      INTEGER*8 l,ipself,ipnear,m,iglp,igp(3),ix,iy,iz,ig(3),ignear,
     &        noempty,jlo,n,igl,mself,i,j,ipgp,
     &        igt,igtp, ngnear, mn, nself, indexm, indexn, tmp

      INTEGER*8 ii
 
      COMPLEX(cplx_kind) ck(2),ceps(2),cmu(2)
      COMPLEX(cplx_kind) ceta(2),ce(2),ch(2)

cccccccccccccccccccccccccccccccccccccccccccccccccc
c......Prepare for calculating interaction.......c
c............between near elements...............c
cccccccccccccccccccccccccccccccccccccccccccccccccc


 	do ii=1,2
	   ceps(ii)=cmplx(epsr(ii),epsi(ii))
	   cmu(ii) =cmplx(amur(ii),amui(ii))
	   ck(ii)=rk0*sqrt(ceps(ii)*cmu(ii))*(1.0, 0.0e-5)
	   ceta(ii)=eta0*sqrt(cmu(ii)/ceps(ii))
	   ce(ii)=ci*ck(ii)*ceta(ii)
	   ch(ii)=(eta0*eta0)*ci*ck(ii)/ceta(ii)
	enddo

	ipself=1


ccccccccccccccccccccccccccccccccccccccccc
c.....Find near interaction element.....c
c.....not belong to precondition part...c
ccccccccccccccccccccccccccccccccccccccccc

      l=lmax
      ipointer=igrs(1)
      noempty=igrs(2)-igrs(1)
      ipnear = ipself
      ngnear = 0

      do 1000 ipgp=igrs(1),igrs(2)-1
         igblkrs(ipgp) = ngnear + 1
         igt=igcs(ipgp)
         call igxyz(igt,lxyz(1,l),igp)
         igtp=iglp(igt,lxyz(1,l),lxyz(1,l-1))
         mself=noself(1,ipgp)
         jlo=1
         do ix=max(1,igp(1)-ifar),min(lxyz(1,l),igp(1)+ifar)
            ig(1)=ix
         do iy=max(1,igp(2)-ifar),min(lxyz(2,l),igp(2)+ifar)
            ig(2)=iy
         do iz=max(1,igp(3)-ifar),min(lxyz(3,l),igp(3)+ifar)
            ig(3)=iz
            ignear=igl(lxyz(1,l),ig)

cccccccccccccccccccccccccccccccccccccccccc
c...is this belong to near neighbors?....c
cccccccccccccccccccccccccccccccccccccccccc

            if(sqrt(real((ix-igp(1))**2+(iy-igp(2))**2
     &                  +(iz-igp(3))**2)).lt.dfar) then

cccccccccccccccccccccccccccccccccccccccc
c.....is this box empty or not?........c
cccccccccccccccccccccccccccccccccccccccc

               call huntint(igcs(ipointer),noempty,ignear,jlo)
               if(jlo.gt.0) then
                  ngnear = ngnear + 1
                  igblk(ngnear) = ipnear
                  igblkcs(ngnear) = jlo
                  ipnear=ipnear+mself*noself(1,ipointer+jlo-1)
               endif
            endif
         enddo
         enddo
         enddo
1000  continue

ccccccccccccccccccccccccccccccccccccccccccccccccccc
c....Calculate the near interaction entries.......c
c....not belong to precondition part..............c
ccccccccccccccccccccccccccccccccccccccccccccccccccc
 
      igblkrs(igrs(2)) = ngnear + 1
!      igblk(ngnear+1)=ipnear-1
      tmp = (igrs(2)-igrs(1)) / 10
      if (tmp.eq.0) then
         tmp = 1
      endif
!$OMP PARALLEL PRIVATE(mself,indexm,ngnear,jlo,
!$OMP&nself,indexn,ipnear,i,m,j,n,mn)
!$OMP DO
	 do ipgp = igrs(1), igrs(2)-1
         mself = noself(1,ipgp)
         indexm = igall(ipgp) - 1
         if (mod(ipgp,tmp) .eq. 0) then
            print *,ipgp,'/',(igrs(2)-igrs(1))
         endif
         do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
            jlo = igblkcs(ngnear)
            nself = noself(1,jlo)
            indexn = igall(jlo) - 1
            ipnear = igblk(ngnear)

            do i = 1, mself
               m = indexm + i
               do j = 1, nself
                  n = indexn + j
                  mn = ipnear + (i-1)*nself + j-1
                  call camnone(xyznode,ipatpnt,iedge,
     &                         xyzctr, xyznorm, edge,paera,  
     &                         ngrid, vt1, vt2, vt3, wt,
     &                         ceps,cmu,ck,ceta,ce,ch,
     &                         index(m), index(n),
     &                 canear_e(mn), canear_m)
               enddo
            enddo

c$$$            call rwg_camn(indexm, indexn, mself, nself, index,
c$$$     $           ipgp, igrs(2)-1, canear_e(ipnear),
c$$$     $           canear_e(nearm*maxedge))
         enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL

c$$$      call build_sparse_pc_nonsmooth(xyznorm, igrs, noself, igall,
c$$$     $     igblkrs, igblkcs, igblk, index, canear)

c     figure out sparsity pattern of near matrix so that it
c     can be preallocated
c      call allocate_near_z(igrs, noself, igall, igblkrs, igblkcs, index)

c     fill in BC near matrix
      do ipgp = igrs(1), igrs(2)-1
         mself = noself(1,ipgp)
         indexm = igall(ipgp) - 1
         do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
            jlo = igblkcs(ngnear)
            nself = noself(1,jlo)
            indexn = igall(jlo) - 1
            ipnear = igblk(ngnear)
c$$$            call bc_camn(indexm, indexn, mself, nself, index,
c$$$     $           ipgp, igrs(2)-1, bc_canear(ipnear))
         enddo
      enddo

c$$$      do ipgp = igrs(1), igrs(2)-1 
c$$$      mself = noself(1,ipgp)
c$$$      indexm = igall(ipgp) - 1
c$$$      do ngnear = igblkrs(ipgp), igblkrs(ipgp+1)-1
c$$$         jlo = igblkcs(ngnear)
c$$$         nself = noself(1,jlo)
c$$$         indexn = igall(jlo) - 1
c$$$         ipnear = igblk(ngnear)
c$$$
c$$$         do i = 1, mself
c$$$            m = indexm + i
c$$$            do j = 1, nself
c$$$               n = indexn + j
c$$$               mn = ipnear + (i-1)*nself + j-1
c$$$               call get_bc_z_near(index(m), index(n), bc_canear(mn))
c$$$            enddo
c$$$         enddo
c$$$      enddo
c$$$      enddo


      return
      end
