      subroutine INITIAL_INTE
     &( maxrule, maxgrid, match,
     &  irule_fld, irule_src, irulef_near, irules_near,
     &  vt1, vt2, vt3, wt,
     &  ngrid)

      IMPLICIT NONE

c.....Input Data

      INTEGER*8 maxrule, maxgrid, match ! maxrule =11, maxgrid = 64, match=1
      INTEGER*8 irule_fld, irule_src ! irule_fld = 2, irule_src = 2
c.....Output Data

      INTEGER*8 irulef_near, irules_near
       REAL vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &      vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c..... check consistancy between 'match' and 'irule_fld'..........c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      if( match.eq.1 ) then
         if( irule_fld .gt. 8 ) then
           write(*,*) 'conflict in irule_fld and match'
           stop 
         end if
      end if
      if( match.eq.2 ) then
         if( irule_fld .le. 8 ) then
           write(*,*) 'conflict in irule_fld and match'
           stop 
         end if
      end if

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
c.. .....set parameters (grids,coefficients) for numerical.........c 
c.............integration over an arbitrary triangle...............c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

       call setcoef(maxrule, maxgrid, vt1, vt2, vt3, wt, ngrid)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c........... ......set near-field integral rules...................c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      call setrule(irule_fld,irulef_near) !irulef_near = 5
      call setrule(irule_src,irules_near) !irules_near = 5

      if(irule_src.eq.8) then
         irulef_near = irule_fld
         irules_near = irule_src
      end if

      RETURN
      END
