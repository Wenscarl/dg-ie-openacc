//
//      dd_vector.h        Basic vector class (CMPLX precision)
//
// modified from: Sparselib_1_5d/include/tmvec.h
//                Sparselib_1_5d/inclued/tblas1.h
#ifndef MYVECTOR_H
#define MYVECTOR_H
#include <math.h>           // for blas1
#include <stdlib.h>         // for blas1
#include <iostream>       // for formatted printing of matrices
#include "float_complex.h"
#include "comp.h"

using namespace std;

#ifdef VECTOR_BOUNDS_CHECK
#include <assert.h>
#endif

typedef long long int Int;
typedef unsigned long long int Unsigned;
 
class Vector
{
 protected:
  Complex *p_;
  Unsigned dim_;
 public:

  /*::::::::::::::::::::::::::*/
  /* Constructors/Destructors */
  /*::::::::::::::::::::::::::*/
  Vector();
  Vector(Unsigned);
  Vector(Unsigned, const Complex &);
  Vector(Int n, float val );
  Vector(const Vector &);
Vector(Unsigned n, const Complex* data);
  ~Vector();

  /*::::::::::::::::::::::::::::::::*/
  /*  Indices and access operations */
  /*::::::::::::::::::::::::::::::::*/
  // code for operator() is defined here, otherwise some compilers
  // (e.g. Turbo C++ v 3.0) cannot inline them properly...
  //
  Complex&       operator()(Unsigned i)
    {
#ifdef VECTOR_BOUNDS_CHECK
      assert(i < dim_);
#endif
      return p_[i];
    }

  const  Complex&    operator()(Unsigned i) const
    {
#ifdef VECTOR_BOUNDS_CHECK
      assert(i < dim_);
#endif
      return p_[i];
    }


  Complex&       operator[](Unsigned i)
    {
#ifdef VECTOR_BOUNDS_CHECK
      assert(i < dim_);
#endif
      return p_[i];
    }


  const  Complex&    operator[](Unsigned i) const
    {
#ifdef VECTOR_BOUNDS_CHECK
      assert(i < dim_);
#endif
      return p_[i];
    }

  //
  //   the following line causes ambiguatities with template instantiations
  //   should be avoid.  Used &v(0) explicitly when converting to TYPE*.
  //
  //    inline                operator const  TYPE*() const {return p_;}
  inline Unsigned             size() const { return dim_;}
  //  inline Int                      ref() const { return  ref_;}
  inline Int                      null() const {return dim_== 0;}
  //
  // Create a new *uninitalized* vector of size N
  Vector & newsize(Unsigned );

  inline void reset()
    {
      Unsigned i;
      for (i = 0; i < dim_; i ++) p_[i] = 0.0;
    }

  /*::::::::::::::*/
  /*  Assignment  */
  /*::::::::::::::*/
  Vector & operator=(const Vector&);
  Vector & operator=(const Complex&);
  Vector & operator=(const float&);

  /*::::::::::::::*/
  /*  Print out   */
  /*::::::::::::::*/
  friend ostream& operator<<(ostream &s, const Vector &A);


  //get function
  //=============
  Complex *getVtrPtr() { return p_;}

  //set function
  //=============
  void setVtrPtr(Complex *p) { p_ = p; }
  void setSize( Int dim );
  // Utility functions

  void conjugate();

  // read and write on a file
  //==========================
  void readFromBinaryFile( char *filename, Int dim );
  void readFromBinaryFile( char *filename);
  void printOnBinaryFile( char *filename );
  void appendOnBinaryFile( char *filename);
  void readFromASCIIFile( char *filename);
  void printOnASCIIFile( char *filename);
  void appendOnASCIIFile( char *filename);
};

//================================
// BLAS1 OPERATIONS
//================================
Vector& operator*=(Vector &x, const Complex &a);
Vector operator*(const Complex &a, const Vector &x);
Vector operator*(const Vector &x, const Complex &a);
Vector operator*(const Vector &x, const float &a);  // added
Vector operator+(const Vector &x, const Vector &y);
Vector operator-(const Vector &x, const Vector &y);
Vector& operator+=(Vector &x, const Vector &y);
Vector& operator-=(Vector &x, const Vector &y);

//===================================
// DOT PRODUCT AND 2-NORM OPERATIONS
//===================================
Complex dot(const Vector &x, const Vector &y);
Complex dotHerm(const Vector &x, const Vector &y);
float norm2(const Vector &x);
float normInf(const Vector &x);
float norm1(const Vector &x);

#endif
/* Vector_H */
