#ifndef NORM_COB_HPP
#define NORM_COB_HPP

#include <vector>
#include <complex>
#include "iml_vector.hpp"

typedef long long int Int;
typedef unsigned long long int Unsigned;

typedef std::complex<float> Complex_FMM;

typedef Complex_FMM::value_type Real;


class NormalizationPc {
private:

public:
  NormalizationPc(const Int *igrs, const Int noself[][2], const Int *igall,
			     const Int *igblkrs, const Int *igblkcs, const Int *igblk,
			     const Complex_FMM *canear, const Int *index);

  void mult_U(const Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size) const;
  void mult_UH(const Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size) const;
  void mult_V(const Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size) const;
  void mult_VH(const Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size) const;

  template <class Vector>
  Vector solve(const Vector &x)
  {
    Vector result(x.size());
    Int x_size = x.size(), result_size = result.size();
    
    iml_vector xx(x_size);
    iml_vector rr(result_size);

    for (Int i = 0; i < x_size; i++)
	{
		xx(i) = x(i);
	}

    mult_UH(&xx(0), &x_size, &rr(0), &result_size);
    mult_V(&rr(0), &result_size, &rr(0), &result_size);
  
    for (Int i = 0; i < x_size; i++)
	{
		result(i) = rr(i);
	}
    return result;
  }

  template <class Vector>
  Vector solve_trans(const Vector &x)
  {
    Vector result(x.size());
    Int x_size = x.size(), result_size = result.size();
    mult_VH(&x(0), &x_size, &result(0), &result_size);
    mult_U(&result(0), &result_size, &result(0), &result_size);
    return result;
  }

  Unsigned reducedDim() const { return N-dropSize; }

private:
  // near field matrix information from mlfmm
  const Int *igrs;
  const Int (*noself)[2];
  const Int *igall;
  const Int *igblkrs;
  const Int *igblkcs;
  const Int *igblk;
  const Int *index;

  Unsigned N;
  Unsigned dropSize;

  Unsigned GetDropSize(const std::vector<Real> &S) const;
  void GetSvd(std::vector<Complex_FMM> &A, std::vector<Complex_FMM> &U,
	      std::vector<Complex_FMM> &VT, std::vector<Real> &S,
	      const Int mself) const;

  struct RawCobBlock {
    std::vector<Complex_FMM> U;
    std::vector<Complex_FMM> VT;
    Unsigned highDropSize;
    Unsigned lowDropSize;
  };

  std::vector<RawCobBlock> rawCobZ, rawCobDZ;
  std::vector<Unsigned> vtr_index;
  
  void writeUV() const;
};


extern "C" {
  void initialize_ncob_(Int *igrs, Int noself[][2], Int *igall, Int *igblkrs,
			Int *igblkcs, Int *igblk, std::complex<float> *canear,
			Int *index);

  void mult_u_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size);
  void mult_uh_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size);
  void mult_v_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size);
  void mult_vh_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size);
}

#endif
