      subroutine  igxyz(igt,lxyz,ig)

c.....find #ig(x,y,z) from #igl
      
      integer*8 igt,lxyz(3),ig(3),ixy

      ixy  =1+(igt-1)/lxyz(3)
      ig(1)=1+(ixy-1)/lxyz(2)
      ig(3)=igt-lxyz(3)*(ixy  -1)
      ig(2)=ixy-lxyz(2)*(ig(1)-1)

      return
      end
