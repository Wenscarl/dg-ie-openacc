#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>

#include "ident_pc.hpp"
#include "myvector.h"
#include "matrix_fmm.hpp"
#include "norm-cob.hpp"


// #include "gcrodr.hpp"
// #include "complex.h"

#include "gcr.hpp"

typedef long long int Int;

// this needs to match whatever precision the mlfmm code is using
typedef std::complex<float> Complex_FMM;

extern "C" {
  void cfie_solve_(Complex_FMM *crhs, Complex_FMM *cx, Int *icgbcg, Int *itmax,
		   float *epscg, Int *ier, Int *nunss, Complex_FMM *cr,
		   Complex_FMM *cp, Complex_FMM *ct, Complex_FMM *cwork1, Complex_FMM *cwork2,
		   Complex_FMM *cvf, Complex_FMM *cvs, Int *maxedge, Int *lmax,
		   Int *lsmax, Int *ncmax, Int *kpm, Int *kp0, Int *nsm,
		   Int *ntlm, Int *niplm, Int *lxyz, Int *modes, Int *igall,
		   Int *igcs, Int *igrs, Int *index, Int *family, Int *lrsmup,
		   Int *lrsmdown, Complex_FMM *csm, Complex_FMM *cvlr, Complex_FMM *cgm,
		   Complex_FMM *ctl, Int *lrstl, Int *indextl, Int *kpstart,
		   Int *nkpm, Complex_FMM *c_shift, Int *icsa, Int *irsa,
		   float *array, Int *ngsndm, Int *igsndcs, Int *igsndrs,
		   Int *nearm, Complex_FMM *canear, Int noself[][2], Int *ipvt,
		   Int *ngnearm, Int *igblkrs, Int *igblkcs, Int *igblk,
		   Int *mfinestm, Complex_FMM *cfinest);
}

  void cfie_solve_(Complex_FMM *crhs, Complex_FMM *cx, Int *icgbcg, Int *itmax,
		   float *epscg, Int *ier, Int *nunss, Complex_FMM *cr,
		   Complex_FMM *cp, Complex_FMM *ct, Complex_FMM *cwork1, Complex_FMM *cwork2,
		   Complex_FMM *cvf, Complex_FMM *cvs, Int *maxedge, Int *lmax,
		   Int *lsmax, Int *ncmax, Int *kpm, Int *kp0, Int *nsm,
		   Int *ntlm, Int *niplm, Int *lxyz, Int *modes, Int *igall,
		   Int *igcs, Int *igrs, Int *index, Int *family, Int *lrsmup,
		   Int *lrsmdown, Complex_FMM *csm, Complex_FMM *cvlr, Complex_FMM *cgm,
		   Complex_FMM *ctl, Int *lrstl, Int *indextl, Int *kpstart,
		   Int *nkpm, Complex_FMM *c_shift, Int *icsa, Int *irsa,
		   float *array, Int *ngsndm, Int *igsndcs, Int *igsndrs,
		   Int *nearm, Complex_FMM *canear, Int noself[][2], Int *ipvt,
		   Int *ngnearm, Int *igblkrs, Int *igblkcs, Int *igblk,
		   Int *mfinestm, Complex_FMM *cfinest)
{
  Matrix_Fmm Z_Rwg(cx, ct, cwork1,
		   cvf, cvs, maxedge, lmax, lsmax,
		   ncmax, kpm, kp0, nsm, ntlm, niplm,
		   lxyz, modes, igall, igcs, igrs, index,
		   family, lrsmup, lrsmdown, csm, cvlr,
		   cgm, ctl, lrstl, indextl, kpstart,
		   nkpm, c_shift, icsa, irsa, array,
		   ngsndm, igsndcs, igsndrs, nearm, noself,
		   canear, ngnearm, igblkrs, igblkcs,
		   igblk, mfinestm, cfinest);

  ident_pc M_Ident;

  NormalizationPc norm_cob(igrs, noself, igall, igblkrs,
			   igblkcs, igblk, canear, index);

  Vector rhs(*maxedge), x(*maxedge);

  for (Int i = 0; i < *maxedge; ++i)
    rhs(i) = crhs[i];

//   fstream fout;
//       fout.open("matrix_fmm",ios::out|ios::binary);
// 
//   
//  for (Int i= 0; i<*maxedge; i++){
//    
//     x = 0.;
//     x(i) = 1.;
// 
//     Vector tmp(*maxedge);
//     tmp = Z_Rwg*x;
//     
//     fout.write((char*)(&tmp(0)), (*maxedge)*sizeof(Complex));
// 
// 
//   }
//   
//   fout.close();

  Int trunc = 30;
  Int recy = 30;
  std::cout << "Number of truncation: " << recy << endl;

//  std::cout << *epscg << std::endl;
//  gcrodr(Z_Rwg, norm_cob, rhs, x, *epscg, *itmax, trunc, recy);

 gcr(Z_Rwg, norm_cob, rhs, x, *epscg, *itmax, recy);
  
  for (Int i = 0; i < *maxedge; ++i)
   rhs(i) = crhs[i];
  std::cout << "CFIE residual: " << norm2(Z_Rwg*x-rhs) / norm2(rhs) << std::endl;
}
