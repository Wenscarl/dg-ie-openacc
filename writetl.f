      subroutine writetl
     &( lmax, lsmax, lsmin, ncmax, kpm, ntlm, niplm, nkpm,
     &  lxyz, modes, sizel,
     &  ctl, lrstl, indextl,
     &  xgl, wgl, dirk, kpstart, rk0, ci, cnste, c_shift, 
     &  icsa, irsa, array)

ccccccccccccccccccccccccccccccccccccccccccccccc
c  fill the transformation matrix ctl
c  fill shifting matrix c_shift
c  fill interpolation matrix array
ccccccccccccccccccccccccccccccccccccccccccccccc

	use mlfma_const
      implicit none

!      nclude 'mlfma_const.inc'
      
      INTEGER*8 lmax, lsmax, lsmin, ncmax, kpm, ntlm, niplm, nkpm
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 lrstl(lmax+1),
     &        indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &        -(2*ifar+1):(2*ifar+1),1:lmax)
      COMPLEX ctl(ntlm)
      INTEGER*8 kpstart(lmax)
       REAL   xgl(lsmax, lmax), wgl(lsmax, lmax), dirk(3,3,nkpm)
       REAL   rk0
      COMPLEX ci, cnste, c_shift(nkpm,2,2,2)
      INTEGER*8 icsa(niplm), irsa(nkpm)
       REAL   array(niplm)

      INTEGER*8   modemax
      PARAMETER(modemax=2000)
       REAL     trash(0:modemax), plgndr(0:modemax), window(0:modemax)
      COMPLEX   cspbh(0:modemax)

      INTEGER*8   ls, kpt, nkp, lsp, lsc, indexa
       REAL     dphi    
      INTEGER*8   ig(3), igp(3), ntl, l, nltl ,kp, ix, iy, iz, ll, 
     &          nflat, ntail, it,iph,lsum
       REAL     xmn(3), xk0, xk
      COMPLEX   c0, cweight, cil, ctemp
       REAL     dxyz(3), shift(3), temp

      INTEGER*8  i,j,k,m

      write(*,*) 'Begin writetl.f'

	open (92,file='Filetl',status='unknown')

         write(92,*) ( ctl(i), i=1,ntlm )

         write(92,*) ( lrstl(i), i=1,lmax+1)

           do k=-(2*ifar+1),(2*ifar+1)
         do j=-(2*ifar+1),(2*ifar+1)
       do i=-(2*ifar+1),(2*ifar+1)
               write(92,*) (indextl(i,j,k,m), m=1,lmax)
             end do
           end do
         end do

       do m=1,2
         do k=1,2
           do j=1,2
              write(92,*) (c_shift(i,j,k,m),i=1,nkpm)
           end do
         end do
       end do

         write(92,*)( icsa(i), i=1,niplm)

         write(92,*)( irsa(i), i=1,nkpm)

         write(92,*)( array(i), i=1,niplm)

       do i=1,3
         do j=1,3
             write(92,*)( dirk(i,j,k),k=1,nkpm)
         end do
       end do

        close(92)


      return
      end
