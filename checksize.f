      subroutine checksize(
     &   maxnode, maxpatch, maxedge,
     &   xyznode, ipatpnt, iedge, wl0,
     &   edgelong, edgeavrg, rlengmax)
      
c.....find maximum size of all patches.
      
      implicit none
      
      integer*8 maxnode, maxpatch, maxedge
      integer*8 iedge(4,maxedge),    ipatpnt(3,maxpatch)
      real xyznode(3,maxnode), wl0, wl0m1, wl0m2
      
      integer*8 ip, jp, i, j, k, l, ifound
      real rlength, rl, rmax, dmin, ravg, 
     &stotal, s, smax, smin, aera3, r1(3), r2(3), r3(3),
     &edgelong, edgeavrg, rlengmax, posmax, posmin 
      
c.....find longest, shortest, and average edge
      
      rmax=0.0
      dmin=1e9
      ravg=0.0
      wl0m1=1./wl0
      wl0m2=wl0m1*wl0m1
      do i = 1, maxedge
            ip = iedge(1, i)
            jp = iedge(2, i)
            rl = rlength(xyznode(1,ip),xyznode(1,jp))
            if (rl .gt. rmax) rmax = rl
            if (rl .lt. dmin) dmin = rl
            ravg = ravg + rl
      enddo
            edgelong = rmax*wl0m1
            edgeavrg = ravg*wl0m1/float(maxedge)
      
      write(6,10)  maxnode 
      write(6,11)  maxpatch
      write(6,12)  maxedge
      write(6,13)  edgelong 
      write(6,14)  dmin*wl0m1
      write(6,15)  edgeavrg 
10    format('    maxnode                           =',1i9)
11    format('    maxpatch                          =',1i9)
12    format('    maxedge                           =',1i9)
13    format('    longest  edge in lambda           =',e10.3)
14    format('    shortest edge in lambda           =',e10.3)
15    format('    average  edge in lambda           =',e10.3)
      
      if(edgelong.gt.0.5 .or. edgeavrg.gt.0.2) then
        write(6,*)
        write(6,*)'Warning: The present MoM requires:'
        write(6,*)'   Longest  edge in lambda < 0.5'
        write(6,*)'   Average  edge in lambda < 0.2'
        write(6,*)'But the present facet file does NOT satisfy above'
        write(6,*)'Hence the solution of FastMoM may be inaccurate'
      endif
      
c....calculated the total area
      
      smax   = 0.0
      smin   = 1e20
      stotal = 0.0
      do ip = 1, maxpatch
            do i = 1, 3
            r1(i) = xyznode(i, ipatpnt(1, ip))
            r2(i) = xyznode(i, ipatpnt(2, ip))
            r3(i) = xyznode(i, ipatpnt(3, ip))
            enddo
            s = aera3(r1, r2, r3)    
            if (s .gt. smax) smax = s
            if (s .lt. smin) smin = s
            stotal = stotal + s
      enddo
      write(6,21) smax*wl0m2
      write(6,22) smin*wl0m2
      write(6,23) stotal*wl0m2/float(maxpatch)
21    format('    largest  patch area in sq lambda  =',e10.3)
22    format('    smallest patch area in sq lambda  =',e10.3)
23    format('    average  patch area in sq lambda  =',e10.3)
      
c....check the connections
      
      do i = 1, maxedge
            do j = 1, 2
		ip = iedge(j, i)
		do k = 3, 4
			jp = iedge(k, i)
			ifound = 0
			do l = 1, 3
				if(ip .eq. ipatpnt(l,jp)) ifound = ifound + 1
			enddo
! 			if(ifound .ne.1) then
! 				write(*,*) '*** wrong index'
! 				write(*,*) i, ip, jp,  iedge(1, i), 
!      &      iedge(2, i), iedge(3, i), iedge(4, i)
! 			endif
		enddo
            enddo
      enddo
      
c...find the size range of the object
      
      rlengmax = 0.0
            
            do i=1,3
		j= ipatpnt(1,1)
		posmax = xyznode(i,j)
		posmin = posmax 
		do j=1,maxpatch
			do k=1,3
				ip = ipatpnt(k,j)
				if (xyznode(i,ip).gt.posmax) posmax=xyznode(i,ip)
				if (xyznode(i,ip).lt.posmin) posmin=xyznode(i,ip)
			enddo
		enddo
		rlengmax = max(rlengmax, (posmax-posmin))
            enddo
      
            rlengmax = rlengmax*wl0m1
      
c....      write(6,25) rlengmax
c.....25    format('size range of the object in sq lambda =',e7.3)
      call flush()
      
      
      return
      end
