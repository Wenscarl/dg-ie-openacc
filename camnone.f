      subroutine camnone(xyznode,ipatpnt,iedge,
     &                   xyzctr, xyznorm,edge,paera,
     &                   ngrid, vt1,vt2, vt3, wt, 
     &                   ceps,cmu,ck,ceta,ce,ch,
     &                   ii, jj, camn,contourMap)

      use mlfma_input
      use mlfma_const
      use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'
	INTEGER*8 contourMap(maxedge)
      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      COMPLEX ck(2),ceps(2),cmu(2),ceta(2),ce(2),ch(2)
      INTEGER*8 ii, jj
      COMPLEX camn, dummy

      COMPLEX cnxet, cnxhx, cnxet2
       REAL   sing
      INTEGER*8 far
      sing=-1
      far=1
!        if( (ii==3) .and.(jj==1.or.jj==2)) then
	call mtxele(ii, jj, ck(1), sing, match,
     &            irule_fld, irule_src, irulef_near, irules_near, 
     &            maxnode, maxpatch, maxedge, maxrule, maxgrid,
     &            xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &            ngrid, vt1, vt2, vt3, wt, rnear2, distmin,
     &            rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &            cnxet, cnxhx)
!       call mtxele_e(ii, jj, ck(1), sing, match,
!      &            irule_fld, irule_src, irulef_near, irules_near, 
!      &            maxnode, maxpatch, maxedge, maxrule, maxgrid,
!      &            xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
!      &            ngrid, vt1, vt2, vt3, wt, rnear2, distmin,
!      &            rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
!      &            cnxet, dummy)
      call mtxele_efie(ii, jj, ck(1),maxnode, maxpatch, maxedge,
     &            xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &            freq, wl0,eta0, ci,dummy,cnxet2,contourMap)

!       print*,ii,jj, cnxet, cnxet2, cnxet+cnxet2
!       pause

      camn = alpha*ce(1)*(cnxet+cnxet2) +(1-alpha)*eta0*cnxhx


!      &
! 	endif
       
500      return
      end
