#ifndef IDENT_PC_HPP_
#define IDENT_PC_HPP_

class ident_pc
{
public:
  ident_pc() {}

  template <class Vector>
  Vector solve(const Vector &x) const
  {
    return x;
  }

  template <class Vector>
  Vector trans_solve(const Vector &x) const
  {
    return x;
  }
};

#endif
