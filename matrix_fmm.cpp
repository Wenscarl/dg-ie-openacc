#include "matrix_fmm.hpp"
#include <algorithm>

Matrix_Fmm::Matrix_Fmm(Complex_FMM *cx0, Complex_FMM *cy, Complex_FMM *cx,
	     Complex_FMM *cvf, Complex_FMM *cvs, Int *maxedge, Int *lmax, Int *lsmax,
	     Int *ncmax, Int *kpm, Int *kp0, Int *nsm, Int *ntlm, Int *niplm,
	     Int *lxyz, Int *modes, Int *igall, Int *igcs, Int *igrs, Int *index,
	     Int *family, Int *lrsmup, Int *lrsmdown, Complex_FMM *csm, Complex_FMM *cvlr,
	     Complex_FMM *cgm, Complex_FMM *ctl, Int *lrstl, Int *indextl, Int *kpstart,
	     Int *nkpm, Complex_FMM *c_shift, Int *icsa, Int *irsa, float *array,
	     Int *ngsndm, Int *igsndcs, Int *igsndrs, Int *nearm, Int noself[][2],
	     Complex_FMM *canear, Int *ngnearm, Int *igblkrs, Int *igblkcs,
	     Int *igblk, Int *mfinestm, Complex_FMM *cfinest)
  : cx0(cx0), cy(cy), cx(cx),
    cvf(cvf), cvs(cvs), maxedge(maxedge), lmax(lmax), lsmax(lsmax),
    ncmax(ncmax), kpm(kpm), kp0(kp0), nsm(nsm), ntlm(ntlm), niplm(niplm),
    lxyz(lxyz), modes(modes), igall(igall), igcs(igcs), igrs(igrs), index(index),
    family(family), lrsmup(lrsmup), lrsmdown(lrsmdown), csm(csm), cvlr(cvlr),
    cgm(cgm), ctl(ctl), lrstl(lrstl), indextl(indextl), kpstart(kpstart),
    nkpm(nkpm), c_shift(c_shift), icsa(icsa), irsa(irsa), array(array),
    ngsndm(ngsndm), igsndcs(igsndcs), igsndrs(igsndrs), nearm(nearm), noself(noself),
    canear(canear), ngnearm(ngnearm), igblkrs(igblkrs), igblkcs(igblkcs),
    igblk(igblk), mfinestm(mfinestm), cfinest(cfinest)
{}
