#include "geometry.h"
#include <stdlib.h>
#include <fstream>
#include <xmesh_trimesh_node.h>
#include <xmesh_trimesh_edge.h>
#include <xmesh_trimesh_face.h>
#include <xmesh_trimesh_sort.h>
#include <string.h>
#include <algorithm>
#include "global.h"
typedef std::map<std::string,cVtr*> FieldMap;
triSurf::triSurf() :tri_mesh()
{
	isFEM=false;
	isSO=false;
	fieldMap.clear();
}
triSurf::~triSurf() 
{
	for(std::map<std::string,cVtr*>::iterator it= fieldMap.begin();
		   it!=fieldMap.end();it++){
		if(it->second!=NULL){
			delete[] it->second;
			it->second=NULL;
		}
	}
}
void triSurf::updateFromFirstOrderField(char* ext, cVtr* data, bool hardcopy) //never called
{
	Int NP= isSO? 6:3;
	if(hardcopy==false){
		FieldMap::iterator it=  fieldMap.find(std::string(ext));
		if(it!=fieldMap.end()){  //Existing:
			if(it->second!=NULL){
				delete[] it->second;
				
			}
			it->second=data;
		}
		else{ //Non Existing
			fieldMap.insert(std::pair<std::string,cVtr*>(std::string(ext),data));
		}
	}
	else{
		FieldMap::iterator it=  fieldMap.find(std::string(ext));
		if(it!=fieldMap.end()){  //Existing:
			if(it->second!=NULL){
				delete[] it->second;
				
			}
			
			it->second=new cVtr[NP*getnTri()];
			memcpy((char*) (it->second),(char*)data,sizeof(cVtr)*NP*getnTri() );
			
		}
		else{
			cVtr* tmp=new cVtr[NP*getnTri()];
			memcpy((char*) (tmp),(char*)data,sizeof(cVtr)*NP*getnTri() );
			fieldMap.insert(std::pair<std::string,cVtr*>(std::string(ext),tmp));
		}
	}
}
Integer triSurf::readField(char* _ext,bool binary) //never called.
{
	if(getnTri()<=0)
		return 0;
	ifstream fin;
	std::string ext=_ext;
	std::string filename= std::string(getName())+ ext;
	Int NP=isSO ? 6:3;
	cVtr* field=NULL;
	if(binary){
		fin.open(filename.c_str(),std::ios::binary);
		if(fin.is_open()==false)
		{
			cout<<"Error! read field failed!\n";
			exit(-1);
		}
		double* temp=new double[NP*getnTri()*6];
		fin.read((char*)temp,sizeof(double)*NP*getnTri()*6);
		fin.close();
		field=new cVtr[NP*getnTri()];
		for(Int i=0;i<NP*getnTri();i++){
			double* basePtr=&(temp[i*6]);
			field[i].setcvtr(Complex(basePtr[0],basePtr[1]),Complex(basePtr[2],basePtr[3]),
					 Complex(basePtr[4],basePtr[5]));
		}
		delete[] temp;
		
		
	}
	else{
		
		fin.open(filename.c_str());
		if(fin.is_open()==false)
		{
			cout<<"Error! read field failed!\n";
			exit(-1);
		}
		
		field=new cVtr[NP*getnTri()];
		for(Int i=0;i<NP*getnTri();i++){
			double basePtr[6];
			for(Int j=0;j<6;j++)
				fin>>basePtr[i];
			field[i].setcvtr(Complex(basePtr[0],basePtr[1]),Complex(basePtr[2],basePtr[3]),
					 Complex(basePtr[4],basePtr[5]));
		}
		fin.close();
	}
	std::map<std::string,cVtr*>::iterator it= fieldMap.find(ext);
	if(it->second !=NULL){
		delete[] it->second;
		it->second=NULL;
	}
	it->second=field;
	return 0;
}

void triSurf::generateFirstOrderTriArray //Never called
		(Integer& _nNode, Integer& _nEdge, Integer& _nTri, 
		 Real** _nodep, Integer** _iedgep, Integer** _itrip)
{ //Attention: fortran numbering convention
	if(getnTri()<=0){
		return;
	}
	//The newNode= [ oldNodes ; midNode of Edges];
	_nNode= getnNode()+getnEdge();
	_nEdge= getnTri()+2*getnEdge();
	_nTri= 4* getnTri();
	
	//Get nodes
	(*_nodep)= new Real[_nNode*3];
	xMesh::tri_node* nodePtr= getNodePtr();
	Integer nOldNode=getnNode();
	for(Int i=0;i<nOldNode;i++){
		(*_nodep)[i*3]=nodePtr[i].getx();
		(*_nodep)[i*3+1]=nodePtr[i].gety();
		(*_nodep)[i*3+2]=nodePtr[i].getz();
	}

	xMesh::tri_edge* edgePtr= getEdgePtr();
	Integer nOldEdge=getnEdge(),cnt=0;
	for(Int i=nOldNode;i<nOldNode+nOldEdge;i++){
		xMesh::vector tmp= (getEdgePtr(cnt)->getNode(0)->getVector()+getEdgePtr(cnt)->getNode(1)->getVector())*0.5;
		(*_nodep)[i*3]= tmp.getx();
		(*_nodep)[i*3+1]=tmp.gety();
		(*_nodep)[i*3+2]=tmp.getz();
		cnt++;
	}
	
	//Generate the edge list
	Integer nOldTri=getnTri();
	(*_iedgep)=new Integer [2* nOldEdge+3*nOldTri];
	
	for(Int i=0;i<nOldEdge;i++){
		(*_iedgep)[4*(2*i) ]= getEdgePtr(i)->getNode(0)->getId()+1;
		(*_iedgep)[4*(2*i) +1]= getEdgePtr(i)->getId()+1;
		for(Int j=0;j<2;j++){
			xMesh::tri_face* tmpFace=getEdgePtr(i)->getTri(j);
			if(tmpFace==NULL){
				(*_iedgep)[4*(2*i) +2+j]=-1;
			}
			else{
				Int ilocal=tmpFace->getLocalNodeNum(static_cast<xMesh::tri_node*>(getEdgePtr(i)->getNode(0)));
				(*_iedgep)[4*(2*i) +2+j]= 4*(tmpFace->getId())+ ilocal;
			}
		}
		
		
		(*_iedgep)[4*(2*i+1) ]= getEdgePtr(i)->getNode(1)->getId()+1;
		(*_iedgep)[4*(2*i+1) +1]= getEdgePtr(i)->getId()+1;
		
		for(Int j=0;j<2;j++){
			xMesh::tri_face* tmpFace=getEdgePtr(i)->getTri(j);
			if(tmpFace==NULL){
				(*_iedgep)[4*(2*i+1) +2+j]=-1;
			}
			else{
				Int ilocal=tmpFace->getLocalNodeNum(static_cast<xMesh::tri_node*>(getEdgePtr(i)->getNode(1)));
				(*_iedgep)[4*(2*i+1) +2+j]= 4*(tmpFace->getId())+ ilocal;
			}
		}
	}
	Integer basement=nOldEdge*2*4;
	for(Int i=0;i<nOldTri;i++){
		for(Int j=0;j<3;j++){
			(*_iedgep)[basement+(i*3+j)*4] =  getTriPtr(i)->getEdge(j)->getId();
			(*_iedgep)[basement+(i*3+j)*4 +1] =  getTriPtr(i)->getEdge((j+1)%3)->getId();
			
			(*_iedgep)[basement+(i*3+j)*4 +2] =  4* getTriPtr(i)->getId()+j;
			(*_iedgep)[basement+(i*3+j)*4 +3] =  4* getTriPtr(i)->getId()+3;
		}
	}
	
	
	
}

void triSurf::exportXYZNODE(Real* dst)const
{
	xMesh::tri_node* nodePtr= getNodePtr();
	for(Int i=0;i<getnNode();i++){
		dst[i*3]=nodePtr[i].getx();
		dst[i*3+1]=nodePtr[i].gety();
		dst[i*3+2]=nodePtr[i].getz();
	}
	Real* base= &(dst[getnNode()*3]);
	if(isSO){
		xMesh::tri_edge* edgePtr= getEdgePtr();
		for(Int i=0;i<getnEdge();i++){
			base[i*3  ]=(edgePtr[i].getNode(0)->getx()+edgePtr[i].getNode(1)->getx())/2.0;
			base[i*3+1]=(edgePtr[i].getNode(0)->gety()+edgePtr[i].getNode(1)->gety())/2.0;
			base[i*3+2]=(edgePtr[i].getNode(0)->getz()+edgePtr[i].getNode(1)->getz())/2.0;
		}
	}
}
void triSurf::exportEDGE(Integer* dst,Integer offsetNode,Integer offsetTri)const
{ 
	//offsetNode and offsetTri are the offset of Node number and triangle number respectively.
	//This is for compatibility issue with fortran routines.
	xMesh::tri_edge* edgePtr= getEdgePtr();
	xMesh::tri_face* triPtr= getTriPtr();
	xMesh::tri_face* fcPtr=NULL;
	xMesh::tri_edge* edPtr=NULL;
	if(isSO==false){
		for(Int i=0;i<getnEdge();i++){
			dst[i*4]=edgePtr[i].getNode(0)->getId()+offsetNode;
			dst[i*4+1]=edgePtr[i].getNode(1)->getId()+offsetNode;
			fcPtr=edgePtr[i].getTri(0);
			if(fcPtr!=NULL)
				dst[i*4+2]=fcPtr->getId()+offsetTri;
			else
				dst[i*4+2]=-1;
			fcPtr=edgePtr[i].getTri(1);
			if(fcPtr!=NULL)
				dst[i*4+3]=fcPtr->getId()+offsetTri;
			else
				dst[i*4+3]=-1;
		}
	}
	else{ //second order i case
		for(Int i=0;i<getnEdge();i++){
			Int iLocal=-1;
			
			
			dst[i*2*4]=edgePtr[i].getNode(0)->getId()+offsetNode;
			dst[i*2*4+1]=edgePtr[i].getId()+getnNode()+offsetNode;
			assert(dst[i*2*4]>=offsetNode);
			assert(dst[i*2*4+1]>=offsetNode);
			
			fcPtr=edgePtr[i].getTri(0);
			iLocal=fcPtr->getLocalNodeNum( static_cast<xMesh::tri_node*> (edgePtr[i].getNode(0)) );
			dst[i*2*4+2]=4*fcPtr->getId()+iLocal+offsetTri;
			
			fcPtr=edgePtr[i].getTri(1);
			if(fcPtr!=NULL){
				iLocal=fcPtr->getLocalNodeNum( static_cast<xMesh::tri_node*> (edgePtr[i].getNode(0)) );
				dst[i*2*4+3]=4*fcPtr->getId()+iLocal+offsetTri;
			}
			else
				dst[i*2*4+3]=-1;
			
			dst[(i*2+1)*4+1]=edgePtr[i].getNode(1)->getId()+offsetNode;
			dst[(i*2+1)*4+0]=edgePtr[i].getId()+getnNode()+offsetNode;
			assert(dst[(i*2+1)*4]>=offsetNode);
			assert(dst[(i*2+1)*4+1]>=offsetNode);
			fcPtr=edgePtr[i].getTri(0);
			if(fcPtr!=NULL){
				iLocal=fcPtr->getLocalNodeNum( static_cast<xMesh::tri_node*> (edgePtr[i].getNode(1)) );
				dst[(i*2+1)*4+2]=4*fcPtr->getId()+iLocal+offsetTri;
			}
			else
				dst[(i*2+1)*4+2]=-1;
			fcPtr=edgePtr[i].getTri(1);
			if(fcPtr!=NULL){
				iLocal=fcPtr->getLocalNodeNum( static_cast<xMesh::tri_node*> (edgePtr[i].getNode(1)) );
				dst[(i*2+1)*4+3]=4*fcPtr->getId()+iLocal+offsetTri;
			}
			
		}
		Integer* dst2=&(dst[getnEdge()*8]);
		for(Int i=0;i<getnTri();i++){
			fcPtr= &( triPtr[i]);
				
				
			edPtr=(fcPtr->getEdge(2));
			dst2[i*3*4 ]=   edPtr->getId()+getnNode()+offsetNode;
			edPtr=(fcPtr->getEdge(1));
			dst2[i*3*4 +1]=   edPtr->getId()+getnNode()+offsetNode;
			dst2[i*3*4 +2]= i*4 +offsetTri;
			dst2[i*3*4 +3]= i*4+3 +offsetTri;
			
			
			edPtr=(fcPtr->getEdge(0));
			dst2[i*3*4 +4 ]=   edPtr->getId()+getnNode()+offsetNode;
			edPtr=(fcPtr->getEdge(2));
			dst2[i*3*4 +4+1]=   edPtr->getId()+getnNode()+offsetNode;
			dst2[i*3*4 +4+2]= i*4+1+offsetTri;
			dst2[i*3*4 +4+3]= i*4+3+offsetTri;
			
			
			edPtr=(fcPtr->getEdge(1));
			dst2[i*3*4 +8 ]=   edPtr->getId()+getnNode()+offsetNode;
			edPtr=(fcPtr->getEdge(0));
			dst2[i*3*4 +8+1]=   edPtr->getId()+getnNode()+offsetNode;
			dst2[i*3*4 +8+2]= i*4+2+offsetTri;
			dst2[i*3*4 +8+3]= i*4+3+offsetTri;
		}
	}
		

}
void triSurf::exportTRI(Integer* dst,Integer offsetNode)const
{
	xMesh::tri_face* facePtr= getTriPtr();
	if(isSO==false){
		for(Int i=0;i<getnTri();i++){
			if(getNormal(i)==xMesh::RH){
				dst[i*3  ]=facePtr[i].getNode(0)->getId()+offsetNode;
				dst[i*3+1]=facePtr[i].getNode(1)->getId()+offsetNode;
				dst[i*3+2]=facePtr[i].getNode(2)->getId()+offsetNode;
			}
			else{
				dst[i*3  ]=facePtr[i].getNode(0)->getId()+offsetNode;
				dst[i*3+1]=facePtr[i].getNode(2)->getId()+offsetNode;
				dst[i*3+2]=facePtr[i].getNode(1)->getId()+offsetNode;
			}
		}
	}
	else{
/*
		  0                              0
		 / \                            / \
		/   \                          / 0 \
	       /     \	                      1-----2
	      /      \	
	     /        \	                   0  2-----1 1
	    /          \	          / \  \ 3 / / \
 	   /            \	         / 1 \  \ / / 2 \
	  1--------------2              1-----2  0 2-----2
		
		*/
		for(Int i=0;i<getnTri();i++){
			if(getNormal(i)==xMesh::RH){
				dst[(i*4)  *3 ]=facePtr[i].getNode(0)->getId()+offsetNode;
				dst[(i*4+1)*3 ]=facePtr[i].getNode(1)->getId()+offsetNode;
				dst[(i*4+2)*3 ]=facePtr[i].getNode(2)->getId()+offsetNode;
				
				dst[(i*4)  *3 +1]=facePtr[i].getEdge(2)->getId()+getnNode()+offsetNode;
				dst[(i*4+1)*3 +1]=facePtr[i].getEdge(0)->getId()+getnNode()+offsetNode;
				dst[(i*4+2)*3 +1]=facePtr[i].getEdge(1)->getId()+getnNode()+offsetNode;
				
				dst[(i*4)  *3 +2]=facePtr[i].getEdge(1)->getId()+getnNode()+offsetNode;
				dst[(i*4+1)*3 +2]=facePtr[i].getEdge(2)->getId()+getnNode()+offsetNode;
				dst[(i*4+2)*3 +2]=facePtr[i].getEdge(0)->getId()+getnNode()+offsetNode;
				
				dst[(i*4+3)*3   ] =facePtr[i].getEdge(0)->getId()+getnNode()+offsetNode;
				dst[(i*4+3)*3 +1] =facePtr[i].getEdge(1)->getId()+getnNode()+offsetNode;
				dst[(i*4+3)*3 +2] =facePtr[i].getEdge(2)->getId()+getnNode()+offsetNode;
			}
			else{
				dst[(i*4)  *3 ]=facePtr[i].getNode(0)->getId()+offsetNode;
				dst[(i*4+1)*3 ]=facePtr[i].getNode(2)->getId()+offsetNode;
				dst[(i*4+2)*3 ]=facePtr[i].getNode(1)->getId()+offsetNode;
				
				dst[(i*4)  *3 +1]=facePtr[i].getEdge(1)->getId()+getnNode()+offsetNode;
				dst[(i*4+1)*3 +1]=facePtr[i].getEdge(0)->getId()+getnNode()+offsetNode;
				dst[(i*4+2)*3 +1]=facePtr[i].getEdge(2)->getId()+getnNode()+offsetNode;
				
				dst[(i*4)  *3 +2]=facePtr[i].getEdge(2)->getId()+getnNode()+offsetNode;
				dst[(i*4+1)*3 +2]=facePtr[i].getEdge(1)->getId()+getnNode()+offsetNode;
				dst[(i*4+2)*3 +2]=facePtr[i].getEdge(0)->getId()+getnNode()+offsetNode;
				
				dst[(i*4+3)*3   ] =facePtr[i].getEdge(0)->getId()+getnNode()+offsetNode;
				dst[(i*4+3)*3 +1] =facePtr[i].getEdge(2)->getId()+getnNode()+offsetNode;
				dst[(i*4+3)*3 +2] =facePtr[i].getEdge(1)->getId()+getnNode()+offsetNode;

			}
		}
	}
	
}
void MLFMARegularization(Integer _maxnode,Integer _maxedge, Integer _maxpatch,
				  Integer* EDGE,Integer* TRI,Integer nodeoffset,Integer edgeoffset,
				 Integer trioffset)
{
	cout<<"Check out the triangle conformity with MLFMA kernel.\n"; 
	Int cnt=0,cnt2=0,cntA=0,cntB=0;
	Int swapA,swapB;
	for(Int i=0;i<_maxedge;i++){
		swapA=0;
		swapB=0;
		Integer triA=EDGE[4*i+2]-trioffset;
		Integer triB=EDGE[4*i+3]-trioffset;
		
		Integer Anode[3],Bnode[3];
		if(triA>=0){
			for(Int j=0;j<3;j++)
				Anode[j]=TRI[triA*3+j];
			for(Int j=0;j<3;j++){
				if(EDGE[4*i ]==Anode[j] && EDGE[4*i+1]==Anode[(j+1)%3]){	
					swapA=1;
					break;
				}
				else if(EDGE[4*i+1 ]==Anode[j] && EDGE[4*i]==Anode[(j+1)%3]){	
					swapA=-1;
					break;
				}	
			}
			if(swapA==0){
				cout<<"Error!swapA==0\n";
			}
		}
		if(triB>=0){
			for(Int j=0;j<3;j++)
				Bnode[j]=TRI[triB*3+j];
			for(Int j=0;j<3;j++){
				if(EDGE[4*i ]==Bnode[j] && EDGE[4*i+1]==Bnode[(j+1)%3]){
					swapB=-1;
					break;
				}
				else if(EDGE[4*i+1 ]==Bnode[j] && EDGE[4*i]==Bnode[(j+1)%3]) {
					swapB=1;
					break;
				}	
			}
			if(swapB==0){
				cout<<"Error!swapB==0\n";
			}
		}
		if( (triB==-1 && swapA==-1)){
			std::swap<Integer> (EDGE[4*i+2],EDGE[4*i+3]);
			cntA++;
			continue;
		}
		if( (triA==-1 && swapB==-1)){
			std::swap<Integer> (EDGE[4*i+2],EDGE[4*i+3]);
			cntB++;
			continue;
		}   
		if(swapA==-1 && swapB==-1)
		{
			std::swap<Integer> (EDGE[4*i+2],EDGE[4*i+3]);
			cnt++;
			continue;
		}
		if(triA!=-1 && triB!=-1 && (swapA * swapB==-1)){
// 			cout<<"Error!\n";
			cnt2++;
		}
	}
	cout<<"The count number of edge swap is : "<<cnt<<" "<<cntA<<" "<<cntB<<" "<<cnt2<<endl;
}
Integer triSurf::getnNodeFO()const
{
	if(isSO)
		return getnNode()+getnEdge();
	else
		return getnNode();
}
Integer triSurf::getnEdgeFO()const
{
	if(isSO)
		return 2*getnEdge()+3*getnTri();
	else
		return getnEdge();
}
Integer triSurf::getnTriFO()const
{
	if(isSO)
		return 4*getnTri();
	else
		return getnTri();
}
Integer triSurf::getFieldFO(Complex* dst,const char* ext)const
{
	char buf[0x200];
	ifstream foo;
	sprintf(buf,"%s_src.%s",getName(),ext);
	foo.open(buf);
	if(foo.is_open()==false){
		
		sprintf(buf,"%s.%s",getName(),ext);
		foo.open(buf);
		if(foo.is_open()==false){
			cout<<"Warning! cannot open file "<<buf<<endl;
			cout<<"Zero field filled!\n";
			for(Int i=0;i<getnTri()*9;i++){
				dst[i]=Complex(0.0);
			}
			return -1;
		}
		else
			cout<<"Warning! old fashioned name file is used.\n";	
	}
		Complex tmp[18];
	if(isSO==false){
		for(Int i=0;i<getnTri();i++){
			for(Int j=0;j<9;j++)
				foo>>reref(dst[i*9+j])>>imref(dst[i*9+j]);
			if(getNormal(i)==xMesh::LH){
				for(Int j=0;j<3;j++)
					std::swap<Complex> (dst[3+j],dst[6+j]);
			}
		}
		
	}
	else{
		for(Int i=0;i<getnTri();i++){
			for(Int j=0;j<18;j++)
				foo>>reref(tmp[j])>>imref(tmp[j]);
			if(getNormal(i)==xMesh::LH){
				assert(false);
				for(Int j=0;j<3;j++)
					std::swap<Complex> (tmp[3+j],tmp[6+j]);
				for(Int j=0;j<3;j++)
					std::swap<Complex> (tmp[12+j],tmp[15+j]);
					
			}
			for(Int j=0;j<3;j++)
				dst[i*4*9 +j]	 	=tmp[j+3*0];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +3 +j]	 =tmp[j+3*5];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +6 +j]	 =tmp[j+3*4];
			
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9 +j]	 =tmp[j+3*1];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9 +3 +j]	 =tmp[j+3*3];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9 +6 +j]	 =tmp[j+3*5];
			
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9*2 +j]	 =tmp[j+3*2];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9*2 +3 +j]	 =tmp[j+3*4];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9*2 +6 +j]	 =tmp[j+3*3];
			
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9*3 +j]	 =tmp[j+3*3];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9*3 +3 +j]	 =tmp[j+3*4];
			for(Int j=0;j<3;j++)
				dst[i*4*9 +9*3 +6 +j]	 =tmp[j+3*5];
		}
	}
	foo.close();
	return 0;
}

void exportTriFO(const char* fname,Int nNode,Int nTri, Real* xyz, Integer* tri) //just for debug use.
{
	char buf[0x200];
	sprintf(buf,"%s.tri",fname);
	ofstream foo;
	foo.open(buf);
	foo.precision(10);
	foo<<1.0<<endl;
	foo<<nNode<<endl;
	for(Int i=0;i<nNode;i++)
		foo<<scientific<<xyz[3*i]<<"\t"<<xyz[3*i+1]<<"\t"<<xyz[3*i+2]<<"\n";
	foo<<nTri<<endl;
	for(Int i=0;i<nTri;i++)
		foo<<tri[3*i]-1<<"\t"<<tri[3*i+1]-1<<"\t"<<tri[3*i+2]-1<<"\n";
	foo.close();
}
void directWriteFieldFO(const char* fname, Complex* field,Integer nTri)
{
	ofstream foo;
	foo.open(fname);
	foo.precision(10);
	for(Int i=0;i<nTri;i++){
		for(Int j=0;j<9;j++)
			foo<<field[i*9+j].real()<<"\t"<<field[i*9+j].imag()<<"\n";
	}
	foo.close();
}

Integer triSurf::writeFieldFO(
			Complex* rhs, 
	 		const char* ext, 
			Integer _maxnode, 
   			Integer _maxedge, 
      			Integer _maxpatch, 
	 		Real* _xyznodep, 
    			Integer* _i_edgep, 
       			Integer* _ipatpntp,
	  		Real* _paerap,
     			Real* _edgep, 
  			Complex* touchingJM,
     			bool *touchMark,
         		Integer offsetNode, 
	   		Integer offsetEdge, 
      			Integer offsetTri,
	 		bool* matchedMark,
			bool isRcv,bool append) const 
		//if isRcv ==true then the output field of touching triangles will be replaced with .tcurE or .tcurH
{
	char buf[0x200];
	InputComplex* Jsca=NULL;
	Integer* count=NULL;
	Integer i,j;
	Real sign=-1.0;
	if(isSO==false)
		assert(_maxpatch==getnTri());
	else
		assert(_maxpatch==getnTri()*4);
	
	
	Jsca=new InputComplex[_maxpatch*9];
	count=new Integer[_maxpatch*3];
		
	for(i=0;i<_maxpatch*9;i++)
		Jsca[i]=0.0;
	for(i=0;i<_maxpatch*3;i++)
		count[i]=0;
		
	for(i=0;i<_maxedge;i++)
	{
		Real AREA=0.0;
		Integer e1=_i_edgep[4*i+2]-offsetTri;
		Integer e2=_i_edgep[4*i+3]-offsetTri;
		Real coefficient=_edgep[i];
		if(e1>=0){
			AREA=_paerap[e1]*2.0;
			//n1,n2,n3 is the global id of 3 nodes
			Integer n3=_ipatpntp[e1*3]+_ipatpntp[e1*3+1]+_ipatpntp[e1*3+2]-_i_edgep[4*i]-_i_edgep[4*i+1]-offsetNode;
			Integer n1=_i_edgep[4*i  ]-offsetNode;
			Integer n2=_i_edgep[4*i+1]-offsetNode;

			////map the three node to the local nodes
			Integer in1=findI(n1+offsetNode,&(_ipatpntp[e1*3]));assert (in1!=-1);
			Integer in2=findI(n2+offsetNode,&(_ipatpntp[e1*3]));assert (in2!=-1);
			Integer in3=findI(n3+offsetNode,&(_ipatpntp[e1*3]));assert (in3!=-1);
			
	// 	      Real coefficient=	1.0f;
			if(AREA>0.0){
 			Jsca[9*(e1)+in1*3  ]+=rhs[i]*(_xyznodep[n3*3  ]-_xyznodep[n1*3  ])/AREA*sign*coefficient;
 			Jsca[9*(e1)+in1*3+1]+=rhs[i]*(_xyznodep[n3*3+1]-_xyznodep[n1*3+1])/AREA*sign*coefficient;
 			Jsca[9*(e1)+in1*3+2]+=rhs[i]*(_xyznodep[n3*3+2]-_xyznodep[n1*3+2])/AREA*sign*coefficient;
 			Jsca[9*(e1)+in2*3  ]+=rhs[i]*(_xyznodep[n3*3  ]-_xyznodep[n2*3  ])/AREA*sign*coefficient;
 			Jsca[9*(e1)+in2*3+1]+=rhs[i]*(_xyznodep[n3*3+1]-_xyznodep[n2*3+1])/AREA*sign*coefficient;
 			Jsca[9*(e1)+in2*3+2]+=rhs[i]*(_xyznodep[n3*3+2]-_xyznodep[n2*3+2])/AREA*sign*coefficient;
			}
			else{
				Jsca[9*(e1)+in1*3  ]+=0.0;
				Jsca[9*(e1)+in1*3+1]+=0.0;
				Jsca[9*(e1)+in1*3+2]+=0.0;
				Jsca[9*(e1)+in2*3  ]+=0.0;
				Jsca[9*(e1)+in2*3+1]+=0.0;
				Jsca[9*(e1)+in2*3+2]+=0.0;
			}	
//                      Complex tmprmid[3];
//                      for(Int ll=0;ll<3;ll++)
//                          tmprmid[ll]=Complex((_xyznodep[n1*3+ll]+_xyznodep[n2*3+ll]+_xyznodep[n3*3+ll])/3.0,0.0);
//                     for(Int ll=0;ll<3;ll++){
//                          Jsca[9*(e1)+ll*3  ]+=rhs[i]*(_xyznodep[n3*3+0]-tmprmid[0])/AREA*sign*coefficient;
//			  Jsca[9*(e1)+ll*3+1]+=rhs[i]*(_xyznodep[n3*3+1]-tmprmid[1])/AREA*sign*coefficient;
//			  Jsca[9*(e1)+ll*3+2]+=rhs[i]*(_xyznodep[n3*3+2]-tmprmid[2])/AREA*sign*coefficient;
// 			}
			count[3*(e1)+in1]++;
			count[3*(e1)+in2]++;
		}		
		if(e2>=0){
			AREA=_paerap[e2]*2.0;
			//AREA=1.0;
			Integer n3=_ipatpntp[e2*3]+_ipatpntp[e2*3+1]+_ipatpntp[e2*3+2]-_i_edgep[4*i]-_i_edgep[4*i+1]-offsetNode;
			Integer n1=_i_edgep[4*i]-offsetNode;
			Integer n2=_i_edgep[4*i+1]-offsetNode;
			Integer in3=findI(n3+offsetNode,&(_ipatpntp[e2*3]));
			Integer in1=findI(n1+offsetNode,&(_ipatpntp[e2*3]));
			Integer in2=findI(n2+offsetNode,&(_ipatpntp[e2*3]));	
			if(AREA>0.0){
			Jsca[9*(e2)+in1*3]-=rhs[i]*(_xyznodep[n3*3]-_xyznodep[n1*3])/AREA*sign*coefficient;
			Jsca[9*(e2)+in1*3+1]-=rhs[i]*(_xyznodep[n3*3+1]-_xyznodep[n1*3+1])/AREA*sign*coefficient;
			Jsca[9*(e2)+in1*3+2]-=rhs[i]*(_xyznodep[n3*3+2]-_xyznodep[n1*3+2])/AREA*sign*coefficient;
			Jsca[9*(e2)+in2*3]-=rhs[i]*(_xyznodep[n3*3]-_xyznodep[n2*3])/AREA*sign*coefficient;
			Jsca[9*(e2)+in2*3+1]-=rhs[i]*(_xyznodep[n3*3+1]-_xyznodep[n2*3+1])/AREA*sign*coefficient;
			Jsca[9*(e2)+in2*3+2]-=rhs[i]*(_xyznodep[n3*3+2]-_xyznodep[n2*3+2])/AREA*sign*coefficient;
			}
			else{
				Jsca[9*(e2)+in1*3]-=0.0;
				Jsca[9*(e2)+in1*3+1]-=0.0;
				Jsca[9*(e2)+in1*3+2]-=0.0;
				Jsca[9*(e2)+in2*3]-=0.0;
				Jsca[9*(e2)+in2*3+1]-=0.0;
				Jsca[9*(e2)+in2*3+2]-=0.0;
			}
//			Complex tmprmid[3];
//                        for(Int ll=0;ll<3;ll++)
//                          tmprmid[ll]=Complex((_xyznodep[n1*3+ll]+_xyznodep[n2*3+ll]+_xyznodep[n3*3+ll])/3.0,0.0);
//                        for(Int ll=0;ll<3;ll++){
                          
//				Jsca[9*(e2)+ll*3  ]-=rhs[i]*(_xyznodep[n3*3+0]-tmprmid[0])/AREA*sign*coefficient;
//				Jsca[9*(e2)+ll*3+1]-=rhs[i]*(_xyznodep[n3*3+1]-tmprmid[1])/AREA*sign*coefficient;
//				Jsca[9*(e2)+ll*3+2]-=rhs[i]*(_xyznodep[n3*3+2]-tmprmid[2])/AREA*sign*coefficient;
//			}	
			count[3*(e2)+in1]++;
			count[3*(e2)+in2]++;
		}
	}
	

	ifstream fprevResult;
	if(append){
		cout<<"Write in Append mode!\n";
		sprintf(buf,"mv %s.%s %s.%s.tmp",getName(),ext,getName(),ext);
		Int rc=system(buf);
		if(rc!=0){
		cout<<"Attention! mv operation is failed, append mode is cancelled!\n";
		append=false;
		}
		else{
			sprintf(buf,"%s.%s.tmp",getName(),ext);
			fprevResult.open(buf,ios::binary);
			if(fprevResult.is_open()==false){
				cout<<"Attention! open of appended file is failed, append mode is cancelled!\n";
				append=false;
			}	
		}
	}
			
	sprintf(buf,"%s.%s",getName(),ext);
	ofstream foo;
	foo.open(buf,ios::binary);
	
	if(isSO==false){

			for(Int i=0;i<_maxpatch;i++){
				InputComplex tmpp[9];
				double toll=0.0,toll2=0.0;
				//First consider the touching issue, if the surface is touching surface, use touching JM to replace Jsca
				if(touchingJM!=NULL && getTriPtr(i)->getFlag()!=0){

					for(Int j=0;j<3;j++){
						if(matchedMark[i*3+j]){
							for(Int k=0;k<3;k++)
								Jsca[i*9+j*3+k]=touchingJM[i*9+j*3+k];
						}
					}
				}
				
				//Here, consider the append,
				//For touching surface , it has been taken care
				if(append){				
					fprevResult.read((char*)tmpp,sizeof(InputComplex)*9);
					for(Int j=0;j<3;j++){
						if(matchedMark[i*3+j]==false){ //Only unmatched node will be appended with the other source
							for(Int k=0;k<3;k++)
								Jsca[i*9+j*3+k]+=tmpp[j*3+k];						
						}
					}
				}

				foo.write((char*)(&(Jsca[9*i])),sizeof(InputComplex)*9);
				
			}
	}
	else{
		InputComplex tmp[18],tmp2[18];
		
		for(Int i=0;i<getnTri();i++){
			for(Int j=0;j<18;j++)
				tmp[j]=0.0;
			
			//First, use Jsca to fill tmp
			//Node0
			for(Int j=0;j<3;j++){
				tmp[j]=Jsca[4*9*i +j];
			}
			
			//Node1
			for(Int j=0;j<3;j++){
				tmp[j +3]=Jsca[4*9*i +9 +j];
			}
			
			//Node2
			for(Int j=0;j<3;j++){
				tmp[j +3*2]=Jsca[4*9*i +9*2 +j];
			}
			
			//Node3
			for(Int j=0;j<3;j++){
				tmp[j +3*3]= (Jsca[4*9*i +9*1 + 3 +j] + Jsca[4*9*i +9*2 + 3*2 +j] + Jsca[4*9*i +9*3 + 3*0 +j] ) /3.0;
			}
			//Node4
			for(Int j=0;j<3;j++){
				tmp[j +3*4]= (Jsca[4*9*i +9*2 + 3 +j] + Jsca[4*9*i +9*0 + 3*2 +j] + Jsca[4*9*i +9*3 + 3*1 +j] ) /3.0;
			}

			//Node5
			for(Int j=0;j<3;j++){
				tmp[j +3*5]= (Jsca[4*9*i +9*0 + 3 +j] + Jsca[4*9*i +9*1 + 3*2 +j] + Jsca[4*9*i +9*3 + 3*2 +j] ) /3.0;
			}
			//If it is marked as the touching surface
			if(touchingJM!=NULL && getTriPtr(i)->getFlag()!=0){
				for(Int j=0;j<6;j++){
					if(matchedMark[i*6+j]){ //If it is matched.  //WXC
					
						for(Int k=0;k<3;k++)
							tmp[j*3+k]=touchingJM[i*18+j*3+k];
					}

				}
			}	
				
			InputComplex tmpp[18];
			if(append){				
				fprevResult.read((char*)tmpp,sizeof(InputComplex)*18);
				for(Int j=0;j<6;j++)
					if(matchedMark[i*6+j]==false){
						for(Int k=0;k<3;k++)
							tmp[j*3+k]+=tmpp[j*3+k];
					}
// 				if(getTriPtr(i)->getFlag()==0){
// 					for(Int j=0;j<18;j++)
// 						tmp[j]+=tmpp[j];
// 				}
// 				else{
// 					for(Int j=0;j<18;j++) //For touching surface, directly grep the coupled solution
// 						tmp[j]+=tmpp[j];
// 				}
			}
			double toll=0.0,toll2=0.0;
			foo.write((char*)tmp,sizeof(InputComplex)*18);
			
		}
	}
// 	ftouch.close();
	foo.close();
	delete[] Jsca;
	delete[] count;
	return 0;
}
void triSurf::exportFlag(Integer* ex)const
{
	Int nn=getnTri();
	xMesh::tri_face* facePtr= getTriPtr();
	for(Int i=0;i<nn;i++){
		ex[i]=facePtr[i].getFlag();
	}
}
