      MODULE mlfma_input

      INTEGER*8 ipol, imono
      INTEGER*8 mvmode
      REAL   freq
      REAL   thetai1, thetai2, phii1, phii2
      REAL   thetas1, thetas2, phis1, phis2
      REAL   alpha
      INTEGER*8 nthetai, nthetas, nphii, nphis
      INTEGER*8 icgbcg, itmax
      REAL   epscg
      INTEGER*8 match, irule_src, irule_fld
      END MODULE
!     COMMON/mlfma_input/ipol, imono,
!     &                   freq,
!     &                   thetai1, thetai2, nthetai, phii1, phii2, nphii,
!     &                   thetas1, thetas2, nthetas, phis1, phis2, nphis,
!     &                   alpha,
!     &                   icgbcg, itmax,
!     &                   epscg,
!     &                   match, irule_src, irule_fld
