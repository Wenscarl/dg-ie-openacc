      subroutine anterpl(kpm, niplm, kptc, kptp, 
     &           array, icsa, irsa, cvlr, cgm)

ccccccccccccccccccccccccccccccccccccccccccccccc
c  anterpolation
c  more points: cvlr; less points: cgm
ccccccccccccccccccccccccccccccccccccccccccccccc

      implicit none

      integer*8 kpm, niplm
      integer*8 kptp, kptc, icsa(niplm), irsa(kpm+1)
      real array(niplm)
      complex cvlr(2,kpm), cgm(2,kpm)

      integer*8 kp, k, j

      do kp = 1, kptc
         cgm(1,kp) = (0.0,0.0)
         cgm(2,kp) = (0.0,0.0)
      enddo
      do kp = 1, kptp
         do k = irsa(kp), irsa(kp+1)-1
            j = icsa(k)
            cgm(1,j) = cgm(1,j) + array(k)*cvlr(1,kp)
            cgm(2,j) = cgm(2,j) + array(k)*cvlr(2,kp)
         enddo
      enddo

      return
      end
