//       subroutine main_box
//      &( memory_tot, lmax, 
//      &  iedge, edge, ipatpnt, xyzctr, 
//      &  paera, xyznorm,xyznode,
//      &  ngrid, vt1, vt2, vt3, wt, igrs, lxyz,
//      &  modes, sizel,lrsmup, lrsmdown, lrstl, 
//      &  family, kpstart)
#include "global.h"
extern "C" void main_box_(Integer*, 
				  Integer*,
				  Real*,
				  Integer*,
				  Integer*,  //5
				  Integer*,
				  Real *MEMORY_TOT,
                        Integer *LMAX,
                        Integer *IEDGEP, 
                        Real *EDGEP, //10
                        Integer *IPATPNTP, 
                           Real *XYZCTRP,
                       Real *PAERAP, 
                         Real *XYZNORMP,
                         Real *XYZNODEP, //15
                         Integer *NGRIDP,
                        Real *VT1P,
                        Real *VT2P,
                        Real *VT3P,
                        Real *WTP,//20
                        Integer *IGRSP,
                        Integer *LXYZP,
                        Integer *MODESP,
                        Real *SIZELP,
                        Integer *LRSMUPP, //25
                        Integer *LRSMDOWNP,
                        Integer *LRSTLP,
                         Integer *FAMILYP,
                        Integer *KPSTARTP,
				Complex* ,//30
				Real*,
				Integer*
                        );
