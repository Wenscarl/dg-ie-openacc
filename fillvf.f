      subroutine fillvf
     &( ie,cvlr,
     &  xyznode,ipatpnt, iedge, xyzctr, xyznorm, edge, paera,
     &  ngrid, vt1, vt2, vt3, wt,
     &  kp0, xyzc, dirk, kpt)

ccccccccccccccccccccccccccccccccccc
c     fill vf in fmm
ccccccccccccccccccccccccccccccccccc

        use mlfma_input
        use mlfma_const
        use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)

      INTEGER*8 ie, kp0, kpt
       REAL   xyzc(3), dirk(3,3,kp0)
      COMPLEX cvlr(2,kp0)

      INTEGER*8 kp, i, j, l, i3,  n1, n2, n3
       REAL   rfld(3), rhofld(3), rk(3), dotmul, signl
      COMPLEX ct
      INTEGER*8 ixyz
       REAL   temp(3), ectr(3), pctr(3)
      COMPLEX cfbt1, cfbp1, cfbt2, cfbp2, ctf1(3), ctf2(3)
  
      do kp = 1, kpt	!kpt is the spectrum number of finest level.
         do i = 1, 2 
            cvlr(i, kp) = 0.
         enddo
      enddo
   
      do kp = 1, kpt

         do j = 1, 3
            ctf1(j) = 0.0
            ctf2(j) =0.0
         enddo

         do ixyz = 1, 3
            rk(ixyz) = rk0*dirk(ixyz, 1, kp)
         enddo

         n1 = iedge(1,ie)
         n2 = iedge(2,ie)

         do ixyz = 1, 3
            ectr(ixyz) = 0.5*( xyznode(ixyz,n1) + xyznode(ixyz,n2) )
         enddo

         do 10 l=1,2
        
            i3 = iedge(2+l,ie)
            if (i3 .le. 0) then 
			goto 10
	    endif
            n3 = ipatpnt(1,i3)+ipatpnt(2,i3)+ipatpnt(3,i3)-n1-n2

            signl = float(3-2*l)
            do ixyz = 1, 3
               pctr(ixyz) = xyzctr(ixyz, i3)
            enddo
            call vctadd( ectr, pctr, rhofld, -1 )
            do i=1,ngrid(irule_fld)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c           ! each integration grid point rfld is the linear 
c           ! combination of the three vertices of the triangle i3.
c           ! the combination coefficients are vt1 for vertix 1,
c           ! vt2 for vertix 2, and vt3 for vertix 3. different
c           ! point i has different combination coeffcient vt(i,:).
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

               do j=1,3
                 rfld(j) = vt1(i,irule_fld)*xyznode(j,n1)+
     &                     vt2(i,irule_fld)*xyznode(j,n2)+
     &                     vt3(i,irule_fld)*xyznode(j,n3)
                 if(match.eq.1) rhofld(j) = rfld(j) - xyznode(j,n3) 
                 rfld(j) = rfld(j) - xyzc(j)   
c                                  ^ shifted to cubic center
               enddo

               ct = signl*wt(i,irule_fld)*exp((dotmul(rk,rfld)*ci))
                  do j = 1, 3
                     ctf1(j) = ctf1(j) + ct*rhofld(j)
                  enddo

                  call xmul(rhofld, xyznorm(1,i3),temp)
                  do j = 1, 3
                     ctf2(j) = ctf2(j) + ct*temp(j) 
                  enddo

            end do
10       continue

         cfbt1 = dirk(1,2,kp)*ctf1(1)
     &          +dirk(2,2,kp)*ctf1(2)
     &          +dirk(3,2,kp)*ctf1(3)
         cfbp1 = dirk(1,3,kp)*ctf1(1)
     &          +dirk(2,3,kp)*ctf1(2)

         cfbt2 = dirk(1,2,kp)*ctf2(1)
     &          +dirk(2,2,kp)*ctf2(2)
     &          +dirk(3,2,kp)*ctf2(3)
         cfbp2 = dirk(1,3,kp)*ctf2(1)
     &          +dirk(2,3,kp)*ctf2(2)

         if(match.eq.1) then
            cfbt1 = cfbt1 * 0.5*edge(ie)
            cfbp1 = cfbp1 * 0.5*edge(ie)
            cfbt2 = cfbt2 * 0.5*edge(ie)
            cfbp2 = cfbp2 * 0.5*edge(ie)
         end if

            cvlr(1,kp)=alpha*cfbt1+(1-alpha)*cfbp2
            cvlr(2,kp)=alpha*cfbp1-(1-alpha)*cfbt2
 
      enddo !do kp = 1, kpt

    
      return
      end
