      subroutine MTXELE
     &( ii, jj, ck, sing, match,
     &  irule_fld, irule_src, irulef_near, irules_near, 
     &  maxnode, maxpatch, maxedge, maxrule, maxgrid,
     &  xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &  ngrid, vt1, vt2, vt3, wt, rnear2, distmin,
     &  rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &  cnxet, cnxhx)

      IMPLICIT NONE

c.....Input Data

      INTEGER*8 ii, jj, match, irule_fld, irule_src,
     &        irulef_near, irules_near,
     &        maxnode, maxpatch, maxedge, maxrule, maxgrid
      INTEGER*8 ipatpnt(3, maxpatch), iedge(4, maxedge),
     &        ngrid(maxrule)
      COMPLEX ck, ci
       REAL   sing, rnear2, distmin, rk0, freq, wl0,
     &        rk2d4,eta0,   cnste,   cnsth
       REAL   xyznode(3, maxnode), xyzctr(3, maxpatch), 
     &        xyznorm(3, maxpatch),
     &        edge(maxedge), paera(maxpatch),
     &        vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
       REAL   pi, eps
       PARAMETER( pi=3.141592653, eps=1e-4 )

c.....Output Data

       COMPLEX cnxet, cnxhx,cnxet2

c.....Working Variables

      INTEGER*8 i, j, igetn3, irulef0, irules0,
     &        lfld, n1fld,  n2fld,   n3fld,   irulef,
     &        lsrc, n1src,  n2src,   n3src,   irules

       REAL signfld,  signsrc,  aera
       REAL rfld(3),  rsrc(3),  rhofld(3), rhosrc(3),
     &      rr(3),    r1(3),    r2(3),     r3(3),
     &      rhoxnt(3),rhoxnx(3), rxns(3),  anorm(3), bnorm(3),  
     &      fld2src2, dotmul,
     &      ectr(3),   pctr(3),   rhot(3), 
     &      rf,       rs,        d,         rnst

      COMPLEX cefie,cefie2,csume,csume2,cze,cze2,cfunex_new
     &        ,cfet,cfet2,cfet3,cfunex,
     &        cmfie,  csumh,   czh,   cfunhx,  cfhx,
     &        cagu,   cagu2,   cg0,   cx,      cfunexs,
     &        ck2d2,   cfunhxs,tmpp
      complex rtemp
       REAL r0(3),  rhod(3), rhoxd(3), rho0(3), ri1p(3), ri3p(3),
     &      fld2src, ri1, ri3, ds
      INTEGER*8 ipfld, ipsrc, nearpat,far


      ! for duffy integral 
!       INTEGER*8 oD,iD ! outer and innter loop for duffy
!       INTEGER*8 ioD,iiD
!       Real vtd1,vtd2,vtd3,wtd
c.......................................................................
!       oD=4
! 	iD=12
      ck2d2=ck*ck/2.0

      n1fld = iedge( 1, ii )
      n2fld = iedge( 2, ii )
      n1src = iedge( 1, jj )
      n2src = iedge( 2, jj )

      call mv1to2( xyznode(1,n1src), r1, 3 )
      call mv1to2( xyznode(1,n2src), r2, 3 )

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
c.... .set integration rules: depending on the distance of the............c 
c..... two basises, different rules are used. when the ...................c
c.....(squared) distance is larger than rnear2, irule_fld and ............c
c......irule_src are used; otherwise, the near-field rules................c
c......(irulef_near, irules_near) are used................................c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      far=1
      irulef0 = irule_fld
      irules0 = irule_src
      d = 0.0

      do i=1,3
        rf = xyznode(i,n1fld)+xyznode(i,n2fld)
        rs = xyznode(i,n1src)+xyznode(i,n2src)
        d = d + abs((rf-rs)*(rf-rs))
      end do

      if( 0.25*d .le. rnear2 ) then
         irulef0 = irulef_near
         irules0 = irules_near
      end if

c.....................................................................

      call getedgectr(xyznode(1,n1fld),xyznode(1,n2fld),ectr)
	cnxet2 = (0.0, 0.0)
      cnxet = (0.0, 0.0)
      cnxhx = (0.0, 0.0)
      do 400 lfld = 1, 2

         signfld = float(3-2*lfld)
         ipfld = iedge(2+lfld,ii)
         
         if( ipfld .le.0) then 
		goto 400
		endif

         n3fld = igetn3( n1fld, n2fld, ipatpnt(1,ipfld) )
         call mv1to2( xyznorm(1,ipfld), anorm, 3 )
         call mv1to2( xyzctr(1,ipfld),  pctr,  3 )
         call vctadd( ectr, pctr, rhot, -1 ) 
         ! pointing from the mid point of triangle to the mid point of edge.
        ! only used when match==2
         cefie = (0.0,0.0)
	   cefie2 = (0.0,0.0)
         cmfie = (0.0,0.0)
         do 200 lsrc = 1, 2

            signsrc = float( 3 - 2 * lsrc )
            ipsrc = iedge( 2+lsrc, jj )
		if (ipsrc .le.0) then
			goto 200
		endif
            n3src = igetn3( n1src, n2src,ipatpnt(1,ipsrc) )
		
            call mv1to2(xyznode(1,n3src), r3, 3)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c..... check the distance of the two patches. if this.................c
c..... distance is small, special treatment is .......................c
c..... equired to calculate the matrix elements.......................c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
            irulef = irulef0
            irules = irules0
		! when the src and receiving triangle are close enough, change the irules_near
            call rules2(ipfld, ipsrc, xyzctr(1,ipfld), xyzctr(1,ipsrc), 
     &                  distmin, irulef_near,  irules_near, 
     &                  nearpat, irulef, irules )
            !for the near coupling, move the normal vector of the source triangle to bnorm.
            if(nearpat.eq.1) call mv1to2(xyznorm(1,ipsrc), bnorm,3)
		if(nearpat.eq.1) far=0
		csume2= (0.0,0.0)
            csume = (0.0,0.0)
            csumh = (0.0,0.0)
		! Go through the every points in the source triangle
            do 100 i = 1, ngrid(irulef)! go throught the integral rule of the receiving triangle.
               call getr( ! get the r and rho of sampling points
     &           xyznode(1,n1fld),xyznode(1,n2fld),xyznode(1,n3fld),
     &           vt1(i,irulef), vt2(i,irulef), vt3(i,irulef),
     &           rfld,rhofld)
               call mv1to2( rhofld, rhoxnt, 3 )  !rhoxnt = rho
               call xmul( rhofld, anorm, rhoxnx ) ! rhoxnx= rho  x Normal
               if(nearpat.eq.1) then
               aera = paera(ipsrc) 
                if(lsrc.eq.1) then
c                     ! the order of the three vertices must 
c                     ! form right hand rule.
                  call proj( rfld,r1,r2,r3,r3,bnorm,aera, wl0,
     &                 ds, r0,rho0,rhod,rhoxd,ri1,ri1p,ri3,ri3p)
                else
                  call proj( rfld,r1,r3,r2,r3,bnorm,aera, wl0,
     &                 ds, r0,rho0,rhod,rhoxd,ri1,ri1p,ri3,ri3p)
                end if
               end if
		   cze = (0.0,0.0)
		   czh = (0.0,0.0)
		   cze2 = (0.0,0.0)			
               do j = 1, ngrid(irules) ! for source
                  call getr
     &             ( xyznode(1,n1src),xyznode(1,n2src),xyznode(1,n3src),
     &               vt1(j,irules),vt2(j,irules),vt3(j,irules),
     &               rsrc,rhosrc )
                  call vctadd( rfld,rsrc,rr, -1 )
                  call cnst_func( rr, ck, ci,
     &                            fld2src, fld2src2, cagu, cagu2, 
     &                            cx, cg0 )
                  if(nearpat.eq.0) then !! near field, is now done by Duffy transform.
                  cfet=cfunex_new(rhoxnt,rhosrc,paera(ipfld),
     &                         paera(ipsrc),edge(ii),edge(jj),nearpat,
     &                     fld2src, cagu, cagu2, cx, cg0, ck, ci,cfet2)
	              endif
                  cfhx=cfunhx(rhoxnx, rr, rhosrc,
     &                        ipfld, ipsrc, nearpat, 
     &                        fld2src,fld2src2, cagu, cagu2, cx, 
     &                        cg0, ck, ci)
			if(nearpat.eq.0) then  !! near field, is now done by Duffy transform.
                     cze=cze + wt(j,irules) * cfet
	      		cze2=cze2 + wt(j,irules) * cfet2
                  endif

                  czh=czh + wt(j,irules) * cfhx
               end do !do j = 1, ngrid(irules)
 
               if(( ipsrc.eq.ipfld ).and.(abs(sing).gt.0.01)) then  
c                      ! the 2*pi*j term contribution for mfie:
                  call vctadd( rfld, r3, rhosrc, -1 )
                  call xmul( rhosrc, bnorm, rxns )
                  czh=czh+2.0*pi*sing*dotmul(rhoxnx,rxns)/paera(ipsrc)
               end if

               if(nearpat.eq.1) then
                 if(ipfld.ne.ipsrc) then
                    czh = czh + cfunhxs( rhoxnx, rho0, rhod, rhoxd, ds,
     &                                   ri1, ri1p, ri3, ri3p, ck2d2 )
                 end if
               end if
			if(nearpat.eq.0) then
			csume2 = csume2 + wt(i,irulef) * cze2
			csume = csume + wt(i,irulef) * cze
			endif
			csumh = csumh + wt(i,irulef) * czh
100         continue


            cefie = cefie + signsrc * csume
		cefie2= cefie2 + signsrc * csume2
            cmfie = cmfie + signsrc * csumh
200      continue !do 200 lsrc = 1, 2
 
         cnxet = cnxet + signfld*cefie
	   cnxet2 = cnxet2 + signfld *cefie2
         cnxhx = cnxhx + signfld*cmfie
400   continue !do 400 lfld = 1, 2
      rnst = edge(jj)*float(match)/(16.0*pi)

      if(match.eq.1) rnst = rnst*edge(ii)
	cnxet2 = cnxet2*rnst
      cnxet = cnxet*rnst+cnxet2*rnst*4.0
      cnxhx = cnxhx*rnst

      RETURN
      END
      
