#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
// #define _buildarrays_C_   1
#include "array_pointer.h"
#include <stdio.h>
#include <assert.h>
Complex* pc_cbdp=NULL;
Integer* pc_bdpidx=NULL;
Integer* pc_bdpipiv=NULL;
Complex* pc_scratch=NULL;

typedef long long int Int;
typedef unsigned long long int Unsigned; 

const Int* 	pc_igrs=NULL;
const Int** 	pc_noself=NULL;
const Int* 	pc_igall=NULL;
const Int* 	pc_igblkrs=NULL;
const Int* 	pc_igblkcs=NULL;
const Int* 	pc_igblk=NULL;
const Complex*	pc_canear=NULL;
const Int* 	pc_index=NULL;
Int		pc_maxedge=0;
Int 		pc_ipgp=-1;
Int 		pc_mself=-1;
Int 		pc_indexm=-1;
Int 		pc_ipbdp=-1;
Int 		pc_ngnear=-1;
Int 		pc_jlo=-1;
Int 		pc_indexn=-1;
Int 		pc_ipnear=-1;
Int 		pc_i=-1;
Int		pc_j=-1;
Int 		pc_mn=-1;
Int		pc_mn2=-1;

#ifdef __cplusplus
extern "C" {
#endif
	void cgetrf_(Int *M, Int *N, Complex* A, Int *LDA, Int *IPIV, Int *INFO);
	void cgetrs_(char *TRANS, Int *N, Int *NRHS, Complex* A,
		     Int *LDA, Int *IPIV, Complex* B, Int *LDB, Int *INFO,
       Int TRANS_len);

// 	void zgetrf_(Int *M, Int *N, std::complex<double> *A, Int *LDA, Int *IPIV, Int *INFO);
// 	void zgetrs_(char *TRANS, Int *N, Int *NRHS, std::complex<double> *A,
// 		     Int *LDA, Int *IPIV, std::complex<double> *B, Int *LDB, Int *INFO,
//        Int TRANS_len);
#ifdef __cplusplus
	｝
#endif

void initializepc_(const Int *igrs,
// 		  const Int noself[][2],
		const Int** noself,
    		const Int *igall,const Int *igblkrs,const Int *igblkcs,
    		const Int *igblk, const Complex *canear, const Int *index);
void solvepc_(const Complex* x,  Complex* result,char trans);
void releasememorypc_();

void initializepc_(const Int *igrs, 
// 		  const Int noself[][2],
		  const Int** noself,
    		const Int *igall,
       		const Int *igblkrs, 
	 	const Int *igblkcs,
   		const Int *igblk, 
     		const Complex *canear, 
       		const Int *index)
{
	pc_igrs=igrs;
	pc_noself=noself;
	pc_igall= igall;
	pc_igblkrs=igblkrs;
	pc_igblkcs=igblkcs;
	pc_igblk= igblk;
	pc_canear=canear;
	pc_index=index;
	
	pc_bdpidx=(Integer*) malloc(sizeof(Integer)*(pc_igrs[1]-pc_igrs[0]+1));
	assert(pc_igrs[0] == 1);

	Unsigned bdpsize = 0;
	pc_bdpidx[0] = 0;
	pc_maxedge = 0;
	
	for (pc_ipgp = pc_igrs[0]; pc_ipgp < pc_igrs[1]; ++pc_ipgp) {
		pc_mself = pc_noself[pc_ipgp-1][0];
		bdpsize += pc_mself*pc_mself;
		pc_maxedge += pc_mself;
		pc_bdpidx[pc_ipgp-1+1] = bdpsize;
	}

// 	cbdp.resize(bdpsize);
// 	scratch.resize(maxedge);
// 	bdpipiv.resize(maxedge);
	pc_cbdp= (Complex*) malloc(sizeof( Complex)*bdpsize);
	pc_scratch= (Complex*) malloc(sizeof( Complex)*pc_maxedge);
	pc_bdpipiv= (Integer*) malloc(sizeof( Integer)*pc_maxedge);
	

	for (pc_ipgp = pc_igrs[0]; pc_ipgp < pc_igrs[1]; ++pc_ipgp) {
		pc_mself = pc_noself[pc_ipgp-1][0];
		pc_indexm = pc_igall[pc_ipgp-1] - 1;
		pc_ipbdp = pc_bdpidx[pc_ipgp-1];

		for (pc_ngnear = pc_igblkrs[pc_ipgp-1]; pc_ngnear < pc_igblkrs[pc_ipgp]; ++pc_ngnear) {
			pc_jlo = pc_igblkcs[pc_ngnear-1];
			pc_indexn = pc_igall[pc_jlo-1] - 1;
			pc_ipnear = pc_igblk[pc_ngnear-1]-1;

			if (pc_indexn != pc_indexm)
				continue;

      // copy the diagonal block of the near field matrix
			for (pc_i = 0; pc_i < pc_mself; ++pc_i){
				for (pc_j = 0; pc_j < pc_mself; ++pc_j) 
				{
				pc_mn	= pc_ipnear + pc_i*pc_mself + pc_j;
				pc_mn2	= pc_ipbdp + pc_i*pc_mself + pc_j;
				pc_cbdp[pc_mn2][0]	= pc_canear[pc_mn][0];
				pc_cbdp[pc_mn2][1]	= pc_canear[pc_mn][1];
				}
			}
				break;
		}

    // factorize block
		Int INFO;
		cgetrf_(&pc_mself, &pc_mself, &pc_cbdp[pc_ipbdp], &pc_mself,
			 &pc_bdpipiv[pc_indexm], &INFO);
	}
}

void solvepc_(const Complex* x, Complex* result,char trans)
{
	if (pc_maxedge==0){
		printf("Error: in Solve PC: pc_maxedge==0\n");
	}
	if(x!= (const Complex*) result){
		for(pc_i=0;pc_i<pc_maxedge;pc_i++){
			result[pc_i][0] = x[pc_i][0];
			result[pc_i][1] = x[pc_i][1];
		}
	}

	for (pc_i = 0; pc_i < pc_maxedge; ++pc_i){
		pc_scratch[pc_i][0] = x[pc_index[pc_i]-1][0];
		pc_scratch[pc_i][1] = x[pc_index[pc_i]-1][1];
	}
	for (pc_ipgp = pc_igrs[0]; pc_ipgp < pc_igrs[1]; ++pc_ipgp) {
		pc_mself = pc_noself[pc_ipgp-1][0];
		pc_indexm = pc_igall[pc_ipgp-1] - 1;
		pc_ipbdp = pc_bdpidx[pc_ipgp-1];

// 		char TRANS = 'N';
		char TRANS= trans;
		Int ONE = 1;
		Int INFO;
		cgetrs_(&TRANS, &pc_mself, &ONE, &pc_cbdp[pc_ipbdp], &pc_mself,
			 &pc_bdpipiv[pc_indexm], &pc_scratch[pc_indexm], &pc_mself, &INFO, 1);
	}

	for (pc_i = 0; pc_i < pc_maxedge; ++pc_i){
		result[pc_index[pc_i]-1][0]= pc_scratch[pc_i][0];
		result[pc_index[pc_i]-1][1]= pc_scratch[pc_i][1];
	}
}
void releasememorypc_()
{
	if(pc_cbdp!=NULL)
		free( pc_cbdp);
	if(pc_bdpidx!=NULL)
		free( pc_bdpidx);
	if(pc_bdpipiv!=NULL)
		free( pc_bdpipiv);
	if(pc_scratch!=NULL)
		free( pc_scratch);	
}
