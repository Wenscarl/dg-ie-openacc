      function CFUNEX 
     &( rhoxn, rr, rhosrc,
     &  nearpat,
     &  fld2src,cagu,cagu2, cx, cg0, ck, ci )

      IMPLICIT NONE

c.....Input Data

       REAL rhoxn(3), rr(3), rhosrc(3)
       INTEGER*8 nearpat
       REAL fld2src
       COMPLEX cagu, cagu2, cx, cg0, ck, ci

c.....Output Data

       COMPLEX cfunex

c.....Working Variables

       REAL    dotmul
       COMPLEX cmp1, cmp2

c..................................................................

      if(nearpat.eq.1) then         
         if(cabs(cagu).lt.0.01) then
            cmp1 = ck*(-0.5*cagu+ci*( 1.0-cagu2/6.0 ))
            cmp2 = ck*(cagu*(0.25-cagu2/72.0)-ci*(2.0-0.2*cagu2)/3.0)
         else
            cmp1 = (cg0-1)/fld2src
            cmp2 = 2.0*(cx*cg0+1.0+0.5*cagu2)/(cagu2*fld2src)
         end if
      else
         cmp1=cg0/fld2src      
! cmp1 is the Green's function.
         cmp2 = 2.0*cx*cg0/(cagu2*fld2src)
! cmp2 is second dirivative of Green's function.
      end if

      cfunex = cmp1*dotmul(rhoxn, rhosrc)+cmp2*dotmul(rhoxn,rr)

      RETURN
      END
