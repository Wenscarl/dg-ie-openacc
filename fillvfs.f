      subroutine fillvfs
     &( cvf, cvs,
     &  xyznode,ipatpnt, iedge,  xyzctr, xyznorm, edge,paera, 
     &  NGRID,  vt1, vt2, vt3, wt,
     &  lmax, lsmax, lsmin, ncmax, kp0,
     &  lxyz, modes, sizel,
     &  igall, igcs, igrs, index, family,
     &  xgl, wgl, dirk)

ccccccccccccccccccccccccccccccccc
c   fill cvf, cvs in fmm
ccccccccccccccccccccccccccccccccc

        use mlfma_input
        use mlfma_const
        use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
      INTEGER*8 lmax, lsmax, lsmin, ncmax, kp0
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
       REAL   xgl(lsmax), wgl(lsmax), dirk(3,3,kp0)
      COMPLEX cvf(2,kp0, maxedge),cvs(2,kp0,maxedge)

      INTEGER*8 ipointer, ls, kpt
      INTEGER*8 l, igt, ig(3), ixyz, ipindex, ib
       REAL   xyzc(3)
!       INTEGER*8 contourMap(maxedge)
c.....the finest level
      l=lmax
      ls=modes(l)
      kpt=4+2*ls*ls

!$OMP PARALLEL DO PRIVATE(igt,ig,ixyz,xyzc,ipindex,ib)
      do ipointer=igrs(1),igrs(2)-1
        igt=igcs(ipointer)
        call igxyz(igt,lxyz(1,l),ig)
          do ixyz=1,3
             xyzc(ixyz)=rmin(ixyz)+(ig(ixyz)-0.5)*sizel(ixyz,l)
          enddo
      do ipindex=igall(ipointer),igall(ipointer+1)-1
          ib=index(ipindex)
          call fillvf(ib,cvf(1,1,ipindex),
     &                xyznode,ipatpnt,
     &                iedge,xyzctr,xyznorm,edge,paera,
     &                ngrid, vt1,vt2, vt3, wt,
     &                kp0, xyzc, dirk, kpt)

          call fillvs(ib,cvs(1,1,ipindex),
     &                xyznode,ipatpnt,
     &                iedge,xyzctr,xyznorm,edge,paera,
     &                ngrid, vt1,vt2, vt3,wt,
     &                kp0, xyzc, dirk, kpt)

      enddo
      enddo
!$OMP END PARALLEL DO

      return
      end
