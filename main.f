cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c...This MLFMA code is to compute the scattering by conducting bodies......c
c..........Using RWG-based MoM by Iterative Solvers such as CG.............c
c.......With the aid of MLFMA-enhanced matrix-vector multiplication........c
c..........re-Written by Xin-Qing Sheng in CityU of Hong Kong..............c
c............................Nov 6, 2000...................................c
c..................Copyright (C) 2000, Xin-Qing Sheng......................c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc  
        subroutine MLFMA_INTERFACE
        use mlfma_input
        use mlfma_const
        use mlfma_param
 

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

	INTERFACE
	    SUBROUTINE c_main_geom( memory_tot)
!DEC$ ATTRIBUTES C,ALIAS:'_c_main_geom':: c_main_geom
	        real memory_tot
!DEC$ ATTRIBUTES REFERENCE ::memory_tot 
          END SUBROUTINE

	    SUBROUTINE bld_maxedge(maxedge)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_maxedge':: bld_maxedge
	        integer*8 maxedge
!DEC$ ATTRIBUTES REFERENCE ::maxedge 
          END SUBROUTINE

	    SUBROUTINE bld_maxpatch(maxpatch)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_maxpatch':: bld_maxpatch
	        integer*8 maxpatch
!DEC$ ATTRIBUTES REFERENCE ::maxpatch 
          END SUBROUTINE

	    SUBROUTINE bld_maxnode(maxnode)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_maxnode':: bld_maxnode
	        integer*8 maxnode
!DEC$ ATTRIBUTES REFERENCE ::maxnode
          END SUBROUTINE

	    SUBROUTINE bld_maxrule(maxrule)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_maxrule':: bld_maxrule
	        integer*8 maxrule
!DEC$ ATTRIBUTES REFERENCE ::maxrule 
          END SUBROUTINE

	    SUBROUTINE bld_maxgrid_maxrule(maxgrid,maxrule)
!DEC$ ATTRIBUTES C,ALIAS:'_bld_maxgrid_maxrule':: bld_maxgrid_maxrule
	        integer*8 maxgrid,maxrule 
!DEC$ ATTRIBUTES REFERENCE ::maxgrid,maxrule 
          END SUBROUTINE
      END INTERFACE

       REAL   memory_tot

      call read_mlfma(freq, ipol, imono,
     &                thetai1, thetai2, nthetai, phii1, phii2, nphii,
     &                thetas1, thetas2, nthetas, phis1, phis2, nphis,
     &                alpha, match, irule_src, irule_fld,
     &                icgbcg, itmax, epscg)

	wl0=0.3/freq
 
         read(1,*) maxnode, maxpatch, maxedge

         maxrule  = 11
         maxgrid  = 64

         call bld_maxedge(maxedge) 
         call bld_maxpatch(maxpatch) 
         call bld_maxnode(maxnode) 
         call bld_maxrule(maxrule) 
         call bld_maxgrid_maxrule(maxgrid,maxrule)

         memory_tot=0
         memory_tot= memory_tot
     &             + maxedge*(4*4+4+4)
     &             + maxpatch*(3*4+3*4+4+3*4)
     &             + maxnode*(3*4)
     &             + maxrule*(4)
     &             + maxgrid*maxrule*(4*4)

      call c_main_geom( memory_tot)

      end
