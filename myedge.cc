#include "myedge.h"
#include <iostream>
using namespace std;
Edge::Edge(Int a,Int b)
{
// 	assert(a>0&&b>0);
	n1=a;n2=b;
}
Int min(Int a,Int b)
{
	return (a<b?a:b);
}
Int max(Int a,Int b)
{
	return (a>b?a:b);
}
bool Edge::operator==(const Edge& right)const
{
	if(n1==right.n1&&n2==right.n2 ||
		  n1==right.n2&&n2==right.n1) 
		return true; 
	else 
		return false;
}
bool Edge::operator <(const Edge& right) const
{
	if(n1==right.n1&&n2==right.n2 ||
		  n1==right.n2&&n2==right.n1) 
		return false;
	if(min(n1,n2)<min(right.n1,right.n2))
		return true;
	else if(min(n1,n2)==min(right.n1,right.n2))
	{
		if(max(n1,n2)<max(right.n1,right.n2))
			return true;
		else 
			return false;
	}
	else
		return false;
}
bool Edge::operator >(const Edge& right)const
{
	if(n1==right.n1&&n2==right.n2 ||
		  n1==right.n2&&n2==right.n1) 
		return false;
	
	if(min(n1,n2)<min(right.n1,right.n2))
		return false;
	else if(min(n1,n2)==min(right.n1,right.n2))
	{
		if(max(n1,n2)<max(right.n1,right.n2))
			return false;
		else 
			return true;
	}
	else
		return true;
}
