      subroutine bicgm2(cb, cx, icgbcg, itmax, epscg, ier, nunss,
     &                    cr, cr1, cp, cp1, cu, cv, cwork1, cwork2, 
     &                    cvf, cvs, maxedge, lmax, lsmax,
     &                    ncmax, kpm, kp0, nsm, ntlm, niplm,
     &                    lxyz, modes,
     &                    igall, igcs, igrs, index, family, 
     &                    lrsmup, lrsmdown, csm, cvlr, cgm, ctl, 
     &                    lrstl, indextl,
     &                    kpstart, nkpm, c_shift, icsa, irsa, array, 
     &                    ngsndm, igsndcs, igsndrs,
     &                    nearm, canear, noself, ipvt,
     &                    ngnearm, igblkrs, igblkcs, igblk,
     &                    mfinestm, cfinest)
      use mlfma_const
      implicit none

!	nclude 'mlfma_const.inc'

      INTEGER*8 maxedge, lmax, lsmax, ncmax, kpm, kp0, nkpm,
     &        nsm, ntlm, niplm, ngsndm, mfinestm,
     &        nearm, ngnearm
      INTEGER*8 ier, nunss, icgbcg, itmax
       REAL   epscg

      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
       REAL   dirk(3,3,nkpm), xgl(lsmax,lmax), wgl(lsmax,lmax)
      INTEGER*8 irsa(nkpm+1), icsa(niplm)
       REAL   array(niplm)
      COMPLEX ctl(ntlm), csm(2,nsm)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 noself(2,ncmax+1), ipvt(maxedge)    
      COMPLEX crhs(maxedge), cbm(maxedge)
      COMPLEX cwork(maxedge,8)
      COMPLEX cvf(2,kp0,maxedge), cvs(2,kp0,maxedge)
      COMPLEX canear(nearm*maxedge), cfinest(mfinestm)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1), 
     &        igblk(ngnearm+1)
      COMPLEX c_shift(nkpm,2,2,2)
      INTEGER*8 igsndcs(ngsndm), igsndrs(ncmax)      

      COMPLEX  cb(maxedge), cx(maxedge)
      COMPLEX  cr(maxedge),  cp(maxedge), 
     &         cu(maxedge),
     &         cr1(maxedge), cp1(maxedge), 
     &         cv(maxedge)
      COMPLEX cwork1(maxedge), cwork2(maxedge)

      INTEGER*8 neqs, i, it
       REAL   rnorm, r10, r20, q20
      COMPLEX  calp, cbet, c1, c2, cpc

      neqs=maxedge
      print*,'total unknowns=',neqs

      c1=0e0
      call cacxpc(cx, cu, 0, cwork1, cwork2, cvf, cvs,
     &            maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &            ntlm, niplm,lxyz, modes,
     &            igall, igcs, igrs, index, family, 
     &            lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, 
     &            indextl,
     &            kpstart, nkpm, c_shift, icsa, irsa, array, 
     &            ngsndm, igsndcs, igsndrs,
     &            nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &            igblkcs, igblk,mfinestm, cfinest)

      do 20 i=1,neqs
      cr(i)=cb(i)-cu(i)
      cp(i)=cr(i)
      cr1(i)=conjg(cr(i))
      cp1(i)=cr1(i)
      c1=c1+cr(i)*conjg(cr1(i))
20    continue
      r10=rnorm(cb,neqs)
      write( *,*)  'Iterations, Residual Norm, Current |x(1)|'
      write( *,5)0,1e0,abs(cx(nunss+1))
 5    format(2x,i6,3x,4(2x,e13.6))
      do 100 it=1,itmax
      call cacxpc(cp, cu, 0, cwork1, cwork2, cvf, cvs,
     &              maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, 
     &              ntlm, niplm, lxyz, modes,
     &              igall, igcs, igrs, index, family, 
     &              lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, 
     &              indextl,
     &              kpstart, nkpm, c_shift, icsa, irsa, array, 
     &              ngsndm, igsndcs, igsndrs,
     &              nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &              igblkcs, igblk, mfinestm, cfinest)

      call cacxpc(cp1,cv, 1, cwork1, cwork2, cvf, cvs,
     &              maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm, ntlm, 
     &              niplm, lxyz, modes,
     &              igall, igcs, igrs, index, family, 
     &              lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, 
     &              indextl,
     &              kpstart, nkpm, c_shift, icsa, irsa, array, 
     &              ngsndm, igsndcs, igsndrs,
     &              nearm, canear, noself, ipvt, ngnearm, igblkrs, 
     &              igblkcs, igblk, mfinestm, cfinest)

      cpc=0e0
      do 30 i=1,neqs
        cpc=cpc+conjg(cp1(i))*cu(i)
30      continue
      if(abs(cpc).lt.1e-30) return
      calp=c1/cpc
      c2=0e0
      do 40 i=1,neqs
        cx(i)=cx(i)+calp*cp(i)
        cr(i)=cr(i)-calp*cu(i)
        cr1(i)=cr1(i)-conjg(calp)*cv(i)
        c2=c2+cr(i)*conjg(cr1(i))
40	continue
      q20=rnorm(cr1,neqs)
      r20=rnorm(cr ,neqs)
        write( *,5) it,sqrt(r20/r10),abs(cx(nunss+1))
      if(r20.lt.r10*epscg*epscg) return
      cbet=c2/c1
      c1=c2
      do 50 i=1,neqs
        cp(i)=cr(i)+cbet*cp(i)
        cp1(i)=cr1(i)+conjg(cbet)*cp1(i)
50      continue
        ier=it+1
100   continue
      ier=-1
      write(*,*)  'too many iterations!'
      return
      end
