#include <set>
#include <ctime>
#include <cstdio>
#include "comp.h"

extern "C" {
  void assm_progress_(const long long int *igrs);
}

void assm_progress_(const long long int *igrs)
{
    static long long int current_size = 0;
    static long long int max_size = 0;
    static clock_t last_time = 0;

#pragma omp critical
    {
      ++current_size;
    }

#pragma omp master
  {
    if (max_size == 0) {
      max_size = igrs[1]-igrs[0];
      last_time = clock();
    }

    int thread_cnt = omp_get_num_threads();
    if ((clock() - last_time) > 15*CLOCKS_PER_SEC*thread_cnt) {
      printf("\r%.4f%%", (current_size*100.0)/max_size);
      fflush(stdout);
      last_time = clock();
    }
  }
}
