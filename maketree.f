      subroutine maketree(
     &   maxnode, maxpatch, maxedge,
     &   xyznode, ipatpnt, iedge,
     &   lmax, lsmax, lsmin, ncmax, kpm, nsm,
     &   lxyz, modes, rmin, sizel,
     &   igall, igcs, igrs, index, family, lrsmup, lrsmdown)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c   From the maxedge positions and the lmax boxes, construct the
c   grid-tree structure
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      implicit none

c.....Input Data

      INTEGER*8 maxnode, maxpatch, maxedge
      INTEGER*8 iedge(4,maxedge),    ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode)
      INTEGER*8 lmax, lsmax, lsmin, ncmax, kpm
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL    rmin(3), sizel(3,0:lmax)

c.....Output Data

      INTEGER*8 nsm
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax)

c.....Working Variables

      INTEGER*8 ipointer, ls
      INTEGER*8 l, n, i, ig(3), nozero, noempty, ipig, iptemp, igtemp, 
     &        lrow, ns, igl, iglp, ip1, ip2

c   for each basis, find which cube it belongs to
 
      l = lmax
      ipointer = 1
      ipig = 1
      igrs(1) = ipig
      do n = 1, maxedge
         index(n) = n
         ip1 = iedge(1, n)
         ip2 = iedge(2, n)
         do i= 1, 3
            ig(i) = 1+ int(abs(0.5*(xyznode(i,ip1) + xyznode(i,ip2))
     &                         - rmin(i))/sizel(i,l) - 1.e-4)

            if ((ig(i).lt.1) .or. (ig(i).gt.lxyz(i,l))) then
c               write(*,*)' *** overflow in index for ig in maketree ***'
c               write(*,*)'It may caused by rounding off error'

c               print*,ig(i),lxyz(i,l)

               if (ig(i).lt.1) then 
                    ig(i) = 1
               end if

               if (ig(i).gt.lxyz(i,l))then
                    ig(i) = lxyz(i,l)
               end if

c               print*,ig(i),lxyz(i,l)
            end if

         enddo
         family(n) = igl(lxyz(1,l),ig)
      enddo

c.....sorting family, family is a vector for family and iwork in the note

      call sort2(maxedge,family,index)

c.....find nonempty groups in the finest level

      nozero=1
      igcs(ipig)=family(1)
      igall(ipig)=ipointer
      do n=2,maxedge
        ipointer=ipointer+1
        if(family(n).ne.igcs(ipig)) then
          nozero=nozero+1
          ipig=ipig+1
          igcs(ipig)=family(n)
          igall(ipig)=ipointer
        endif
      enddo
      igrs(2)=ipig+1

c.....do all higher levels

      do l=lmax-1,0,-1
        lrow=lmax-l+1
        noempty=nozero
        iptemp=igrs(lrow-1)-1
        do n=1,noempty
          igtemp=igcs(iptemp+n)        
          index(ipointer+n)=iptemp+n
          family(n)=iglp(igtemp,lxyz(1,l+1),lxyz(1,l))
        enddo

c.....sorting family

        call sort2(noempty,family,index(ipointer+1))

c.....find nonempty groups in this level

        nozero=0
        do n=1,noempty
          ipointer=ipointer+1
          if(family(n).ne.igcs(ipig)) then
            nozero=nozero+1
            ipig=ipig+1
            igcs(ipig)=family(n)
            igall(ipig)=ipointer
          endif
        enddo
        igrs(lrow+1)=ipig+1
      enddo
      ipointer=ipointer+1
      ipig=ipig+1
      igall(ipig)=ipointer

c....find how many elements in csm

      ns=0
      if(lmax.gt.1) then
        lrsmup(lmax)=ns+1
        do l=lmax,2,-1
          lrow=lmax-l+1
          ls=modes(l)
          ns=ns+(4+2*ls*ls)*(igrs(lrow+1)-igrs(lrow))
          lrsmup(l-1)=ns+1
        enddo
        lrsmup(0)=ns+1
      endif
      if(lmax.gt.2) then
        ns=lrsmup(lmax-2)-1
        lrsmdown(lmax-1)=ns+1
        do l=lmax-1,2,-1
          lrow=lmax-l+1
          ls=modes(l)
          ns=ns+(4+2*ls*ls)*(igrs(lrow+1)-igrs(lrow))
          ns=max(ns,lrsmup(l-2)-1)
          lrsmdown(l-1)=ns+1
        enddo
        lrsmdown(0)=ns+1
      endif

      nsm = ns

      return
      end
