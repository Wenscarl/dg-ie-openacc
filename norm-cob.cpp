#include "norm-cob.hpp"
#include "svd.hpp"

#include <algorithm>
#include <iostream>
#include <cassert>
#include <fstream>
#include <numeric>


static NormalizationPc *g_ncob = 0;


extern "C" {
  void cgemv_(const char *TRANS, const Int *M, const Int *N, const complex<float> *ALPHA,
	      const complex<float> *A, const Int *LDA, const complex<float> *X,
	      const Int *INCX, const complex<float> *BETA, complex<float> *Y,
	      const  Int *INCY, const Int TRANS_len);
  void zgemv_(const char *TRANS, const Int *M, const Int *N, const complex<double> *ALPHA,
	      const complex<double> *A, const Int *LDA, const complex<double> *X,
	      const Int *INCX, const complex<double> *BETA, complex<double> *Y,
	      const  Int *INCY, const Int TRANS_len);

  void cgemm_(char *TRANSA, char *TRANSB, Int *M, Int *N, Int *K, complex<float> *ALPHA,
	      complex<float> *A, Int *LDA, complex<float> *B, Int *LDB,
	      complex<float> *BETA, complex<float> *C, Int *LDC, Int TRANSA_len,
	      Int TRANSB_len);
}



NormalizationPc::NormalizationPc
(const Int *igrs, const Int noself[][2], const Int *igall,
 const Int *igblkrs, const Int *igblkcs, const Int *igblk,
 const Complex_FMM *canear, const Int *index)
  : igrs(igrs), noself(noself), igall(igall), igblkrs(igblkrs), igblkcs(igblkcs),
    igblk(igblk), index(index)
{
  std::cout << "Begin NormalizationPc constructor" << std::endl;
  rawCobZ.resize(igrs[1]-1);
  rawCobDZ.resize(igrs[1]-1);
  vtr_index.resize(igrs[1]);
  vtr_index[0] = 0;
  Int tmp_N(0), tmp_dropSize(0);

  // loop over all groups
#pragma omp parallel for schedule(guided) reduction(+:tmp_N, tmp_dropSize)
  for (Int ipgp = igrs[0]; ipgp < igrs[1]; ++ipgp) {
    Int mself = noself[ipgp-1][0];
    Int indexm = igall[ipgp-1] - 1;

    for (Int ngnear = igblkrs[ipgp-1]; ngnear < igblkrs[ipgp]; ++ngnear) {
      Int jlo = igblkcs[ngnear-1];
      Int indexn = igall[jlo-1] - 1;
      Int ipnear = igblk[ngnear-1]-1;

      // skip off-diagonal blocks
      if (indexn != indexm) { continue; }


      std::vector<Complex_FMM> Z(mself*mself);
      std::vector<Real> SZ(mself);
      std::vector<Complex_FMM> *U  = &rawCobZ[ipgp-1].U;
      std::vector<Complex_FMM> *VT = &rawCobZ[ipgp-1].VT;

      for (Int i = 0; i < mself; ++i)
	for (Int j = 0; j < mself; ++j) {
	  Z.at(i+mself*j) = canear[ipnear+i*mself+j];
	}

      GetSvd(Z, *U, *VT, SZ, mself);

      
      for (Int i = 0; i < mself; ++i)
	for (Int j = 0; j < mself; ++j) {
	  (*U)[i*mself+j] /= SZ[i];
	}

      ////////// check if the diagonal block is correctly inverted
//       for (Int i = 0; i < mself; ++i)
// 	for (Int j = 0; j < mself; ++j)
// 	  Z.at(i+mself*j) = canear[ipnear+i*mself+j];

//       std::vector<Complex_FMM> tmp(mself*mself);
//       char TRANSA = 'C', TRANSB = 'N';
//       Complex_FMM ALPHA = 1.0, BETA = 0.0;
//       Int ONEi = 1;

//       cgemm_(&TRANSA, &TRANSB, &mself, &mself, &mself, &ALPHA, &((*U)[0]),
// 	     &mself, &Z[0], &mself, &BETA, &tmp[0], &mself, 1, 1);

//       TRANSA = 'C';
//       TRANSB = 'N';
//       cgemm_(&TRANSA, &TRANSB, &mself, &mself, &mself, &ALPHA, &((*VT)[0]),
// 	     &mself, &tmp[0], &mself, &BETA, &Z[0], &mself, 1, 1);

//       for (Int i = 0; i < mself; ++i)
// 	Z[(mself+1)*i] -= 1.0;

//       float fro_norm = 0.0;
//       for (Int i = 0; i < mself*mself; ++i)
// 	fro_norm += norm(Z[i]);
//       fro_norm = sqrt(fro_norm/mself);

//       if (fro_norm > 1.0e-12) {
// 	std::cout << "Relative error for block " << ipgp
// 		  << " is " << fro_norm << std::endl;
//       }
      //////////

      rawCobZ[ipgp-1].highDropSize = rawCobDZ[ipgp-1].lowDropSize
	= 0;

      rawCobDZ[ipgp-1].highDropSize = rawCobZ[ipgp-1].lowDropSize
	= 0; // GetDropSize(SZ);

      Unsigned dropped = rawCobZ[ipgp-1].lowDropSize + rawCobZ[ipgp-1].highDropSize;
      if (dropped > 0) {
	std::cout << std::endl << dropped << " dropped" << std::endl;
	std::cout << SZ[mself-3] << "  "
		  << SZ[mself-2] << "  "
		  << SZ[mself-1] << std::endl;
      }

      vtr_index[ipgp] = mself - dropped;

      tmp_N += mself;
      tmp_dropSize += dropped;

      break;
    }
  }
  std::cout << std::endl;

  N = tmp_N;
  dropSize = tmp_dropSize;
  std::partial_sum(vtr_index.begin(), vtr_index.end(), vtr_index.begin());

  std::cout << "Normalized system has " << N-dropSize << " DOFs."
	    << std::endl << std::endl;
}


void NormalizationPc::mult_V(const Complex_FMM *x, Int *x_size,
			     Complex_FMM *result, Int *result_size) const
{
  // sanity check
  assert( *x_size >= N-dropSize );
  assert( *result_size >= N );

  std::vector<Complex_FMM> y(N);

#pragma omp parallel for schedule(guided)
  for (Int ipgp = igrs[0]; ipgp < igrs[1]; ++ipgp) {
    Int mself = noself[ipgp-1][0];
    Int indexm = igall[ipgp-1] - 1;
    Int high = rawCobZ[ipgp-1].highDropSize;
    Int low  = rawCobZ[ipgp-1].lowDropSize;
    Int delta = mself - low - high;

    std::vector<Complex_FMM> part(mself, 0.0);
    Unsigned idx = vtr_index[ipgp-1];
    std::copy(&x[idx], &x[idx] + delta, &part[high]);

    char TRANS = 'C';
    Complex_FMM ONEz = 1.0, ZERO = 0.0;

    Int ONEi = 1;
    cgemv_(&TRANS, &mself, &mself, &ONEz, &(rawCobZ[ipgp-1].VT[0]), &mself,
	   &part[0], &ONEi, &ZERO, &y[indexm], &ONEi, 1);
  }

  for (Unsigned i = 0; i < N; ++i)
    result[index[i]-1] = y[i];

}


void NormalizationPc::mult_VH(const Complex_FMM *x, Int *x_size,
			      Complex_FMM *result, Int *result_size) const
{
  std::vector<Complex_FMM> y(N);

  for (Unsigned i = 0; i < N; ++i)
    y[i] = x[index[i]-1];

#pragma omp parallel for schedule(guided)
  for (Int ipgp = igrs[0]; ipgp < igrs[1]; ++ipgp) {
    Int mself = noself[ipgp-1][0];
    Int indexm = igall[ipgp-1] - 1;
    Int high = rawCobZ[ipgp-1].highDropSize;
    Int low  = rawCobZ[ipgp-1].lowDropSize;
    Int delta = mself - low - high;

    std::vector<Complex_FMM> part(mself);

    char TRANS = 'N';
    Complex_FMM ONEz = 1.0, ZERO = 0.0;
    Int ONEi = 1;
    cgemv_(&TRANS, &mself, &mself, &ONEz, &(rawCobZ[ipgp-1].VT[0]), &mself,
	   &y[indexm], &ONEi, &ZERO, &part[0], &ONEi, 1);

    Unsigned idx = vtr_index[ipgp-1];
    std::copy(&part[high], &part[high] + delta, &result[idx]);
  }
}


void NormalizationPc::mult_UH(const Complex_FMM *x, Int *x_size,
			      Complex_FMM *result, Int *result_size) const
{
  std::vector<Complex_FMM> y(N);

  for (Unsigned i = 0; i < N; ++i)
    y[i] = x[index[i]-1];


#pragma omp parallel for schedule(guided)
  for (Int ipgp = igrs[0]; ipgp < igrs[1]; ++ipgp) {
    Int mself = noself[ipgp-1][0];
    Int indexm = igall[ipgp-1] - 1;
    Int high = rawCobZ[ipgp-1].highDropSize;
    Int low  = rawCobZ[ipgp-1].lowDropSize;
    Int delta = mself - low - high;

    std::vector<Complex_FMM> part(mself);

    char TRANS = 'C';
    Complex_FMM ONEz = 1.0, ZERO = 0.0;
    Int ONEi = 1;
    cgemv_(&TRANS, &mself, &mself, &ONEz, &(rawCobZ[ipgp-1].U[0]), &mself,
	   &y[indexm], &ONEi, &ZERO, &part[0], &ONEi, 1);

    Unsigned idx = vtr_index[ipgp-1];
    std::copy(&part[high], &part[high] + delta, &result[idx]);
  }
}


void NormalizationPc::mult_U(const Complex_FMM *x, Int *x_size,
			     Complex_FMM *result, Int *result_size) const
{
  // sanity check
  assert( *x_size >= N-dropSize );
  assert( *result_size >= N );

  std::vector<Complex_FMM> y(N);

#pragma omp parallel for schedule(guided)
  for (Int ipgp = igrs[0]; ipgp < igrs[1]; ++ipgp) {
    Int mself = noself[ipgp-1][0];
    Int indexm = igall[ipgp-1] - 1;
    Int high = rawCobZ[ipgp-1].highDropSize;
    Int low  = rawCobZ[ipgp-1].lowDropSize;
    Int delta = mself - low - high;

    std::vector<Complex_FMM> part(mself, 0.0);
    Unsigned idx = vtr_index[ipgp-1];
    std::copy(&x[idx], &x[idx] + delta, &part[high]);

    char TRANS = 'N';
    Complex_FMM ONEz = 1.0, ZERO = 0;
    Int ONEi = 1;
    cgemv_(&TRANS, &mself, &mself, &ONEz, &(rawCobZ[ipgp-1].U[0]), &mself,
	   &part[0], &ONEi, &ZERO, &y[indexm], &ONEi, 1);
  }

  for (Unsigned i = 0; i < N; ++i)
    result[index[i]-1] = y[i];

}




Unsigned NormalizationPc::GetDropSize
(const std::vector<Real> &S) const
{
  Unsigned i;
  for (i = 0; i < S.size(); ++i)
    if (S[i] < 1.0e-6) { break; }

  return S.size()-i;
}



void NormalizationPc::GetSvd
(std::vector<Complex_FMM> &A, std::vector<Complex_FMM> &U,
 std::vector<Complex_FMM> &VT, std::vector<Real> &S, const Int mself) const
{
  U.resize(mself*mself);
  VT.resize(mself*mself);
  S.resize(mself);

  Int INFO=0;
  gesvd('A', 'A', mself, mself, &A[0],
	mself, &S[0], &U[0], mself,
	&VT[0], mself, INFO);

  if (INFO != 0) {
    std::cerr << "Error " << INFO << " from gesvd" << std::endl;
    exit(1);
  }
}




void NormalizationPc::writeUV() const
{
  std::ofstream fuz("uz.bin", std::ios::trunc | std::ios::binary);
  std::ofstream fvz("vz.bin", std::ios::trunc | std::ios::binary);

  std::vector<Int> rows, cols;

  for (Int blockNum = 0; blockNum < rawCobZ.size(); ++blockNum) {
    Int mself = noself[blockNum][0];
    Int indexm = igall[blockNum]-1;

    for (Int i = 0; i < mself; ++i)
      for (Int j = 0; j < mself; ++j) {
	rows.push_back(j+indexm);
	cols.push_back(i+indexm);
      }
  }

  fuz.write(reinterpret_cast<const char*>(&N), sizeof(N));
  fuz.write(reinterpret_cast<const char*>(&N), sizeof(N));
  fvz.write(reinterpret_cast<const char*>(&N), sizeof(N));
  fvz.write(reinterpret_cast<const char*>(&N), sizeof(N));

  Int NNZ = rows.size();
  fuz.write(reinterpret_cast<const char*>(&NNZ), sizeof(NNZ));
  fvz.write(reinterpret_cast<const char*>(&NNZ), sizeof(NNZ));

  fuz.write(reinterpret_cast<char*>(&rows[0]), NNZ*sizeof(rows[0]));
  fuz.write(reinterpret_cast<char*>(&cols[0]), NNZ*sizeof(cols[0]));
  fvz.write(reinterpret_cast<char*>(&rows[0]), NNZ*sizeof(rows[0]));
  fvz.write(reinterpret_cast<char*>(&cols[0]), NNZ*sizeof(cols[0]));

  for (Int blockNum = 0; blockNum < rawCobZ.size(); ++blockNum) {
    Int mself = noself[blockNum][0];
    size_t TT = sizeof(Complex_FMM);

    fuz.write(reinterpret_cast<const char*>(&rawCobZ[blockNum].U[0]), mself*mself*TT);
    fvz.write(reinterpret_cast<const char*>(&rawCobZ[blockNum].VT[0]), mself*mself*TT);

    NNZ -= mself*mself;
  }

  assert(NNZ == 0);

  fuz.close();
  fvz.close();
}




void initialize_ncob_(Int *igrs, Int noself[][2], Int *igall, Int *igblkrs,
			Int *igblkcs, Int *igblk, std::complex<float> *canear,
			Int *index)
{
  static NormalizationPc ncob(igrs, noself, igall, igblkrs,
			      igblkcs, igblk, canear, index);

  g_ncob = &ncob;
}

void mult_u_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size)
{
  g_ncob->mult_U(x, x_size, result, result_size);
}

void mult_uh_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size)
{
  g_ncob->mult_UH(x, x_size, result, result_size);
}

void mult_v_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size)
{
  g_ncob->mult_V(x, x_size, result, result_size);
}

void mult_vh_(Complex_FMM *x, Int *x_size, Complex_FMM *result, Int *result_size)
{
  g_ncob->mult_VH(x, x_size, result, result_size);
}
