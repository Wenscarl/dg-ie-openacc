      function CFUNEXS_new
     &( rhoxn, bn, rho0, ds, ri1, ri1p, ri3, ri3p, ck2d2 )

      IMPLICIT NONE

c.....Input Data

       REAL rhoxn(3), bn(3), rho0(3), ds, 
     &      ri1, ri1p(3), ri3, ri3p(3)
       COMPLEX ck2d2

c.....Output 

      COMPLEX CFUNEXS_new

c.....Working Variables

      COMPLEX cmp1, cmp2
      INTEGER*8 i 
c....................................................................


      cfunexs=(0.0, 0.0)

      do i=1,3
         cmp1=rho0(i)*ri1+ri1p(i)
         cmp2=(ri3*bn(i)-ri3p(i))/ck2d2+ds*ri1*bn(i)-ri1p(i)
         cfunexs=cfunexs+rhoxn(i)*(cmp1-cmp2)
      enddo

      RETURN
      END

