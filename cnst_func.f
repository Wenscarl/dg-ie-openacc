      subroutine CNST_FUNC
     &( rr, ck, ci,
     &  fld2src, fld2src2, cagu, cagu2, cx, cg0)

      IMPLICIT NONE

c.....Input Data 

       REAL    rr, dotmul
       COMPLEX ck, ci

c.....Output Data

       REAL    fld2src, fld2src2
       COMPLEX cagu, cagu2, cx, cg0

c.............................................................

       fld2src2 = dotmul(rr, rr)
       fld2src  = sqrt(fld2src2)

       cagu  = fld2src*ck
       cagu2 = cagu*cagu

       cx  = ci*cagu-1.0
       cg0 = exp((ci*cagu))

       RETURN
       END
