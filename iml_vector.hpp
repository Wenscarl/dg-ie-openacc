#ifndef IML_VECTOR_HPP
#define IML_VECTOR_HPP

#include <complex>
#include <vector>
#include <algorithm>
#include <functional>

class iml_vector
{
public:
  typedef std::complex<float> scaler_type;

  friend scaler_type dot(const iml_vector &, const iml_vector &);
  friend iml_vector operator*(const scaler_type &, const iml_vector &);
  friend float norm_inf(const iml_vector &);

  iml_vector()
  {
    dim = 0;
  }

  iml_vector(unsigned long long int n)
  {
    dim = n;
    data.resize(n);
  }

  ~iml_vector()
  {
  }

  iml_vector(const iml_vector &x)
  {
    dim = x.dim;
    data = x.data;
  }

  iml_vector &operator=(const iml_vector &x)
  {
    if (&x == this)
      return *this;

    data = x.data;
    dim = x.dim;
    return *this;
  }

  iml_vector &operator=(const scaler_type &x)
  {
    std::fill(data.begin(), data.end(), x);
    return *this;
  }

  iml_vector operator+(const iml_vector &x) const
  {
    iml_vector result(std::min(dim, x.dim));

#pragma omp parallel for schedule(static)
    for (unsigned long long int i = 0; i < result.dim; ++i)
      result.data[i] = data[i] + x.data[i];

    return result;
  }

  iml_vector operator-(const iml_vector &x) const
  {
    iml_vector result(std::min(dim, x.dim));

#pragma omp parallel for schedule(static)
    for (unsigned long long int i = 0; i < result.dim; ++i)
      result.data[i] = data[i] - x.data[i];

    return result;
  }

  const scaler_type &operator()(long long int n) const
  {
    return data[n];
  }

  scaler_type &operator()(long long int n)
  {
    return data[n];
  }

  void operator+=(const iml_vector &x)
  {
    if (dim != x.dim) return;
#pragma omp parallel for schedule(static)
    for (unsigned i = 0; i < dim; ++i)
      data[i] += x.data[i];
  }

  void operator-=(const iml_vector &x)
  {
    if (dim != x.dim) return;
#pragma omp parallel for schedule(static)
    for (unsigned i = 0; i < dim; ++i)
      data[i] -= x.data[i];
  }

  void operator/=(const scaler_type x)
  {
    scaler_type x_inv = scaler_type(1.0) / x;
#pragma omp parallel for schedule(static)
    for (unsigned i = 0; i < dim; ++i)
      data[i] *= x_inv;
  }

  unsigned long long int GetDim() const
  {
    return dim;
  }

private:
  std::vector<scaler_type> data;
  unsigned long long int dim;
};

iml_vector::scaler_type dot(const iml_vector &x, const iml_vector &y);
float norm(const iml_vector &x);
float norm_inf(const iml_vector &x);
iml_vector operator*(const std::complex<double> &C, const iml_vector &v);

#endif
