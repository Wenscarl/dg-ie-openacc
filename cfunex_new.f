      function CFUNEX_new
     &( vrhoxn, vrhosrc, area_fld,area_src,
     &  edge_flg,edge_src,nearpat,  
     &  fld2src,rk,r2k2, cx, ejkr, ck, ci ,
     &  cfunex_new2)

      IMPLICIT NONE

c.....Input Data

       REAL vrhoxn(3), vr(3), vrhosrc(3)
       INTEGER*8 nearpat
       REAL fld2src,area_fld,area_src
       COMPLEX rk, r2k2, cx, ejkr, ck, ci
       real edge_flg,edge_src
c.....Output Data

       COMPLEX CFUNEX_new,cfunex_new2

c.....Working Variables

       REAL    dotmul
       COMPLEX cmp1, cmp2,green
       
       

        if(fld2src.eq.0.0) then
          write(*,*)'!!!'
        endif
        green= ejkr/fld2src
         
        cfunex_new = green*(dotmul(vrhoxn, vrhosrc))
        cfunex_new2= green*(-1/(abs(ck*ck)))

      RETURN
      END       
