      MODULE mlfma_param
 
      INTEGER*8 maxnode, maxpatch, maxedge, maxrule, maxgrid
      INTEGER*8 irulef_near, irules_near
      REAL   rk0, rk2d4, wl0, eta0, distmin, rnear2, rmin(3)
      COMPLEX cnste, cnsth, ci

      END MODULE

!      common/mlfma_param/maxnode, maxpatch, maxedge, maxrule, maxgrid,
!     &                   irulef_near, irules_near,
!     &                   rk0, rk2d4, wl0,
!     &                   eta0, distmin, rnear2, rmin,
!     &                   cnste, cnsth, ci
