#ifndef _arrayptrs_H_
#define _arrayptrs_H_

  typedef float Real;
  typedef Real Complex[2];
  typedef long long int Integer;
  #ifndef _buildarrays_C_
    #define EXTERN extern
    #define INITLONG
    #define INITFLOAT
    #define INITCOMPLEX
    #define INITCHAR
  #else
    #define EXTERN
    #define INITCHAR =(char *)NULL
    #define INITLONG = (Integer *)NULL
    #define INITFLOAT = (Real *)NULL
    #define INITCOMPLEX = (Complex *)NULL
    #define EXTERN2 extern
    #define INITCHAR2
    #define INITLONG2
    #define INITFLOAT2
    #define INITCOMPLEX2
  #endif


/**********************
 dimen_mom2.inc
*/

/* maxedge */
EXTERN Integer *iedgep INITLONG;
EXTERN Real *edgep INITFLOAT;

//EXTERN Complex *Hinc INITCOMPLEX;
//EXTERN Complex *Einc INITCOMPLEX;
/* maxpatch */
EXTERN Integer *ipatpntp INITLONG;
EXTERN Real *xyzctrp INITFLOAT;
EXTERN Real *paerap INITFLOAT;
EXTERN Real *xyznormp INITFLOAT;

/* maxnode */
EXTERN Real *xyznodep INITFLOAT;

/* maxrule */
EXTERN Integer *ngridp INITLONG;

/* maxgrid,maxrule */
EXTERN Real *vt1p INITFLOAT;
EXTERN Real *vt2p INITFLOAT;
EXTERN Real *vt3p INITFLOAT;
EXTERN Real *wtp  INITFLOAT;



/**********************
 dimen_fmm2.inc
*/

/* lmax */
EXTERN Integer *igrsp INITLONG;
EXTERN Integer *lxyzp  INITLONG;
EXTERN Integer *modesp INITLONG;
EXTERN Real *sizelp INITFLOAT;
EXTERN Integer *lrsmupp INITLONG;
EXTERN Integer *lrsmdownp INITLONG;
EXTERN Integer *lrstlp INITLONG;
EXTERN Integer *kpstartp INITLONG;

/* ncmax */
EXTERN Integer *igallp INITLONG;
EXTERN Integer *igcsp INITLONG;
EXTERN Integer *noselfp INITLONG;
EXTERN Integer *igsndrsp INITLONG;


/* ncmax, maxedge */
EXTERN Integer *familyp INITLONG;
EXTERN Integer *indexp INITLONG;

/* kpm */
EXTERN Complex *cvlrp  INITCOMPLEX;
EXTERN Complex *cgmp  INITCOMPLEX;

/* nkpm */
EXTERN Complex *c_shiftp INITCOMPLEX;
EXTERN Integer *irsap INITLONG;

/* ntlm */
EXTERN Complex *ctlp INITCOMPLEX;

/* niplm */
EXTERN Real *arrayp  INITFLOAT;
EXTERN Integer *icsap INITLONG;

/* nsm */
EXTERN Complex *csmp INITCOMPLEX;

/* ifar */
EXTERN Integer *indextlp INITLONG;

/* ngsndm */
EXTERN Integer *igsndcsp INITLONG;

/**********************
 dimen_coat.inc
*/

/* maxcoat */
EXTERN char *icoat_readinp INITCHAR;
EXTERN char *icoat_usedp INITCHAR;
EXTERN char *iboundaryp INITCHAR;
EXTERN char *ccoat_impedancep INITCHAR;

/* fem */
EXTERN char *epmp INITCHAR;
EXTERN char *mump INITCHAR;
EXTERN char *ipinp INITCHAR;
EXTERN char *ipoutp INITCHAR;
EXTERN char *xp INITCHAR;
EXTERN char *yp INITCHAR;
EXTERN char *zp INITCHAR;
EXTERN char *lvp INITCHAR;
EXTERN char *lep INITCHAR;
EXTERN char *lcp INITCHAR;
EXTERN char *lpp INITCHAR;
EXTERN char *volmatp INITCHAR;
EXTERN char *ipvolp INITCHAR;
EXTERN char *surmatp INITCHAR;
EXTERN char *ipsurp INITCHAR;
EXTERN char *diagp INITCHAR;

EXTERN Complex *crhsp INITCOMPLEX;

  //FOR PRECONDITIONER USE
EXTERN Complex* pc_cbdp;
EXTERN Integer* pc_bdpidx;
EXTERN Integer* pc_bdpipiv;
EXTERN Complex* pc_scratch;

#endif
