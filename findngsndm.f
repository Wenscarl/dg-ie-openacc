      subroutine findngsndm
     &( lmax, ncmax, lxyz, ifar, dfar,
     &  sizel, igcs, igrs, ngsndm)

cccccccccccccccccccccccccccccccccccccccccccccccccccccc
c  find total number of second neighbor groups
cccccccccccccccccccccccccccccccccccccccccccccccccccccc

      implicit none

c.....Input Data

      INTEGER*8 lmax, ncmax
      INTEGER*8 lxyz(3,0:lmax), ifar
       REAL    dfar, sizel(3,0:lmax)
      INTEGER*8 igcs(ncmax), igrs(lmax+2)

c.....Output Data

      INTEGER*8 ngsndm

c.....Working variables

      INTEGER*8 ngsnd
      INTEGER*8 l, lrow, ipointer, noempty, ipgp, ig, ig3(3), igp3(3), 
     &        jlo, ix, iy, iz, i, ign, ign3(3), ignp3(3),
     &        ndp3(3), ndc3(3), igl

c.....Find total number of second neighbor groups

      ngsnd = 0
      do l = lmax, 2, -1
         lrow = lmax - l + 1
         ipointer = igrs(lrow)
         noempty = igrs(lrow+1) - ipointer
         do ipgp = igrs(lrow), igrs(lrow+1)-1
            ig = igcs(ipgp)
            call igxyz(ig,lxyz(1,l),ig3)
            call ighigh(ig3, igp3)
            jlo=1
            do ix=max(1,ig3(1)-ifar*2-1),min(lxyz(1,l),ig3(1)+ifar*2+1)
               ign3(1)=ix
            do iy=max(1,ig3(2)-ifar*2-1),min(lxyz(2,l),ig3(2)+ifar*2+1)
               ign3(2)=iy
            do iz=max(1,ig3(3)-ifar*2-1),min(lxyz(3,l),ig3(3)+ifar*2+1)
               ign3(3)=iz
               ign = igl(lxyz(1,l),ign3)
               call ighigh(ign3,ignp3)
               do i = 1, 3
                  ndc3(i) = abs( ig3(i) -  ign3(i))
                  ndp3(i) = abs(igp3(i) - ignp3(i))
               enddo

c....Does this belong to parent's near neighbors?
               if((ndp3(1).le.ifar) .and. (ndp3(2).le.ifar) .and.
     &            (ndp3(3).le.ifar) .and.
     &            (sqrt(real(ndp3(1)**2+ndp3(2)**2+ndp3(3)**2))
     &            .lt.dfar)) then

c....Is this not belong to near neighbors?
                  if((ndc3(1).gt.ifar) .or. (ndc3(2).gt.ifar) .or.
     &               (ndc3(3).gt.ifar) .or.
     &               (sqrt(real(ndc3(1)**2+ndc3(2)**2+ndc3(3)**2))
     &               .ge.dfar)) then

c....Is this box empty or not?
                     call huntint(igcs(ipointer),noempty,ign,jlo)
                     if((jlo.gt.0)) then
                        ngsnd = ngsnd + 1
                     endif
                  endif
               endif
            enddo
            enddo
            enddo
         enddo
      enddo

      ngsndm = ngsnd

      return
      end
