#ifndef GEOMETRY_H
#define GEOMETRY_H
#include "global.h"
#include "float_vtr.h"
#include "float_cvtr.h"
#include <xmesh_trimesh_mesh.h>
#include <string>
#include <map>
#include "utility.h"
// using namespace xMesh;
class triSurf : public xMesh::tri_mesh
{
	private:
		bool isFEM;
		bool isSO;
		std::map<std::string,cVtr*> fieldMap;
	public:
		triSurf();
		~triSurf();
		
		Integer readField(char*,bool);
		
		void generateFirstOrderTriArray(Integer& nNode, Integer& nEdge, Integer& nTri, Real** inodep, Integer** iedgep, Integer** itrip);
		void generateFirstOrderField(char*);
		
		void updateFromFirstOrderField(char*,cVtr* field, bool Hardcopy=false); //import the field
		void writeField(char*, bool binary); 
		
// 		void setName(char* fname){name_= std::string(fname);}
// 		const char* getName()const{return name_.c_str();}
		void setFem(bool is){isFEM=is;}
		bool getFem()const{return isFEM;}
		void setSO(bool is){isSO=is;}
		bool getSO()const{return isSO;}
		void exportXYZNODE(Real* )const;
		void exportEDGE(Integer* ,Integer offsetNode=0,Integer offsetTri=0)const ;
		void exportTRI(Integer* ,Integer offsetNode=0)const ;
		
		
		Integer getnNodeFO()const;
		Integer getnEdgeFO()const;
		Integer getnTriFO()const;
		Integer getFieldFO(Complex* dst, const char* ext) const;
		
		Integer writeFieldFO(Complex* rhs, const char* ext, Integer _nNode, Integer _nEdge, Integer _nTri, Real* _xyznodep, Integer* _iedgep, Integer* _ipat,Real* areap,Real* edgep,
				     Complex*,bool*,
				     Integer offsetNode, Integer offsetEdge, Integer offsetTri,bool* matchedMark,
				     bool isRcv=false,bool append=false) const;
		
		void exportFlag(Integer*)const;
		
};
void exportTriFO(const char* ,Int nNode,Int nTri, Real* xyz, Integer* tri);
void directWriteFieldFO(const char* fname, Complex* field,Integer nTri);
void MLFMARegularization(Integer _maxnode,Integer _maxedge, Integer _maxpatch,Integer* EDGE,Integer* TRI,Integer,Integer,Integer);
#endif
