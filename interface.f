        subroutine mlfma_wrapper
     &  ( memory_tot, lmax, 
     &  iedge, edge, ipatpnt, xyzctr, 
     &  paera, xyznorm,xyznode,
     &  ngrid, vt1, vt2, vt3, wt, igrs, lxyz,
     &  modes, sizel,lrsmup, lrsmdown, lrstl, 
     &  family, kpstart,frequency,
     &	nNode,nTri,nEdge)
        use mlfma_input
        use mlfma_const
        use mlfma_param  
      implicit none
      INTEGER*8 lmax
      REAL   memory_tot
      REAL   xyznode(3,maxnode)
      INTEGER*8 ipatpnt(3,maxpatch), iedge(4,maxpatch)
      REAL xyzctr(3,maxpatch), xyznorm(3,maxpatch)
      REAL edge(maxedge),      paera(maxpatch)
      REAL vt1(maxgrid, maxrule), vt2(maxgrid, maxrule)
      REAL vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)
c      INTEGER*8 lsmax, lsmin, kpm, kp0
      INTEGER*8 igrs(lmax+2)
      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 family(maxedge)
      INTEGER*8 MVmodelong
      INTEGER*8 direct
      REAL frequency
      INTEGER*8 nNode,nTri,nEdge

      ipol=1
      imono=1
      thetai1=0.0
      thetai2=0.0
      nthetai=1
      phii1=0.0
      phii2=0.0
      nphii=1
      thetas1=0.0
      thetas2=0.0
      nthetas=1
      phis1=0.0
      phis2=0.0
      nphis=1
      alpha=0.5
      icgbcg=1
      itmax=300
      epscg=0.001
      match=1
      irule_src=1
      irule_fld=1

      mvmode=MVmodelong
      freq=frequency
      wl0=0.3/freq
      maxrule  = 11
      maxgrid  = 64
      maxnode =nNode
      maxpatch =nTri
      maxedge=nEdge

      call main_box( memory_tot, lmax,iedge, edge, ipatpnt, xyzctr, 
     &  paera, xyznorm,xyznode,ngrid, vt1, vt2, vt3, wt, igrs, lxyz,
     &  modes, sizel,lrsmup, lrsmdown, lrstl,family, kpstart)
      return
      end
