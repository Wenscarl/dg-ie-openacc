      subroutine main_cal(memory_tot,lmax, ncmax, lsmax, lsmin, 
     &                    nearm, kp0, kpm, nkpm, ngsndm, 
     &                    niplm, nsm, ntlm, ngnearm, mfinestm, 
     &                    nanglem,iedge, edge, ipatpnt, xyzctr, 
     &                    paera, xyznorm,xyznode,ngrid, vt1, 
     &                    vt2, vt3, wt, lxyz, modes, 
     &                    sizel,igrs, igcs, igall, family, 
     &                    index, lrsmup, lrsmdown, lrstl, cvlr, 
     &                    cgm, dirk,irsa, xgl,wgl, 
     &                    ctl, array,icsa,csm,indextl, 
     &                    kpstart,noself, ipvt, cbm, 
     &                    cwork, cvf, cvs, canear, igblkrs, 
     &                    igblkcs, igblk, cfinest,  c_shift, igsndcs, 
     &                    igsndrs,cs11old, cs21old, bcs11old, bcs21old,
!     &                    current,
     &                    crhs,contourMap)

        use mlfma_input
        use mlfma_const
        use mlfma_param

      implicit none

!      nclude 'mlfma_input.inc'
!      nclude 'mlfma_const.inc'
!      nclude 'mlfma_param.inc'

c.....Input Data

       REAL   memory_tot
      INTEGER*8 lmax, lsmax, lsmin, ncmax, nearm, kp0, kpm,
     &        nkpm, ngsndm, niplm, nsm, ntlm, ngnearm, mfinestm, nanglem

      INTEGER*8 iedge(4,maxedge), ipatpnt(3,maxpatch)
       REAL   xyznode(3,maxnode), edge(maxedge),  xyzctr(3, maxpatch), 
     &        paera(maxpatch), xyznorm(3, maxpatch)
       REAL   vt1(maxgrid, maxrule), vt2(maxgrid, maxrule),
     &        vt3(maxgrid, maxrule), wt(maxgrid, maxrule)
      INTEGER*8 ngrid(maxrule)

      INTEGER*8 lxyz(3,0:lmax), modes(lmax)
       REAL   sizel(3,0:lmax)
      INTEGER*8 igall(ncmax+1), igcs(ncmax), igrs(lmax+2)
      INTEGER*8 index(ncmax+maxedge), family(maxedge)
      INTEGER*8 lrsmup(0:lmax), lrsmdown(0:lmax), lrstl(lmax+1)
      COMPLEX cvlr(2, kpm), cgm(2, kpm)
       REAL   dirk(3,3,nkpm), xgl(lsmax,lmax), wgl(lsmax,lmax)
      INTEGER*8 irsa(nkpm+1), icsa(niplm)
       REAL   array(niplm)
      COMPLEX ctl(ntlm), csm(2,nsm)
      INTEGER*8 igsndcs(ngsndm), igsndrs(ncmax)      
      COMPLEX c_shift(nkpm,2,2,2)
      INTEGER*8 indextl(-(2*ifar+1):(2*ifar+1),-(2*ifar+1):(2*ifar+1),
     &                -(2*ifar+1):(2*ifar+1),1:lmax)
      INTEGER*8 kpstart(lmax)
      INTEGER*8 noself(2,ncmax+1), ipvt(maxedge)    
      COMPLEX canear(nearm*maxedge), cfinest(mfinestm)
      INTEGER*8 igblkrs(ncmax+1), igblkcs(ngnearm+1),
     &        igblk(ngnearm+1)
      INTEGER*8 contourMap(maxedge)
      COMPLEX crhs(maxedge), cbm(maxedge)
      COMPLEX cwork(maxedge,8)
      COMPLEX cvf(2,kp0,maxedge), cvs(2,kp0,maxedge)

       REAL   bcs11old(nanglem), bcs21old(nanglem)
      COMPLEX cs11old(nanglem),  cs21old(nanglem)

      INTEGER*8 i, ii,ier, nun, kk, ipolold, inc_count, iall_count, neqs

      INTEGER*8  ichrcs
      INTEGER*8 npol, ithe, iphi, l, k, ipl

      INTEGER*8 ithetai, iphii, ithetas, iphis

       REAL   dthetai, dphii, dthetas, dphis, 
     &        thetai,  phii,  thetas,  phis,
     &        rcstheta, rcsphi, 
     &        dbtheta,  dbphi

       REAL   fldr, fldi, amp, degph(2), dbamp(2)
      COMPLEX cfld(2)

      COMPLEX czero, cetheta, cephi, cfield(2)

       REAL   rcs_4pi
       REAL   bcs11, bcs21, bcs12, bcs22

      COMPLEX cs11,  cs21,  cs12,  cs22

      REAL          time_tot
      CHARACTER*256 string_rept

       REAL   ck(2)
       REAL   epsr(2), epsi(2), amur(2), amui(2)
       double precision RHSNORM

      CHARACTER*256 prjName, fileName
      REAL theta, phi, EincMag, EincPhs, polAng
      INTEGER*8 npts, id, outCurJ

      REAL tau, zeta
      REAL cpolUnit(2), xpolUnit(2)  
      REAL cpolr, cpoli, xpolr, xpoli      
       
!       complex current(3, maxpatch)
!      COMPLEX  Einc(9,maxpatch),Hinc(9,maxpatch)
!       COMPLEX crhs(maxedge)
        do i=1,2
           epsr(i)=1.0
           epsi(i)=0.0
           amur(i)=1.0
           amui(i)=0.0
	enddo
      time_tot=0.0        
      string_rept='Before filling matrices'
      call time2screen(time_tot, memory_tot, string_rept)
      
      czero = 0.0

 	do ii=1,2
	   ck(ii)=rk0*sqrt(epsr(ii)*amur(ii))!*(1.0,0.01)
	enddo

      if(mvmode.eq.0)then

      if(lmax.gt.1) then

      call filltl(lmax, lsmax, lsmin, ncmax, kpm, ntlm, 
     &               niplm, nkpm,lxyz, modes, sizel,
     &               ctl(1), lrstl, indextl,xgl, wgl, dirk, 
     &               kpstart, ck(1), ci, cnste, c_shift(1,1,1,1), 
     &               icsa, irsa, array)

!      call writetl(lmax, lsmax, lsmin, ncmax, kpm, ntlm, 
!     &               niplm, nkpm,lxyz, modes, sizel,
!     &               ctl(1), lrstl, indextl,xgl, wgl, dirk, 
!     &               kpstart, ck(1), ci, cnste, c_shift(1,1,1,1), 
!     &               icsa, irsa, array)

      string_rept='After filling tl'

      call time2screen(time_tot, memory_tot, string_rept)


      call findsndgrp(lmax, ncmax, lxyz, ifar, dfar,
     &                sizel, igcs, igrs, 
     &                ngsndm, igsndcs, igsndrs)

!      call writesndgrp(lmax, ncmax, lxyz, ifar, dfar,
!     &                sizel, igcs, igrs, 
!     &                ngsndm, igsndcs, igsndrs)

      call fillvfs(cvf, cvs, 
     &             xyznode,ipatpnt,iedge,  xyzctr, xyznorm, edge,paera, 
     &             ngrid, vt1, vt2,vt3, wt,
     &             lmax, lsmax, lsmin, ncmax, kp0,
     &             lxyz, modes, sizel,
     &             igall, igcs, igrs, index, family,
     &             xgl(1,lmax), wgl(1,lmax), 
     &             dirk(1,1,kpstart(lmax)+1))

!      call writevfs(cvf, cvs, 
!     &             xyznode,ipatpnt,iedge,  xyzctr, xyznorm, edge,paera, 
!     &             ngrid, vt1, vt2,vt3, wt,
!     &             lmax, lsmax, lsmin, ncmax, kp0,
!     &             lxyz, modes, sizel,
!     &             igall, igcs, igrs, index, family,
!     &             xgl(1,lmax), wgl(1,lmax), 
!     &             dirk(1,1,kpstart(lmax)+1))

      endif
      string_rept='After filling vfs'
      call time2screen(time_tot, memory_tot, string_rept)

      call fillamn(xyznode,ipatpnt,iedge,
     &             xyzctr, xyznorm, edge,paera,  
     &             ngrid, vt1, vt2, vt3, wt,  
     &             lmax, ncmax, nearm,
     &             lxyz,
     &             igall, igcs, igrs, index, family, noself,
     &             canear, ipvt,
     &             ngnearm, igblkrs, igblkcs, igblk,
     &             epsr,epsi,amur,amui,contourMap)
!        pause
!      call writeamn(xyznode,ipatpnt,iedge,
!     &             xyzctr, xyznorm, edge,paera,  
!     &             ngrid, vt1, vt2, vt3, wt,  
!     &             lmax, ncmax, nearm,
!     &             lxyz,
!     &             igall, igcs, igrs, index, family, noself,
!     &             canear, ipvt,
!     &             ngnearm, igblkrs, igblkcs, igblk,
!     &             epsr,epsi,amur,amui)

      string_rept='After filling Amn'
      call time2screen(time_tot, memory_tot, string_rept)

      else

!       call readtl(lmax, lsmax, lsmin, ncmax, kpm, ntlm, 
!      &               niplm, nkpm,lxyz, modes, sizel,
!      &               ctl(1), lrstl, indextl,xgl, wgl, dirk, 
!      &               kpstart, ck(1), ci, cnste, c_shift(1,1,1,1), 
!      &               icsa, irsa, array)

      call filltl(lmax, lsmax, lsmin, ncmax, kpm, ntlm, 
     &               niplm, nkpm,lxyz, modes, sizel,
     &               ctl(1), lrstl, indextl,xgl, wgl, dirk, 
     &               kpstart, ck(1), ci, cnste, c_shift(1,1,1,1), 
     &               icsa, irsa, array)

      string_rept='After filling tl'

      call time2screen(time_tot, memory_tot, string_rept)

!      call readsndgrp(lmax, ncmax, lxyz, ifar, dfar,
!     &                sizel, igcs, igrs, 
!     &                ngsndm, igsndcs, igsndrs)

!      call readvfs(cvf, cvs, 
!     &             xyznode,ipatpnt,iedge,  xyzctr, xyznorm, edge,paera, 
!     &             ngrid, vt1, vt2,vt3, wt,
!     &             lmax, lsmax, lsmin, ncmax, kp0,
!     &             lxyz, modes, sizel,
!     &             igall, igcs, igrs, index, family,
!     &             xgl(1,lmax), wgl(1,lmax), 
!     &             dirk(1,1,kpstart(lmax)+1))
!      string_rept='After filling vfs'
!      call time2screen(time_tot, memory_tot, string_rept)

!      call readamn(xyznode,ipatpnt,iedge,
!     &             xyzctr, xyznorm, edge,paera,  
!     &             ngrid, vt1, vt2, vt3, wt,  
!     &             lmax, ncmax, nearm,
!     &             lxyz,
!     &             igall, igcs, igrs, index, family, noself,
!     &             canear, ipvt,
!     &             ngnearm, igblkrs, igblkcs, igblk,
!     &             epsr,epsi,amur,amui)
!      string_rept='After filling Amn read'
!      call time2screen(time_tot, memory_tot, string_rept)

      endif


cccccccccccccccccccccccccccccccc
       open(21, file="info", status='unknown')
       read(21, *) prjName         
       fileName = trim(prjName)//'.in'        
       close(21)


       open(22, file=fileName, status='unknown')    
       read(22,*)                        ! Ignore Header Line
       read(22,*) npts, freq, epscg      ! 2nd line
          
c       open(33, file='inparam', status='unknown')

c       read(33,*)         !    Incident wave parameters
c       read(33,*) freq    !Frequency (Ghz)
c       read(33,*) ipol    !Polarization index (1: theta(V), 2: phi(H), 3: Both
c       read(33,*) thetai  !Incident angle theta (deg)
c       read(33,*) phii    !Incident angle phi (deg)
c       read(33,*)         !     Observation parameters
c       read(33,*) ichrcs  !RCS type (1 for monostatic, 2 for bistatic)
c       read(33,*) thetas1 !Receive angle theta start value (0.--180.) (deg)
c       read(33,*) thetas2 !Receive angle theta end value (0.--180.) (deg) 
c       read(33,*) nthetas !number of receive theta
c       read(33,*) phis1   !Receive angle phi start value (0.--180.) (deg)
c       read(33,*) phis2   !Receive angle phi end value (0.--180.) (deg)
c       read(33,*) nphis   !number of receive phi
c       read(33,*)

c       close(33)
       nthetas = 0
       nphis = 0
       ipol=1
       
       filename = trim(prjName)//'.rcs'
       open(10, file=filename, status='unknown')
       write(10,51)'Theta Phi Cpol(dBsm) Xpol(dBsm)'

       filename = trim(prjName)//'.efar'
       open(11, file=filename, status='unknown')
       write(11,51)'Theta Phi Cpol(re) Cpol(im) Xpol(re) Xpol(im)'

51     format(A)

       

cccccccccccccccccccccccccccccccc
c     prepare iteration        c
cccccccccccccccccccccccccccccccc
! 
      ipolold=ipol
! 
      neqs=maxedge

      do kk=1,neqs
        cbm(kk)=0e0
      enddo

      nun=0

      thetai=rad*(180-thetai)
      phii=rad*(360-phii)

      dthetas = 0.0
      if (nthetas.gt.1) dthetas = (thetas2-thetas1)/float(nthetas-1)
      dphis = 0.0
      if (nphis.gt.1) dphis = (phis2-phis1)/float(nphis-1)

      npol=1
      if (ipol.eq.3) ipol=1

      if (ipol.eq.1) then
c         write(*,98)
      else
c         write(*,99)
      endif
98    format('Vertical (theta) polarization for incident'
     &       '  electric field')
99    format('Horizontal (phi) polarization for incident'
     &       '  electric field')

!     if bistatic mode
c      if (ichrcs.eq.2) then

c      do i=1,neqs
c          crhs(i)=0.
c      end do

c      call rhside(ipol,thetai,phii, alpha,
c     &            maxnode, maxpatch, maxedge, maxrule, maxgrid,
c     &            match, irule_fld, 
c     &            xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
c     &            ngrid, vt1, vt2, vt3, wt,
c     &            rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
c     &            epsr, epsi, amur, amui,
c     &            crhs)

c      string_rept='After filling one RHS'
c      call time2screen(time_tot, memory_tot, string_rept)

c 	do i=1,neqs
c	  cbm(i)=0.
c	enddo

c        call cfie_solve(crhs, cbm, icgbcg, itmax, epscg, ier, nun,
c     &          cwork(1,1), cwork(1,2), cwork(1,3), cwork(1,4),
c     &          cwork(1,5), cvf, cvs,
c     &          maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm,
c     &          ntlm, niplm, lxyz, modes,
c     &          igall, igcs, igrs, index, family,
c     &          lrsmup, lrsmdown, csm, cvlr, cgm, ctl,
c     &          lrstl, indextl,
c     &          kpstart, nkpm, c_shift, icsa, irsa, array,
c     &          ngsndm, igsndcs, igsndrs,
c     &          nearm, canear, noself, ipvt,
c     &          ngnearm, igblkrs, igblkcs, igblk,
c     &          mfinestm, cfinest)

c      write(*,10) inc_count
c      call flush()
c10    format ('Inc angle =',i4)
c      string_rept='After solving for one RHS'
c      call time2screen(time_tot, memory_tot, string_rept)

c      do kk = 1, 10
c         print*,cbm(kk)
c      enddo

c      do kk = 1, maxedge
c         crhs(kk) = cbm(kk)
c      enddo

c      end if 
!     End bistatic mode



!      rcs_4pi=4.*pi


!      inc_count  = 0
!      iall_count = 0

cccccccccccccccccccccccccccccccccccccccccccccccccccc
c........begin iteration of incident wave.........c
cccccccccccccccccccccccccccccccccccccccccccccccccccc

!       do 800 ithetai=1,nthetai
!          thetai=thetai1+(ithetai-1.)*dthetai
!       do 700 iphii=1,nphii
!          phii=phii1+(iphii-1.)*dphii
!          inc_count=inc_count+1
! 
!       do i=1,neqs
!           crhs(i)=0.
!       end do

      ichrcs = 1  ! Dead switch for monostatic RCS mode

      k=0
      do 180 ii = 1, npts
         read(22,*) id,theta,phi,EincMag,EincPhs,polAng,outCurJ  
         
c        Convert inc angles to rad          
         thetas = rad*theta
         phis   = rad*phi
         k = k+1

         tau=rad*polAng

c        Find Co-pol unit vector
         cpolUnit(1)=cos(tau)   !unit vector theta 
         cpolUnit(2)=sin(tau)   !unit vector phi

c        Find Cross-pol unit vector
         zeta=90.0*rad
         xpolUnit(1)=cpolUnit(1)*cos(zeta)-cpolUnit(2)*sin(zeta)
         xpolUnit(2)=cpolUnit(1)*sin(zeta)+cpolUnit(2)*cos(zeta)  

c      do 180 ithe = 1, nthetas
c         thetas = rad*(thetas1+dthetas*float(ithe-1))
c      do 180 iphi = 1, nphis
c         phis = rad*(phis1+dphis*float(iphi-1))

         if (ichrcs.eq.1) then

            thetai = thetas    
            phii   = phis

            write(*,*) 
            write(*,40) '-------------------------------------------'
40          format(A)
            write(*,41) 'Solving #RHS: ',ii,' /',npts
41          format(A, I6, A, I6)

          do i=1,neqs
             crhs(i)=0.
          end do

      call rhside(ipol,thetai,phii, alpha,
     &            maxnode, maxpatch, maxedge, maxrule, maxgrid,
     &            match, irule_fld,
     &            xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &            ngrid, vt1, vt2, vt3, wt,
     &            rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &            epsr, epsi, amur, amui,
     &            crhs, EincPhs, polAng)

      string_rept='After filling one RHS'
      call time2screen(time_tot, memory_tot, string_rept)

       do i=1,neqs
         cbm(i)=0.
       enddo

        call cfie_solve(crhs, cbm, icgbcg, itmax, epscg, ier, nun,
     &          cwork(1,1), cwork(1,2), cwork(1,3), cwork(1,4),
     &          cwork(1,5), cvf, cvs,
     &          maxedge, lmax, lsmax, ncmax, kpm, kp0, nsm,
     &          ntlm, niplm, lxyz, modes,
     &          igall, igcs, igrs, index, family,
     &          lrsmup, lrsmdown, csm, cvlr, cgm, ctl,
     &          lrstl, indextl,
     &          kpstart, nkpm, c_shift, icsa, irsa, array,
     &          ngsndm, igsndcs, igsndrs,
     &          nearm, canear, noself, ipvt,
     &          ngnearm, igblkrs, igblkcs, igblk,
     &          mfinestm, cfinest)


c      write(*,10) inc_count
c      call flush()

c      string_rept='After solving for one RHS'
c      call time2screen(time_tot, memory_tot, string_rept)
!     calculation of current is done by C++ wrapper
!       call out_current( current, cbm, xyznode, ipatpnt, iedge,
!      &                  xyzctr, edge, paera )

!      do kk = 1, 10
!         print*,cbm(kk)
!      enddo

      do kk = 1, maxedge
         crhs(kk) = cbm(kk)
      enddo

!100   continue
!      else
!          write(*,*) 'Zero rhs!'
      end if

	call farfld(thetas,phis, crhs,
     &  maxnode, maxpatch, maxedge, maxrule, maxgrid, 
     &  match, irule_src, 
     &  xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &  ngrid, vt1, vt2, vt3, wt,
     &  rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &  epsr, epsi, amur, amui,
     &  cfld )

       amp = 0.


c      cfld(1)=>Etheta, cfld(2)=>Ephi
       cpolr=real(cfld(1))*cpolUnit(1)+real(cfld(2))*cpolUnit(2)
       cpoli=aimag(cfld(1))*cpolUnit(1)+aimag(cfld(2))*cpolUnit(2)

       xpolr=real(cfld(1))*xpolUnit(1)+real(cfld(2))*xpolUnit(2)
       xpoli=aimag(cfld(1))*xpolUnit(1)+aimag(cfld(2))*xpolUnit(2)    




       amp=4.0*pi*(cpolr*cpolr+cpoli*cpoli)
       dbamp(1)=10.0*alog10(amp+1e-30)
       amp=4.0*pi*(xpolr*xpolr+xpoli*xpoli)
       dbamp(2)=10.0*alog10(amp+1e-30)



c       do l=1,2
c          fldr = real (cfld(l))
c          fldi = aimag(cfld(l))
c          amp  = 4.0*pi*abs(cfld(l))**2
c          degph(l)=atan2(fldi, fldr)/rad
c          dbamp(l)=10.0*alog10(amp+1e-30)
c       enddo


c       print*,thetas/rad,amp
   
       write(11,88) thetas/rad,phis/rad,EincMag*cpolr,EincMag*cpoli,
     &             EincMag*xpolr,EincMag*xpoli
88     format(F8.3, F8.3, E15.7, E15.7, E15.7, E15.7)

       write(10,*) thetas/rad,phis/rad, (dbamp(l),l=1,2)

180   continue

      close(10)
      close(22) 
!      write(*,*)
!      write(*,*)' c........ RCS by MLFMA Nov. 2000........c'
!      write(*,*)
!      write(*,*) '   Total Unknowns        =', neqs     
!      write(*,*)
!      write(*,*) '   Total CPU seconds     =', time_tot
!      write(*,*)

      return
      end
