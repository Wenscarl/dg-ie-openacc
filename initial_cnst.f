      subroutine INITIAL_CNST
     &( wl0, 
     &  rk0, rk2d4, eta0, cnste, cnsth, distmin, ci)

      use mlfma_const

c.....Input Data

      REAL wl0

c.....Output Data

      REAL    rk0, rk2d4, eta0, distmin
      COMPLEX cnste, cnsth, ci

c.....Parameter

c      PARAMETER( pi=3.141592653)
      REAL rc

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c eta0 = free space wave impedance (120*pi)................................c
c  wl0 = free space wavelength.............................................c
c  rk0 = free space wavenumber.............................................c
c
c cnste = 1.0/( i * omega * mu * 4 * pi ); ................................c
c     where  1/(4*pi) comes from green's function..........................c
c     the factor 0.25 from basis and test function ........................c
c     for efie has been accounted for be rk2d4.............................c
c
c cnsth = -0.25 * eta0/(4*pi); ............................................c
c     where  1/(4*pi) comes from green's function..........................c
c     0.25 comes from basis (1/2) and test (1/2) functions.................c
c
c rk2d4 = rk0*rk0/4.0 (here rk2 means rk0 squared, d4 means divide by 4)...c
c
c distmin=minimum distance to be considered for singularity subtraction....c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      ci   = (0.0, -1.0)

      rc = 299792458.0
      eta0 = 4.0e-7*pi*rc
      rk0  = 2.0*pi/wl0

c      cnste= -30.0*ci/rk0
      cnste= -ci*eta0*eta0/(rk0*rc*16.0e-7*pi*pi)
      cnsth= -0.25*eta0/(4.0*pi)

      rk2d4= 0.25*rk0*rk0

      distmin= wl0*0.15


      RETURN
      END
