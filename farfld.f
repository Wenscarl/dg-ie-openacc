      subroutine FARFLD
     &( thetas,phis, crhs, 
     &  maxnode, maxpatch, maxedge, maxrule, maxgrid, 
     &  match, irule_src, 
     &  xyznode, ipatpnt, iedge, xyzctr, xyznorm, edge, paera, 
     &  ngrid, vt1, vt2, vt3, wt,
     &  rk0, freq, wl0, rk2d4, eta0, cnste, cnsth, ci,
     &  epsr, epsi, amur, amui,
     &  cfld )

      IMPLICIT NONE

C.....Input Data
  
      INTEGER*8 maxnode, maxpatch, maxedge, maxrule, maxgrid
      INTEGER*8 match, irule_src
       REAL   thetas, phis     
      COMPLEX crhs(maxedge)
      INTEGER*8 ipatpnt(3,maxpatch), iedge(4,maxedge)
       REAL   xyznode(3,maxnode),
     &        xyzctr(3,maxpatch), xyznorm(3,maxpatch), edge(maxedge), 
     &        paera(maxpatch)
      INTEGER*8 ngrid(maxrule)
       REAL   vt1(maxgrid,maxrule), vt2(maxgrid,maxrule), 
     &        vt3(maxgrid,maxrule),wt(maxgrid,maxrule)
       REAL   rk0, freq, wl0, rk2d4, eta0
      COMPLEX cnste, cnsth, ci
       REAL   epsr(2), epsi(2), amur(2), amui(2)
       REAL   pi, eps
      PARAMETER( pi=3.141592653, eps=1e-4 )

c.....Output

      COMPLEX cfld(2)

c.....Working Variables

      INTEGER*8 l, k, i,     j,   i2,  i3,     n1,     n2,     n3
       REAL   rks(3), the(3), phi(3), rc(3),  pc(3), bn(3)
       REAL   dotmul, signl
       REAL   thesin, thecos, phisin, phicos
!	 REAL   thetas, phis, thesin, thecos, phisin, phicos
      COMPLEX ctmp, csum1, csum2, ceta, cx, ceps, cmu

      thesin=sin(thetas)
      thecos=cos(thetas)
      phisin=sin(phis)
      phicos=cos(phis)

      rks(1)=thesin*phicos
      rks(2)=thesin*phisin
      rks(3)=thecos

      the(1)= thecos*phicos
      the(2)= thecos*phisin
      the(3)=-thesin

      phi(1)=-phisin
      phi(2)= phicos
      phi(3)= 0.0

      cfld(1)=( 0.0, 0.0 )
      cfld(2)=( 0.0, 0.0 )

      ceps=cmplx( epsr(1), epsi(1) )
      cmu =cmplx( amur(1), amui(1) )
      cx  =ci*rk0*sqrt(ceps*cmu)
      ceta=120.0*pi*sqrt(cmu/ceps)

      do 100 i=1,maxedge

        n1 = iedge(1,i)
        n2 = iedge(2,i)

        csum1=0.0
        csum2=0.0

        do l=1,2

          i3 = iedge(l+2,i)
           if (i3.le.0) goto 90

          n3=ipatpnt(1,i3)+ipatpnt(2,i3)+ipatpnt(3,i3)-n1-n2
          signl = float(3-2*l)

          call getnorm( bn, xyznorm(1,i3) )

          do k=1,ngrid(irule_src)

            do j=1,3
              rc(j) = vt1(k,irule_src)*xyznode(j,n1)+
     &                vt2(k,irule_src)*xyznode(j,n2)+
     &                vt3(k,irule_src)*xyznode(j,n3)
              pc(j) = rc(j) - xyznode(j,n3)
            end do

            ctmp=wt(k,irule_src)*signl*exp(-cx*dotmul(rks,rc))
            csum1=csum1+dotmul(the,pc)*ctmp
            csum2=csum2+dotmul(phi,pc)*ctmp

          end do
90        end do

        cfld(1) = cfld(1)+edge(i)*crhs(i)*csum1
        cfld(2) = cfld(2)+edge(i)*crhs(i)*csum2

100   continue

      cfld(1)=cfld(1)*cx*ceta/(8.0*pi)
      cfld(2)=cfld(2)*cx*ceta/(8.0*pi)

      RETURN
      END
