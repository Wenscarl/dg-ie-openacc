ccccccccccccccccccccccccccccccccc
c......read geometry data.......c
ccccccccccccccccccccccccccccccccc

      subroutine read_geom(maxnode,maxpatch,maxedge,
     &                     xyznode, ipatpnt, iedge) 


      implicit none

      integer*8 maxnode, maxpatch, maxedge,i,ip
      integer*8 iedge(4,maxedge),ipatpnt(3,maxpatch)
      real    xyznode(3,maxnode)
      integer*8 mn,kl,ke

ccccccccccccccccccccccccccccccccccccccccccccc
c......read x, y, and z for each nodes......c
ccccccccccccccccccccccccccccccccccccccccccccc

      read(3,*) mn
      
      if (mn.ne.maxnode) then
        write(*,*) 'mn.NE.maxnode'
        stop
      else
      endif

      do ip = 1, maxnode
         read(3,*) (xyznode(i,ip), i=1,3)
      end do

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c......read index of 3 nodes for each triangular patch......c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      read(5,*) kl

      if (kl.ne.maxpatch) then
        write(*,*) 'kl.NE.maxpatch'
        stop
      else
      endif
 
      do ip = 1, maxpatch
         read(5,*) (ipatpnt(i,ip), i=1,3)
      enddo

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c.....read index 2 nodes and 2 triangulars for each interior edge.....c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      read(4,*) ke

      if (ke.ne.maxedge) then
        write(*,*) 'ke.NE.maxedge'
        stop
      else
      endif

      do ip = 1, maxedge
         read(4,*) (iedge(i,ip), i=1,4)
      enddo
      close(3)
	close(5)
      close(4)

      return
      end
