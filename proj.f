      subroutine PROJ
     &( r, r1, r2, r3, r30, bn, aera, wl0,
     &  ds, r0,rho0,rhod,rhoxd,ri1,ri1p,ri3,ri3p)

      IMPLICIT NONE
      
c.....Input Data

       REAL r(3), r1(3), r2(3), r3(3), r30(3), bn(3), aera, wl0

c.....Output Data

       REAL ds, r0(3), rho0(3), rhod(3), rhoxd(3), 
     &      ri1, ri1p(3), ri3, ri3p(3)

c.....Working Variables

      INTEGER*8 iv, i
       REAL d, dotmul, tmp(3)
       REAL u1(3),   r10, r11, p10,  al1(2), d0(3)
       REAL u(3,3),  rm(3), rp(3)
       REAL pmid(3), rmid(3), al(2,3)
       REAL tmp1, tmp2, r02, f, beta,  h, zerod
       REAL signd,tx,ty
       
c.......................................................................

      call norm3d( r1,r2,r3,bn ) !obtain the normal vector in the triangle
      zerod = wl0 * 1e-20
      call vctadd( r, r1, tmp, -1 )  ! tmp= r-r1
      ds = dotmul( tmp, bn )         ! ds= dot( (r-r1), normal)
      d0(1) =  bn(1)*ds              ! vec{d0}= vec{bn} * ds
      d0(2) =  bn(2)*ds
      d0(3) =  bn(3)*ds
      call vctadd( r,    d0,  r0,   -1 )
      call vctadd( r0,   r30, rho0, -1 )
      call vctadd( rho0, d0,  rhod,  1 )
      call xmul(   rho0, bn,  rhoxd ) 
      signd = 1.0
      if( ds.le.0.0 ) signd = -1.0
      d = abs(ds)

      do iv=1,3
         if(iv.eq.1) then
           call trans(bn,r,r0,r2,r3, u1,p10,r10,r11,al1)
         else if(iv.eq.2) then
           call trans(bn,r,r0,r3,r1, u1,p10,r10,r11,al1)
         else if(iv.eq.3) then
           call trans(bn,r,r0,r1,r2, u1,p10,r10,r11,al1)
         end if
         u(1,iv)=u1(1)
         u(2,iv)=u1(2)
         u(3,iv)=u1(3)
         pmid(iv) = p10
         rmid(iv) = r10
         rm(iv) = r11
         al(1,iv) = al1(1)
         al(2,iv) = al1(2)
      end do
      rp(1) = rm(2)
      rp(2) = rm(3)
      rp(3) = rm(1)
      
      ri1 = 0.0
      ri3 = 0.0
      do i=1,3
         ri1p(i) = 0.0
         ri3p(i) = 0.0
      end do

      do iv=1,3

          r02 = rmid(iv)*rmid(iv)

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
c..... mathematically, f=alog( (rp+alp)/(rm+alm) )......................c
c..... when rp+alp is very small, replace log(rp+alp) by................c
c..... log(rp); when rm+alm is very small, replace .....................c
c..... log(rm+alm) by log(rm). the singular term(s) will be.............c
c..... cancelled by the contribution from other edges...................c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


          f=0.0
!..... this part is replaced by the paragraph below, due to potential blow up if f value
!           if( rp(iv).gt.zerod ) then
!              f=f+alog( rp(iv) )
!              tmp2 = abs(1.0+al(2,iv)/rp(iv))
!              if(tmp2.gt.1e-5) f=f+alog(tmp2)
!           end if
!           if( rm(iv).gt.zerod ) then
!              f=f-alog( rm(iv) )
!              tmp1 = abs(1.0+al(1,iv)/rm(iv))
!              if(tmp1.gt.1e-5) f=f-alog(tmp1)
!           end if
!.....
          if( (rp(iv).gt.zerod).and.(rm(iv).gt.zerod) ) then	! make sure field point is NOT close to a vertex
	  if( abs(rm(iv)+al(1,iv) ) .gt. abs(rp(iv)-al(2,iv) ) ) then
	  f = alog( (rp(iv)+al(2,iv)) ) - alog( (rm(iv)+al(1,iv)) )
	  else
	  f = alog( (rm(iv)-al(1,iv)) ) - alog( (rp(iv)-al(2,iv)) )
	  endif
	  endif

          h = rp(iv)*al(2,iv)-rm(iv)*al(1,iv)

          tx=pmid(iv)*al(2,iv)
          ty=r02+d*rp(iv)
          if ((tx.eq.0).and.(ty.eq.0)) then
             tmp2=0
          else
             tmp2 = atan2(tx,ty)
          endif

          tx=pmid(iv)*al(1,iv)
          ty=r02+d*rm(iv)
          if ((tx.eq.0).and.(ty.eq.0)) then
             tmp1=0
          else
             tmp1 = atan2(tx,ty)
          endif

          beta = tmp2 - tmp1

          ri3 = ri3 + signd * beta
          ri1 = ri1 + pmid(iv)*f - d*beta
          do i=1,3
             ri1p(i) = ri1p(i) + u(i,iv)*( r02*f+h )/2.0
             ri3p(i) = ri3p(i) - u(i,iv)*f
          end do
       end do

       ri1 = ri1/aera
       ri3 = ri3/aera
       do i=1,3
          ri1p(i) = ri1p(i)/aera
          ri3p(i) = ri3p(i)/aera
       end do

      return
      end
