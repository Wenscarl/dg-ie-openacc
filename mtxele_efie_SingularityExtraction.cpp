#include <set>
#include <ctime>
#include <cstdio>
#include <complex>
#include <math.h>
#include "quadrature.h"
#include <xmesh_vector.h>
#include <stdio.h>
#include <stdlib.h>
#include "comp.h"

typedef int Integer;
typedef float Real;
typedef std::complex<float> Complex;
const Real PI=acos(-1.0);
FILE* fp_global=NULL;
FILE* fp_global2=NULL;
static Integer total=0;
extern "C" {
  void mtxele_efie_( 
             Integer* ii, 
		 Integer* jj, 
		 Complex* ck,
		 Integer* maxnode, 
		 Integer* maxpatch, 
		 Integer* maxedge,
		 Real* xyznode, 
		 Integer* ipatpnt, 
		 Integer* iedge, 
		 Real* xyzctr, 
		 Real* xyznorm, 
		 Real* edge, 
		 Real* paera,
		 Real* freq, 
		 Real* wl0,
		 Real* eta0, 
		 Complex* ci,
		 Complex* cnxet,
		 Complex* );
  void openfile_();
  void closefile_();
void rules2(Integer i1,
		Integer i2,
		Real* 	r1,
		Real* 	r2,
		Real		distmin,
		Integer	irulef_near,
		Integer	irules_near,
		bool&	nearpat,
		Integer&	irulef,
		Integer&	irules);
}
void openfile_()
{
	fp_global=fopen("checkNF_new","w");
	if(fp_global==NULL)
		exit(-1);
	fp_global2=fopen("checkNF2_new","w");
	if(fp_global2==NULL)
		exit(-1);
}
void closefile_()
{
	fclose(fp_global);fclose(fp_global2);
}
void getr(Real* r1_fld,Real* r2_fld,Real* r3_fld,
	    Real vt1,Real vt2,Real vt3,Real* rfld,Real* rhofld);

void SingularIntegral(Complex& czev_singular, Complex& czes_singular,
                     xMesh::vector** srcPts,const xMesh::vector r3_src,
			   const xMesh::vector  rfld,const xMesh::vector  rhofld,
			   const Complex ci,Complex ck);
template<class T> T mysqr(T dd){
	return dd*dd;
}
void diff(Real* a,Real* b,Real* c){
	for (int i=0;i<3;i++){
		c[i]=a[i]-b[i];
	}
}
void xmul(Real* a,Real* b,Real* c){
	for (int i=0;i<3;i++){
		c[i]=a[(i+1)%3]*b[(i+2)%3]-a[(i+2)%3]*b[(i+1)%3];
	}
}
Real dotmul(Real* a,Real* b)
{
	return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}


void SingularIntegral(Complex& czev_singular, Complex& czes_singular,
                     xMesh::vector** srcPts,const xMesh::vector r3_src,
			   const xMesh::vector  rfld,const xMesh::vector  rhofld,
			   const Complex ci,Complex ck)
{
	//czev_singular= \int 
	czev_singular=Complex(0.0);
	czes_singular=Complex(0.0);
	/*
                               rm[i]      rp[i] 
      0          l_vector[0]= node[1] -> node[2]
     / \         l_vector[1]= node[2] -> node[0]
   2/  \ 1       l_vector[2]= node[0] -> node[1]
   /   \
  1----2
     0       
     
     */
	//Suppose the three nodes right handed.
	
	xMesh::vector l_vector[3];
	Real l[3];
	for(int i=0;i<3;i++){
		l_vector[i] = *(srcPts[(i+2)%3]) - *(srcPts[(i+1)%3]);
		l[i]=l_vector[i].norm();
		l_vector[i].normalize();
	}
	xMesh::vector normal = l_vector[0] * l_vector[1]; 
	if(normal.norm()==Complex(0.0)){
		// illegal triangles
		printf("illegal triangles!\n");
		exit(-1);
	}
	normal.normalize();
	
	//Obtain d
	Real d=dotP( rfld- *(srcPts[0]),normal);
	xMesh::vector d_vector =  normal * d;
	xMesh::vector r0 = rfld- d_vector;
	
	xMesh::vector u[3];
	for(int i=0;i<3;i++){
		u[i] = l_vector[i] * normal;
		u[i].normalize();
	}
	
	xMesh::vector* r_minus[3]={srcPts[1],srcPts[2],srcPts[0]};
	xMesh::vector* r_plus [3]={srcPts[2],srcPts[0],srcPts[1]};
	
	Real  P_minus[3],P_plus[3];
	Real  R_minus[3],R_plus[3];
	xMesh::vector  Pv_minus[3],Pv_plus[3];
	xMesh::vector  Rv_minus[3],Rv_plus[3];
	xMesh::vector  Rv0[3],Pv0[3];
	Real  P0[3],R0_2[3],l_minus[3],l_plus[3];
	Real  signP0[3];
	xMesh::vector tmp;
	Real part1=0.0,part2=0.0;
	for(int i=0;i<3;i++){
		Pv_minus[i]= ((*r_minus[i] )-r0);
		Rv_minus[i]= ((*r_minus[i] )-rfld);
		Pv_plus[i] = ((*r_plus[i]  )-r0);
		Rv_plus[i] = ((*r_plus[i]  )-rfld);
		P_minus[i]= Pv_minus[i].norm();
		P_plus[i] = Pv_plus[i].norm();
		R_minus[i]= Rv_minus[i].norm();
		R_plus[i] = Rv_plus[i].norm();
		P0[i]     = dotP(u[i],Pv_plus[i]);
		
		l_minus[i]=dotP(Pv_minus[i],l_vector[i]);
		l_plus[i] =l[i] + l_minus[i];	
		
		tmp= (*r_minus[i]) - l_vector[i] * l_minus[i];
		Rv0[i] = tmp - rfld;
		Pv0[i] = tmp - r0;
		R0_2[i]= dotP(Rv0[i],Rv0[i]);
		
		
		
		if(P0[i]>0.0)
			signP0[i]=1.0;
		else if(P0[i]<0.0)
			signP0[i]=-1.0;
		else
			signP0[i]=0.0;
			
		
		Real f=Real(0.0);
		if(R_plus[i]> 1.e-20){
			f+= log(R_plus[i]);
			Real tmp2=fabs(1.0 + l_plus[i]/ R_plus[i]);
			if(tmp2>1.e-20)
				f+=log(tmp2);
		}
		if(R_minus[i]> 1.e-20){
			f-= log(R_minus[i]);
			Real tmp2=fabs(1.0+ l_minus[i]/ R_minus[i]);
			if(tmp2>1.e-20)
				f-=log(tmp2);
		}
		part1+=  P0[i]*f;
		
		//calculate beta;
		Real beta;
		beta=  atan(P0[i]* l_plus [i]/(R0_2[i]+fabs(d)*R_plus [i]))
		      -atan(P0[i]* l_minus[i]/(R0_2[i]+fabs(d)*R_minus[i]))	;	
		part2+= fabs(d)* beta;
	}
	
	
	
	czes_singular=part1+part2;
	
	
	//The scalar potential part:
	//\int{1/|rfld-r'| dr'} = 
	
}

void mtxele_efie_( 
             Integer* ii_, 
		 Integer* jj_, 
		 Complex* ck_,
		 Integer* maxnode, 
		 Integer* maxpatch, 
		 Integer* maxedge,
		 Real* xyznode, 
		 Integer* ipatpnt, 
		 Integer* iedge, 
		 Real* xyzctr, 
		 Real* xyznorm, 
		 Real* edge, 
		 Real* paera,
		 Real* freq_, 
		 Real* wl0_,
		 Real* eta0_, 
		 Complex* ci_,
		 Complex* cnxetv,
		 Complex* cnxets)
{
	Integer ii=*ii_ -1;
	Integer jj=*jj_ -1;
	Complex& ck=*ck_;
	Real& freq=*freq_;
	Real& wl0=*wl0_;
	Real& eta0=*eta0_;
	Complex& ci=*ci_;
	
	bool writeout=false; //For debug use
#ifdef _DEBUG0	
	if(ii==448 && jj==448)
		writeout=true;
#endif
		
#ifdef _DEBUG2
	if(ii==448 && ( jj==448||jj==441||jj==449))
		writeout=true;
#endif
	Complex   ck2d2=ck*ck/ Complex(2.0);
	Integer n1fld = iedge[  ii*4 +0]  -1;
	Integer n2fld = iedge[  ii*4 +1]  -1;
	Integer n1src = iedge[  jj*4 +0]  -1;
	Integer n2src = iedge[  jj*4 +1]  -1;
	
	xMesh::vector r1_src(&(xyznode[n1src*3]));
	xMesh::vector r2_src(&(xyznode[n2src*3]));
	xMesh::vector r1_fld(&(xyznode[n1fld*3]));
	xMesh::vector r2_fld(&(xyznode[n2fld*3]));
	
	xMesh::vector ectr;
	ectr=(r1_fld+r2_fld)/2.0;
	
	*cnxetv= Complex(0.0);
	*cnxets= Complex(0.0);

	
	Integer outerGauss=6;
	Integer innerGauss=6;
	Integer outerDuffy=4;
	Integer innerDuffy=27;
	
	//For receiving trangles binded to the receiving edge.
	for(Integer idFld=0;idFld<2;idFld++){ 
		Real signfld = float(1.0f-2.0f*idFld);
		Integer ipfld=iedge[2+idFld+ (ii)*4] -1;
		Integer n3fld= ipatpnt[ipfld*3]+ ipatpnt[ipfld*3+1]+ipatpnt[ipfld*3+2] - n1fld -n2fld -3;
		xMesh::vector r3_fld(&(xyznode[n3fld*3]));
		xMesh::vector midFld= (r3_fld+r2_fld+r1_fld) * 0.3333f;
		xMesh::vector anorm(&(xyznorm[ipfld*3]));
		xMesh::vector pctr(&(xyzctr[ipfld*3]));
		xMesh::vector rhot;
		rhot=ectr-pctr;
		Complex cefiev=Complex(0.0f);
		Complex cefies=Complex(0.0f);
		//For source triangles
		for(Integer idSrc=0;idSrc<2;idSrc++){
		     Real signsrc = float( 1.0 - 2 * idSrc );
		     Integer ipsrc = iedge[2+idSrc+ jj*4 ]-1;
		     Integer n3src= ipatpnt[ipsrc*3]+ ipatpnt[ipsrc*3+1]+ipatpnt[ipsrc*3+2] - n1src -n2src -3;
		     xMesh::vector r3_src(&(xyznode[n3src*3]));

			xMesh::vector bnorm(&(xyznorm[ipsrc*3]));
			xMesh::vector midSrc= (r3_src+r2_src+r1_src) * 0.3333f;
			bool SE=false; //Singularity Extraction?
			bool isNear=(ipsrc==ipfld);
			if(isNear){
				SE=true;
			}
			else{
				SE=false; //To be tested.
				xMesh::vector tmp=midFld-midSrc;
				Real rk0= abs(ck)*tmp.norm();
				if(rk0< 0.1||ii==jj){
					outerGauss=27;
					innerGauss=27;
				}else if(rk0<0.5){
					outerGauss=7;
					innerGauss=7;
				}else if(rk0<1){
					outerGauss=7;
					innerGauss=7;
				}else{
					outerGauss=1;
					innerGauss=1;
				}
			}

#ifdef _DEBUG0
			if(writeout&& isNear){
					fprintf(fp_global2,
						"ipfld %d ipsrc %d fld: (%f %f %f) (%f %f %f) (%f %f %f)\n",
						  ipfld,ipsrc,
						  r1_fld[0],r1_fld[1],r1_fld[2],
						  r2_fld[0],r2_fld[1],r2_fld[2],
						  r3_fld[0],r3_fld[1],r3_fld[2]
					);
					fprintf(fp_global2,
						"src: (%f %f %f) (%f %f %f) (%f %f %f)\n",
						  r1_src[0],r1_src[1],r1_src[2],
						  r2_src[0],r2_src[1],r2_src[2],
						  r3_src[0],r3_src[1],r3_src[2]
					);
			}
#endif	
			Complex csumev=Complex(0.0);
			Complex csumes=Complex(0.0);
			
			Integer outerLoop= isNear?outerDuffy:outerGauss;
			Integer innerLoop= isNear?innerDuffy:innerGauss;
			for(Integer ifgrid=0;ifgrid<outerLoop;ifgrid++){
				Real vt[3],wto;
				GetFormula(outerLoop,ifgrid,&(vt[0]),&(vt[1]),&(vt[2]),&wto);
				xMesh::vector rfld= (r1_fld*vt[0]+ r2_fld*vt[1]+r3_fld*vt[2]);
				xMesh::vector rhofld=rfld-r3_fld;				
				xMesh::vector rhoxnt=rhofld;
				xMesh::vector rhoxnx= rhofld*anorm;

				Complex czev=Complex(0.0); //Vector part
				Complex czes=Complex(0.0); //Scalar part
#ifdef _DEBUG0		
				if(writeout&& isNear){
					fprintf(fp_global2,
						  "Start m: %d (%f %f %f) vt: %f %f %f\n",
						  ifgrid,rfld[0],rfld[1],rfld[2],vt[0],vt[1],vt[2]);
				}
#endif		
				for(int isgrid=0;isgrid<innerLoop;isgrid++){ //Go through every points in the source grid
					Real vti[3],wti;	
					if(!isNear)
						GetFormula(innerLoop,isgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
					else
						getDuffy(outerLoop,innerLoop,ifgrid,isgrid,&(vti[0]),&(vti[1]),&(vti[2]),&wti);
					xMesh::vector rsrc;
					if(isNear) 
						rsrc= (r1_fld*vti[0] + r2_fld*vti[1]+r3_fld*vti[2]);	
					else
						rsrc= (r1_src*vti[0] + r2_src*vti[1]+r3_src*vti[2]);
					xMesh::vector rhosrc = rsrc-r3_src;
					xMesh::vector rr = rsrc-rfld;
					Real distance= rr.norm();
					if(distance==0.0f){
						printf("Suck!\n");
					}
					Complex cg0= exp(ci*ck*distance);
					Complex Green=Complex(0.0);
					if(!SE) {
						Green = cg0 / distance;
					}
					else{
						if(distance * abs(ck)> 0.05 )
							Green = (cg0 - Complex(1.0)) / distance;
						else{
							Complex ckr= ck * distance; 
							Green = ci * ck * (  Complex(1.0) - ckr * ckr/Complex(6.0) 
							- Complex(0.5) * ck * ckr);
						}
					}
					Real zr=dotP(rhosrc,rhofld);
					//Complex comp1= wti* Green * zr;
					Complex comp1= wti* ( cg0 / distance) * zr;
					Complex comp2= Green*Complex(- wti*4.0,0.0)/(ck*ck);
					czev += comp1;
					czes += comp2;
// 					czes += Green*Complex( -4.0*wti,0.0) /(ck*ck);
#ifdef _DEBUG0					
					if(writeout&& isNear){						
						fprintf(fp_global2,
						" %d  %d ( %f %f %f)   %f    (%f %f)  vti:( %f %f %f)\n",
						  ifgrid,isgrid,
// 						  rfld[0],rfld[1],rfld[2],
						  rsrc[0],rsrc[1],rsrc[2],
						  fabs(ck*distance),Green.real() *wti*wto,Green.imag() *wti*wto
							,vti[0],vti[1],vti[2]
						);
					}
#endif			
				}
				//Second Step, singular integral
				//Based on Wilton's paper in 1984
				if(SE){
					Complex czev_singular(0.0);
					Complex czes_singular(0.0);
					xMesh::vector* srcPts[3];
					if(idSrc==0){ //Keep the right hand rotation
						srcPts[0]= & r1_src;
						srcPts[1]= & r2_src;
						srcPts[2]= & r3_src;
					}
					else{
						srcPts[1]= & r1_src;
						srcPts[0]= & r2_src;
						srcPts[2]= & r3_src;
					}
					SingularIntegral(czev_singular,czes_singular,
							     srcPts,
							     r3_src,rfld,rhofld,ci,ck);
					czev += czev_singular/paera[ipsrc];
					czes += czes_singular/paera[ipsrc];
				}
#ifdef _DEBUG0					
				if(writeout&& isNear){						
					Complex result=wto * czes/Complex(4.0) *(ck*ck);		
					Complex result2=wto * czev/Complex(4.0) *(ck*ck);
					fprintf(fp_global2,"subtotal m: %d (%f,%f) (%f,%f)\n",
					  ifgrid,
					  result2.real(),result2.imag(),
					  result.real(),result.imag());
				}
#endif
				csumev += wto * czev;
				csumes += wto * czes;	
				
			} //for(Integer ifgrid=0;ifgrid<outerLoop;ifgrid++){
#ifdef _DEBUG2
			if(writeout){			
				float flag=signsrc*signfld;
				fprintf(fp_global2,
					  "%d %d  %3.1f\t%3.1f\t %d (%f,%f)\t (%f,%f) %d %d\n",
					  ii,jj,
					  signfld*ipfld,signsrc*ipsrc,isNear,
					   flag *csumev.real(), flag *csumev.imag(), 
					  flag *csumes.real(), flag *csumes.imag(),
					  outerLoop,innerLoop);
			}
#endif
			cefiev += signsrc * csumev;
			cefies += signsrc * csumes;
		}//for(Integer idSrc=0;idSrc<2;idSrc++){
		*cnxetv += signfld* cefiev;
		*cnxets += signfld* cefies;
	}//for(int idFld=0;idFld<2;idFld++)  
// 	*cnxets/=(ck*ck);
//       if(total++<10000 && writeout){
// 		fprintf(fp_global2," %d\t%d\t (%f,%f)\t (%f,%f)\n",ii+1,jj+1,cnxetv->real(),cnxetv->imag(),cnxets->real(),cnxets->imag());
// 	}

#ifdef _DEBUG2
	if(writeout){
		fprintf(fp_global2,
			 "Summation: (%f %f) (%f %f) (%f %f)\n\n",
			 cnxetv->real(),cnxetv->imag(),
			 cnxets->real(),cnxets->imag(),
			 cnxetv->real()+cnxets->real(),
			 cnxetv->imag()+cnxets->imag()			  
 			);
	}
#endif
	Real rnst= edge[ii]*edge[jj]/(16.0f*PI);
	*cnxetv*=rnst;
	*cnxets*=rnst;	
}
void getr(Real* r1_fld,Real* r2_fld,Real* r3_fld,
	    Real vt1,Real vt2,Real vt3,Real* rfld,Real* rhofld)
{
	for (int i=0;i<3;i++){
		rfld[i]= vt1*r1_fld[i]+vt2*r2_fld[i]+vt2*r2_fld[i];
		rhofld[i]=rfld[i]-r3_fld[i];
	}
}
void rules2(Integer i1,
		Integer i2,
		Real* 	r1,
		Real* 	r2,
		Real		distmin,
		Integer	irulef_near,
		Integer	irules_near,
		bool&		nearpat,
		Integer&	irulef,
		Integer&	irules)
{
	Real d=0.0f;
	nearpat=false;
	if(i1==i2)
		d=0.0f;
	else{
		for(int i=0;i<3;i++)
			d+= mysqr<Real>(r1[i]-r2[i]);
	}
	if(sqrt(d)<distmin){ 
		nearpat=true;
		irulef=irulef_near;
		irules=irules_near;
	}
}
