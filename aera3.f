      function aera3( r1,r2,r3 )
      implicit none
      real r1(3), r2(3),  r3(3), r12(3), r23(3), r31(3)
      real aera3, e1, e2, e3,    s,      dotmul
      call vctadd( r2, r1, r12, -1 )
      call vctadd( r3, r2, r23, -1 )
      call vctadd( r1, r3, r31, -1 )
      e1 = sqrt( dotmul( r12,r12 ) )
      e2 = sqrt( dotmul( r23,r23 ) )
      e3 = sqrt( dotmul( r31,r31 ) )
      s = 0.5*(e1+e2+e3)
      aera3 = sqrt(abs(s*(s-e1)*(s-e2)*(s-e3)))
      return
      end
