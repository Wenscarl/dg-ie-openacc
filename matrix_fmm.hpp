#ifndef MATRIX_FMM_HPP
#define MATRIX_FMM_HPP

 #include "iml_vector.hpp"

typedef long long int Int;

extern "C" {
  typedef std::complex<float> Complex_FMM;
  void cacx_(Complex_FMM *, Complex_FMM *, Int *, Complex_FMM *, Complex_FMM *, Complex_FMM *,
	     Int *, Int *, Int *, Int *, Int *, Int *,
	     Int *, Int *, Int *, Int *, Int *,
	     Int *, Int *, Int *, Int *, Int *,
	     Int *, Int *, Complex_FMM *, Complex_FMM *, Complex_FMM *, Complex_FMM *,
	     Int *, Int *,
	     Int *, Int *, Complex_FMM *, Int *, Int *, float *,
	     Int *, Int *, Int *, Int *, Int [][2], Complex_FMM *, Int *, Int *,
	     Int *, Int *, Int *, Complex_FMM *);
}

class Matrix_Fmm
{
  typedef std::complex<float> Complex_FMM;
public:
  Matrix_Fmm(Complex_FMM *cx0, Complex_FMM *cy, Complex_FMM *cx,
	     Complex_FMM *cvf, Complex_FMM *cvs, Int *maxedge, Int *lmax, Int *lsmax,
	     Int *ncmax, Int *kpm, Int *kp0, Int *nsm, Int *ntlm, Int *niplm,
	     Int *lxyz, Int *modes, Int *igall, Int *igcs, Int *igrs, Int *index,
	     Int *family, Int *lrsmup, Int *lrsmdown, Complex_FMM *csm, Complex_FMM *cvlr,
	     Complex_FMM *cgm, Complex_FMM *ctl, Int *lrstl, Int *indextl, Int *kpstart,
	     Int *nkpm, Complex_FMM *c_shift, Int *icsa, Int *irsa, float *array,
	     Int *ngsndm, Int *igsndcs, Int *igsndrs, Int *nearm,
	     Int noself[][2], Complex_FMM *canear, Int *ngnearm, Int *igblkrs,
	     Int *igblkcs, Int *igblk, Int *mfinestm, Complex_FMM *cfinest);

  template <class Vector>
  Vector operator*(const Vector &x)
  {
    Int zero = 0;

    iml_vector wx(x.size());

    Int x_size = x.size();

    for (Int i=0; i<x_size; i++)
       wx(i) = x(i);


    std::copy(&wx(0), &wx(0)+(*maxedge), cx0);

    cacx_(cx0, cy, &zero, cx, cvf, cvs, maxedge, lmax, lsmax, ncmax, kpm, kp0,
	  nsm, ntlm, niplm, lxyz, modes, igall, igcs, igrs, index, family,
	  lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, indextl,
	  kpstart, nkpm, c_shift, icsa, irsa, array, ngsndm, igsndcs, igsndrs,
	  nearm, noself, canear, ngnearm, igblkrs, igblkcs, igblk, mfinestm, cfinest);

    iml_vector y(*maxedge);
    std::copy(cy, cy+(*maxedge), &y(0));

    Vector wy(y.GetDim());

    Int y_size = y.GetDim();
    for (Int i=0; i<y_size; i++)
       wy(i) = y(i);

    return wy;
  }

  template <class Vector>
  Vector tran_mult(const Vector &x)
  {
    Int one = 0;
    std::copy(&x(0), &x(0)+(*maxedge), cx0);

    cacx_(cx0, cy, &one, cx, cvf, cvs, maxedge, lmax, lsmax, ncmax, kpm, kp0,
	  nsm, ntlm, niplm, lxyz, modes, igall, igcs, igrs, index, family,
	  lrsmup, lrsmdown, csm, cvlr, cgm, ctl, lrstl, indextl,
	  kpstart, nkpm, c_shift, icsa, irsa, array, ngsndm, igsndcs, igsndrs,
	  nearm, noself, canear, ngnearm, igblkrs, igblkcs, igblk, mfinestm, cfinest);

    Vector y(*maxedge);
    std::copy(cy, cy+(*maxedge), &y(0));
    return y;
  }

private:
  Complex_FMM *cx0, *cy, *cx, *cvf, *cvs;
  Int *maxedge, *lmax, *lsmax, *ncmax, *kpm, *kp0, *nsm, *ntlm, *niplm,
    *lxyz, *modes, *igall, *igcs, *igrs, *index, *family, *lrsmup,
    *lrsmdown;
  Complex_FMM *csm, *cvlr, *cgm, *ctl;
  Int *lrstl, *indextl, *kpstart, *nkpm;
  Complex_FMM *c_shift;
  Int *icsa, *irsa;
  float *array;
  Int *ngsndm, *igsndcs, *igsndrs, *nearm;
  Int (*noself)[2];
  Complex_FMM *canear;
  Int *ngnearm, *igblkrs, *igblkcs, *igblk, *mfinestm;
  Complex_FMM *cfinest;
};

#endif
