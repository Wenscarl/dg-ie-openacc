      subroutine INITIAL_GEOM
     &( maxnode,maxpatch,maxedge,
     &  xyznode,ipatpnt,iedge,          
     &  xyzctr, xyznorm,edge, paera, rnear2)

      IMPLICIT NONE

c.....Input Data

      INTEGER*8 maxnode, maxpatch, maxedge
       REAL   xyznode(3,maxnode)
      INTEGER*8 ipatpnt(3,maxpatch), iedge(4,maxedge)

c.....Output Data

      REAL xyzctr(3,maxpatch), xyznorm(3,maxpatch),
     &     edge(maxedge),      paera(maxpatch)
      REAL rnear2

c.....Working Variable
     
      INTEGER*8 i, ie, ip,  n1, n2, n3      
       REAL   dx,     dy,     dz,     e1,     e2,      e3
       REAL   r1(3),  r2(3),  r3(3),  rc(3),  dotmul,  s
       REAL   r12(3), r23(3), r31(3), tmp(3), edgemax

ccccccccccccccccccccccccccccccccccccccc
c..... compute the edge length........c
ccccccccccccccccccccccccccccccccccccccc

      edgemax = 0.0
      do 10 ie=1,maxedge
         n1=iedge(1,ie)
         n2=iedge(2,ie)
         dx=xyznode(1,n1)-xyznode(1,n2)
         dy=xyznode(2,n1)-xyznode(2,n2)
         dz=xyznode(3,n1)-xyznode(3,n2)
         edge(ie)=sqrt(dx*dx+dy*dy+dz*dz)
         if(edge(ie).gt.edgemax) edgemax = edge(ie)
10    continue

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c... a small distance to set the near-by basis functions....c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      rnear2 = 1.02*edgemax*edgemax

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c....compute patch size peaera, patch center xyzctr, ...c
c............and normal directions xyznorm..............c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      do 50 ip = 1, maxpatch

         n1=ipatpnt(1,ip)
         n2=ipatpnt(2,ip)
         n3=ipatpnt(3,ip)

         do i=1,3
          r1(i)=xyznode(i,n1)
          r2(i)=xyznode(i,n2)
          r3(i)=xyznode(i,n3)
         end do

         call vctadd( r2, r1, r12, -1)
         e1 = sqrt( dotmul(r12,r12) )
         call vctadd( r3, r2, r23, -1)
         e2 = sqrt( dotmul(r23,r23) )
         call vctadd( r1, r3, r31, -1)
         e3 = sqrt( dotmul(r31,r31) )
         s = 0.5 * (e1 + e2 + e3 )
  
         paera(ip) = sqrt( abs(s*(s-e1)*(s-e2)*(s-e3)) )

         call vctadd(r1, r2, tmp, 1)
         call vctadd(tmp,r3, rc,  1)
         
         call xmul( r12, r23, tmp )
         s = sqrt( dotmul(tmp,tmp) )

         do i=1,3
            rc(i) = rc(i)/3.0
            xyzctr( i,ip) =  rc(i)
            xyznorm(i,ip) = tmp(i)/s
         end do

50    continue

c      write(*,*) 'Begin Area.f'
c      open (99,file='Area',status='unknown')
c      do ip=1,maxpatch
c         s = s + paera(ip)
c         write(99,*) ip, paera(ip), s
c      end do
c      close(99)


      write(6,21) s
21    format('    total area in sq lambda           =',e10.3)

      RETURN
      END
