#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include "array_pointer.h"

#ifdef __cplusplus
extern "C" 
#endif

/* declare the Fortran subroutine to be called by C */ 
void /*_stdcall*/ main_geom_(Real *MEMORY_TOT,
                         Real *XYZNODEP, 
			 Integer *IPATPNTP, 
			 Integer *IEDGEP,
                         Real *XYZCTRP,
			 Real *XYZNORMP,
			 Real *EDGEP, 
			 Real *PAERAP,
			 Integer *NGRIDP, 
			 Real *VT1P, 
			 Real *VT2P, 
			 Real *VT3P,
			 Real *WTP,
			 Integer *FAMILYP);

void /*_stdcall*/ main_box_(Real *MEMORY_TOT,
			Integer *LMAX,
			Integer *IEDGEP, 
			Real *EDGEP, 
			Integer *IPATPNTP, 
			   Real *XYZCTRP,
                       Real *PAERAP, 
			 Real *XYZNORMP,
			 Real *XYZNODEP,
			 Integer *NGRIDP, //10
			Real *VT1P,
			Real *VT2P,
			Real *VT3P,
			Real *WTP,
			Integer *IGRSP,  //15
			Integer *LXYZP,
			Integer *MODESP,
			Real *SIZELP,
			Integer *LRSMUPP,
			Integer *LRSMDOWNP,  //20
			Integer *LRSTLP,
			 Integer *FAMILYP,
			Integer *KPSTARTP,
			Integer* );

void /*_stdcall*/ main_tree_(Real *MEMORY_TOT,Integer *LMAX,
			Integer *NCMAX, 
			Integer *LSMAX, 
						Integer *LSMIN,
						Integer *KP0, 
						Integer *KPM,
						Integer *IEDGEP,
						Real *EDGEP, 
						Integer *IPATPNTP,
						Real *XYZCTRP,
						Real *PAERAP,
						Real *XYZNORMP,
						Real *XYZNODEP,
						Integer *NGRIDP,
						Real *VT1P,
						Real *VT2P,
						Real *VT3P,
						Real *WTP,
						Integer *IGRSP,
						Integer *LXYZP,
						Integer *MODESP,
						Real *SIZELP,
						Integer *LRSMUPP,
						Integer *LRSMDOWNP,
						Integer *LRSTLP,
						Integer *IGALLP,
						Integer *IGCSP,
						Integer *FAMILYP,
						Integer *INDEXP,
						Complex *CVLRP,
						Complex *CGMP,
						Integer *INDEXTLP,
						Integer *NOSELFP,
						Integer *KPSTARTP
						,Complex*,Integer*);

void /*_stdcall*/ main_cal_ 
                       (Real *MEMORY_TOT,
					    Integer *LMAX, 
						Integer *NCMAX,
						Integer *LSMAX,
						Integer *LSMIN,
						Integer *NEARM,
						Integer *KP0,
						Integer *KPM,
						Integer *NKPM,
						Integer *NGSNDM,
						Integer *NIPLM,
						Integer *NSM,
						Integer *NTLM,
						Integer *NGNEARM,
						Integer *MFINESTM,
						Integer *NANGLEM,
						Integer *IEDGEP,
						Real *EDGEP,
						Integer *IPATPNTP,
						Real *XYZCTRP,
						Real *PAERAP,
						Real *XYZNORMP,
						Real *XYZNODEP,
						Integer *NGRIDP,
						Real *VT1P,
						Real *VT2P,
						Real *VT3P,
						Real *WTP,
						Integer *LXYZP,
						Integer *MODESP,
						Real *SIZELP,
						Integer *IGRSP,
						Integer *IGCSP,
						Integer *IGALLP,
						Integer *FAMILYP,
						Integer *INDEXP,
						Integer *LRSMUPP,
						Integer *LRSMDOWNP,
						Integer *LRSTLP,
						Complex *CVLRP,
						Complex *CGMP,
						Real *DIRKP,
						Integer *IRSAP,
						Real *XGLP,
						Real *WGLP,
						Complex *CTLP,
						Real *ARRAYP,
						Integer *ICSAP,
						Complex *CSMP,
						Integer *INDEXTLP,
						Integer *KPSTARTP,
						Integer *NOSELFP,
						Integer *IPVTP,
// 						Complex *CRHSP,
						Complex *CBMP,
						Complex *CWORKP,
						Complex *CVFP,
						Complex *CVSP,
						Complex *CANEARP,
						Integer *IGBLKRSP,
						Integer *IGBLKCSP,
						Integer *IGBLKP,
						Complex *CFINESTP,
						Complex *C_SHIFTP,
						Integer *IGSNDCSP,
						Integer *IGSNDRSP,
						Complex *CS11OLDP,
						Complex *CS21OLDP,
						Real *BCS11OLDP,
						Real *BCS21OLDP,
//                                    		Complex *,
//						Complex *,
						Complex *crhs,
						Integer*);



void process_mem_usage_C (double* vm_usage, double* resident_set)
{
        typedef char Char[30];
        
        *vm_usage     = 0.0;
        *resident_set = 0.0;
        
        Char pid, comm, state, ppid, pgrp, session, tty_nr;
        Char tpgid, flags, minflt, cminflt, majflt, cmajflt;
        Char utime, stime, cutime, cstime, priority, nice;
        Char O, itrealvalue, starttime;
        unsigned long long vsize;
        long long int rss;
        FILE* fp;
        fp=fopen("/proc/self/stat","r");
        if(fp==NULL){
                rss=0;
                vsize=0;
                return;                
        }
        int ret = fscanf(fp,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %llu %lld",
        pid,comm,state,ppid,pgrp, session, tty_nr,
        tpgid,flags,minflt,cminflt,majflt,cmajflt,
        utime,stime,cutime,cstime,priority,nice,
        O,itrealvalue,starttime,&vsize,&rss);
        fclose(fp);
        long long int page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
        *vm_usage     = vsize / 1024.0;
        *resident_set = rss * page_size_kb;
}
                       
                       
void c_main_tree_(Real *memory_tot, Integer *lmax,
                  Integer *ncmax, Integer *lsmax, Integer *lsmin,
                  Integer *kp0, Integer *kpm, Integer *contourMap,Integer* maxedge) {


    main_tree_( memory_tot, lmax,
                ncmax, lsmax, lsmin, kp0, kpm,
                iedgep, edgep, ipatpntp, xyzctrp, paerap,
                xyznormp,xyznodep,
                ngridp, vt1p, vt2p, vt3p, wtp, igrsp, lxyzp,
                modesp, sizelp,lrsmupp, lrsmdownp, lrstlp,
                igallp,igcsp, familyp, indexp,
                cvlrp, cgmp, indextlp, noselfp, kpstartp,crhsp,contourMap);

}

void c_main_cal_(Integer *maxnode, Integer *maxpatch, Integer *maxedge,
		 Integer *maxrule, Integer *maxgrid,
		 Real *memory_tot, Integer *lmax,
                 Integer *ncmax, Integer *lsmax, Integer *lsmin,
                 Integer *nearm, Integer *kp0, Integer *kpm,
                 Integer *nkpm,
                 Integer *ngsndm, Integer *niplm, Integer *nsm, Integer *ntlm,
                 Integer *ngnearm, Integer *mfinestm, Integer *nanglem,Complex* crhs
                 ,Integer* contourMap) {

     Integer *ipvtp;
     Complex *crhsp, *cbmp, *cworkp;
     Complex *cvfp, *cvsp;
     Complex *canearp, *cfinestp;
     Real *xglp, *wglp, *dirkp;
     Integer *igblkrsp, *igblkcsp, *igblkp;
     Complex *cs11oldp, *cs21oldp;
     Real *bcs11oldp, *bcs21oldp;

     ipvtp  = (Integer *) malloc ((*(maxedge)) *sizeof(Integer));
     if (ipvtp == NULL) {fprintf(stderr, "malloc fail for c_main1 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }
      
      //The memory of crhs is managed by c++ wrapper /WXC
     cbmp  = (Complex *) malloc ((*(maxedge)) *sizeof(Complex));
     if (cbmp == NULL) {fprintf(stderr, "malloc fail for c_main3 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }

     cworkp  = (Complex *) malloc ((8*(*(maxedge))) *sizeof(Complex));
     if (cworkp == NULL) {fprintf(stderr, "malloc fail for c_main4 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }


    if (*kp0 != 0) {

     long long int total= *kp0;
     total*= 2*(*maxedge);
     total*= sizeof(Complex);
     cvfp   = (Complex *) malloc (total);
     if (cvfp == NULL) {fprintf(stderr, "malloc fail for c_main5 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }

     cvsp   = (Complex *) malloc (total);
     if (cvsp == NULL) {fprintf(stderr, "malloc fail for c_main6 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }
    }

	 long long int aa = *maxedge;
	 long long int bb = *nearm;
	aa*= bb;
	aa*=sizeof(Complex);
     //canearp   = (Complex *) malloc (((*maxedge)*(*nearm))  *sizeof(Complex));
     canearp   = (Complex *) malloc (aa );
	printf("memory for near:%lld %lld %lld\n", *maxedge,*nearm,aa);
     if (canearp == NULL) {fprintf(stderr, "malloc fail for c_main7 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }

     cfinestp   = (Complex *) malloc (((*mfinestm))  *sizeof(Complex));
     if (cfinestp == NULL) {fprintf(stderr, "malloc fail for c_main8 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }

     igblkrsp   = (Integer *) malloc (((*ncmax) + 1) *sizeof(Integer));
     if (igblkrsp == NULL) {fprintf(stderr, "malloc fail for c_main9 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }


    if (*ngnearm != 0) {

     igblkcsp   = (Integer *) malloc ((*ngnearm + 1) *sizeof(Integer));
     if (igblkcsp == NULL) {fprintf(stderr, "malloc fail for c_main10 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }

     igblkp   = (Integer *) malloc ((*ngnearm + 1) *sizeof(Integer));
     if (igblkrsp == NULL) {fprintf(stderr, "malloc fail for c_main11 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }
    }


    if (*lsmax != 0) {

     xglp   = (Real *) malloc (((*lsmax)*(*lmax)) *sizeof(Real));
     if (xglp == NULL) {fprintf(stderr, "malloc fail for c_main12 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }

     wglp   = (Real *) malloc (((*lsmax)*(*lmax)) *sizeof(Real));
     if (wglp == NULL) {fprintf(stderr, "malloc fail for c_main13 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }
    }


    if (*nkpm != 0) {

     dirkp   = (Real *) malloc ((9*(*nkpm)) *sizeof(Real));
     if (xglp == NULL) {fprintf(stderr, "malloc fail for c_main14 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }
    }


    if (*nanglem != 0) {

      cs11oldp   = (Complex *) malloc ((*nanglem) *sizeof(Complex));
      cs21oldp   = (Complex *) malloc ((*nanglem) *sizeof(Complex));
     bcs11oldp   = (Real *) malloc ((*nanglem) *sizeof(Real));
     bcs21oldp   = (Real *) malloc ((*nanglem) *sizeof(Real));
     if (cs11oldp == NULL ||  cs21oldp == NULL ||
        bcs11oldp == NULL || bcs21oldp == NULL)
       {fprintf(stderr, "malloc fail for c_main15 \n");
                         printf("  Estimated memory requirement:  %6.1f MB \n",
                                *memory_tot);
         exit(1);
     }
    }

// 	if (*maxpatch != 0) {
// 
//      currentp   = (Complex *) malloc ((3*(*maxpatch)) *sizeof(Complex));
//      if (currentp == NULL) {fprintf(stderr, "malloc fail for c_main16 \n");
//                          printf("  Estimated memory requirement:  %6.1f MB \n",
//                                 *memory_tot);
//          exit(1);
//      }
//     }


    main_cal_(
      memory_tot,lmax, ncmax, lsmax,lsmin, 
      nearm, kp0, kpm,nkpm, ngsndm, 

      niplm, nsm, ntlm, ngnearm, mfinestm,
      nanglem,iedgep, edgep, ipatpntp, xyzctrp, 

      paerap,xyznormp,xyznodep,ngridp, vt1p, 
      vt2p, vt3p, wtp,lxyzp, modesp, 

      sizelp,igrsp,igcsp, igallp, familyp, 
      indexp,lrsmupp, lrsmdownp, lrstlp,cvlrp, 

      cgmp, dirkp, irsap, xglp, wglp, 
      ctlp,arrayp, icsap, csmp, indextlp, 

      kpstartp,noselfp, ipvtp, 
//  	  crhsp,
		/*crhs,*/cbmp, //55
      cworkp, cvfp, cvsp,canearp, igblkrsp, 

      igblkcsp, igblkp, cfinestp,c_shiftp,igsndcsp, 
      igsndrsp,cs11oldp, cs21oldp, bcs11oldp, bcs21oldp,
//       currentp,
//      Einc,Hinc,
      crhs,contourMap);
       double vm,rss;
       process_mem_usage_C(&vm,&rss);
       vm = vm/(1024*1024);
       rss = rss/(1024*1024);
       printf("MLFMM: memory cost: VM: %f \t RSS: %f GB\n",vm,rss);
       if (ipvtp      != (Integer *)NULL)  free(ipvtp);
//        if (crhsp      != (Complex *)NULL)  free(crhsp);
       if (cbmp       != (Complex *)NULL)  free(cbmp);
       if (cworkp     != (Complex *)NULL)  free(cworkp);
       if (cvfp       != (Complex *)NULL)  free(cvfp);
       if (cvsp       != (Complex *)NULL)  free(cvsp);
       if (canearp    != (Complex *)NULL)  free(canearp);
       if (cfinestp   != (Complex *)NULL)  free(cfinestp);
       if (igblkrsp   != (Integer *)NULL)  free(igblkrsp);
       if (igblkcsp   != (Integer *)NULL)  free(igblkcsp);
       if (igblkp     != (Integer *)NULL)  free(igblkp);
       if (dirkp      != (Real *)NULL)  free(dirkp);
       if (xglp       != (Real *)NULL)  free(xglp);
       if (wglp       != (Real *)NULL)  free(wglp);
       if ( cs11oldp  != (Complex *)NULL)  free( cs11oldp);
       if ( cs21oldp  != (Complex *)NULL)  free( cs21oldp);
       if (bcs11oldp  != (Real *)NULL)  free(bcs11oldp);
       if (bcs21oldp  != (Real *)NULL)  free(bcs21oldp);
         
       
       //Freee the external allocated memory:
       if(igcsp!=(Integer *)NULL) 
	       free(igcsp);
       if(igallp!=(Integer *)NULL) 
	       free(igallp);
       if(noselfp!=(Integer *)NULL) 
	       free(noselfp);
       if(igsndrsp!=(Integer *)NULL) 
	       free(igsndrsp);
       
       
       if(indexp!=(Integer *)NULL) 
	       free(indexp);
       
       
       if(cvlrp!=(Complex *)NULL) 
	       free(cvlrp);
       if(cgmp!=(Complex *)NULL) 
	       free(cgmp);
       
       
       if(irsap!=(Integer *)NULL) 
	       free(irsap);
       if(c_shiftp!=(Complex *)NULL) 
	       free(c_shiftp);
       
       if(ctlp!=(Complex *)NULL) 
	       free(ctlp);
       
       
       if(icsap!=(Integer *)NULL) 
	       free(icsap);
       if(arrayp!=(Real *) NULL) 
	       free(arrayp);
       
       
       if(csmp!=(Complex *)NULL) 
	       free(csmp);
	       
       
       if(indextlp!=(Integer *)NULL) 
	       free(indextlp);
       
       if(igsndcsp!=(Integer *)NULL) 
	       free(igsndcsp);
       
}

