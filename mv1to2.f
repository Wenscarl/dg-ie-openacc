      subroutine MV1TO2( r1, r2, n)
c.....Copy a real vector from 'r1' to 'r2' 


c.....Input Data      
      INTEGER*8 n
       REAL r1(n)

c......Output Data
       REAL r2(n)

       do i = 1,n
          r2(i) = r1(i)
       enddo

       RETURN
       END
